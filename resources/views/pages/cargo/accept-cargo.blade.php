@extends('layouts.cabinet')

@section('title', __('cargo.Accept'))

@section('content-cabinet')
    <livewire:cargo.acceptance :transfer="$transfer" />
@endsection
