@extends('layouts.app')

@section('title', __('cargo.New shipment'))

@section('content')
    <div class="section">
        <div class="container js-import _pt-xl" data-geo-access data-url="{{ route('cabinet.cargo.save-location', $transfer) }}" data-crf="{{ csrf_token()}}">
            <div class="title title--size-h1 _text-center _mt-xl _mb-df">
                Підтвердіть геолокацію щоб отримати документ!
            </div>
        </div>
    </div>
@endsection
