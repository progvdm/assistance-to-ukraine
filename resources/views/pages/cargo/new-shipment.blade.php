@extends('layouts.cabinet')

@section('title', __('cargo.New shipment'))

@section('content-cabinet')
    <livewire:cargo.new-shipment :isDonor="request()->get('add-donor') ?? false"/>
@endsection
