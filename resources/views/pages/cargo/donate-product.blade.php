@extends('layouts.cabinet')

@section('title', __('cargo.New shipment'))

@section('content-cabinet')
    <livewire:cargo.new-shipment-donate :transferId="$transfer->id"/>
@endsection
