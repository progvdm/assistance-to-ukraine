@extends('layouts.cabinet')

@section('title', __('cargo.Cargo main'))

@section('content-cabinet')
    <div class="_mb-df">
        @widget('cargo.acceptanceButton', ['cargo' => $cargo])
    </div>

    <div class="">
        <livewire:cargo.history :cargo="$cargo"/>
    </div>

@endsection
