@extends('layouts.what_we_did')

@section('title', 'Main')

@section('content')
    @include('layouts.header', ['class' => 'header--white-bg _pb-lg'])

    <div class="main">
        <div class="section">
            <div class="container container--white _pb-xxl">
                @widget('breadcrumbs')
                <livewire:cargo.sign-document :transferDocument="$transferDocument"/>
            </div>
        </div>

    </div>
@endsection
