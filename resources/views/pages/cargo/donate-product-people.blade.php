@extends('layouts.cabinet')

@section('title', __('cargo.New shipment'))

@section('content-cabinet')
    <livewire:cargo.new-shipment :is-people="true"/>
@endsection
