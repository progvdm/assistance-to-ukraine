@extends('layouts.app')

@section('title', __('cargo.Accept'))

@section('content')
    <livewire:cargo.approve :transfer="$transfer" />
@endsection
