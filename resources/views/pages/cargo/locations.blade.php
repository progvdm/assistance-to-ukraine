@extends('layouts.cabinet')

@section('title', __('cargo.New shipment'))

@push('scripts')
    <script src="https://maps.googleapis.com/maps/api/js?key={{env('googleAPIKey')}}&language={{app()->getLocale()}}"></script>
@endpush

@section('content-cabinet')
    <div class="title title--size-h2 _mb-df">
        {{ __('dashboard.Position on the map') }}
    </div>

    <div id="map" class="delivery-map js-import" data-delivery-map data-url-json="{{route('cabinet.locations.cargo.json', $transfer)}}"></div>
@endsection
