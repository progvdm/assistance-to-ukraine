@extends('layouts.app')

@section('title', __('auth.Restore'))

@section('content')
    <livewire:auth.restore :userId="$userId" />
@endsection
