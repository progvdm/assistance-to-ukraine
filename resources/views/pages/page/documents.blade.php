@extends('layouts.what_we_did')

@section('title', 'Main')

@section('content')
    @include('layouts.header', ['class' => '_pb-lg'])

    <div class="main">
        <div class="section">
            <div class="container container--white">
                @widget('breadcrumbs')

                <div class="title title--size-h1 _mb-md _df:mb-lg">
                    {{ $page->h1 }}
                </div>

                <div class="table _mb-df _pb-df">
                    @if($files->count())
                        @foreach($files as $file)
                            <div class="table__line">
                                <a class="table__link _ml-xs" href="{{ route('page.documents.pdf', ['pdf'=>$file->id]) }}">{{ $file->title }}</a>
                            </div>

                            <div class="table__description">
                                {{ $file->description }}
                            </div>

                            <hr>
                        @endforeach
                    @endif
                </div>

                <div class="_mb-df _pb-df">
                    {!! $page->text !!}
                </div>
            </div>
        </div>

    </div>
@endsection
