@extends('layouts.what_we_did')

@section('title', 'Main')

@section('content')
    @include('layouts.header', ['class' => '_pb-lg'])

    <div class="main">
        <div class="section">
            <div class="container container--white _pb-xxl">
                @widget('breadcrumbs')

                <a href="{{ $pdf->url() }}" class="button button--transparent button--small button--uppercase _mb-md" download="{{ $pdf->title }}">
                    <i class="_mr-xs fa-solid fa-arrow-down"></i>
                    Download
                </a>

                <div class="_flex _justify-center">
                    {{-- для работы локально --}}
{{--                    <iframe src="https://docs.google.com/viewer?url=https://test.aidmonitor.org/storage/2022/08/12/ee32040b12466153ea9ef95b364ceca7fae432bc.pdf&embedded=true"--}}
{{--                            style="width: 100%; height: 600px;" frameborder="0">Ваш браузер не поддерживает фреймы</iframe>--}}
                    <iframe src="https://docs.google.com/viewer?url={{ $pdf->url() }}&embedded=true"
                            style="width: 100%; height: 600px;" frameborder="0">Ваш браузер не поддерживает фреймы</iframe>

                </div>

            </div>
        </div>

    </div>
@endsection
