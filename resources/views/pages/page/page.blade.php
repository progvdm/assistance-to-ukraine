@extends('layouts.what_we_did')

@section('title', 'Main')

@section('content')
    @php
        $route = \Illuminate\Support\Facades\Route::getCurrentRoute()->page;
    @endphp

    @if($route == 'what-we-did')
        <div class="first-screen _mb-df _lg:mb-hg" style="
                background: linear-gradient(358.7deg, rgba(0, 0, 0, 0.5) 9.6%, rgba(79, 79, 79, 0) 80.05%), url('/images/vegetables.jpg')">
            @include('layouts.header', ['class' => '_pb-lg'])

            @widget('page.whatWeDid')
        </div>
    @elseif($route == 'recipients')
        <div class="_mb-df _lg:mb-hg" style="
                    background: url('/images/ukraine-bg.jpg')">
            @include('layouts.header', ['class' => '_pb-lg'])
        </div>

        @widget('page.usersList', ['donor' => false])
    @elseif($route == 'donors')
        <div class="_mb-df _lg:mb-hg" style="
                background: url('/images/ukraine-bg.jpg')">
            @include('layouts.header', ['class' => '_pb-lg'])
        </div>

        @widget('page.usersList', ['donor' => true])
    @elseif($route == 'about-us')
        <div class="_mb-df _lg:mb-hg" style="
                background: url('/images/ukraine-bg.jpg')">
            @include('layouts.header', ['class' => '_pb-lg'])
        </div>

        @widget('page.aboutUs')
    @elseif($route == 'user-info')
        <div class="_mb-df _lg:mb-hg" style="
                background: url('/images/ukraine-bg.jpg')">
            @include('layouts.header', ['class' => '_pb-lg'])
        </div>

        @widget('page.userInfo', ['id' => request()->get('id')])
    @endif
@endsection
