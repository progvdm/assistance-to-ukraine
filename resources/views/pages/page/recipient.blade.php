@extends('layouts.what_we_did')

@section('title', 'Main')

@section('content')
    @include('layouts.header', ['class' => '_pb-lg'])

    <div class="main _pt-df _lg:pt-hg">
        <livewire:pages.user-list :type="$type" :route="$route" :h1="$h1">
    </div>
@endsection
