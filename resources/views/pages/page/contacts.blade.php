@extends('layouts.what_we_did')

@section('title', 'Main')

@section('content')
    @include('layouts.header', ['class' => '_pb-lg'])

    <div class="main">
        <div class="section">
            <div class="container container--white">
                @widget('breadcrumbs')

                <div class="title title--size-h1 _mb-md _df:mb-lg">
                    {{ $page->h1 }}
                </div>

                <div class="title title--size-h5 title--color-blue title--weight-bold _mb-md">
                    {{ __('contacts.Categories of questions:') }}
                </div>

                <livewire:pages.contacts />

                <div class="_mb-df _lg:mb-hg _pb-df">
                    {!! $page->text !!}
                </div>
            </div>
        </div>
    </div>
@endsection
