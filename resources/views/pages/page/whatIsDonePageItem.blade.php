@extends('layouts.what_we_did')

@section('title', 'Main')

@section('content')
    @include('layouts.header', ['class' => '_pb-lg'])

    <div class="main _pt-df _lg:pt-hg">
        <div class="section">
            <div class="container container--white _pt-md">
                <div class="_grid _spacer _spacer--md _md:spacer--xxl">
                    @if($page->images()->exists())
                        <div class="_cell _cell--12 _md:cell--5">
                            <div class="_grid _spacer _spacer--md">
                                <div class="_cell _cell--12">
                                    <div class="_posr">
                                        <div class="gallery-slider _flex ">
                                            <div class="_overflow">
                                                <div class="gallery-slider__slider gallery-slider__slider--top swiper-container js-import _mb-md"
                                                     data-swiper-slider='{"containerClass":"gallery-slider__slider","type":"GallerySlider","navigation":{"type":"default-outside","outside":true}}'>
                                                    <div class="swiper-wrapper js-import" data-mfp="gallery">
                                                        @foreach($page->images as $image)
                                                            <a href="{{ $image->url() }}" class="gallery-slider__image tab__item swiper-slide">
                                                                <img src="{{ $image->url() }}"  title="{{ $image->title }}" loading="lazy" alt="">
                                                            </a>
                                                        @endforeach
                                                    </div>
                                                </div>

                                                <div class="gallery-slider__slider gallery-slider__slider--bottom swiper-container swiper-thumbs">
                                                    <div class="swiper-wrapper">
                                                        @foreach($page->images as $image)
                                                            <div class="swiper-slide">
                                                                <div class="swiper-slide-container">
                                                                    <img width="50" src="{{ $image->url() }}" title="{{ $image->title }}">
                                                                </div>
                                                            </div>
                                                        @endforeach
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="slider-navigation ">
                                            <div
                                                class="slider-navigation__button slider-navigation__button--prev swiper-button-prev swiper-button-disabled">
                                                <svg width="30" height="30" viewBox="0 0 30 30" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                    <circle r="15" transform="matrix(4.37114e-08 -1 -1 -4.37114e-08 15 15)" fill="#07273F"/>
                                                    <path d="M16.5132 10.9287L11.4421 15.9998" stroke="white" stroke-width="1.5"/>
                                                    <path d="M11.4421 14.9287L16.5132 19.9998" stroke="white" stroke-width="1.5"/>
                                                </svg>
                                            </div>
                                            <div class="slider-navigation__button slider-navigation__button--next swiper-button-next">
                                                <svg width="30" height="30" viewBox="0 0 30 30" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                    <circle cx="15" cy="15" r="15" transform="rotate(-90 15 15)" fill="#07273F"/>
                                                    <path d="M13.4868 10.9287L18.5579 15.9998" stroke="white" stroke-width="1.5"/>
                                                    <path d="M18.5579 14.9287L13.4868 19.9998" stroke="white" stroke-width="1.5"/>
                                                </svg>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif
                    <div class="_cell _cell--12 _md:cell--7">
                        <div class="_nmt-md">
                            @widget('breadcrumbs')
                        </div>

                        <div class="_mb-xxl">
                            <div class="delivery__info--title _mb-md">{{ $page->country }}</div>
                            <div class="delivery__info--title _mb-md" style="font-weight: 300; line-height: 1.2;">{{ $page->h1 }}</div>
                            <div class="delivery__text">
                                {!! $page->text !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

