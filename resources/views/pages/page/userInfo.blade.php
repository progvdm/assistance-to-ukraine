@extends('layouts.what_we_did')

@section('title', 'Main')

@section('content')
    @include('layouts.header', ['class' => 'header--white-bg _pb-lg'])

    <div class="main">
        <div class="section">
            <div class="container container--white _pt-md">
                <div class="_grid _spacer _spacer--md _md:spacer--xxl">
                    <div class="_cell _cell--12 _md:cell--12">
                        <div class="_nmt-md">
                            @widget('breadcrumbs')
                        </div>
                        <div class="title title--size-h1 _mb-md _df:mb-lg">
                            {{ Seo::meta()->getH1() }}
                        </div>
                        <br>
                        <div class="_mb-xxl">
                            <div class="delivery__text">
                                {!! $organisation->text !!}
                            </div>
                        </div>

                        <div class="_mb-xxl">
                            <div class="delivery__text">
                                @if($organisation->website)
                                    <div class="_mb-sm">
                                        <b style="font-weight: 700">Website: </b> <a href="{{ (strpos($organisation->website, 'http://') !== false or strpos($organisation->website, 'https://') !== false )? $organisation->website : 'http://' . $organisation->website}}" target="_blank">{{ $organisation->website }}</a>
                                    </div>
                                @endif
                                @if($organisation->instagram)
                                    <div class="_mb-sm">
                                        <b style="font-weight: 700">Instagram: </b><a href="{{ $organisation->instagram }}" target="_blank">{{ $organisation->instagram }}</a>
                                    </div>
                                @endif
                                @if($organisation->facebook)
                                    <div class="_mb-sm">
                                        <b style="font-weight: 700">Facebook: </b><a href="{{ $organisation->facebook }}" target="_blank">{{ $organisation->facebook }}</a>
                                    </div>
                                @endif
                                @if($organisation->additional_email)
                                    <div class="_mb-sm">
                                        <b style="font-weight: 700">Email: </b><a href="mailto:{{ $organisation->additional_email }}">{{ $organisation->additional_email }}</a>
                                   </div>
                                @endif
                                @if($organisation->additional_phone)
                                    <div class="_mb-sm">
                                        <b style="font-weight: 700">Phone: </b><a href="tel:{{preg_replace("/[^0-9]/", '', $organisation->additional_phone)}}">{{ $organisation->additional_phone }}</a>
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

