@extends('layouts.what_we_did')

@section('title', 'Main')

{{--@section('content')--}}
{{--        <div class="_mb-df _lg:mb-hg" style="--}}
{{--                background: url('/images/ukraine-bg.jpg')">--}}
{{--            @include('layouts.header', ['class' => '_pb-lg'])--}}
{{--        </div>--}}
{{--        @widget('page.aboutUs')--}}
{{--@endsection--}}


@section('content')
    @include('layouts.header', ['class' => '_pb-lg'])

    <div class="main">
        <div class="section section--about">
            <div class="container container--white">
                @widget('breadcrumbs')

                <div class="title title--size-h1 _mb-md _df:mb-lg">
                    {{ $page->h1 }}
                </div>
                <div class="_mb-df _lg:mb-hg _pb-df">
                    <div class="_grid _spacer _spacer--lg">
                        <div class="_cell _cell--12 _md:cell--7">
                            {!! $page->text !!}
                        </div>
                        <div class="_cell _cell--12 _md:cell--5">
                            <div class="video__block js-import" data-video>
                                <video class="_m-auto"
                                       src="{{asset('static/How it works-'. app()->getLocale(). '.MP4')}}"
                                       poster="/images/map.jpg"
                                       autobuffer></video>

                                <svg class="video__play" width="52" height="52" viewBox="0 0 52 52" fill="none" xmlns="http://www.w3.org/2000/svg"><circle class="circle" cx="25.5091" cy="25.5091" r="25.0091"/><path class="triangle" d="M19.9636 35.491V15.5273L35.4909 25.5092L19.9636 35.491Z"/></svg>
                            </div>
                        </div>

                        <div class="_cell _cell--12">
                            <div class="title title--size-h5 title--color-blue title--weight-bold _mb-md">
                                {{ __('pages.How it works on the platform') }}
                            </div>
                        </div>
                        <div class="_cell _cell--12">
                            <picture>
                                <source srcset="/images/about-us--blocks-{{ app()->getLocale() }}--big.svg"
                                        media="(min-width: 800px)">
                                <img src="/images/about-us--blocks-ua--big.svg" alt=""/>
                            </picture>
                        </div>
                    </div>
                </div>
            </div>

            <div class="container container--about _pb-xl">
                <picture>
                    <source srcset="/images/about-us--example-{{ app()->getLocale() }}--big.svg"
                            media="(min-width: 800px)">
                    <img src="/images/about-us--example-{{ app()->getLocale() }}--big.svg" alt=""/>
                </picture>
                <div class="padding">
                    <div class="_grid _spacer _spacer--lg">
                        <div class="_cell _cell--12 _md:cell--6">
                            <p>
                                {{ __('pages.This way of organizing the provision and distribution of humanitarian aid makes these processes transparent, as each step is displayed in the donor\'s and recipient\'s workspace. The scheme resembles a necklace in which, after the first donor, each new recipient who receives humanitarian aid is added to the chain of supply, reporting and verification. The donor can see the remains of the humanitarian cargo provided by him in the warehouses of each individual recipient, as well as the entire network formed as a result of transfers to other recipients and final beneficiaries. No recipient report is possible without GPS verification from the place of receipt of the humanitarian cargo and without a corresponding photo report.') }}
                            </p>
                        </div>
                        <div class="_cell _cell--12 _md:cell--6">
                            <p>
                                {{ __('pages.The platform automatically generates reports, sends 5-digit codes for digital signatures via SMS messages, which is similar to SMS confirmations of the banking system and complies with the current legislation of Ukraine.') }}
                            </p>
                            <p>
                                {!! __('pages.The platform is fully functional in the version for mobile phones, and an application has been developed<br> for scanning QR codes, GPS verification, adding photos and video reports') !!}
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
