@extends('layouts.what_we_did')

@section('title', 'Main')

@section('content')
        @include('layouts.header', ['class' => '_pb-lg'])

        <div class="main">
            <div class="section">
                <div class="container _pt-df _lg:pt-hg">
                    <div class="title title--size-h1 _mb-md _df:mb-lg">
                        {{ Seo::meta()->getH1() }}
                    </div>

                    <div class="container">
                        <div class="grid-elements">
                            @if($items->count())
                                <div class="_grid _spacer _spacer--md">
                                @foreach($items as $item)
                                    <div class="_cell _cell--12 _sm:cell--6 _df:cell--4">
                                        <a href="{{ route('page.whatIsDone.page', ['slug' => $item->slug]) }}" class="item"
                                           @if($item->images()->exists())
                                            style='background-image: url("{{$item->images->first()->url()}}")'
                                           @endif
                                        >
                                            <div class="item__gradient">
                                                <div class="item__title">{{ $item->country }}</div>
                                                <div class="item__info">{{ $item->name }}</div>
                                                <div class="item__text">
                                                    {{ $item->text_short }}
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                @endforeach
                            </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>

        </div>
@endsection
