@extends('layouts.what_we_did')

@section('title', 'Main')

{{--@section('content')--}}
{{--        <div class="_mb-df _lg:mb-hg" style="--}}
{{--                background: url('/images/ukraine-bg.jpg')">--}}
{{--            @include('layouts.header', ['class' => '_pb-lg'])--}}
{{--        </div>--}}
{{--        @widget('page.aboutUs')--}}
{{--@endsection--}}


@section('content')
    @include('layouts.header', ['class' => '_pb-lg'])

    <div class="main">
        <div class="section section--about">
            <div class="container container--white">
                @widget('breadcrumbs')

                <div class="title title--size-h1 _mb-md _df:mb-lg">
                    {{ $page->h1 }}
                </div>
                <div class="_mb-df _lg:mb-hg _pb-df">
                    <div class="_grid _spacer _spacer--lg">
                        <div class="_cell _cell--12">
                            {!! $page->text !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
