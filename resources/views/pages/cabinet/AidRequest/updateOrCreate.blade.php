@extends('layouts.cabinet')

@section('title', __('cabinet.Cabinet'))

@section('content-cabinet')
    <livewire:cabinet.request.aid-requests-item :id="$id ?? null"/>
@endsection
