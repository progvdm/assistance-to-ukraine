@extends('layouts.cabinet')

@section('title', __('cabinet.Cabinet'))

@section('content-cabinet')
    @widget('cabinet.allDeliveries', ['donate_products' => true, 'sent' => true])
@endsection
