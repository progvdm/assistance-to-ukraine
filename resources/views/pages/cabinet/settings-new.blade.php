@extends('layouts.cabinet')

@section('title', __('cabinet.Cabinet'))

@section('content-cabinet')

    <div wire:ignore>
        <script src="/vendor/tinymce/tinymce.min.js"
                referrerpolicy="origin"></script>
    </div>
    <livewire:cabinet.user-settings/>

    <script>
        let lang = $('textarea[data-tinymce]').attr('data-tinymce');
        tinymce.init({
            selector: '.tinymce',
            plugins: 'preview importcss searchreplace autolink autosave save directionality code visualblocks visualchars fullscreen image link media template codesample table charmap pagebreak nonbreaking anchor insertdatetime advlist lists wordcount help charmap quickbars emoticons',

            //plugins: 'a11ychecker advcode casechange export formatpainter image editimage linkchecker autolink lists checklist media mediaembed pageembed permanentpen powerpaste table advtable tableofcontents tinycomments tinymcespellchecker',
            //toolbar: 'a11ycheck addcomment showcomments casechange checklist code export formatpainter image editimage pageembed permanentpen table tableofcontents',
            //toolbar_mode: 'floating',
            tinycomments_mode: 'embedded',
            tinycomments_author: 'Author name',
            language: lang,
            setup: function (editor) {
                editor.on('init change', function () {
                    editor.save();
                });
                editor.on('change', function (e) {
                    console.log(e.target.targetElm);
                    let id = e.target.targetElm.getAttribute('id');
                    @this.set(id, editor.getContent());
                });
            }
        });
    </script>
@endsection
