@extends('layouts.cabinet')

@section('title', __('cabinet.Cabinet'))

@section('content-cabinet')
    @widget('cabinet.allDeliveries', ['sent' => true])
@endsection
