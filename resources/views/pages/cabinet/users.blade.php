@extends('layouts.cabinet')

@section('title', __('cabinet.Cabinet'))

@section('content-cabinet')
    <livewire:cabinet.settings-users />
@endsection
