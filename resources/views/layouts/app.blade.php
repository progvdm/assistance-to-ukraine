<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    @include('layouts.head')
    <body class="page antialiased @if(url()->current() !== route('home')) page--gradient @endif">
        <div class="main @if(url()->current() == route('home')) main--page main--picture @endif">

            @include('layouts.header', ['class' => ''])

            @yield('content')
        </div>

        @yield('after-main-block')

        @include('layouts.footer')

        <livewire:scripts />
        <script src="/build/app.bundle.js?v{{ time() }}"></script>

    </body>
</html>

