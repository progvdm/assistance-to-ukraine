@include('components.flash-message')
<footer class="footer _pt-xl _pb-xl _mt-auto">
    <div class="section">
        <div class="container">
            <div class="_flex _flex-column _md:flex-row _justify-between _items-center _flex-wrap">
                <div class="_mb-xs _md:mb-none">© Всі права захищені | Омні Оптіма ІТ</div>
                <div class="menu _md:show _mb-xs _md:mb-none _m-auto">
                    <div class="_flex _justify-between _items-center">
                        <div class="menu__item">
                            <a href="{{ route('page.about') }}" class="menu__link @if(stripos(url()->current(), route('page.about')) !== false) is-active @endif">{{__('default.About us')}}</a>
                        </div>
                        @if(app()->getLocale() == 'uk')
                            <div class="menu__item">
                                <a href="/#faq" class="menu__link">FAQ</a>
                            </div>
                        @endif
                        <div class="menu__item">
                            <a href="{{ route('page.documents') }}" class="menu__link @if(stripos(url()->current(), route('page.documents')) !== false) is-active @endif"> {{__('default.Documents')}}</a>
                        </div>
                    </div>
                </div>
                <div class="menu__item _mr-xs _mb-xs _md:mb-none"><a class="menu__link" href="/documents-static/User agreement- uk.pdf?v=3">Публічна оферта</a></div>
                <div class="menu__item"><a class="menu__link" href="https://greatpeople.com.ua/en/" target="_blank">Developed by Great People</a></div>
            </div>
        </div>
    </div>
    <svg class="waves _nmt-hg _hide" width="1920" height="348" viewBox="0 0 1920 348" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M-35 106C-35 106 25.0754 237.714 503.134 280.5C981.193 323.286 1920 134 1920 134" stroke="#F8D748"/>
            <path d="M-35 113C-35 113 25.0754 244.714 503.134 286.5C981.193 328.286 1920 141 1920 141" stroke="#F8D748"/>
            <path d="M-35 120C-35 120 25.0754 250.714 503.134 292.5C981.193 334.286 1920 148 1920 148" stroke="#F8D748"/>
            <path d="M-35 127C-35 127 25.0754 256.714 503.134 298.5C981.193 340.286 1920 155 1920 155" stroke="#F8D748"/>
            <path d="M-35 135C-35 135 25.0754 263.714 503.134 305.5C981.193 347.286 1920 163 1920 163" stroke="#F8D748"/>
            <path d="M-35 143C-35 143 25.0754 270.714 503.134 312.5C981.193 354.286 1920 171 1920 171" stroke="#F8D748"/>
            <path d="M-35 151C-35 151 25.0754 277.714 503.134 319.5C981.193 361.286 1920 179 1920 179" stroke="#F8D748"/>
            <path d="M-35 158C-35 158 25.0754 283.714 503.134 325.5C981.193 367.286 1920 186 1920 186" stroke="#F8D748"/>
            <path d="M-35 165C-35 165 25.0754 289.714 503.134 331.5C981.193 373.286 1920 193 1920 193" stroke="#F8D748"/>
            <path d="M-35 173C-35 173 25.0754 296.714 503.134 338.5C981.193 380.286 1920 201 1920 201" stroke="#F8D748"/>
            <path d="M-35 181C-35 181 25.0754 303.714 503.134 345.5C981.193 387.286 1920 209 1920 209" stroke="#F8D748"/>
            <path d="M-35 189C-35 189 25.0754 310.714 503.134 352.5C981.193 394.286 1920 217 1920 217" stroke="#F8D748"/>
            <path d="M-35 197C-35 197 25.0754 317.714 503.134 359.5C981.193 401.286 1920 225 1920 225" stroke="#F8D748"/>
            <path d="M-35 204.502C-35 204.502 25.0754 324.216 503.134 366.002C981.193 407.788 1920 232.502 1920 232.502" stroke="#F8D748"/>
            <path d="M-35 212.502C-35 212.502 25.0754 332.216 503.134 374.002C981.193 415.788 1920 240.502 1920 240.502" stroke="#F8D748"/>
            <path d="M-35 221.502C-35 221.502 25.0754 341.216 503.134 382.002C981.193 422.788 1920 249.502 1920 249.502" stroke="#F8D748"/>
            <path d="M-35 230.502C-35 230.502 25.0754 349.216 503.134 390.002C981.193 430.788 1920 258.502 1920 258.502" stroke="#F8D748"/>
            <path d="M-35 240.502C-35 240.502 25.0754 358.216 503.134 399.002C981.193 439.788 1920 268.502 1920 268.502" stroke="#F8D748"/>
            <path d="M-35 250.502C-35 250.502 25.0754 367.216 503.134 408.002C981.193 448.788 1920 278.502 1920 278.502" stroke="#F8D748"/>
            <path d="M-35 261.502C-35 261.502 25.0754 377.216 503.134 418.002C981.193 458.788 1920 289.502 1920 289.502" stroke="#F8D748"/>
            <path d="M-35 273.502C-35 273.502 25.0754 388.216 503.134 429.002C981.193 469.788 1920 301.502 1920 301.502" stroke="#F8D748"/>
            <path d="M-35 284.502C-35 284.502 25.0754 398.216 503.134 439.002C981.193 479.788 1920 312.502 1920 312.502" stroke="#F8D748"/>
            <path d="M-35 296.502C-35 296.502 25.0754 409.216 503.134 450.002C981.193 490.788 1920 324.502 1920 324.502" stroke="#F8D748"/>
            <path d="M-35 309.502C-35 309.502 25.0754 421.216 503.134 462.002C981.193 502.788 1920 337.502 1920 337.502" stroke="#F8D748"/>
            <path opacity="0.05" d="M-35 1C-35 1 25.0754 140.714 503.134 183.5C981.193 226.286 1920 18 1920 18" stroke="#ABC7F4"/>
            <path opacity="0.1" d="M-35 5C-35 5 25.0754 144.714 503.134 187.5C981.193 230.286 1920 23 1920 23" stroke="#ABC7F4"/>
            <path opacity="0.15" d="M-35 9C-35 9 25.0754 148.714 503.134 191.5C981.193 234.286 1920 28 1920 28" stroke="#ABC7F4"/>
            <path opacity="0.2" d="M-35 13C-35 13 25.0754 152.714 503.134 195.5C981.193 238.286 1920 33 1920 33" stroke="#ABC7F4"/>
            <path opacity="0.25" d="M-35 17C-35 17 25.0754 156.714 503.134 199.5C981.193 242.286 1920 38 1920 38" stroke="#ABC7F4"/>
            <path opacity="0.3" d="M-35 21C-35 21 25.0754 160.714 503.134 203.5C981.193 246.286 1920 43 1920 43" stroke="#ABC7F4"/>
            <path opacity="0.35" d="M-35 25C-35 25 25.0754 164.714 503.134 207.5C981.193 250.286 1920 48 1920 48" stroke="#ABC7F4"/>
            <path opacity="0.4" d="M-35 29C-35 29 25.0754 168.714 503.134 211.5C981.193 254.286 1920 53 1920 53" stroke="#ABC7F4"/>
            <path opacity="0.45" d="M-35 33C-35 33 25.0754 172.714 503.134 215.5C981.193 258.286 1920 58 1920 58" stroke="#ABC7F4"/>
            <path opacity="0.5" d="M-35 37C-35 37 25.0754 176.714 503.134 219.5C981.193 262.286 1920 63 1920 63" stroke="#ABC7F4"/>
            <path opacity="0.55" d="M-35 41C-35 41 25.0754 180.714 503.134 223.5C981.193 266.286 1920 68 1920 68" stroke="#ABC7F4"/>
            <path opacity="0.6" d="M-35 46C-35 46 25.0754 185.714 503.134 228.5C981.193 271.286 1920 74 1920 74" stroke="#ABC7F4"/>
            <path opacity="0.65" d="M-35 52C-35 52 25.0754 190.714 503.134 233.5C981.193 276.286 1920 80 1920 80" stroke="#ABC7F4"/>
            <path opacity="0.7" d="M-35 57C-35 57 25.0754 195.714 503.134 238.5C981.193 281.286 1920 85 1920 85" stroke="#ABC7F4"/>
            <path opacity="0.75" d="M-35 64C-35 64 25.0754 201.714 503.134 244.5C981.193 287.286 1920 92 1920 92" stroke="#ABC7F4"/>
            <path opacity="0.8" d="M-35 71C-35 71 25.0754 207.714 503.134 250.5C981.193 293.286 1920 99 1920 99" stroke="#ABC7F4"/>
            <path opacity="0.85" d="M-35 78C-35 78 25.0754 213.714 503.134 256.5C981.193 299.286 1920 106 1920 106" stroke="#ABC7F4"/>
            <path opacity="0.9" d="M-35 85C-35 85 25.0754 219.714 503.134 262.5C981.193 305.286 1920 113 1920 113" stroke="#ABC7F4"/>
            <path opacity="0.95" d="M-35 92C-35 92 25.0754 225.714 503.134 268.5C981.193 311.286 1920 120 1920 120" stroke="#ABC7F4"/>
            <path d="M-35 99C-35 99 25.0754 231.714 503.134 274.5C981.193 317.286 1920 127 1920 127" stroke="#ABC7F4"/>
        </svg>
</footer>
@if(in_array(Route::current()->getName(), ['cabinet.home', 'cabinet.aid-requests.index']))
    @include('partials.modal-cabinet')
@else
    @include('partials.modal')
@endif

<script type="text/javascript">
    (function (window) {
        window.app = {
            routeComponent: '{{ route('render-component') }}',
            language: '{{ app()->getLocale() }}',
            i18n: {
                select: {
                    searchPlaceholder: '{{ __('cms-ui::site.Поиск') }}'
                },
                confirm: {
                    yes: '{{ __('cms-ui::site.Да') }}',
                    no: '{{ __('cms-ui::site.Нет') }}'
                },
                confirmReloadPage: '{{ __('cms-ui::site.Yes, reload page') }}',
                csrfExpires: '{{ __('cms-ui::site.This page has expired due to inactivity') }}',
                error503: '{{  __('cms-ui::site.Sorry, we do some service') }}',
                error503HelpText: '{{  __('cms-ui::site.Please check back soon') }}',
                no: '{{ __('cms-ui::site.No') }}',
                ok: '{{ __('cms-ui::site.ok') }}',
                pleaseTryAgainLater: '{{ __('cms-ui::site.Please try again later') }}',
                serverError: '{{ __('cms-core::site.Server error') }}',
                wouldYourLikeRefreshPage: '{{ __('cms-ui::site.Would you like to refresh the page?') }}'
            },
        };
    })(window);

    document.addEventListener('DOMContentLoaded', function () {
        window.livewire.on('urlChange', (url) => {
            history.pushState(null, null, url);
        });
    });
</script>
