<div class="preloader mfp-preloader is-hide"
     wire:loading.class.remove="is-hide"
     @if(isset($target) and $target !='')
        wire:target="{{$target}}"
     @endif
></div>
