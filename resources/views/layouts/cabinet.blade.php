@extends('layouts.app')

@section('title', Seo::meta()->getH1())

@section('content')
    <div class="section _mt-xl">
        <div class="container container--white _pb-xl _mb-df">
            <div class="_flex _justify-between _items-center">
                @widget('breadcrumbs')
                <livewire:notification.list-message />
            </div>

            <div class="_mt-xl">
                @yield('content-cabinet')
            </div>
        </div>
    </div>
@endsection
