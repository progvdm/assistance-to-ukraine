<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    @include('layouts.head')
    <body class="page antialiased
                    @if(url()->current() !== route('home')) page--gradient @endif
                    @if(url()->current() == route('page.marketplace')) page__marketplace @endif">

        @yield('content')

        @yield('after-main-block')

        @include('layouts.footer')

        <livewire:scripts />
        <script src="/build/app.bundle.js?v{{ time() }}"></script>
    </body>
</html>
