<div class="recipient__line is-open" wire:loading.attr="disabled">
    <div class="_grid _flex-nowrap">
        <div class="_cell _cell--3 _pl-df _flex _flex-column _items-start">
            @if(!$transfer->human)
                <div
                    class="recipient__status _nml-df {{ \App\Enums\Cargo\CargoStatusesEnum::getClass($transfer->status) }} _pl-df _pr-xxl">
                    {{ \App\Enums\Cargo\CargoStatusesEnum::get($transfer->status) }}
                </div>
            @endif
            <div>
                <span class="recipient__name ">
                    {{ $numList }}.
                    {!! \App\Enums\Cargo\CargoStatusesEnum::getIconSvg($transfer->status ?? '')  !!}
                    <span class="_ml-xs _mb-xs">{{$user}}</span>
                </span>
                <div class="recipient__time _ml-xl _pl-xxs">{{$transfer->created_at->format('d.m.y')}}</div>
            </div>
            @if(\Organisation::isFounder())
                @if($transfer->human)
                    @if($transfer->recipient_id == Organisation::getOrganisationId() and  $transfer->status == \App\Enums\Cargo\CargoStatusesEnum::SEND_CARGO)
                        <div class="_flex _items-center _text-center _mt-xs">
                            <a x-data="Alpine.plugins.openModal('popup.file-human-popup', {{json_encode(['transferId' => $transfer->id])}})"
                               @click="open"
                               class="button button--white-text button--main button--small button--uppercase _mr-sm">{{__('dashboard.Confirm receipt')}}</a>
                        </div>
                    @else

                    @endif
                @else
                    @if($transfer->recipient_id == Organisation::getOrganisationId() and  $transfer->status == \App\Enums\Cargo\CargoStatusesEnum::SEND_CARGO)
                        <div class="_flex _items-center _text-center _mt-xs">
                            <a class="button button--main button--small _mr-sm"
                               href="{{route('cabinet.accept-cargo', $openRowData['transfer']->id ?? $transfer->id)}}">{{__('dashboard.Confirm receipt')}}</a>
                        </div>
                    @elseif($transfer->status == \App\Enums\Cargo\CargoStatusesEnum::SEND_APPROVE and  Organisation::isRecipient())
                        <div class="_flex _items-center _text-center _mt-xs">
                            <a class="button button--main button--small _mr-sm"
                               href="{{route('cabinet.approve', $transfer->id)}}">{{__('Approve')}}</a>
                        </div>
                    @elseif(Organisation::isDonor())
                        <div class="_flex _items-center _text-center _mt-xs">
                            @if($transfer->status == \App\Enums\Cargo\CargoStatusesEnum::DELIVERY_CONFIRMED or $transfer->status == \App\Enums\Cargo\CargoStatusesEnum::SEND_CARGO)
                                {{-- <a class="button button--main button--small _mr-sm" href="{{route('cabinet.donate-product', $transfer->id)}}">
                                         {{__('View')}}
                                 </a>--}}
                            @else
                                <a class="button button--main button--small _mr-sm"
                                   href="{{route('cabinet.donate-product', $transfer->id)}}">
                                    {{__('Edit')}}
                                </a>
                            @endif
                        </div>
                    @endif
                @endif
            @endif
            <div class="recipient__attach _mb-md">
                <div class="_flex _flex-wrap _mt-md">
                    <div class="_flex _flex-column _mr-df _mb-md">
                        <div class="_mb-sm">
                            Attached info:
                        </div>
                        <div class="_flex _items-end _spacer _spacer--sm">
                            <div
                                x-data="Alpine.plugins.openModal('popup.files-transfer-popup',  {{ json_encode(['transferId' => $transfer->id, 'popup-files.'.$transfer->id]) }})"
                                @click="open"
                            >
                                @svg('dashboard', 'document', [23, 29])
                            </div>
                            <div
                                x-data="Alpine.plugins.openModal('popup.images-transfer-popup',  {{ json_encode(['transferId' => $transfer->id, 'popup-photo.'.$transfer->id]) }})"
                                @click="open"
                            >
                                @svg('dashboard', 'photo', [23, 23])
                            </div>
                            <div
                                x-data="Alpine.plugins.openModal('popup.videos-transfer-popup',  {{ json_encode(['transferId' => $transfer->id, 'popup-photo.'.$transfer->id]) }})"
                                @click="open">
                                <svg width="23" height="23" viewBox="0 0 23 23" fill="none"
                                     xmlns="http://www.w3.org/2000/svg">
                                    <circle cx="11.5002" cy="11.5" r="10.85" fill="white" stroke="#353535"
                                            stroke-width="1.3"/>
                                    <path d="M9.00024 16V7L16.0002 11.5L9.00024 16Z" class="cl1"/>
                                </svg>
                            </div>
                            @if($openRowData['locations'] and ($transfer->status == \App\Enums\Cargo\CargoStatusesEnum::SEND_CARGO or $transfer->status == \App\Enums\Cargo\CargoStatusesEnum::DELIVERY_CONFIRMED  or $transfer->status == \App\Enums\Cargo\CargoStatusesEnum::DELIVERY_NOT_CONFIRMED))
                                <a href="{{ route('cabinet.locations.cargo', $transfer->id) }}">
                                    @svg('dashboard', 'geo', [23, 29])
                                </a>
                            @endif
                        </div>
                    </div>
                    @if($transfer->status == \App\Enums\Cargo\CargoStatusesEnum::SEND_CARGO or $transfer->status == \App\Enums\Cargo\CargoStatusesEnum::DELIVERY_CONFIRMED  or $transfer->status == \App\Enums\Cargo\CargoStatusesEnum::DELIVERY_NOT_CONFIRMED)
                        <div class="svg">
                            {!! \App\Helper\QrCodeGenerator::generate(route('cabinet.cargo', $transfer->id), 85) !!}
                        </div>
                    @endif
                </div>
            </div>
            <div wire:click="$set('open', false)" class="recipient__close">
                @svg('dashboard', 'close-line', [15, 16])
                {{__('dashboard.Close')}}
            </div>
        </div>

        <div class="_cell _cell--5 _nmt-sm">
            <div class="recipient__list">
                <div class="_grid">
                    <div class="_cell _cell--4 _pl-xs _pr-xs">{{__('dashboard.Goods category')}}</div>
                    <div class="_cell _cell--1 _text-center text-small">{{__('dashboard.Units')}}</div>
                    <div class="_cell _cell--2 _text-center text-small">{{__('dashboard.Amount')}}</div>
                    @if(!$openRowData['human'])
                        <div class="_cell _cell--2 _text-center text-small">{{__('dashboard.Residue')}}</div>
                        <div class="_cell _cell--2 _text-center text-small">{{__('dashboard.Online balance')}}</div>
                    @endif
                    <div class="_cell _cell--1"></div>
                </div>
                @foreach($openRowData['categories'] as $category)
                    <div class="_grid @if($category->id == $openCategoryId) is-open @endif"
                         data-donor="/category-{{$category->id}}/">
                        <span @if($category->id == $openCategoryId)
                              wire:click="closeCategory"
                              @else
                              wire:click="openCategory({{$category->id}})"
                              @endif
                              class="_cell _cell--4 _pl-xs _pr-md">{{$category->name}}</span>
                        <div class="_cell _cell--1 _text-center">-</div>
                        <div class="_cell _cell--2 _text-center">{{$category->amount}}</div>
                        @if(!$openRowData['human'])
                            <div
                                class="_cell _cell--2 _text-center">{{ ($transfer->status == \App\Enums\Cargo\CargoStatusesEnum::DELIVERY_CONFIRMED and isset($openRowData['warehousesData']['residueCategory'][$category->id])) ? $openRowData['warehousesData']['residueCategory'][$category->id] : 0}}</div>
                            <div class="_cell _cell--2 _text-center">
                                {{ $openRowData['onlineBalance']['categories'][$category->id] ?? 0 }}
                            </div>
                        @endif
                        <div class="_cell _cell--1"><span class="recipient__pseudo-line"></span></div>
                    </div>
                    @if($category->id == $openCategoryId)
                        @include('components.dashboard.open-category')
                    @endif
                @endforeach
            </div>
        </div>
        @if(!$openRowData['nextOrganizationData'])
            <div class="_cell _cell--4 _nmt-sm"></div>
        @endif

        @foreach($openRowData['nextOrganizationData'] as $nextOrganizationData)
            @php
                $loopOld = $loop;
            @endphp
            <div class="_cell _cell--5 _nmt-sm">
                <div class="recipient__list recipient__list--right">
                    <div class="_grid">
                        <div class="_cell
                        @if(count($openRowData['nextOrganizationData']) != $loopOld->iteration) _cell--5 @else _cell--6 @endif
                            _pl-xs _pr-xs">{{__('dashboard.Name of recipients')}}</div>
                        <div class="_cell _cell--2 _text-center text-small">{{__('dashboard.Taken')}}</div>
                        <div class="_cell _cell--2 _text-center text-small">{{__('dashboard.Residue')}}</div>
                        <div class="_cell _cell--2 _text-center text-small">{{__('dashboard.Online balance')}}</div>

                        @if(count($openRowData['nextOrganizationData']) != $loopOld->iteration)
                            <div class="_cell _cell--1"></div>
                        @endif
                    </div>
                    @foreach($nextOrganizationData as $key => $organization)
                        <div class="_grid {{ $organization['isPeople'] ? 'final_point' : '' }}"
                             data-donor="{{$organization['lines']}}">
                            <div class="_cell
                                 @if(count($openRowData['nextOrganizationData']) != $loopOld->iteration) _cell--5 @else _cell--6 @endif _pl-xs _pr-xs">
                                <a x-data="Alpine.plugins.openModal('popup.list-products-transfer', {{json_encode(['transferId' => $key])}})"
                                   @click="open">
                                    {{$organization['name']}}
                                </a>
                                @if($organization['isPeople'] and $organization['human'])
                                    <span class="recipient__confirmation-icon"
                                          x-data="Alpine.plugins.openModal('popup.file-human-popup', {{json_encode(['transferId' => $key])}})"
                                          @click="open">
                                        @if($organization['human']['confirmed'])
                                            @svg('dashboard', 'document-approved', [16, 19])
                                        @else
                                            @svg('dashboard', 'document-rejected', [16, 20])
                                        @endif
                                    </span>
                                @endif

                            </div>
                            <div class="_cell _cell--2 _text-center">{{$organization['taken']}}</div>
                            <div class="_cell _cell--2 _text-center"
                                 style="{{ $organization['status'] != \App\Enums\Cargo\CargoStatusesEnum::DELIVERY_CONFIRMED ? 'color:red;' : ''}}">{{ $organization['isPeople'] ? '-' : $organization['residue']}}</div>
                            <div class="_cell _cell--2 _text-center">
                                @if($organization['isPeople'])
                                    {{'-'}}
                                @else
                                    {{ (count($openRowData['nextOrganizationData']) != $loopOld->iteration) ? $organization['onlineBalance'] : 0}}
                                @endif
                            </div>
                            @if(count($openRowData['nextOrganizationData']) != $loopOld->iteration)
                                <div class="_cell _cell--1"><span class="recipient__pseudo-line"></span></div>
                            @endif
                        </div>
                    @endforeach
                </div>
            </div>
        @endforeach
    </div>
    @if($popupListProducts)
        @include('components.cargo.popupListProducts')
    @endif
</div>
