@foreach($openRowData['products'] as $product)
    @if($product->category_id != $openCategoryId)
        @continue
    @endif
    <div class="recipient__list--sup _grid" data-donor="/product-{{$product->id}}/">
        <div class="_cell _cell--4 _pl-xs _pr-xs" title="{{$product->name}}">{{$product->name}}</div>
        <div class="_cell _cell--1 _text-center">{{$product->units}}</div>
        <div class="_cell _cell--2 _text-center">{{$product->amount}}</div>
        @if(!$openRowData['human'])
            <div class="_cell _cell--2 _text-center">{{ ($transfer->status == \App\Enums\Cargo\CargoStatusesEnum::DELIVERY_CONFIRMED and isset($openRowData['warehousesData']['residue'][$product->id])) ? $openRowData['warehousesData']['residue'][$product->id] : 0 }}</div>
            <div class="_cell _cell--2 _text-center">
               {{ $openRowData['onlineBalance']['products'][$product->id] ?? 0 }}
            </div>
        @endif
        <div class="_cell _cell--1"><span class="recipient__pseudo-line"></span></div>
    </div>
@endforeach
