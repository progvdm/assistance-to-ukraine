<div class="recipient__line" wire:loading.attr="disabled">
    <div class="_grid _spacer _spacer--xs">
        <div class="_cell _cell--3">
            <span wire:click="$set('open', true)" class="recipient__name">
                {{$numList}}
                {!! \App\Enums\Cargo\CargoStatusesEnum::getIconSvg($transfer->status ?? '')  !!}
                {{--                @if($item['done'])--}}
                {{--                @endif--}}
                <span class="_letter-spacing-xxs _ml-xs _mb-xs">{{$user}} {{ isset($name_donor) ? '('.$name_donor.')' : ''}}</span>
            </span>
            <div class="recipient__time _ml-xl _pl-xxs">{{$transfer->created_at->format('d.m.Y')}}</div>
        </div>
        <div class="_cell _cell--5 _pl-xs _pr-xs">
            {{\Illuminate\Support\Str::limit($categoriesImplode, 50, '...')}}
        </div>
        <div class="_cell _cell--4">
            @if($transfer->status == \App\Enums\Cargo\CargoStatusesEnum::DELIVERY_CONFIRMED)
                {{\Illuminate\Support\Str::limit($childs, 50, '...')}}
            @endif
        </div>
    </div>
</div>
