<div class="popup">
    <div class="popup__wrapper">
        <button wire:click="closePopup" class="popup__close js-import" data-close-popup>
            <i class="fa-xl fa-solid fa-circle-xmark"></i>
        </button>
        <div class="_flex _flex-column _mb-md js-import">
            <div class="_mb-md">Locations:</div>
            <div class="_grid _spacer _spacer--md _items-center">
                <div class="_cell _cell--6 ellipsis-text _justify-start">
                    @foreach($locations as $loc)
                        <div class="_flex">
                            <div>
                                <a target="_blank" href="{{ asset('storage/keeex/'. $loc->photo )}}" alt="">Photo</a>
                                <div>
                                    <a target="_blank" href="https://www.google.com.ua/maps/search/{{ $loc->geo_mark }}" alt="">{{ $loc->geo_mark }}</a>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
