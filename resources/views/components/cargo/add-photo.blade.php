<div class="_grid _spacer _spacer--lg _mb-xs">
    <div class="_cell _cell--12 _m-none">
        <label class="with-anim-underline" data-text="{{ __('Download (webp, jpg, png)') }}">
            <input type="file" wire:model.defer="photos.{{$index}}.photo" {{ (isset($finish) and $finish) ? 'disabled' : '' }}
                   placeholder="{{__('default.form.Choose img')}}" accept=".png, .jpg, .jpeg, .webp" data-text="{{ __('Download (webp, jpg, png)') }}">
        </label>
        <div class="_flex _items-center _mt-sm">
            <span wire:loading wire:target="photos.{{$index}}.photo">
                <i class="fa-solid fa-lg fa-spinner fa-spin"></i>
                Uploading...
            </span>
        </div>
        @if($photos[$index]['photo'])
            <div class="_flex _items-center _mt-xs _mb-xs">
                <i class="fa-solid fa-lg fa-file-arrow-up"></i>
                <span class="_ml-xs">{{$photos[$index]['photo']->getClientOriginalName()}}</span>
            </div>
            <div class="_grid _mt-df">
                <div class="_cell _cell--6">
                    <img src="{{ $photos[$index]['photo']->temporaryUrl() }}" alt="">
                </div>
            </div>
        @endif
        @include('components.form.error-field',['field' => 'photos.*.photo'])
    </div>
</div>
