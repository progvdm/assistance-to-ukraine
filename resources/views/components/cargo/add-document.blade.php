<div class="_grid _spacer _spacer--lg _mb-xs">
    <div class="_cell _cell--12 _m-none">
        <label class="with-anim-underline" data-text="Download (pdf, rtf, doc)">
            <input type="file" {{ (isset($finish) and $finish) ? 'disabled' : '' }} accept=".pdf, .rtf, .doc, .docx, .excel" wire:model.defer="documents.{{$index}}.document" placeholder="{{__('default.form.Choose file')}}" data-text="Download (pdf, rtf, doc)">
        </label>
        <div class="_flex _items-center _mt-sm">
            <span wire:loading wire:target="documents.{{$index}}.photo">
                <i class="fa-solid fa-lg fa-spinner fa-spin"></i>
                Uploading...
            </span>
        </div>
        @if($documents[$index]['document'])
            <div class="_flex _items-center _mt-xs _mb-xs">
                <i class="fa-solid fa-lg fa-file-arrow-up"></i>
                <span class="_ml-xs">{{$documents[$index]['document']->getClientOriginalName()}}</span>
            </div>
        @endif
        @include('components.form.error-field',['field' => 'documents.*.document'])
    </div>
</div>
