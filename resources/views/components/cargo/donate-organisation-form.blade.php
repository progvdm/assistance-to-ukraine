<div class="form js-import" data-form>
    <div class="_grid _spacer _spacer--lg _mb-md">
        <div class="_cell _cell--12 _md:cell--6">
            <label class="with-anim-underline">
                <span class="form__sub-title">1. {{__('cargo.human_form.Name organisation')}}</span>
                <input class="_mt-xs" type="text" wire:model.defer="additionalInfo.name" {{ (isset($finish) and $finish) ? 'disabled' : '' }}>
            </label>
            @include('components.form.error-field',['field' => 'additionalInfo.name'])
        </div>
        <div class="_cell _cell--12 _md:cell--6">
            <label class="with-anim-underline">
                <span class="form__sub-title">2. {{__('cargo.human_form.Phone')}}</span>
                <input class="_mt-xs" type="text" wire:model.defer="additionalInfo.phone" {{ (isset($finish) and $finish) ? 'disabled' : '' }}>
            </label>
            @include('components.form.error-field',['field' => 'additionalInfo.phone'])
        </div>
        <div class="_cell _cell--12 _md:cell--6">
            <label class="with-anim-underline">
                <span class="form__sub-title">3. {{__('cargo.human_form.Address')}}</span>
                <input class="_mt-xs" type="text" wire:model.defer="additionalInfo.address" {{ (isset($finish) and $finish) ? 'disabled' : '' }}>
            </label>
            @include('components.form.error-field',['field' => 'additionalInfo.address'])
        </div>
        <div class="_cell _cell--12 _md:cell--6">
            <label class="with-anim-underline">
                <span class="form__sub-title">4. {{__('cargo.human_form.Id Code')}}</span>
                <input class="_mt-xs" type="text" wire:model.defer="additionalInfo.id_code" {{ (isset($finish) and $finish) ? 'disabled' : '' }}>
            </label>
            @include('components.form.error-field',['field' => 'additionalInfo.id_code'])
        </div>
        <div class="_cell _cell--12 _md:cell--6">
            <label class="with-anim-underline">
                <span class="form__sub-title">5. {{__('cargo.human_form.Responsible person')}}</span>
                <input class="_mt-xs" type="text" wire:model.defer="additionalInfo.responsible_person" {{ (isset($finish) and $finish) ? 'disabled' : '' }}>
            </label>
            @include('components.form.error-field',['field' => 'additionalInfo.responsible_person'])
        </div>
    </div>
</div>
