<div class="popup">
    <div class="popup__wrapper">
        <button wire:click="closePopup" class="popup__close js-import" data-close-popup>
            <i class="fa-xl fa-solid fa-circle-xmark"></i>
        </button>
        <div class="_flex _flex-column _mb-md js-import">
            <div class="_mb-md">{{__('cargo.history.Acts')}}:</div>
            <div class="_grid _spacer _spacer--md _items-center">
                <div class="_cell _cell--6 ellipsis-text _justify-start">
                    <div class="_flex">
                        <img class="_mr-xs" src="/images/svg/pdf-file.svg"
                             alt="" width="35" height="">
                        <div>
                            <a class="popup__file-link" href="{{asset('storage/acts/'.$confirmPopupData['transfer']['document'])}}"
                               target="_blank" title="{{ $confirmPopupData['transfer']['document'] }}">
                                {{ 1 }}. {{ $confirmPopupData['transfer']['document'] }}
                            </a>
                            <div
                                class="popup__file-time _mt-xs">{{ \Carbon\Carbon::parse($confirmPopupData['transfer']['created_at'])->format('d.m.Y') }}</div>
                        </div>
                    </div>
                </div>
                @if($confirmPopupData['human']['file'])
                    <div class="_cell _cell--6 ellipsis-text _justify-start">
                    <div class="_flex">
                        <img class="_mr-xs" src="/images/svg/doc-file.svg"
                             alt="" width="35" height="">
                        <div>
                            <a class="popup__file-link" href="{{asset('storage/humans/'.$confirmPopupData['human']['file'])}}"
                               target="_blank" title="{{ str_replace('humans/','',$confirmPopupData['human']['file']) }}">
                                {{ 2 }}. {{ str_replace('humans/','',$confirmPopupData['human']['file']) }}
                            </a>
                            <div
                                class="popup__file-time _mt-xs">{{ \Carbon\Carbon::parse($confirmPopupData['human']['created_at'])->format('d.m.Y') }}</div>
                        </div>
                    </div>
                </div>
                @endif
            </div>
        </div>
        @if(!$confirmPopupData['human']['file'] and $confirmPopupData['uploadAccess'])
            <div class="form _mt-df js-import" data-form>
            <div class="_grid _spacer _spacer--lg _mb-xs">
                <div class="_cell _cell--12 _m-none">
                    <label class="with-anim-underline" data-text="{{ __('Download file') }}">
                        <input type="file" wire:model.defer="photos"
                               placeholder="{{__('default.form.Choose img')}}" data-text="{{ __('Download file') }}">
                    </label>
                    <div class="_flex _items-center _mt-sm">
                        <span wire:loading wire:target="photos">
                            <i class="fa-solid fa-lg fa-spinner fa-spin"></i>
                            Uploading...
                        </span>
                    </div>
                    @if(isset($photos) and $photos)
                        <div class="_flex _items-center _mt-xs _mb-xs">
                            <i class="fa-solid fa-lg fa-file-arrow-up"></i>
                            <span class="_ml-xs">{{$photos->getClientOriginalName()}}</span>
                        </div>
                    @endif
                </div>
            </div>
            <button class="button button--main button--small button--uppercase _mr-sm"
                    wire:click="confirmUserAct({{$confirmPopupData['idConfirm']}})">{{__('default.Confirm')}}</button>

        </div>
        @endif
    </div>
</div>
