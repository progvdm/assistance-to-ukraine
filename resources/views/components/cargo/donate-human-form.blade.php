<div class="form js-import" data-form>
    <div class="_grid _spacer _spacer--lg _mb-md">
        <div class="_cell _cell--12 _md:cell--6">
            <label class="with-anim-underline">
                <span class="form__sub-title">1. {{__('cargo.human_form.Full name')}}</span>
                <input class="_mt-xs" type="text" wire:model.defer="additionalInfo.name" {{ (isset($finish) and $finish) ? 'disabled' : '' }}>
            </label>
            @include('components.form.error-field',['field' => 'additionalInfo.name'])
        </div>
        <div class="_cell _cell--12 _md:cell--6">
            <label class="with-anim-underline">
                <span class="form__sub-title">2. {{__('cargo.human_form.Phone')}}</span>
                <input class="_mt-xs" type="text" wire:model.defer="additionalInfo.phone" {{ (isset($finish) and $finish) ? 'disabled' : '' }}>
            </label>
            @include('components.form.error-field',['field' => 'additionalInfo.phone'])
        </div>
        <div class="_cell _cell--12 _md:cell--6">
            <label class="with-anim-underline">
                <span class="form__sub-title">3. {{__('cargo.human_form.passport.Series')}}</span>
                <input class="_mt-xs" type="text" wire:model.defer="additionalInfo.passport.series" {{ (isset($finish) and $finish) ? 'disabled' : '' }}>
            </label>
            @include('components.form.error-field',['field' => 'additionalInfo.passport.series'])
        </div>
        <div class="_cell _cell--12 _md:cell--6">
            <label class="with-anim-underline">
                <span class="form__sub-title">4. {{__('cargo.human_form.passport.Number')}}</span>
                <input class="_mt-xs" type="text" wire:model.defer="additionalInfo.passport.number">
            </label>
            @include('components.form.error-field',['field' => 'additionalInfo.passport.number'])
        </div>
        <div class="_cell _cell--12 _md:cell--6">
            <label class="with-anim-underline">
                <span class="form__sub-title">5. {{__('cargo.human_form.passport.Date')}}</span>
                <input class="_mt-xs" type="date" max="{{ date('Y-m-d') }}" wire:model.defer="additionalInfo.passport.date" {{ (isset($finish) and $finish) ? 'disabled' : '' }}>
            </label>
            @include('components.form.error-field',['field' => 'additionalInfo.passport.date'])
        </div>
        <div class="_cell _cell--12 _md:cell--6">
            <label class="with-anim-underline">
                <span class="form__sub-title">6. {{__('cargo.human_form.passport.Issued')}}</span>
                <input class="_mt-xs" type="text" wire:model.defer="additionalInfo.passport.issued" {{ (isset($finish) and $finish) ? 'disabled' : '' }}>
            </label>
            @include('components.form.error-field',['field' => 'additionalInfo.passport.issued'])
        </div>
        <div class="_cell _cell--12 _md:cell--6">
            <label class="with-anim-underline">
                <span class="form__sub-title">7. {{__('cargo.human_form.passport.Registration address')}}</span>
                <input class="_mt-xs" type="text" wire:model.defer="additionalInfo.passport.registration_address" {{ (isset($finish) and $finish) ? 'disabled' : '' }}>
            </label>
            @include('components.form.error-field',['field' => 'additionalInfo.passport.registration_address'])
        </div>
    </div>
</div>
