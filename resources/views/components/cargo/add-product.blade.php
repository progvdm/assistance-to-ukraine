<div class="_grid _spacer _spacer--lg _mb-df">
    <div class="_cell _cell--12 _m-none">
        <label class="with-anim-underline">
            <select wire:model.defer="formProducts.{{$index}}.category_id" {{ (isset($finish) and $finish) ? 'disabled' : '' }}>
                <option value="">
                    {{__('cargo.form.categories')}}
                </option>
            @foreach ($categories as $category)
                    <option value="{{$category->id}}">{{$category->current->name}}</option>
                @endforeach
            </select>
        </label>
        @include('components.form.error-field',['field' => 'formProducts.'.$index.'.category_id'])
    </div>
</div>
<div class="_grid _spacer _spacer--lg _mb-df">
    <div class="_cell _cell--12 _m-none">
        <div class="form__item _flex _flex-column">
            <label class="with-anim-underline" for="formProducts.{{$index}}.name">
                {{__('cargo.products.name')}}:
            </label>
            <input type="text" wire:model.defer="formProducts.{{$index}}.name" id="formProducts.{{$index}}.name" {{ (isset($finish) and $finish) ? 'disabled' : '' }}>

            @include('components.form.error-field',['field' => 'formProducts.'.$index.'.name'])
        </div>
    </div>
</div>
<div class="_grid _spacer _spacer--lg _mb-df">
    <div class="_cell _cell--12 _m-none">
        <div class="form__item _flex _flex-column">
            <label class="with-anim-underline" for="formProducts.{{$index}}.code">
                {{__('cargo.products.code')}}:
            </label>
            <input type="text" wire:model.defer="formProducts.{{$index}}.code" id="formProducts.{{$index}}.code" {{ (isset($finish) and $finish) ? 'disabled' : '' }}>

            @include('components.form.error-field',['field' => 'formProducts.'.$index.'.code'])
        </div>
    </div>
</div>
<div class="_grid _spacer _spacer--lg _mb-df">
    <div class="_cell _cell--6 _m-none">
        <div class="form__item _flex _flex-column">
            <label class="with-anim-underline" for="formProducts.{{$index}}.quantity" >
                {{__('cargo.products.quantity')}}:
            </label>
            <input type="text" wire:model.defer="formProducts.{{$index}}.quantity" id="formProducts.{{$index}}.quantity" {{ (isset($finish) and $finish) ? 'disabled' : '' }}>

            @include('components.form.error-field',['field' => 'formProducts.'.$index.'.quantity'])
        </div>
    </div>
    <div class="=_cell _cell--6 _m-none">
        <div class="form__item _flex _flex-column">
            <label class="with-anim-underline" for="formProducts.{{$index}}.volume">
                {{__('cargo.products.volume')}}:
            </label>
            <input type="text" wire:model.defer="formProducts.{{$index}}.volume" id="formProducts.{{$index}}.volume" {{ (isset($finish) and $finish) ? 'disabled' : '' }}>

            @include('components.form.error-field',['field' => 'formProducts.'.$index.'.volume'])
        </div>
    </div>
</div>
<div class="_grid _spacer _spacer--lg _mb-df">
    <div class="_cell _cell--6 _m-none">
        <label class="with-anim-underline">
            <select wire:model.defer="formProducts.{{$index}}.type" {{ (isset($finish) and $finish) ? 'disabled' : '' }}>
                <option value="">
                    {{__('cargo.products.type')}}
                </option>
                @foreach ($productTypes as $type => $name)
                    <option value="{{$type}}">{{$name}}</option>
                @endforeach
            </select>
        </label>
        @include('components.form.error-field',['field' => 'formProducts.'.$index.'.type'])
    </div>
</div>



