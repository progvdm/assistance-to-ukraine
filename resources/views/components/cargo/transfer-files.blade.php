<div class="popup">
    <div class="popup__wrapper popup__wrapper--scroll-block">
        <button wire:click="closePopup" class="popup__close js-import" data-close-popup>
            <i class="fa-xl fa-solid fa-circle-xmark"></i>
        </button>
        @if($acts)
            <div class="_flex _flex-column _mb-md js-import" >
                <div class="_mb-md">{{__('cargo.history.Acts')}}:</div>
                <div class="popup__scroll-content _pb-md">
                    <div class="_grid _spacer _spacer--md _items-center">
                        @foreach($acts as $file)
                            @if(isset($file['human']) and $file['human'] == true)
                                <div class="_cell _cell--6 _md:cell--4">
                                    <img class="_mr-xs" src="/images/svg/{{ explode('.', $file['document'])[1] }}-file.svg" alt="" width="35" height="">
                                    <div class="ellipsis-text">
                                        <a class="popup__file-link" href="{{asset('storage/humans/'.$file['document'])}}" target="_blank" title="{{ $file['document'] }}">
                                            {{ $loop->iteration }}. {{ $file['document']}}
                                        </a>
                                        <div class="popup__file-time _mt-xs">{{ \Carbon\Carbon::parse($file['created_at'])->format('d.m.Y') }}</div>
                                    </div>
                                </div>
                            @else
                                <div class="_cell _cell--6 _md:cell--4">
                                    <img class="_mr-xs" src="/images/svg/{{ explode('.', $file['document'])[1] }}-file.svg" alt="" width="35" height="">
                                    <div class="ellipsis-text">
                                        <a class="popup__file-link" href="{{asset('storage/acts/'.$file['document'])}}" target="_blank" title="{{ $file['document'] }}">
                                            {{ $loop->iteration }}. {{ $file['document'] }}
                                        </a>
                                        <div class="popup__file-time _mt-xs">{{ \Carbon\Carbon::parse($file['created_at'])->format('d.m.Y') }}</div>
                                    </div>
                                </div>
                            @endif


                        @endforeach
                    </div>
                </div>
            </div>
        @endif
        @if($files)
            <div class="_flex _flex-column _mb-md js-import" >
                <div class="_mb-md">{{__('cargo.history.Documents')}}:</div>
                <div class="popup__scroll-content _pb-md">
                    <div class="_grid _spacer _spacer--md _items-center">
                        @foreach($files as $file)
                            <div class="_cell _cell--6 _md:cell--4">
                                <img class="_mr-xs" src="/images/svg/pdf-file.svg" alt="" width="35" height="">
                                <div class="ellipsis-text">
                                    <a class="popup__file-link" href="{{$file['url']}}" target="_blank" title="{{ $file['name'] }}">
                                        {{ $loop->iteration }}. {{ $file['name'] }}
                                    </a>
                                    <div class="popup__file-time _mt-xs">{{ \Carbon\Carbon::parse($file['created_at'])->format('d.m.Y') }}</div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        @endif
    </div>
</div>
