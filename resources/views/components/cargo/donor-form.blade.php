<div class="_grid _spacer _spacer--lg _mb-xxs">
    <div class="_cell _cell--12 _md:cell--6">
        <label class="with-anim-underline">
            <span class="form__sub-title">6. {{__('cargo.donor_form.Driver name')}}</span>
            <input class="_mt-xs" type="text" wire:model.defer="additionalInfo.driver_name" {{ (isset($finish) and $finish) ? 'disabled' : '' }}>
        </label>
        @include('components.form.error-field',['field' => 'additionalInfo.driver_name'])
    </div>
    <div class="_cell _cell--12 _md:cell--6">
        <label class="with-anim-underline">
            <span class="form__sub-title">7. {{__('cargo.donor_form.Brand car')}}</span>
            <input class="_mt-xs" type="text" wire:model.defer="additionalInfo.brand_car" {{ (isset($finish) and $finish) ? 'disabled' : '' }}>
        </label>
        @include('components.form.error-field',['field' => 'additionalInfo.driver_name'])
    </div>
    <div class="_cell _cell--12 _md:cell--6">
        <label class="with-anim-underline">
            <span class="form__sub-title">8. {{__('cargo.donor_form.Licence plate')}}</span>
            <input class="_mt-xs" type="text" wire:model.defer="additionalInfo.licence_plate" {{ (isset($finish) and $finish) ? 'disabled' : '' }}>
        </label>
        @include('components.form.error-field',['field' => 'additionalInfo.driver_name'])
    </div>
    <div class="_cell _cell--12 _md:cell--6">
        <label class="with-anim-underline">
            <span class="form__sub-title">9. {{__('cargo.donor_form.Checkpoint')}}</span>
            <input class="_mt-xs" type="text" wire:model.defer="additionalInfo.checkpoint" {{ (isset($finish) and $finish) ? 'disabled' : '' }}>
        </label>
        @include('components.form.error-field',['field' => 'additionalInfo.driver_name'])
    </div>
</div>
