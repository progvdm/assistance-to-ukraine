<div class="popup">
    <div class="popup__wrapper">
        <button wire:click="closePopup" class="popup__close js-import" data-close-popup>
            <i class="fa-xl fa-solid fa-circle-xmark"></i>
        </button>
          <div class="_flex _flex-column _mb-md">
            <div class="_mb-md">{{__('cargo.history.Video')}}:</div>
            <div class="_grid _spacer _spacer--md _items-center">
                @foreach($videos as $video)
                    <div class="_cell _cell--12 _md:cell--6">
                        <video class="_mb-md" controls>
                            <source src="{{$video['url']}}" type="video/mp4"/>
                        </video>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
</div>
