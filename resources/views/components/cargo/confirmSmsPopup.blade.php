    <div class="popup">
        <div class="popup__wrapper">
            <span wire:click="submitConfirmSms(true)" class="popup__close js-import" data-close-popup>
                <i class="fa-xl fa-solid fa-circle-xmark"></i>
            </span>
            <div class="form js-import _mt-df" data-form>
                <div class="_flex _flex-column _mb-md js-import">
                    <div class="_cell _cell--6 _m-none">
                        <div class="form__item _flex _flex-column">
                            <label class="with-anim-underline" for="confirmSms($event.target.value)">
                                {{__('cargo.form.Sms code')}}:
                            </label>
                            <input type="number" wire:change="confirmSms($event.target.value)" id="confirmSms($event.target.value)">
                        </div>
                    </div>
                </div>
                <button class="button button--main button--small _mr-sm">{{__('default.Confirm')}}</button>
            </div>
        </div>
    </div>

