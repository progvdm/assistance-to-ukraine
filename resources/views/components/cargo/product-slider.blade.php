<div class="_grid _spacer _spacer--md">
    <div class="_cell _cell--12">
        <div class="_posr">
            <div class="delivery-slider _flex ">
                <div class="_overflow">
                    <div class="delivery-slider__slider delivery-slider__slider--top swiper-container js-import _mb-md"
                         data-swiper-slider='{"containerClass":"delivery-slider__slider","type":"DeliverySlider","navigation":{"type":"default-outside","outside":true}}'>

                        <div class="delivery-slider__rating-block">
                            @if($feedback)
                                <button wire:click="ratingPopup" class="delivery-slider__set-rating">
                                    <svg width="15" height="13" viewBox="0 0 15 13" fill="none"
                                         xmlns="http://www.w3.org/2000/svg">
                                        <path fill-rule="evenodd" clip-rule="evenodd"
                                              d="M0 1.44444C0 0.6467 0.671573 0 1.5 0H13.5C14.3284 0 15 0.6467 15 1.44444V9.38889C15 10.1866 14.3284 10.8333 13.5 10.8333H10.0607L8.03033 12.7885C7.73744 13.0705 7.26256 13.0705 6.96967 12.7885L4.93934 10.8333H1.5C0.671573 10.8333 0 10.1866 0 9.38889V1.44444ZM13.5 1.44444H1.5V9.38889H5.25C5.44891 9.38889 5.63968 9.46498 5.78033 9.60042L7.5 11.2564L9.21967 9.60042C9.36032 9.46498 9.55109 9.38889 9.75 9.38889H13.5V1.44444ZM3 3.97222C3 3.57335 3.33579 3.25 3.75 3.25H11.25C11.6642 3.25 12 3.57335 12 3.97222C12 4.37109 11.6642 4.69444 11.25 4.69444H3.75C3.33579 4.69444 3 4.37109 3 3.97222ZM3 6.86111C3 6.46224 3.33579 6.13889 3.75 6.13889H8.25C8.66421 6.13889 9 6.46224 9 6.86111C9 7.25998 8.66421 7.58333 8.25 7.58333H3.75C3.33579 7.58333 3 7.25998 3 6.86111Z"
                                              fill="white"/>
                                    </svg>
                                    <span>{{__('default.rating.Leave feedback')}}</span>
                                </button>


                            <div class="delivery-slider__rating">
                                <svg width="14" height="13" viewBox="0 0 14 13" fill="none"
                                     xmlns="http://www.w3.org/2000/svg">
                                    <path
                                        d="M7 0.614055L8.81494 4.68577L8.87558 4.82182L9.0241 4.83325L13.3953 5.16975L9.96334 8.2294L9.84918 8.33118L9.88781 8.47915L10.9333 12.4846L7.12495 10.2869L7 10.2148L6.87505 10.2869L3.07646 12.479L4.17647 8.48235L4.21804 8.33131L4.10024 8.22803L0.611507 5.16922L4.9759 4.83325L5.12441 4.82182L5.18506 4.68577L7 0.614055Z"
                                        fill="#F8D748" stroke="#F0CF3E" stroke-width="0.5"/>
                                </svg>
                                <span>{{(int)$grade}}</span>
                            </div>
                            @endif
                        </div>

                        <div class="swiper-wrapper">
                            @forelse($photos as $photo)
                                <div class="delivery-slider__image swiper-slide">
                                    <img src="{{asset('storage/images/'.$photo->photo)}}" alt="" loading="lazy">
                                </div>
                            @empty
                                <img src="{{asset('/images/noimage.jpg')}}" loading="lazy">
                            @endforelse
                        </div>
                    </div>

                    <div class="delivery-slider__slider delivery-slider__slider--bottom swiper-container swiper-thumbs">
                        <div class="swiper-wrapper">
                            @foreach($photos as $photo)
                                <div class="swiper-slide">
                                    <div class="swiper-slide-container">
                                        <img width="50" src="{{asset('storage/images/'.$photo->photo)}}" alt="" loading="lazy">
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
            <div class="slider-navigation ">
                <div
                    class="slider-navigation__button slider-navigation__button--prev swiper-button-prev swiper-button-disabled">
                    <svg width="30" height="30" viewBox="0 0 30 30" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <circle class="circle" r="15" transform="matrix(4.37114e-08 -1 -1 -4.37114e-08 15 15)"/>
                        <path d="M16.5132 10.9287L11.4421 15.9998" stroke="white" stroke-width="1.5"/>
                        <path d="M11.4421 14.9287L16.5132 19.9998" stroke="white" stroke-width="1.5"/>
                    </svg>
                </div>
                <div class="slider-navigation__button slider-navigation__button--next swiper-button-next">
                    <svg width="30" height="30" viewBox="0 0 30 30" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <circle class=circle cx="15" cy="15" r="15" transform="rotate(-90 15 15)"/>
                        <path d="M13.4868 10.9287L18.5579 15.9998" stroke="white" stroke-width="1.5"/>
                        <path d="M18.5579 14.9287L13.4868 19.9998" stroke="white" stroke-width="1.5"/>
                    </svg>
                </div>
            </div>
        </div>
    </div>
</div>

@if($popupRating)
    <div>
        <div class="popup popup--transparent">
            <div class="popup__wrapper">
                <button wire:click="close" class="popup__close js-import" data-close-popup>
                    <i class="fa-xl fa-solid fa-circle-xmark"></i>
                </button>
                <livewire:rating.rating :cargo="$cargos->first()"/>
            </div>
        </div>
    </div>
@endif
