<div class="_grid _spacer _spacer--lg _mb-xs">
    <div class="_cell _cell--12 _m-none">
        <label class="with-anim-underline" data-text="Download (mp4, mov)">
            <input type="file" wire:model.defer="videos.{{$index}}.video" accept=".mp4, .mov" placeholder="{{__('default.form.Choose file')}}" {{ (isset($finish) and $finish) ? 'disabled' : '' }} data-text="Download (mp4, mov)">
        </label>
        <div class="_flex _items-center _mt-sm">
            <span wire:loading wire:target="videos.{{$index}}.photo">
                <i class="fa-solid fa-lg fa-spinner fa-spin"></i>
                Uploading...
            </span>
        </div>
        @if($videos[$index]['video'])
            <div class="_flex _items-center _mt-xs _mb-xs">
                <i class="fa-solid fa-lg fa-file-arrow-up"></i>
                <span class="_ml-xs">{{$videos[$index]['video']->getClientOriginalName()}}</span>
            </div>
        @endif
        @include('components.form.error-field',['field' => 'videos.*.video'])
    </div>
</div>
