<div class="form__table">
    <div class="form__table-title _p-xs">
        <div class="_grid _spacer _spacer--xs _items-end _flex-nowrap">
            <div class="_cell _cell--1"></div>
            <div class="_cell _cell--4">{{__('cargo.products.Name')}}</div>
            <div class="_cell _cell--2 _pl-none">{{__('cargo.products.Units')}}</div>
            <div class="_cell _cell--2 _pl-none _ml-md _mr-df _text-center">{{__('cargo.products.Received')}}</div>
            <div class="_cell _pl-none _text-center">{{__('cargo.products.Amount')}}</div>
            <div class="_cell _text-center">
                <div class="form__table-amount">
                    <div class="_flex _items-center _justify-center">
                        {{__('cargo.products.Residue')}}
                        <svg class="up" width="5" height="4" viewBox="0 0 5 4" fill="none"
                             xmlns="http://www.w3.org/2000/svg">
                            <path d="M2.5 4L0.334936 0.25L4.66506 0.250001L2.5 4Z" fill="#353535"/>
                        </svg>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="form__table-wrapper _mb-md _p-xs _pr-sm">
        @forelse($formProducts as $index => $product)
            <div class="_grid _spacer _spacer--xs _items-end _flex-nowrap">
                <div class="_cell _cell--1">
                    <input type="checkbox" {{ ($finish or $product['quantity_storage'] == 0) ? 'disabled' : '' }} wire:model.defer="formProducts.{{$index}}.checked"
                           value="1">
                </div>
                <div class="_cell _cell--4">{{$product['name'] . ' - ' . $product['name_sender']}}</div>
                <div class="_cell _cell--3">
                    <input disabled="disabled" type="text" wire:model.defer="formProducts.{{$index}}.type">
                </div>
                <div class="_cell _cell--2">
                    <input disabled="disabled" type="text" value="{{ $product['original_quantity'] }}">
                </div>
                <div class="_cell">
                    <label class="with-anim-underline">
                        <input class="_text-center" {{ ($finish or $product['quantity_storage'] == 0) ? 'disabled' : '' }} type="number"  wire:model.defer="formProducts.{{$index}}.quantity">
                    </label>
                </div>
                <div class="_cell">
                    <label class="with-anim-underline">
                        <input class="_text-center" disabled="disabled" type="text" wire:model.defer="formProducts.{{$index}}.quantity_residue">
                    </label>
                </div>
            </div>
        @empty
            <div></div>
        @endforelse
    </div>

</div>
<div class="_mt-sm _ms:mt-none">
    <button wire:click="validateProductQuantity()" target="_blank"
            class="button button--transparent button--small button--uppercase _mr-sm">
        <i class="_mr-xs fa-sharp fa-solid fa-rotate"></i>
        {{__('Check list')}}
    </button>
</div>
