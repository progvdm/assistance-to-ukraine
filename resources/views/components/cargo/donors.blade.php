<div>
    <div class="form__sub-title _mb-df">
        {{__('cargo.form.Donor')}}
    </div>
    <div class="_flex _flex-column _mb-df">
        <label class="with-anim-underline">
            <select wire:model.defer="donor_id" wire:change="addNewDonor($event.target.value)">
                <option value="0">
                    {{ __('cargo.form.Not selected') }}
                </option>
                <option value="new">
                    {{ __('cargo.form.Add new') }}
                </option>
                @foreach ($donors as $value)
                    <option value="{{$value['id']}}">{{$value['name']}}</option>
                @endforeach
            </select>
        </label>
    </div>
    @if($isNewDonor)
        <div class="form__sub-title _mb-df _mt-md _lg:mt-xl">
            {{__('cargo.Agent')}}
        </div>
        <div class="_flex _flex-column _mb-df">
            <label class="with-anim-underline">
                <input wire:model.defer="newDonor.user.name" type="text" placeholder="{{__('auth.form.name')}}">
            </label>
            @include('components.form.error-field',['field' => 'newDonor.user.name'])
        </div>
        <div class="form__sub-title _mb-df _mt-md _lg:mt-xl">
            {{__('auth.General')}}
        </div>
        <div class="_flex _flex-column _mb-df">
            <label class="with-anim-underline">
                <input wire:model.defer="newDonor.user.email" type="text" placeholder="{{__('auth.form.email')}}">
            </label>
            @include('components.form.error-field',['field' => 'newDonor.user.email'])
        </div>
        <div class="_flex _flex-column _mb-df">
            <label class="_flex _flex-row with-anim-underline">
                <input wire:model.lazy="newDonor.organisation.id_code" class="_mr-sm" type="text"
                       placeholder="{{__('auth.form.id_code')}}">
                <div wire:click="substituteCompanyData" type="button">
                    <svg fill="#000000" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 64 64" width="25px"
                         height="25px">
                        <path
                            d="M 24 2.8886719 C 12.365714 2.8886719 2.8886719 12.365723 2.8886719 24 C 2.8886719 35.634277 12.365714 45.111328 24 45.111328 C 29.036552 45.111328 33.664698 43.331333 37.298828 40.373047 L 52.130859 58.953125 C 52.130859 58.953125 55.379484 59.435984 57.396484 57.333984 C 59.427484 55.215984 58.951172 52.134766 58.951172 52.134766 L 40.373047 37.298828 C 43.331332 33.664697 45.111328 29.036548 45.111328 24 C 45.111328 12.365723 35.634286 2.8886719 24 2.8886719 z M 24 7.1113281 C 33.352549 7.1113281 40.888672 14.647457 40.888672 24 C 40.888672 33.352543 33.352549 40.888672 24 40.888672 C 14.647451 40.888672 7.1113281 33.352543 7.1113281 24 C 7.1113281 14.647457 14.647451 7.1113281 24 7.1113281 z"/>
                    </svg>
                </div>
            </label>
            @include('components.form.error-field',['field' => 'newDonor.organisation.id_code'])
        </div>
        <div class="_flex _flex-column _mb-df">
            <label class="with-anim-underline">
                <input wire:model.lazy="newDonor.organisation.zip_code" class="_mr-sm" type="text"
                       placeholder="{{__('auth.form.zip_code')}}">
            </label>
            @include('components.form.error-field',['field' => 'newDonor.organisation.zip_code'])
        </div>
        <div class="_flex _flex-column _mb-df">
            <label class="with-anim-underline">
                <input wire:model.defer="newDonor.organisation.name" type="text"
                       placeholder="{{__('auth.form.Name organisation')}}">
            </label>
            @include('components.form.error-field',['field' => 'newDonor.organisation.name'])
        </div>
        <div class="form__sub-title _mb-df _mt-md _lg:mt-xl">
            {{__('auth.Address')}}
        </div>
        <div class="_grid _spacer _spacer--lg  _mb-df">
            <div class="_cell _cell--6 _m-none">
                <div class="_flex _flex-column">
                    <label class="with-anim-underline">
                        <input wire:model.defer="newDonor.organisation.country" type="text" placeholder="{{__('auth.form.country')}}">
                    </label>
                    @include('components.form.error-field',['field' => 'newDonor.organisation.country'])
                </div>
            </div>
            <div class="_cell _cell--6  _m-none">
                <div class="_flex _flex-column">
                    <label class="with-anim-underline">
                        <input wire:model.defer="newDonor.organisation.city" type="text" placeholder="{{__('auth.form.city')}}">
                    </label>
                    @include('components.form.error-field',['field' => 'newDonor.organisation.city'])
                </div>
            </div>
        </div>
        <div class="_grid _spacer _spacer--sm _mb-df">
            <div class="_cell _cell--9 _m-none">
                <div class="_flex _flex-column">
                    <label class="with-anim-underline">
                        <input wire:model.defer="newDonor.organisation.street" type="text" placeholder="{{__('auth.form.street')}}">
                    </label>
                    @include('components.form.error-field',['field' => 'newDonor.organisation.street'])
                </div>
            </div>
            <div class="_cell _cell--3 _m-none">
                <div class="_flex _flex-column">
                    <label class="with-anim-underline">
                        <input wire:model.defer="newDonor.organisation.build" type="text" placeholder="{{__('auth.form.build')}}">
                    </label>
                    @include('components.form.error-field',['field' => 'newDonor.organisation.build'])
                </div>
            </div>
        </div>
    @endif
</div>
