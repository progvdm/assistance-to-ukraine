<div class="popup">
    <div class="popup__wrapper popup__wrapper--big popup__wrapper--scroll-block _p-none">
        <div class="popup__header">
            <div class="_flex _justify-between _items-center">
                <div>
                    {{__('cargo.history.Photo')}}
                    <button class="button button--main button--small button--small--xs _ml-sm _pr-none">
                        <svg width="22" height="16" viewBox="0 0 22 16" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M0.292893 7.29289C-0.0976314 7.68342 -0.0976315 8.31658 0.292892 8.7071L6.65685 15.0711C7.04738 15.4616 7.68054 15.4616 8.07107 15.0711C8.46159 14.6805 8.46159 14.0474 8.07107 13.6569L2.41421 8L8.07107 2.34314C8.46159 1.95262 8.46159 1.31946 8.07107 0.928931C7.68054 0.538407 7.04738 0.538406 6.65686 0.928931L0.292893 7.29289ZM22 7L1 7L1 9L22 9L22 7Z" fill="white"/></svg>
                    </button>
                </div>
                <button wire:click="closePopup" class="popup__close js-import" data-close-popup>
                    <i class="fa-xl fa-solid fa-circle-xmark"></i>
                </button>
            </div>
        </div>
        <div class="popup__body">
            <div class="_grid _spacer _spacer--md _items-center _mb-none">
                @foreach($photos as $photo)
                    @if(isset($photo['geo_mark']))
                        <div class="_cell _cell--4">
                            <a class="tab__item tab__item--small" href="{{asset('storage/keeex/'.$photo['photo'])}}"
                               data-description="{{ $photo['comment'] }}" style="background-image: url('{{asset('storage/keeex/'.$photo['photo'])}}')">
                            </a>
                        </div>
                    @else
                        <div class="_cell _cell--4">
                            <a class="tab__item tab__item--small" href="{{$photo['url']}}"
                               data-description="" style="background-image: url('{{$photo['url']}}')">
                            </a>
                        </div>
                    @endif
                @endforeach
            </div>
        </div>
    </div>
</div>
