@php
    $active = false;
        if($loop->first and !isset($tabs[$tabContainer])){
            $active = true;
        }
@endphp

<div
    id="tab-{{$tabContainer}}-{{$key}}"
    class="_pt-xl {{ ((isset($tabs) and isset($tabs[$tabContainer]) and $tabs[$tabContainer] == $key) or $active) ? 'is-active' : '_hide' }}"
>
