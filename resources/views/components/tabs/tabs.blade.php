
<div class="tabs">
    <div class="_flex">
        @foreach($tabsList as $key => $tab)
            @php
                $hasError= false;
                    $active = false;
                        if($loop->first and !isset($tabs[$tabContainer])){
                            $active = true;
                        }
                        if(isset($field) and isset($fieldsArr)){
                            $errorsFieldsTab = [];
                            foreach ($fieldsArr as $itemField){
                                $errorsFieldsTab[] = $field.'.'.$key.'.'.$itemField;
                            }
                            $hasError = $errors->hasAny($errorsFieldsTab);
                        }
            @endphp
            <span wire:click="selectTab('{{$tabContainer}}', '{{$key}}')" class="tabs__item {{ ((isset($tabs) and isset($tabs[$tabContainer]) and $tabs[$tabContainer] == $key) or $active) ? 'is-active' : $tabContainer }} {{ $hasError ? 'has-error' : '' }}">
               @if(is_array($tab))
                    {{$tab['name']}}
                @else
                    {{$tab}}
                @endif
            </span>
        @endforeach
    </div>
</div>
