<div class="card {{ $class ?? '' }}">
    <div class="card__head">
        <div class="_flex _flex-wrap _justify-between">
            <span>№ {{$item->numberSystem()}}</span>
            <span>{{ __('aid-request.marketplace.Valid until') }} -  {{$item->active_date->format('d.m.y')}}</span>
        </div>
    </div>
    <div class="card__body" @if($item->getProduct() or auth()->user())x-data="{ opened: false }" @endif>
        {{$item->description}}

        @if($item->getProduct() or auth()->user())
            <div class="card__body--big" @if($item->getProduct() or auth()->user()) x-show="opened" x-collapse.duration.1000ms @endif>
                @if($item->getProduct())
                    <div class="_mt-sm _mb-xs text--weight-bold">{{ __('aid-request.card.Product') }}</div>
                    <span>{{ $item->getProductNameForSite() }}</span>
                @endif
                @if(auth()->user())
                    <div class="_mt-sm _mb-xs text--weight-bold">{{ __('aid-request.card.Organisation') }}</div>
                    <span>{{ $item->organisation->name }}</span>

                    <div class="_mt-sm _mb-xs text--weight-bold">{{ __('aid-request.card.Contacts') }}</div>
                    <span>Email: <a
                            href="mailto:{{ $item->contact_email }}">{{ $item->contact_email }}</a></span>
                    @if($item->contact_phone)
                        <span>{{ __('aid-request.card.Phone:') }} {{ $item->contact_phone }}</span>
                    @endif
                    @if($item->contact_social)
                        <span>{{ __('aid-request.card.Social:') }} {{ $item->contact_social }}</span>
                    @endif
                @endif

            </div>
        @endif
        @if($item->getProduct() or auth()->user())
            <div class="_flex _justify-between _mt-md">
                <div class="card__details" @click="opened = ! opened"
                     :class="opened ? 'is-active' : ''">
                    {{ __('aid-request.marketplace.Details') }}
                </div>
                @if(!auth()->user())
                    <div @if($item->getProduct() or auth()->user()) x-show="opened" x-transition.duration.300ms @endif>
                        <div class="card__apply"
                             x-data="Alpine.plugins.openModal('popup.aid-request-popup',  {{ json_encode(['cardId' => $item->id]) }})"
                             @click="open"
                             @mouseenter="open">
                            {{ __('aid-request.marketplace.Apply') }}
                        </div>
                    </div>
                @endif
            </div>
        @else
            @if(!auth()->user())
                <div class="_flex _justify-end _mt-md">
                    <div @if($item->getProduct() or auth()->user()) x-show="opened" x-transition.duration.300ms @endif>
                        <div class="card__apply"
                                x-data="Alpine.plugins.openModal('popup.aid-request-popup',  {{ json_encode(['cardId' => $item->id]) }})"
                                @click="open"
                                @mouseenter="open">
                            {{ __('aid-request.marketplace.Apply') }}
                        </div>
                    </div>
                </div>
            @endif
        @endif
    </div>
</div>
