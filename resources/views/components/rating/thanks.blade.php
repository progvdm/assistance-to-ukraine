<div class="_flex _flex-column _mb-md">
    <div class="js-import" data-auto-redirect="{{redirect()->back()->getTargetUrl()}}" hidden></div>
    <div class="popup__title">
        <svg width="15" height="13" viewBox="0 0 15 13" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path fill-rule="evenodd" clip-rule="evenodd"
                  d="M0 1.44444C0 0.6467 0.671573 0 1.5 0H13.5C14.3284 0 15 0.6467 15 1.44444V9.38889C15 10.1866 14.3284 10.8333 13.5 10.8333H10.0607L8.03033 12.7885C7.73744 13.0705 7.26256 13.0705 6.96967 12.7885L4.93934 10.8333H1.5C0.671573 10.8333 0 10.1866 0 9.38889V1.44444ZM13.5 1.44444H1.5V9.38889H5.25C5.44891 9.38889 5.63968 9.46498 5.78033 9.60042L7.5 11.2564L9.21967 9.60042C9.36032 9.46498 9.55109 9.38889 9.75 9.38889H13.5V1.44444ZM3 3.97222C3 3.57335 3.33579 3.25 3.75 3.25H11.25C11.6642 3.25 12 3.57335 12 3.97222C12 4.37109 11.6642 4.69444 11.25 4.69444H3.75C3.33579 4.69444 3 4.37109 3 3.97222ZM3 6.86111C3 6.46224 3.33579 6.13889 3.75 6.13889H8.25C8.66421 6.13889 9 6.46224 9 6.86111C9 7.25998 8.66421 7.58333 8.25 7.58333H3.75C3.33579 7.58333 3 7.25998 3 6.86111Z"
                  fill="white"/>
        </svg>

        {{__('default.rating.Leave feedback')}}
    </div>
    <div class="popup__sub-title _mb-df">

    </div>

    <div class="great _pl-df _mb-df">
        {{__('default.rating.Great')}}
    </div>

    <div class="great _pl-df _mb-sm">
        {{__('default.rating.Your choose is')}}:
    </div>

    <div class="_grid _spacer _spacer--lg _pl-df _mb-sm">
        <div class="_cell _cell--12 _md:cell--4">
            <div class="popup__rating-count _mb-sm">
                <div class="_flex _justify-between _items-center">
                    <div>{{$grade}}</div>
                </div>
            </div>
            <div class="popup__rating-stars">
                <div class="_flex _justify-between">
                    @for($i = 1; $i <= 5; $i++)
                        @if($i <= $grade)
                            <span>
                                <svg width="19" height="17" viewBox="0 0 19 17" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path
                                        d="M9.08496 0.622825L11.4831 6.0948L11.5432 6.23199L11.6925 6.24368L17.4878 6.69743L12.9409 10.8204L12.8292 10.9216L12.8667 11.0677L14.2558 16.4804L9.21151 13.5197L9.08496 13.4454L8.95841 13.5197L3.9238 16.4747L5.38612 11.0709L5.42646 10.9218L5.31119 10.819L0.688775 6.69691L6.47739 6.24368L6.62673 6.23199L6.68686 6.0948L9.08496 0.622825Z"
                                        fill="#F8D748" stroke="#F0CF3E" stroke-width="0.5"/>
                                </svg>
                            </span>
                        @else
                            <span>
                                <svg width="19" height="17" viewBox="0 0 19 17" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path
                                        d="M9.19259 0.616684L11.6224 6.0958L11.6829 6.23219L11.8317 6.2437L17.6973 6.69756L13.0976 10.8194L12.9842 10.921L13.0224 11.0684L14.4287 16.4834L9.31803 13.5191L9.19259 13.4463L9.06716 13.5191L3.9663 16.4777L5.44665 11.0716L5.48784 10.9211L5.37081 10.818L0.694618 6.69704L6.55352 6.2437L6.70228 6.23219L6.76277 6.0958L9.19259 0.616684Z"
                                        fill="#C4C4C4" stroke="#B1B1B1" stroke-width="0.5"/>
                                </svg>
                            </span>
                        @endif
                    @endfor
                </div>
            </div>
        </div>
    </div>

    <div class="popup__title _pl-df">{{__('default.rating.Thank for your feedback!')}}</div>
</div>
