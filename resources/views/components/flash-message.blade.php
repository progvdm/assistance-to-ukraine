@if(Session::has(\App\Foundation\FlashMessage::ERROR))
    <div class="noty">
        <p class="noty__message noty__message--error">{{ Session::get(\App\Foundation\FlashMessage::ERROR) }}</p>
    </div>
@elseif(Session::has(\App\Foundation\FlashMessage::SUCCESS))
    <div class="noty">
        <p class="noty__message noty__message--success">{{ Session::get(\App\Foundation\FlashMessage::SUCCESS) }}</p>
    </div>
@endif
@if(Session::has(\App\Foundation\FlashMessage::INFO))
    <div class="noty">
        <p class="noty__message noty__message--info">{{ Session::get(\App\Foundation\FlashMessage::INFO) }}</p>
    </div>
@endif
