<div class="sidebar">
    <div class=" swiper-container js-import"
         data-swiper-slider='{"containerClass":"sidebar","type":"SidebarSlider","navigation":{"type":"default-outside","outside":true}}'>
        <div class="swiper-wrapper">
            <div class="sidebar__item swiper-slide">
                <a class="sidebar__link _flex _items-center"
                   href="{{route('cabinet.home')}}">
                        <span class="sidebar__icon">
                            <i class="fa-solid fa-xl fa-table"></i>
                        </span>
                    {{ __('dashboard.Dashboard') }}
                </a>
            </div>
            <div class="sidebar__item swiper-slide">
                <a class="sidebar__link _flex _items-center"
                   href="{{route('cabinet.aid-requests.create')}}">
                       <span class="sidebar__icon">
                            <i class="fa-solid fa-xl fa-circle-plus"></i>
                       </span>
                    {{ __('aid-request.Add request') }}
                </a>
            </div>
        </div>
    </div>

    <div class="slider-navigation">
        <div class="slider-navigation__button slider-navigation__button--prev swiper-button-prev swiper-button-disabled">
            @svg('slider', 'prev', [40, 41])
        </div>
        <div class="slider-navigation__button slider-navigation__button--next swiper-button-next">
            @svg('slider', 'next', [40, 41])
        </div>
    </div>
</div>
