<div class="sidebar">
    <div class="swiper-container js-import"
         data-swiper-slider='{"containerClass":"sidebar","type":"SidebarSlider","navigation":{"type":"default-outside","outside":true}}'>
        <div class="swiper-wrapper">
            @if(Organisation::isRecipient() and \Organisation::isFounder())
                <div class="sidebar__item swiper-slide">
                    <a class="sidebar__link _flex _items-center @if(url()->full() == route('cabinet.donate-product-people')) is-active @endif"
                       href="{{route('cabinet.donate-product-people')}}"
                       title="{{ __('cabinet.Choose the category of cargo, the cargo itself, and transfer it to an individual') }}">
                        @svg('dashboard', 'heart', [21, 16], 'sidebar__icon')
                        @svg('dashboard', 'heart--hover', [21, 16], 'sidebar__icon sidebar__icon--hover')
                        {{ __('cabinet.Assistance provide to individual') }}
                    </a>
                </div>
                <div class="sidebar__item swiper-slide">
                    <a class="sidebar__link _flex _items-center @if(url()->full() == route('cabinet.donate-product-recipient')) is-active @endif"
                       href="{{route('cabinet.donate-product-recipient')}}"
                       title="{{__('cabinet.Select the cargo category, the cargo itself, and pass it on to another user')}}">
                        @svg('dashboard', 'plus', [19, 20], 'sidebar__icon')
                        @svg('dashboard', 'plus--hover', [19, 20], 'sidebar__icon sidebar__icon--hover')
                        {{ __('cabinet.Assistance to') }}</a>
                </div>
                <div class="sidebar__item swiper-slide">
                    <a class="sidebar__link _flex _items-center @if(url()->full() == route('cabinet.new-shipment-received')) is-active @endif"
                       href="{{route('cabinet.new-shipment-received')}}"
                       title="{{ __('cabinet.Previously received help from an unregistered donor on the site') }}">
                        @svg('dashboard', 'received', [20, 19], 'sidebar__icon')
                        @svg('dashboard', 'received--hover', [20, 19], 'sidebar__icon sidebar__icon--hover')
                        {{ __('cabinet.Received assistance') }}</a>
                </div>
            @elseif(Organisation::isDonor() and \Organisation::isFounder())
                <div class="sidebar__item swiper-slide">
                    <a class="sidebar__link _flex _items-center @if(url()->full() == route('cabinet.donate-product')) is-active @endif"
                       href="{{route('cabinet.donate-product')}}">
                        @svg('dashboard', 'heart', [21, 16], 'sidebar__icon')
                        @svg('dashboard', 'heart--hover', [21, 16], 'sidebar__icon sidebar__icon--hover')
                        {{ __('cabinet.Provide assistance') }}
                    </a>
                </div>

            @endif
            @if(Organisation::isRecipient() and \Organisation::isFounder())
                <div class="sidebar__item swiper-slide">
                    <a class="sidebar__link _flex _items-center @if(url()->full() == route('cabinet.warehouse')) is-active @endif"
                       href="{{route('cabinet.warehouse')}}">
                        @svg('dashboard', 'warehouse', [18, 17], 'sidebar__icon')
                        @svg('dashboard', 'warehouse--hover', [18, 17], 'sidebar__icon sidebar__icon--hover')
                        {{__('cabinet.Warehouse')}}
                    </a>
                </div>
            @endif
            @if(\Organisation::isFounder())
                <div class="sidebar__item swiper-slide">
                    <a class="sidebar__link _flex _items-center"
                       href="{{route('cabinet.aid-requests.index')}}">
                        @svg('dashboard', 'offer', [20, 17], 'sidebar__icon')
                        @svg('dashboard', 'offer--hover', [20, 17], 'sidebar__icon sidebar__icon--hover')
                        Marketplace
                    </a>
                </div>
                <div class="sidebar__item swiper-slide">
                    <a class="sidebar__link _flex _items-center @if(url()->full() == route('cabinet.users')) is-active @endif"
                       href="{{route('cabinet.users')}}">
                        @svg('dashboard', 'persons', [23, 16], 'sidebar__icon')
                        @svg('dashboard', 'persons--hover', [23, 16], 'sidebar__icon sidebar__icon--hover')
                        {{__('cabinet.System users')}}
                    </a>
                </div>
                <div class="sidebar__item swiper-slide">
                    <a class="sidebar__link _flex _items-center @if(url()->full() == route('cabinet.settings')) is-active @endif"
                       href="{{route('cabinet.settings')}}">
                        @svg('dashboard', 'gear', [20, 20], 'sidebar__icon')
                        @svg('dashboard', 'gear--hover', [20, 20], 'sidebar__icon sidebar__icon--hover')
                        {{__('cabinet.Settings')}}
                    </a>
                </div>
            @endif
            <div class="_hide Убрать эту обвертку">
                <div class="sidebar__item swiper-slide">
                    <a class="sidebar__link _flex _items-center"
                       href="">
                        @svg('dashboard', 'offer', [20, 17], 'sidebar__icon')
                        @svg('dashboard', 'offer--hover', [20, 17], 'sidebar__icon sidebar__icon--hover')
                        We offer
                    </a>
                </div>

                <div class="sidebar__item swiper-slide">
                    <a class="sidebar__link _flex _items-center"
                       href="">
                        @svg('dashboard', 'required', [15, 19], 'sidebar__icon')
                        @svg('dashboard', 'required--hover', [15, 19], 'sidebar__icon sidebar__icon--hover')
                        Required
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div class="slider-navigation">
        <div class="slider-navigation__button slider-navigation__button--prev swiper-button-prev swiper-button-disabled">
            @svg('slider', 'prev', [40, 41])
        </div>
        <div class="slider-navigation__button slider-navigation__button--next swiper-button-next">
            @svg('slider', 'next', [40, 41])
        </div>
    </div>
</div>
