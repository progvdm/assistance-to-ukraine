<div class="_grid _grid--1 _md:grid--2 _md:grid--3 _spacer _spacer--md _mb-xxl">
    @foreach($photos as $photo)
        <div class="_cell">
            <a class="tab__item"
               href="{{asset('storage/images/'.$photo->photo)}}"
               data-description="description"
               style="background-image: url('{{asset('storage/images/'.$photo->photo)}}')"></a>
        </div>
    @endforeach
</div>
