<div class="section">
    <div class="container">
        <div class="_grid _spacer _spacer--md">
            <div class="_cell _cell--12 _mt-df">
                <div class="form form--read js-import" data-form>
                    <div class="_grid _spacer _spacer--md _md:spacer--lg">
                        <div class="_cell _cell--12 _md:cell--4">
                            <div class="input-indicator">
                                <div class="form__sub-title _mb-df">
                                    1. {{__('auth.General')}}
                                </div>
                                <div class="form__item _flex _flex-column _mb-df">
                                    <label class="_flex with-solid-underline" for="id_code">
                                        {{__('auth.form.id_code')}}:
                                    </label>
                                    <input disabled id="id_code" class="_mr-sm" type="text"
                                           value="{{ $user->id_code }}">
                                </div>
                                <div class="form__item _flex _flex-column _mb-df">
                                    <label class="with-solid-underline" for="zip_code">
                                        {{__('auth.form.zip_code')}}:
                                    </label>
                                    <input disabled id="zip_code" class="_mr-sm" type="text" value="{{ $user->zip_code }}">
                                </div>
                                <div class="form__item _flex _flex-column">
                                    <label class="with-solid-underline" for="name">
                                        {{__('auth.form.name')}}:
                                    </label>
                                    <input disabled id="name" type="text"
                                           value="{{ $user->name }}">
                                </div>
                            </div>
                            <div class="input-indicator">
                                <div class="form__sub-title _mt-xxl _mb-df">
                                    2. {{__('auth.Address')}}
                                </div>
                                <div class="_grid _spacer _spacer--lg  _mb-df">
                                    <div class="_cell _cell--12 _m-none">
                                        <div class="form__item _flex _flex-column">
                                            <label class="with-solid-underline" for="country">
                                                {{__('auth.form.country')}}:
                                            </label>
                                            <input disabled id="country" type="text" value="{{ $user->country }}">
                                        </div>
                                    </div>
                                </div>
                                <div class="_grid _spacer _spacer--lg  _mb-df">
                                    <div class="_cell _cell--12 _m-none">
                                        <div class="form__item _flex _flex-column">
                                            <label class="with-solid-underline">
                                                {{__('auth.form.city')}}:
                                            </label>
                                            <input disabled id="city" type="text" value="{{ $user->city }}">
                                        </div>
                                    </div>
                                </div>
                                <div class="_grid _spacer _spacer--sm _mb-df">
                                    <div class="_cell _cell--12 _m-none">
                                        <div class="form__item _flex _flex-column">
                                            <label class="with-solid-underline" for="street">
                                                {{__('auth.form.street')}}:
                                            </label>
                                            <input disabled id="street" type="text" value="{{ $user->street }}">
                                        </div>
                                    </div>
                                </div>
                                <div class="_grid _spacer _spacer--sm _mb-df">
                                    <div class="_cell _cell--12 _m-none">
                                        <div class="form__item _flex _flex-column">
                                            <label class="with-solid-underline" for="build">
                                                {{__('auth.form.build')}}:
                                            </label>
                                            <input disabled id="build" type="text" value="{{ $user->build }}">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="_cell _cell--12 _md:cell--4">
                            <div class="input-indicator">
                                <div class="form__sub-title _mb-df">
                                    3. {{__('auth.Agent')}}
                                </div>
                                <div class="form__item _flex _flex-column _mb-df">
                                    <label class="with-solid-underline" for="name">
                                        {{__('auth.form.name')}}:
                                    </label>
                                    <input disabled id="name" type="text" value="{{ $user->name }}">
                                </div>
                                <div class="form__item _flex _flex-column _mb-df">
                                    <label class="with-solid-underline" for="additional_email">
                                        {{__('auth.form.additional_email')}}:
                                    </label>
                                    <input disabled id="additional_email" type="text" value="{{ $user->additional_email }}">
                                </div>
                                <div class="form__item _flex _flex-column _mb-df">
                                    <label class="with-solid-underline" for="phone">
                                        {{__('auth.form.phone')}}:
                                    </label>

                                    <input disabled id="phone" type="text" placeholder="" value="{{ $user->phone }}">
                                </div>
                                <div class="form__item _flex _flex-column _mb-df">
                                    <label class="with-solid-underline" for="number_cells">
                                        {{__('auth.form.number_cells')}}:
                                    </label>
                                    <input disabled id="number_cells" type="text" value="{{ $user->number_cells }}">
                                </div>
                                <div class="form__item _flex _flex-column _mb-df">
                                    <label class="with-solid-underline" for="website">
                                        {{__('auth.form.website')}}:
                                    </label>
                                    <input disabled id="website" type="text">
                                </div>
                                <div class="form__item _flex _flex-column _mb-df">
                                    <label class="with-solid-underline" for="facebook">
                                        {{__('auth.form.facebook')}}:
                                    </label>
                                    <input disabled id="facebook" type="text" value="{{ $user->facebook }}">
                                </div>
                                <div class="form__item _flex _flex-column _mb-df">
                                    <label class="with-solid-underline" for="instagram">
                                        {{__('auth.form.instagram')}}:
                                    </label>
                                    <input disabled id="instagram" type="text" value="{{ $user->instagram }}">
                                </div>
                            </div>
                        </div>

                        <div class="_cell _cell--12 _md:cell--4">
                            <div class="input-indicator">
                                <div class="_flex _flex-column _mb-df">
                                    <label class="with-solid-underline _flex-row _justify-between">
                                        <span class="form__checkbox-text _mr-df">{{__('auth.verification.sanction')}}</span>
                                        <input class="width-a" disabled
                                               @if($user->verification and $user->verification['sanction']) checked @endif
                                               type="checkbox">
                                    </label>
                                </div>
                                <div class="_flex _flex-column _mb-df">
                                    <label class="with-solid-underline _flex-row _justify-between">
                                    <span
                                        class="form__checkbox-text _mr-df">{{__('auth.verification.founderRu')}}</span>
                                        <input class="width-a" disabled
                                               @if($user->verification and $user->verification['founderRu']) checked @endif
                                               type="checkbox">
                                    </label>
                                </div>
                                <div class="_flex _flex-column">
                                    <label class="with-solid-underline _flex-row _justify-between">
                                    <span
                                        class="form__checkbox-text _mr-df">{{__('auth.verification.criminalCourts')}}</span>
                                        <input class="width-a" disabled
                                               @if($user->verification and $user->verification['criminalCourts']) checked @endif
                                               type="checkbox">
                                    </label>
                                </div>
                                @if($user->photo or ($user->open_info and ($user->text or $user->description_file)))
                                    <div class="form__sub-title _mt-xxl _mb-df">
                                        4. {{__('auth.Company information')}}
                                    </div>
                                    @if($user->photo)
                                        <div class="_grid">
                                            <div class="_cell _cell--6">
                                                <img src="{{$user->getImage()}}" alt="" loading="lazy">
                                            </div>
                                        </div>
                                    @endif
                                    @if($user->open_info)
                                        <div class="_flex _flex-column _mb-df">
                                            @if($user->description_file)
                                                <div class="_grid _mt-df ">
                                                    <div class="_cell _cell--6">
                                                        <a href="{{asset('storage/'.$user->description_file)}}" target="_blank"><i class="fa-solid fa-xl fa-file-arrow-up"></i> file.{{ explode('.', $user->description_file)[1] }}</a>
                                                    </div>
                                                </div>
                                            @endif

                                        </div>
                                        @if($user->text)
                                            <div class="_mt-df">
                                                <label class="without-underline">
                                            <textarea disabled type="text" placeholder="{{__('auth.form.text')}}"
                                                      rows="10">{{ $user->text }}</textarea>
                                                </label>
                                            </div>
                                        @endif
                                    @endif
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
