@if($errors->has($field))
    <div class="_cell _cell--12">
        <div class="_flex _flex-column _justify-center">
            <span class="text text--error text--color-main {{ $class ?? '_mt-xs' }}">{{ $errors->first($field) }}</span>
        </div>
    </div>
@endif
