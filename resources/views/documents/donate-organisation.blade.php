<html>
<head>
    <meta content="text/html;charset=UTF-8" http-equiv="content-type">
    <style>
        @media print {
            body {
                margin: 0 50px 0 35px;
                color: #000;
                background-color: #fff;
                padding: 0;
            }

            footer {
                break-after: page;
            }
        }

        * {
            /*font-family: Helvetica, sans-serif;*/
            font-family: "DejaVu Sans", sans-serif;
        }

        table, th, td {
            font-weight: normal;
            vertical-align: baseline;
        }

        body {
            margin: 0 50px 0 35px;
            color: #000;
            background-color: #fff;
            padding: 0;
        }
    </style>
</head>
<body>

<table style="width:40%;font-size:13px;margin:0 auto 15px">
    <tr>
        <th>
            <b>Анкета</b> <br>
            для отримання гуманітарної допомоги
    </tr>
</table>

<table style="font-size:11px;margin-bottom:20px;line-height:20px;">
    <tr>
        <th style="vertical-align:top;font-weight:bold;width:5px">1.</th>
        <th style="text-align:left;vertical-align:top;width:150px;font-weight:bold;">Назва організації:</th>
        <th style="text-decoration:underline;width:100%;text-align:justify">
            {{$additionalInfo['name']}}
        </th>
    </tr>
    <tr>
        <th style="vertical-align:top;font-weight:bold;width:5px">2.</th>
        <th style="text-align:left;vertical-align:top;width:150px;font-weight:bold;">ЄДРПОУ:</th>
        <th style="text-decoration:underline;width:100%;text-align:justify">
            {{$additionalInfo['id_code']}}
        </th>
    </tr>
    <tr>
        <th style="vertical-align:top;font-weight:bold;width:5px">3.</th>
        <th style="text-align:left;vertical-align:top;width:150px;font-weight:bold;">Адреса:</th>
        <th style="text-decoration:underline;width:100%;text-align:justify">
            {{$additionalInfo['address']}}
        </th>
    </tr>
    <tr>
        <th style="vertical-align:top;font-weight:bold;width:5px">4.</th>
        <th style="text-align:left;vertical-align:top;width:150px;font-weight:bold;">Номер телефону:</th>
        <th style="text-decoration:underline;width:100%;text-align:justify">
            {{$additionalInfo['phone']}}
        </th>
    </tr>
    <tr>
        <th style="vertical-align:top;font-weight:bold;width:5px">5.</th>
        <th style="text-align:left;vertical-align:top;width:150px;font-weight:bold;">Відповідальна особа:</th>
        <th style="text-decoration:underline;width:100%;text-align:justify">
            {{$additionalInfo['responsible_person']}}
        </th>
    </tr>
</table>

<table style="font-size:11px;line-height:20px;">
    <tr>
        <th style="text-align:left;vertical-align:top;font-weight:bold;">Перелік отриманої гуманітарної
            допомоги:
        </th>
    </tr>
</table>

<table style="font-size:11px;line-height:8px;margin-bottom:10px;">
    @foreach($data['products'] as $transferProduct)
        <tr>
            <th style="text-align:left;font-weight:bold;box-sizing:border-box;">
                - {{$transferProduct->product->current()->first()->name}}
                - {{$transferProduct->quantity}} {{$transferProduct->product->current()->first()->type}}
            </th>
            <th></th>
        </tr>
    @endforeach
</table>
<table style="font-size:11px;line-height:13px;margin-bottom:50px;">
    @if(isset($additionalInfo['description_product']) and $additionalInfo['description_product'] != '')
        <tr>
            <th style="text-align:left;font-weight:bold;box-sizing:border-box;margin-bottom:20px;">Додатковий опис <br>
                допомоги:
            </th>
            <th style="text-decoration:underline;text-align:justify">{{$additionalInfo['description_product']}}</th>
        </tr>
    @else
        <tr>
            <th style="text-align:left;font-weight:bold;box-sizing:border-box">Додатковий опис <br> допомоги:</th>
            <th style="text-align:justify">
                _______________________________________________________________________________
                _______________________________________________________________________________
                _______________________________________________________________________________
            </th>
        </tr>
    @endif
</table>

<table style="font-size:11px;margin-bottom:20px;line-height:20px;">
    <tr>
        <th style="text-align:left;vertical-align:top;width:100px;font-weight:bold;">Надавач:</th>
        <th style="text-decoration:underline;width:100%;text-align:justify">
            {{ $data['donorName'] }}
        </th>
    </tr>
</table>

<table style="font-size:13px;margin-bottom:35px">
    <tr>
        <th style="width:100px"></th>
        <th style="width:200px">
            <div
                style="text-align: center; border-top:1px solid black;margin-top: 20px;font-size: 9px;font-weight:bold">
                (дата)
            </div>
        </th>
        <th style="width:70px"></th>
        <th style="width:200px">
            <div
                style="text-align: center; border-top:1px solid black;margin-top: 20px;font-size: 9px;font-weight:bold">
                (підпис)
            </div>
        </th>
        <th style="width:200px"></th>
    </tr>
</table>

<table style="font-size:11px;margin-bottom:20px;line-height:20px;">
    <tr>
        <th style="text-align:left;vertical-align:top;width:100px;font-weight:bold;">Отримувач:</th>
        <th style="text-decoration:underline;width:100%;text-align:justify">
            {{$additionalInfo['name']}}
        </th>
    </tr>
</table>

<table style="font-size:13px;margin-bottom:35px">
    <tr>
        <th style="width:100px"></th>
        <th style="width:200px">
            <div
                style="text-align: center; border-top:1px solid black;margin-top: 20px;font-size: 9px;font-weight:bold">
                (дата)
            </div>
        </th>
        <th style="width:70px"></th>
        <th style="width:200px">
            <div
                style="text-align: center; border-top:1px solid black;margin-top: 20px;font-size: 9px;font-weight:bold">
                (підпис)
            </div>
        </th>
        <th style="width:200px"></th>
    </tr>
</table>

<table>
    <tr>
        <th style="font-size:13px;text-align:left">*Підписанням цієї анкети, я надаю дозвіл на зберігання та обробку
            моїх персональних даних відповідно до
            Закону України “Про захист персональних даних”
        </th>
    </tr>
</table>
</body>
</html>
