<html>
<head>
    <meta content="text/html; charset=UTF-8" http-equiv="content-type">
    <style>
        @media print {
            body {
                margin: 0;
                color: #000;
                background-color: #fff;
            }
            footer {
                break-after: page;}
        }
        * {
            /*font-family: Helvetica, sans-serif;*/
            font-family: "DejaVu Sans", sans-serif;
        }
        table, th, td {
            font-weight:normal;
        }
    </style>
</head>
<body>

<table style="width:100%;font-size:18px;margin-bottom:5px">
    <tr>
        <th style="display: flex; flex-direction: column;">
            <img style="padding: 15px; border: 1px solid #D6D6D6;" src="data:image/svg+xml;base64,'.{{base64_encode($qr)}}.'"  width="150" height="150" />
            <img style="margin-top: 15px;" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAMwAAAAmCAYAAACI/XQWAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAlQSURBVHgB7V07bBNJGP6dmEABulwHDXEkqCFISAkBznSBBpAA6SpsqIGkBApCAS2vnjgtFJAGrmN5BFKBrwUk9qCA7nICpEsI5P5v2fGNx7Ozs/F649jzSRvvjmd2nv/8z3FypKBYLPYvLi6ey+VyR/ixwFc/OXg/fvyYWb9+fcXzvHly6Frk5IeRkZFzPT09k+SIBPB403icz+evOyJxEKgRzOjo6DX+GKfuRkAkS0tLlbm5OZ8cHBTk8WfPnj2XqEuJZXl52WeuOs233rNnzzxycDAgNzw8XOjt7X1HGWD79u20b9++2vOrV6+Cqxls2bKFDh48WHv++PEjPXz4MK7YPHOSG+SIJFXwxluin3pvANb7Oo5T53l3zYyzbNu2jU6dOlV7vn37dtMEs3nz5rp34n0GggGhHHVE0hrw2J7kj6J4Zv3P4w+fOgh57uRv1D3o50msksOqYvfu3TtZqqkZlpgLebRGAB1mJ3URvn79WuAPRzSriHXr1t3jjbogJeVojSBPGQLi0pUrV2rPb968oazBvhRnMm8RmAgus94yLZ6/f//uU4chU4L59OmTjULusEbRDbphDzk4OFjDEYyDQwJYiWSbNm0KTMLwocDvsXHjxiD9y5cv9PLlS7DiwP8Rh5X4YYaGhoIyKAsTMsQ66D537961qrNZsG9hUtyzk3P+xYsX13Ef+q/OcVqBZfd+fMfJMyzDezrfA+cvcn6YXQthks9lHj9//rxCCRG+67CoW7TNVL+MMF5wPKJfde9O8t44P8zIyMg43inaLJWbFPc8HpMU3/Yj/I4dcv8ZVX7+s6+v735cKBPmjt0pJfHM/a0+ffr0Pkn9p/+NYVXux2Men+D73Ojo6LLp5SdOnAj8HIJIovDgwQOampoyLmI4GC9evFh7hh8GVxRQr+xjUXHz5s2AeG7dulVLAwGeOXMmsgwP8IEksrYyPv7s7OwgIiP4PZMRRUAIR3niA0scJvjbt2/3SPJPqPlZOT5g4+DDZPJETykWpgZw/ZNc/2XDe1RnddCvmPAoRERMiIWlA5d/RFI/1bHmcXsX13ZuR6TFzDLW0ecFfkNsADqERPFISqqw5W4ibp44z5BRJLtw4QKdPXs2lliAQ4cOBQvXJq8N4ogFQNuOHz9OWSKGWADseo84X7BD8STULSJdfkweFjHF1It8cQsOQPt48SbyCFvEEhZ4Id7bu3dvkVYB6D8TC4ggzsoJ7nEtDPeyRtw88SZUBeeKJJiohY8dHZYu7OQQj2RAZMIibhYQ+3TEAhFQiHG4B/bv308ZohBDLAIQO8Sk2fi5QDRTUV+CmCzrlbFTFnXi6ifLWEJeOFPgmpQhIOol7T/yQwS0zI6jLKZ5AteawE0kwWBBXr16NfCbgEggOo2NjVG5XA7SIPYcO3YsEItkgNM0y2VQhwrUg/pRLy7cm8S5VgJxaCxGDUJ8wAXRgxqdoUUxyfBPMDv/VeTnRVdG0KeaP2ohMjGpu+W8+k60QX0np11Ksrg17RziSxXBCtAhaAVgMXEwfK8vp4v6DOKY2n+IvWXRVnzqxpQ5jW3/kWc+ap44vSxE5lgrGbgJFjAWp9jVZdy5cye4ZIBomgEUfBmoW61DpDcbi5YUGFSWy8dlnQNyOg/0AQ0RABP8/aSsiELR5x0LRFannOoWYiiqleQ0rqfhnWgDK7xDahsWFhZKZN8vtZ1Vvo7yrafkzSycCvqGKoZC58MYirbiM2JM+5eWlqwiWZi4yrp5wpjKelgqZmVWBOue1QWfFGp5HbEIwNiQJXCgTJeOgebFekNN551Kmx8Ep8nfMLlMiGqaF2VZ07WBF1uRLIDFEvUdiIli2tkq8EIuKkmREdDhmE7LaUxENtywGmXMUC1uqRBMmiEu0INkvH37VsvZBFQ9qsXwTSZLXlieklQlA2DOVMr/oubh3bRuccIUTQaoIhTMrxQP3/SlJmA1Mx0GpmP5GUfFTfl5TD0lqUDxdfxFlrD2w2AhwxeTNT5//kxrBTyZ8zxhcpLRH8CWGTW/DXzTl9hl2eJFaQKbRNrvtAVzmAH5mccr0ZjqNiEVnOdvsoSRYEAo0F/SUOQdHDoBkQQD0y78KqqI5ODQzYgkGHAWlVjgxY/SGRDC4uDQ6dASDMQv2TQMpfv8+fNGEy7iyRwc0gZi2VjHoHaB1kqmmnWfPHmSub9DAKJhNyMMfpRhNOmKkBwJRiW53cH9/0d+ZsNKwZSfzfD9pvLNwsqsXK1WKSvowm1MBodO17HYpOvLzxZmYtUMbW0ybVOoi++wKTMTlPq9TynCimDizMlpW9BUvw4ipqNw+vRp6mSEfgWZSxTZxHtSlzeMClDDSCKji9sFzBUHor7T+KqOaLhoAF1UBJulU+2/lmDUBQt9xiQamRb0SqBGDiAQU60DJm9EU3c6hwkdpeouW0FgJ359BQ+IlwpD/xuimXGOhdoM3EZVTCyh/bjUvGFYiqeURzR4SUR44xOBlkxcqt7gp31sWqv0i6hgYfkCB8GBLSxkmZhAROA+zYbCqEAoDAhE5lyIggbhoH4QSTfpNuztL4eLoSafI7Czr68PYfxw1pHOAYqQlnb8IT1EK3DbalwCfZHa36Dhox9cpiglIRp8CmVMDlUuE3kmaKUAh9EqhYhIVkNScPJRnFPBhQNhaRMLgHp1kcggIBCxTCxZx5KtBsIYqYmExaZN8WGriaRiEriEJp7NCORfyWnWOIBgfN0XUL5LpVJsrJY4BpA2wGXiwvcR8p/0V2jW6g/5YfLDcHs/Jis2wInZ2dkStSlWQgAg/ohjESqC/rdqs8hzA2Zk9igDxIIzL9jVxc4OcQhEguv169fBgsU9v6eunA5Jf5cMBAMOAs62a9eumoiGciAo1IM0+Z2mQE0KVIJk/7oCkyTu4+KYNmzYML+4uJgkf1XOz/qGb8ofHnsexKlHzouwdyjLBfp5PgTm06rNmfak7QRsxiHJ75JhQbPuUYESr57xj0LIMSqi/3y/Iywb9B+cCxuiRf8TjbuMXHjmHOe7M4tAXS1g0lvBph26B72+7/+7devWBb4fo85GxfTDEA4ONghMEx8+fJgbGBj4lW+HqTNRZQ/w79gcyMGhCdRsee/fv/+DiQYmvSJ1EHD+noml7P7tnkMaaLB5hz9yNh6e287sKGqaCC0pM1AC3f+CcUgT/wEjPwEpQdIzmQAAAABJRU5ErkJggg==" width="120"/>
        </th>
        <th style="text-align: left" valign="top">
            <img style="width: 100%;" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAABAAAAACaCAIAAAC4za3iAAAACXBIWXMAAAsTAAALEwEAmpwYAAAE7mlUWHRYTUw6Y29tLmFkb2JlLnhtcAAAAAAAPD94cGFja2V0IGJlZ2luPSLvu78iIGlkPSJXNU0wTXBDZWhpSHpyZVN6TlRjemtjOWQiPz4gPHg6eG1wbWV0YSB4bWxuczp4PSJhZG9iZTpuczptZXRhLyIgeDp4bXB0az0iQWRvYmUgWE1QIENvcmUgNi4wLWMwMDYgNzkuZGFiYWNiYiwgMjAyMS8wNC8xNC0wMDozOTo0NCAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczpkYz0iaHR0cDovL3B1cmwub3JnL2RjL2VsZW1lbnRzLzEuMS8iIHhtbG5zOnBob3Rvc2hvcD0iaHR0cDovL25zLmFkb2JlLmNvbS9waG90b3Nob3AvMS4wLyIgeG1sbnM6eG1wTU09Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9tbS8iIHhtbG5zOnN0RXZ0PSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvc1R5cGUvUmVzb3VyY2VFdmVudCMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIDIyLjQgKFdpbmRvd3MpIiB4bXA6Q3JlYXRlRGF0ZT0iMjAyMi0wNi0yMFQxMjoxMzowMiswMzowMCIgeG1wOk1vZGlmeURhdGU9IjIwMjItMDYtMjBUMTI6MTU6MTIrMDM6MDAiIHhtcDpNZXRhZGF0YURhdGU9IjIwMjItMDYtMjBUMTI6MTU6MTIrMDM6MDAiIGRjOmZvcm1hdD0iaW1hZ2UvcG5nIiBwaG90b3Nob3A6Q29sb3JNb2RlPSIzIiB4bXBNTTpJbnN0YW5jZUlEPSJ4bXAuaWlkOmY4Njc2NDZlLTRjMmEtYTY0MC04YzAxLWY3YzcyYmE1MWIyOCIgeG1wTU06RG9jdW1lbnRJRD0ieG1wLmRpZDpmODY3NjQ2ZS00YzJhLWE2NDAtOGMwMS1mN2M3MmJhNTFiMjgiIHhtcE1NOk9yaWdpbmFsRG9jdW1lbnRJRD0ieG1wLmRpZDpmODY3NjQ2ZS00YzJhLWE2NDAtOGMwMS1mN2M3MmJhNTFiMjgiPiA8eG1wTU06SGlzdG9yeT4gPHJkZjpTZXE+IDxyZGY6bGkgc3RFdnQ6YWN0aW9uPSJjcmVhdGVkIiBzdEV2dDppbnN0YW5jZUlEPSJ4bXAuaWlkOmY4Njc2NDZlLTRjMmEtYTY0MC04YzAxLWY3YzcyYmE1MWIyOCIgc3RFdnQ6d2hlbj0iMjAyMi0wNi0yMFQxMjoxMzowMiswMzowMCIgc3RFdnQ6c29mdHdhcmVBZ2VudD0iQWRvYmUgUGhvdG9zaG9wIDIyLjQgKFdpbmRvd3MpIi8+IDwvcmRmOlNlcT4gPC94bXBNTTpIaXN0b3J5PiA8L3JkZjpEZXNjcmlwdGlvbj4gPC9yZGY6UkRGPiA8L3g6eG1wbWV0YT4gPD94cGFja2V0IGVuZD0iciI/PlGu3DEAAD3ASURBVHic7Z1PbOLYlv9vpSA4UFVxOnk1zkiUSDRdIao8yUgzetCbBqkWYfNUWWaZZZa1fKtWr96ylllmmWWi2ZBFSfAW3TAzTwKpGYWonxKrkF6sTNNxeoIx+VP8FudXd/xsMIbwn+9n0aKMfX1sOvY5537PuU8ajQYDAAAAAAAATAczwzYAAAAAAAAAMDgQAAAAAAAAADBFIAAAAAAAAABgikAAAAAAAAAAwBSBAAAAAAAAAIApAgEAAAAAAAAAUwQCAAAAAAAAAKYIBAAAAAAAAABMEQgAAAAAAAAAmCIQAAAAAAAAADBFIAAAAAAAAABgikAAAAAAAAAAwBSBAAAAAAAAAIApAgEAAAAAAAAAUwQCAAAAAAAAAKYIBAAAAAAAAABMEQgAAAAAAAAAmCIQAAAAAAAAADBFIAAAAAAAAABgikAAAAAAAAAAwBThGbYBAAAAAACDoFKplEolv9+/trbm9/uHbQ4AQ+NJo9EYtg0AAAAAAH1E1/VisaiqKv3T7/evrq6urq4O1yoAhgUCAAAAAABMLHd3d2dnZ2dnZ3d3d0+ePBEE4fPnz/V6nTFGUwHBYHDYNgIwaBAAAAAAAGAyUVW1WCzqus4Ym52dDQQCMzMzjLF6va7r+ufPnxljwWAQiiAwbSAAAAAAAMCkQXL/SqXCGPN4PH6/3+v1WvYxDKNWq1EYQIoghAFgSkAAAMCok8/ny+XysK1wYm1tbW1tbdhW/B+KolxcXKiqqmkaY0wQBEmSVlZWQqGQ84GlUuni4sK+fXl5WRRFSZL6YCwAoMfc3d2dnp6enZ0xxp48eTI3Nzc3N9dq58+fP+u6DkUQmDYQAAAw6iAAcE+pVEqn07zOz4IoivF4PBKJtDr88PCwUCi0+lYURVmWE4nE4+0EAPQDu9x/bm7uyZMnbQ/8/Pnz//7v/97f3zPG/H5/JBJZXFzsv70ADA20AQVgPAgEAoIgDNsKK7qu12q1YVvBGGOGYaRSKbP7LgiCKIqCIKiqahgGY0zTtKOjI0VRksmk882UJInvoGkazSRompbJZEql0s7Ozgj+FgBMOZVKJZ/Pk9zf4/E8f/6c5P5umJmZmZ+fp8IAXdd/+OEHFAaAyQYBAABgEtjf36fEvyAIsVhMlmVRFPm3mqblcrlsNssYKxQKqqo6O/HJZNKiFyoUCul0WtM0VVXT6XQymezThQAAOkXX9Xw+T3L/mZmZZ8+e2eX+bvD5fD6fj/Ia5XK5XC6PzvQmAL0FAQAAYOxJpVLk/YuiuLOzY3b9CVEUNzc3o9Ho/v5+d068LMuhUGhvb88wjEKhkEgkMAkAwNDpSO7vEp/Pd39/f3d3xxgrl8uLi4tLS0s9sLX/KIpyfn7e0SGRSMT+wATTAAIAAMB4Q9l9xpggCE29fw6FB+TE53K59fX1tmXBlsPD4XChUDAMQ9M01AQDMFzOzs5OT0/JUxcEwe/3u5H7O9BoNKgvUKPR8Hq91Beou8mEoXB+fp7JZDo6ZGVlBQHAdIIAAAAw3qTTafqQSCTavslEUYxGo/SOTKfTOzs7HZ2Lj99FAKAoCvvSksjyFUUUhmGIomi/BE3T6FgywFyf4HyupjQ9BQDjRaVSKRaL19fXjDGPx/Ps2bOnT58+ckysDACmCgQAAIAxhgQ59FmWZTeHxGKxXC5nGIaiKIZhdKTkoWJixlin+h9N0/b398nIra0ty7d7e3uaptEEhXm7qqqpVMrizQuCQM2IWtnAz9WUeDyORkZgfOmV3N/M3d1drVajmYSlpaXXr1+Pi+bHQiKRsP918+Zm7969c+iBBqYNBAAAgDGGd/wMhUIunXJBEEKhUKlUYoydnJx09Eako2iEjm1tAdUWM8bi8bg5N5/P54+PjynkkCRJkiTtC7lcjpoRNc3l8yjF8i2dBYAxpesWnw40Go1qtUqLAHi93jdv3rx69apH9gIw0iAAAACMMbzirSOPXJIkcuU78om5px6Lxdwf5Qy1FmWMybJsDkU0TSPvXxTFra0t89VlMhmy5PDwsKmEiQcAlgjhu+++65XZAAyYcrlcLBYpSe/z+QKBwHDl/nd3d8VikTEGpRAYUxAAAADGmFbZbmcWFhboQ6sA4Orqyjygoij5fJ6kOJIkxePxzi1tDml1RFG0TNyn02m6NHuaPx6P12q1XC6nKIqiKPbIh6/MAK0/mAAqlUqpVCLNj8fj8fv9PdH83NzckNx/aWlJlmX3Trx5IoLMo4KBR5o0dBRF0TTt6upKEISVlRU0OZh4EAAAMB7c3d3R62qkoIUzhwgPAHrL0dGRfSOtMBCNRu1fNQ0kBEFwViW1Ev/wwoZQKNTUiacyBsZYPp+3BwA0JrqUgnFH1/XT01NaB31mZsbv9/t8vkeOeX9/r+s6+e7z8/Nv3rzpSO5vWWvs6dOn3Mi1tbVgMPhI84ZCNpvNZDKWZykVGiGJMMEgAABgPLi9vR22CaMIfz91FAnwHLl7Lzkajbaqu9U07cOHD/btziV3qqqS+CcWi1l2Mxc2ND2WOvmYuwNZ7GGMIYEHxpc+yf1rtRr97Xu93rW1tdXVVfeHX19fF4tFmoh4+vRpIBCgiYjZ2dlqtUqlyWdnZ//2b/82RoogUhLyBmW0dDoVGtGCibu7u8O2EfQLBAAAjDqSJI34G2VxcXFYp24r5mkK37lVfmtnZ4ec71wul0qlGGOlUikWiz0+ra4oyuHhoWEYVIQQCoXsgqK25tFX9J62f8VXRH6kqQAMBVVVi8UiZdlnZ2cDgcDMzMwjxzQMQ9f1RqPBGFtdXV1bW+tI7u+w1tjs7Ozs7Cy1EL2+vv748eMYtRAVBEFVVT63yR8aqVQql8upqprP59E4aFJBAADAqLO8vLy8vDxsK0YUnuculUruV/blifO2NzYajRqG4Vx0K4ri+/fv7dtbTRfwvqV0rEMr0u7WNKWooId9igAYDH2S+1er1YeHB8bY0tLSmzdv5ufn3R/ucq0xn8/n9XqpqrhcLquqSmHGI43vN4Ig7O7u2sWKiUSCVjwslUoIACYVBAAAgDGGVsWihbSaVsTa0TSNcuSiKLrZPx6Pn5+fU8VtNptt2gLIvVI2HA4nk0l6s6bT6UKhoGlaq/XIuFTJPXxaAEEjGCOcs+zd8fDwUK1WyXf3+/2yLD9G7v/8+XPniQiqUhAEQdf1er0+LoUBTZ9dJAdSVZXLEcHk8dhpNQAAGC68KpcvCewM3819Zmtra4syZMfHxw6L7LqB3qzUSogsp7jCvA9/JTu8fekru9Cfm4caADAunJ6efvz4kbz/ubm5hYWFR3r/JPe/vr6+u7sjuf+3337r3vvXdf2HH3744YcfdF2fmZl58eLF/Py8SxkSrU324sWLmZkZKgz4y1/+QlHEeAEN4cSDAAAAMN7EYjHymBVFaRsDUNKdMSaKosuVg2lnvnwvKfi7NfYf4FXFmUzGrOanaQ3WOgBQVZUvEGb5Kp/Ps06WRQNgiFQqlY8fP5LGxuPxLCwstNLYuKder19dXZHiPxgMvn371r3inyYiPn78WKlUaCJiYWGhCxmS1+tdWFh49uzZzMwMFQbwyYRRRlXVQqGQzWZ5gzIwwUACBAAYbwRB2Nraoob65Ek37V5HUn7qnskY29ra6qjDXTgcjkajuVyOigG2t7d7ZfnBwYFhGOYCA0EQotFoJpOh9QfsMxU8zrGsHqCqKs0AuI9tABgKlB0nuT9lzXsi96/VaqT5WVpaev36dUeaH5dyf/dYCgNIETSChQE0CakoSp+6KoPRBAEAAGDsCYVCW1tbh4eHjLFCoVAoFGRZliRpeXnZMIyrqytVVUulEn+9WdbWdUkikSiVSpqmlUqlVsUAnRIOh0OhEBUYlEqlcDhM22OxGJUHHB0dGYYRiUT4nEA6naYOQnzqg6D+QowxURRRtwdGmV9++eXHH3+kz16v98WLF48c8OHhoVar1et1xpjf73/9+vWrV6/cH96P4mOCL19wfX3daDROT09VVf3mm296Nf4jMQwjlUqZJ0WXl5dJpnh4eIgCgMkGAQAAfeeXX34xN34BFt6+ffv4QWRZDoVC+/v7NHPtcMO3t7e5n90RgiDs7Ozs7e0ZhpHJZHq1WObW1haNmUqluHSHzkWXc3x8fHx8TL4+n5ePRCKbm5v0WVEUmgAhNE377rvvWp0uk8nQ+gPOyxQA0D+Wlpbevn37448/0ppcV1dXL168ePr0aRdDNRoNSrE3Gg2v17u6urq6uurevdZ1vVgskqc7MzMTCARmZ2e7MMOBx3Qg7TdcErm1tWWZNoSGcOJBAADAIBh99ecEQO04C4VCPp/nKnmCN7VgjKVSKUmSulvhUhTFRCKRSqUMwzg4OKAOeo83O5FIkOg2nU7zZqaiKO7s7KTTaUVRzC3/Q6GQLMvw3cFY4/f73759Wy6XT09PdV3XNM3n883NzXUUBtze3larVVoiXZKkjY0N9933+7HWmP0UNzc3ZN7S0pIsy6O2OABJImVZhmhwCkEAAMCA8Hq9z549G7YVo8XV1VXPx+QvM+4007q5jLFMJkN+9v7+/s7Ojj0G2Nra4sW+rYhGo7zvkHtEUfz+++87HZMXH9O18KU6W43z/v37toFNq3WLARg8wWBQkiQS39fr9dvbW5cNQB8p9+eBB2PM5/P5/f7HrzVm5pEdSAeDw4KDrRYZBJMEAgAABkdv3zHAGe73c+LxeK1Wo0Le/f39nuTvB4P9WgCYDKhNZzAYpMb5uq4bhkG6+ab7U4tPWiKDjl1dXXV/uv7J/bl5FknSCFb9EpRNMAyjUChEIhH+hKFqIgQAEw8CAADAFJFMJuv1ej6f5/MA4xIDADDB+P3+SCQSDAYLhYKu6zc3N/V6PRAIWBRB5PqTnp5cf/fuez/WGrMwynL/ppCgkR6G1H2YBIekM6RiITCpIAAAAPQYSoA57LCwsDAwY+y8e/fu6upKURRVVQ8ODlqtwgsAGDBUHNy0MOAxevrByP07lSTRImU9tKELotGoYRjZbNYsmEwmk9FoVFVVBACTDQIAAECP+fz5M72nR5adnZ1SqXRxccEYU1V1YhbN7Wg2o+fpTwDaks/n19bWnH33YDC4uLhILnu9Xiev/eHhgXWlp69UKnwRLo/H8/z5857L/bvoQEpW9aQBWlucS5vi8Xg8Hqf1Q8xSQ0mSHMqWwASAAAAA0BeoH59l48ePH4dijJ1wONxdM9ARRJIk0jK5CQCowaggCBMT9oAxolwuq6raVhnv9/s3NjZWV1epMIB1Jffvx1pjZrrrQHp9fV0sFsmq0aGLdVHAuIMAAADQF7xe76j1vJtUBEFw//7uaGcAeg5p8WlZ3GAw6LAnFQYsLS3puj5qcv8uOpCarQJg6CAAAAAAAMDgmJmZofT8p0+fIpFIW0VQR4NTU1GS4wuC4Pf7hy73t1tlXqUEgKGAAAAAAAAAg2NhYaFer+u6XqlUPn78GAwG2xYGuKFSqRSLxevra8aYx+N59uxZd6sLt6K7DqRNixAQAIChgwAAAAAAAAPF5/N5vd5arWYYRrlcrlQqTauGXNJvuT/rqgPpAKwCoGsQAAAAAABg0MzMzAQCgbm5OV3XdV0vFotnZ2dtCwMsDKbFZ6cdSMmq09NTxhhZhYIoMGogAAAA9IXr62tq3wHcQz25VVXd3t4eti0ADAKeGqcwIJ/PX1xcuKmpZYzpuv6Xv/yFhPUzMzPz8/M9b/FZrVZpfPcdSPtdhABAT0AAACaWarVaqVT+6Z/+qdWS8qCvqKqqquqwregAwzA0TaP/kjLBMIxkMjlIG0qlEi2+oygKevWA6cHn8/l8PioMoEeHm8IAv9//7bffUk+hz58/X19f+/3+njzwqcUnCfepxadz31KiUqmUSiXS/Hg8Hr/fD80PGFkQAICJ5eTkpF6vX15e/v73v0cMMEg8Hs8Y3XBKuhcKBVoI04wgCAMOAGRZTqVShmFcXFwgAAB95f7+/uHhYaT+VKkwQNf1er1O84eRSMT5EGoVura29uOPP+q6fnNzo+v6ixcvHlP+S64/yf3J9Xcj9y8Wi5TyIGnT7Oxs1wYAMAAQAIAJp16v//TTT7Isezz4v31AzM7OjsvLT1GUg4ODph05zItiDpJwOFwoFMZr8gSMHfV6vVAo3N/fv3r1qtM+m32FFEEej6darbo/yu/3v337tlwun56e6rquaZrP55ubm+s0DOiixecAihAA6AdwicDEsr6+XiwW7+/v6/V6sViUZXnYFoHRQlXV/f19+hwKhcLh8PLyMjn9Q3H9zQzdADDZXF9f39/fM8Y+ffrk8/levnw5bIt6QDAYlCSJJPj1ev329tb9EmDdyf1VVS0Wi6QUmp2dDQQCvS1CAKB/IAAAE0sgEFhZWfn5558ZY9Vq9eeff/7666+HbRQYFTRNOzg4YIyJori1tTU6ehtSIi0vLw/bEDDJvHz58u9//ztl2ekhORkxALXnDwaDVBig67phGM6FAST3pxafJPd30+ITcv+RRdO0w8NDwzC2trYkSRq2OaMLQlUwybx8+ZLPbl9eXqIpDeCUSiVN0wRB2NnZGSnvX1EUURTD4fCwbQETzsbGRiAQoM/n5+cdSW6GC7ndDlBhwDfffOP3+z9//nxzc/Pbb789PDzY96zX61dXV6T4DwaDb9++bav4v7u7KxaLP/zwQ6VSefLkid/vn5+fh/c/OhweHiqKYp7gBU3BDACYcF69enV/f39xccEY+/Tpk8fjQW51gjG3HhJF0cGzz2azjDFZlrsT2xiGoaoqZesFQZAkqdU4mqZZyosFQRBFURCEVlbF4/FWJ6UmReaNTWsVFEXhu0mS1DQNxg2TJMluDF0gY6zpPeTj07W0SrPxQdoabMf9Twm6w+PxrK+v//TTT/V6/f7+nnSSI1UT3IpSqcQYi0Qizj2ClpaWHAoDIPefeJo+YwEHAQCYfFZXVx8eHi4vLxljZ2dns7Ozi4uLwzYK9JhCoZBOp+3OcTwetzcS4b57LBZjjBmGkc/nyd0UBGFlZcU5AZ9Op3O5nKV0WJKk7e1tu2ubz+eps6cFURRlWU4kEuaNKysrCwsLdoMVRUmn04qi2MeJx+PmQbLZbCaTsdgmimIymbRcFDfs/fv3ljdlqVQifVQsFrN43q3Gb3qfS6XS4eGh3WZy6BOJRNNIoKNTgMfg8/l+//vf8xjgp59+GpeeaZVK5ePHj25ahQaDwcXFRXLc6/X63d2dIAgPDw/1ep0x5vV637x58+rVKzdnzOfzJPf3eDzPnz+H3H80SSaTXAI0bFtGGgQAYCpYWVmpVqs0x/23v/1NEAQ+9w0mgHQ6zZ1sSniTlkbTtKOjI03TLH721dUV+5K9tnvzuVxOFMXt7W17YtswjP39fR4qkHNsGAbNOH/48GFzc5OCCjvhcJj72WRbJpNRFGVnZ8e8j/1A7o5bphoKhYJlz0KhcHx8zG0TBIHfh4ODg+3tbTfKIk3TUqkU++Jzm786PDykk5IlgiDQtbe6z02vnUwqFAqKouzu7lpiD/5T8rkF558SPBKKAagjEPVMG5cYgDFWLpcrlQqp9h128/v9Gxsbq6urvDCAfenu70buf319XSwWSXf09OnTQCAAwc8oI0nS7u7usK0YAxAAgKnAMtl9cnIyRi854AwljBljkiTt7OyYHc1UKkVLawmCYPbLyYOXJCmVSuVyOWoBNDc3V6vVVFWlNQH29vbs5QGpVIqOjcVi8XjcfK79/X1N046Pj5eXl5vqVZLJpDnhnclkKKnfds0v7o7v7OyYR7AHAOFwOBQKybJszpRz2w4PD//0pz85nIjgEynmm0nb6YyRSGRzc9N+7ZlMZmVlxc2153K5VCqladrJyYnZVP5ThkKh7e1t8ykODg5UVbX/lODx+Hy+jY0N3jPt5ORkY2Nj9PsmP3v27O7ujhrwn52dUe2vw/5UGLC0tHR6evrixQs3iw3f3d2dnp6enZ0xxp48eeK+pxAAo8+o/4WDaYCmYu/v76ktHf2TMfbw8EACTfpM39oPdMDj8Zj7QD979owOweIAk0Qul2Nf/GOzw0rtffb29sg3jUQiZoeSMUbOdyKRsOS5Y7HY3t4eY+zw8NCcotY0jTxgWZY3NzfNh9DZP3z4wBhLp9PmpH4r4vF4Op1mjF1dXTkEAFxt76ZcgWqaLRtFUUwkEjQnrmma8yCFQoGu0aLPoXtIo717984y/s7Ozt7enmEYqVTKTe4tGo1ms1lLdYRhGPynNHv/5lPYf0rQEyw904rF4ujHALRiAC0cput6Pp//5Zdf3CiCXK57QO1E6R0kCILf7x8Xub+qqoZh8KeKYRilUunq6koQhFbpCTO0P32mWTiHPzeaoHO5M3NXPUUDuikWoiulcSyncB7BYrb5njStXGoFP5BfUavLJ1P5/aF/ujkFvzqqAXM4C1luuRsOjPSfNxhTyMk2DINWmiQ/njaSH0//Ezf16Qdp5N/+9jf0Whl3Tk5O6MlrzsdzBEGIRqPHx8eGYViSzYQkSfaKW9qYyWToJcH/JyF/nX2pHLBAbxEKKuid5Gw5930XFhbaXOSX8d3s1pRarcZcvFA1TaNrtIt/Tk5O6EPTpZGpniGXy6mq2nZCg6CHgLkin/oysdY/pSzLVBugquoAaoLb5hc6gic4Rhafz7e4uEhCl2q1en5+PhZ9k30+n8/nq9fruq6Xy+VyueymMMCZSqVSLBavr68ZYx6P59mzZ49ZV3jwHBwcaJpGckS7xLFVtRL70kDTXmtE1UqWQ1RVTaVS9p2j0WgikWjaVyCbzdqrpyyD04wiY0wQhN3dXYdHFpcLhkIhc+KD9/959+6dy3oqc4lRq8qlpnz//ff0gWZBLZbYTeUFV01vXVNkWaZiBufrYoxRiqSVDXYQAIDOIJ/eMAzu5fOcumEYw/XpO2WMTAWt4M/QVs2d1tfXSRbfNKkTjUabHhWJROh5fXJywgMALv1vlV8Jh8NkT9Ngw4xhGORqh0IhZ1+WS+0LhUIXJbCUzKNraaucMYt/LF/xjGAra0OhEOXvLy4u2nrn6XTaMAxLt1Nq1cUcf0r+o/Q7AKjX63/961/7eooR5/Ly0ufzuamOHQV8Ph9NBdTrdSoMaKsIagrNJFAUxGcY+mDvICBvvlAo8GolyjqTH2kvv6Ht5J1TOoNn66lixyJBpDCDfSm74jtTIsDyDLFXT/EKJRr8/fv3FvsNwzg8PGzly9KJOr0n+Xz+6OiIPlvMpu00u2iJOnjqvbssDJ8+NWNPx7SaQ3CpOrP3wGgLAgDQBPLm6/U6+fokDO2ff09afI/HQzPOXJpvFvB4vV57DqatiJ/CFf7PT58+8ayez+cbi/wWcIa79a2ccovsx0IrX5PadFKFK99IIzi8A8xJLPu3qVSKG0P+dCKRaBWBmO1PJBKULjo8PIxEIm5eQlT8wL68ukgN1Wq+iwyjl7EgCBa9PuE89cxMd7JpoGW/dnsHpLY/pfPtBT3nt99+G64BlUqFMvpudiZ/3e/3V6tV8uNPT083NjZcyiEmr8UnafmSySR/yPCyKE3Tstms+Q+Qymxo6nJ7e9scYPOKnYODA7PALxaLXV1dWZL9vLSpVCqZHzgO1VPpdLrVLKhDiRQlETq6IdzLty/+mMvlTk5OVlZWGGPhcNjyqOT5e0sI5PKkTZcjsHco+vDhg6ZpVMnW0SkYY4VCoWmvOWcQAEwvVA5brVZ5Rp/7+o8cmRx3QRDoA/2XPHj6J/3x0+deXEp77u/vz8/P+aUFAoH19XUUAU8D/E1jflvwjQ5CHQoAzFu4E9zqEOdUDU+ic87Pz8PhcFsHJRqNCoKQz+e5QN8l3FHWNO38/Jyybs6GCYJAeiELba/dGfu1k0kdzWk0/Sn7R2+fDzzBMcpUq1We4hluisTr9c7MzHA/3n06f2Zm5vnz56QI0nX9P//zP90ogsrlcrFYJLm/z+cLBAJj7foThmHs7u6aHy/k+H748IHqbWKxGP+b4vnjzc1Ni8MdjUavrq4or5/P5/nfbNPkRTwez2azhmFcXFxwN5pXT4XDYXv1VKtmnaSotBRiEdSqgQ53nw7gGk67Hx+NRtvmYrqDbmxHdnYKl27a31nOjPrzCDwe8unJ0b+5uaH/du3lc5+e2mj6fD76yzR/6KHxPYHWuOHrXL58+XJlZWX0X8bADWansFVdlH1jdzO59BDv+jlufuuQMiedTu/t7bUSdJqRZTkUCpE8VxAEerM6BAPJZJLE+nQianZkn5c3G0aCAWpkJAiCxSS6tw5vF/5V01/Bcu2KotCchmEYXJjk/qccQAWwz+f713/9136fZaQol8ukemdfeoMO8WH+9OnThYUF7sfn8/lPnz6Fw2GXS7hQYQBXBJXL5bW1tbW1NfueZs2Px+Px+/3jq/mxEAqF7MkFqqUhIb65loZCdEmSmj6LEokE6W3aChE1TbOrZWh9Q+ZChWhGlmUS51gmKxhj1BZZlmXm+Bg0QypKxlgoFHpMPVVHZLPZQqFAWscuBEsuIS1WIpHI5/MIAKaXer1OLj4lcqrVKqX5Ox3H5/ORi0/Z+qdPn5J/P8icfa+ghj884AkGg+OiagVu4I/yVoWhTYUlZr2KQ5sI9o+vMfKSHZ6wzip2i+hTkqSrqyvq3O8mEU4T6+ZmoG7efPS+F0Vxf3+/1Xy6+IXd3V3KDh4fH6+vr5v9bEmSSEDcyjunpRVYi+DKfu21Wi2TyZhb+pgVPk1nRcwLA7e9cNAR5XL506dP9Hno3j/HXOBbqVR++OGHxcVF950b/H6/IAgUBtA0gn2fSqVSqVRmZmb8fv8oXPIA4E8nXq7DO9K0Kq2hwifz4txNUVWVymdFUTQ/0ywLp7hkYWEhGo1mMplcLmeWPlKbZsZYIpHgSf22nJ+f04eBdf6gZApjbGdnJ5/P9+ks6XSa3mLxeLzTs4yZMwc4PK9PXj75/R35+h6Ph9x67tyTxz9JD8FqtXpycgLvf4JZX1+nzApJXOw78Gfi+vo630hiGMMwSIRjP4o3vTF/S1PSlMBuei4udHEpO15ZWSkUCm5yNrwHf1OBflv4Ic4tR3m9AdUomxv+8CvK5/NN03j82s332QFzYzsKAPhPeXJy0vQGNv0pweM5Pz//+9//Tp8DgcCoNQD1+Xyzs7O1Wq1er1MY4D5JT4UBbWe8vV7vJL34nDHPxdEH5+id4CkASyths8ad/HLqAmQ+tm31VCtisRi5+6lUant7m5nkLrFYrKMB+cxtq7qv3kJFz8zWT7m3KIqSyWQoK9TF4SP0Rw4cIBf/9vb25uam07y+xdGfnZ2lpmYj9YjvB5eXl+fn53SjPB7PysrKy5cvh21US6rV6u3t7bCt6D0ue1x2Dc3nUkcIe4Esb/Zs6fTMGKPcUqFQaPoiocwN+0dfMxaLUaFV007/pVKJzuWmYT9BSam2ghZaAIsxlkgknNNXFJzY9+GlzG0bSkSj0ZOTE0VRcrnc+vo6v2nUFomkw/Y2/Fzj6356nZvEh+r6pwSP4eeff768vKTPi4uL//Iv/zKCr4YnT55QOp/K1fj6MKAn8ACpa3Ed75PDGCuVSpFIxBzD8/42nQ4rCMLW1tb+/j49XUOhEJfUdyrZH0zhEIfslGXZ3ma6V5BokzEWj8e7izFmemwR6AX1ev3y8vLTp08///xzoVD4j//4j7/+9a+lUuns7Ozy8tJcp2WBdDsvX74MBoNff/21LMt/+MMf/vCHP8iyHA6HV1ZWlpeXFxcXSeEz4IsaMOVy+eeff6YbRetcjrL3zxhrNBqfJ5EB3DqqIaOMi7lpj6Io1JuZNeteT36//SjDMHh7O0vyhjrk0Mi0MC0/pFAo8LlvS/arKdQSmxfGOexJzTdYs978dtLp9MHBweHhoXmmnmoAmGvXmdfk0dph9JkmB8ievb098/h0ny3HOlMqlfiSama3oLufEnQHFUdx7//ly5fhcHiUXw0k1Jmfn8dyvL2Fz350UeQjiuL79+//9Kc/ff/997u7u+Fw2P6IoKdod9VTvFEydSKi50YXLi9PRfHpjv5RKBRyuZzLd0HX0A2JxWJdtIcmRvdPfXroLrtPef1AIEB5ffowys/uQXJ9fT2CelY3uG9aN/p8/PhxMCcKhULJZJKehvv7+9Sq0pyRSiaTdiebut2Rr7+/v08idXONr32FYMZYNBoleUypVCqVSnQufgiN2erNZG4Gx82TJMnZoyULXU7y0uw2NQuy3AeyzU0SjiINWgfNXH7Hr51e8PbxHeRJTa/d/oIMhUKk6+3opwRdUK/XT05OeGuEMZJHUhgwgELwScVeqsQ/OPjorbrUm6Elxv785z8bhpFKpfgjiz9aW1UQOZNMJimi4KmQLlxec4lRp8d2BAmWGGMO74LHQ+8gURQtXZU6Av7ioCHt/m+//WYYxvX1tUt33+fzPXv2zKzhGRePdriMoJ7VGa/X+5gFLKeWaDQaDodJKG92Scm9bhVTUcdlepKaXX9yQ1slyyn5lM/nqR6AnysWi1G/zlZGml88VA8XDoed22JQgZezb22GmgXRFZltk2W5I8lsLBZTVZVWEFtfX+c3MB6Ph8Nh3sDH/fj2a19ZWWl6u+LxuCzLnf6UoFPGvTXCzAz0C11iX9RP/LLsSaFQaJqP0DSNMvpuphBDoRBf1ZtvoQm97tbwliQpGo1SHzPW7RwgX1HR5fq7XUOLqSUSif49rDRNy2QygiB0J/3njI1jNL6YE/zk8bc9hLv7gUAAqf0umJ+f//rrrx8eHn73u9/h1k0JoihubW1tbW1RpZql7YzzUYwxCgDoRdg2RyXLsizLVA/X9lyJRKLriWDnY/kq9Gb4FbW9Dw6D03RB06/4OjVu7jPdqFbftqK7nxK4h6aa6fPKyso///M/D9ce0A/oz8fyNKNld5lNfUc1USRNtGcleLMd81fUlMz+t0luunk7r55q2tTfTYlwIpGgjADpmR32bAV1T6bLN69mYL6cxxcX0b0NhUL9k/4zxnhJ2CMfjPCNeg95/L/99lu9Xr++vm6b4CfhPgfufk8YccU/6B/d5V268DIpLd3FuQZDv20bwLWP8u0dawKBwPLy8q+//vrq1Ss8KicVwzD29vb4kreGYeTzefIdeUkPh/fbOT4+NgyDV+GT6o9X7HAXmSsn4/E4d6ZJ/UIOvTnyFwSBUvikHtze3uZNwLhJ79+/d16WMZFInJ+fP0ZVn0gkaGr06OjIMK1AoqoqzWrG4/HHq/bF1kub9RBZlh+/chkczR7APX5qytnW4/f5fPPz88+ePYOYBwAAwOBZXV1dXV0dthWgj5AHb6+lodY6lnwH6Umo1J4W6LBU7lKplXl/agx6dHR0dHRk2TkSiVhS7Mlksl6v5/P5phVEblLvj1+slyqpSKJzfHx8fHxsNlsQhJ70rOuuU3NHiKLYk14ICAC64f7+/vr6Wtf1arXaNsePBD8AAAAABgmlonk5DW0MhUJ275/vv7u7m8vlyE03u/6yLNuXBt/d3S0UCtlsltbrddiZePfuHe/jaXb9W+3fDyRJ2t3d5XMa3PXvtEqqFW07NT8eCtV6UgT/pNFoPH6UiYdW1dV13Y2O3+zxz8/PI8EPfvnllx9//NHr9b548aLVPjc3N/V6PRKJBINBy1e6rtdqtT7b+Cjm5ubstcv//u//zhhbXFx0OLBSqTDG/vjHP/bPNgDA6ODmsdBzHJ4z5XI5n89T3d2ImPR4Pnz4oGlaKBSich3DMKiXANU4uRmho0M6HZ/2d1lw1T/4+sRTW2KEVHRLSMFPOX7eK60p8PhBXzk9PS2Xy8O2wom1tbW1tbVhWwEAAMAK9d3q3yGdjt+FPf0AJUYIAP4Pnua/vr52FvbA4weDZ2ZmZgQ73w1stS8AAAAA9IppDwBIze+mfpdX7r548SIQCAzSSAAYY3NzcyO49s3oy5MAAAAAYGEaAwCu7alUKg5qfkrzv3jxYn5+PhAIoHIXAAAAAABMANPi1Nbr9V9//bVtCS9P83/11VcQ9gAAAABgvHjkArFgSpjkAIA7/b/++quDtocS/JTpR5ofAAAAAOPL1La1AR0xaf6uG6eftD2Li4t+vx/aHgAAAAAAMFVMgu9Lmv7r62tnp39+fn5+ft7v98/Pzw/YQgAAAAAAAEaEcQ0AePceh0Je7vSjbw8AAAAAAADEOAUA1Kf/119/pcW5mu4Dpx8AAAAAAAAHxiAAIFl/pVJp1affrOmHvAcAAAAAAAAHRjQAuL+///XXX51l/ZTmR5N+MC7c39//9ttvDt8O0piB4XDJAIDppF6vT3Oj7bu7u2GbAMCIBQAk66eK3qY7+Hy+xcVFtOwE40ij0ZjC5/4UXjIAoBV+v1/X9ZubG13XX7x48fTp02FbNFAeHh6q1So9Ff1+/7DNAVPN8H3otsl+j8fz1VdfYXEuML7Mz89/8803LvfstzEDw+UlPx5VVQ3DCIVC9E/DMEql0tXVlSAIy8vLfHsraH/6LIqiJEmCILTaWdM0RVFc7sxta/Wt3TYa3H4tCwsLoVDI0t6bBhcEQZKkVqcwDENVVfOYdIpWR5nH1DRN07SmdtrH57fCfsmCIIii6HyjDMNQFOXq6sowjOXlZUmSml6swwgcURTpWMMw6JBWbdHJeOcbCHrL27dvy+Xy6empruuapvl8vrm5uWkIAxqNhmEYuq4zxrxe7+rq6tra2rCNAlPN0AKAtsl+rvCZJJcITCder3dpaWnYVgyagV3ywcGBpmmbm5uxWCydTudyObOnKEnS9vZ2UxdQ07TDw0Pu0HNkWU4kEnYHNJVK2XeORqOJRKKVd9v0EEIUxffv31vs2d/fZ4zt7OxIkkTXYjEsmUzyc+XzedphZ2enlY++t7enaVooFOKLg9IpZFne2tqy7Ly/v68oiiAIu7u7jLGTk5Pj42PGWDweTyQSTcc/ODhQFMV8LfRz2PeUJCkajUYiEct2TdPS6XShULBst5y01bB23r17R2cplUqHh4esxf2hu61pWtNbAfpHMBiUJOns7Oz09LRer9/e3s7Nzc3NzQ3brj5Crn+j0WCMkevv9XqHbRSYdgYaAPA2PpeXlw7J/vn5+a+++goKHwCAe8ibLxQKgiCQt0c5Y1VV9/f3d3d3LT46becZ4lAoRDtrmlYoFBRF2dnZMccA3AGVJEmSJL5zLpdTVZW713arGGOCIJjP3taRPT8/Pzw81DSNJ7MpiigUCpqm8XNFIhEKANLpdFMDaH/GWCwWcz4jYyybzdJZePATiUQymYxhGLlcrmkAwOdD7OPTLaXPdK9UVT06OjIMw7yzoigHBwf8VzBfbCaTEQSB72wP4ZreW8aYS1cylUq5jChAz/F6vWtra8Fg8PT0tFwu67puGIbf75+8Sf67u7tarUaan6WlpdevX09hMgiMJoNwsu/v7//nf/7HoY0Pkv0AgEdCKeRkMhmNRmmLpmmpVKpUKmmals1mzS6spmnkdwqCsL29bU4P53I5cg0PDg4oEU7EYrGrqytLsj+TyaTTaUVRSqVSOBy2W0Wu7ebmpjnzTYl2h2vJZDKiKJrz1jxdrShKPp+n0SRJCoVCiqIoiqKqql3Hks/nGWOiKDa1zYymaZlMhjEWCoX4DRQEIRqNUgzQ9ALT6TR9sH8liqI5rW4YBs0VZDIZcwBAQRrtbL/YTCYTiUTohlsiHE3TPnz4QKfuIn+fTqe57gsMC7/fH4lEgsFgoVCgwoB6vR4IBCZDEfTw8KDr+u3tLWPM7/dvbGxAaQZGij4GAM4iHyT7AQA9xDCM3d1d8yuW3MoPHz5QDjsWi3HfPZ1OU/Z3c3PTIg6JRqNXV1eU1+euNm23nzQej2ezWcMwLi4uHAKATuUN5P2bc960hVzeQqHArUokEiTpyefzyWTSPAhPz8fj8bZnpMkQi9fOGIvFYiSpymazlgsk1T5jTJblViJ7DsUSiqJYdPzRaJRmVMxhlSiK0Wj0+PiYTtE2eukUCi1EUcQMwCiwtLQ0YYUBJPev1WqNRoPk/qurq4PU/DhXBzl/a6nGCYVCztU7FnjhEMdlCRBNqF5dXTUtAbIM3rT4yl6z1EXhkNmkUqlUq9XoPjR9CtED0KUxZtyUbw2A3nveVM7bSuQTCATI6UeyHwDQQ0KhkP1hKgiCLMvkwqqqyp/FlP2VJMkuSWeMJRIJktaYXe2maJrmUGbKX4QdvUHpWuwDkqiG8v308qA9aWOhULDMTlB6XhTFlZUV59PxcCgej1vOKwhCOBwmTZSiKOaXGU2tMMZalQdYoCXb7dfV9AW5vr5O5QdXV1duBncPL7SgGKO3g4OuCQaDi4uLZ2dnZ2dn9Xr97u5ubm6u0z+cUaBer+u6/vnzZ8ZYMBhcW1sbfLcf0iuaK3/cfNu0JkoURaqJcnnqpvU87EtUb9cKUnLBUrjFGItGo5aMhnnw9+/f2//f4N9+//33tMWhBMuCueKo1X2Ix+OW1wFN/4qiaJeY5vN5mlPlxpiPoq/evXs3CQEAifsvLy9bdfKZn59fXFxEGx8AwIBZXl6mDxcXF+baANa6uQ0lZki27jCyqqpUYyqKYtM4gQcAvXrKS5JEryVN0/iY4XCYQgKzzEnTNApymsYSFiPpbSTLctOriEQi9Fo9OTkx3zEKMNqOz89C+7upRjDjMoHnHop2ZFnmMQYYEUgns7q6SoUB1Wq1VquNUWHAWMv98/k8zbmxf6zGoeeDuRrHJbIs0wee3T8+PlZV1TzHyMV+zNSXjHLnFBI8sjrfntdvVTjE/5nNZkn0yGz34ejoaG5uzjwVsLW1RV0WDg8Pt7e33ZjU9nk7SB4VAFAHz8vLy6bifoh8AABDh78AuCvJk8oOnisFAIZhUCUu387zx+zLu4S6ADUd5OLigjV72XQNt+Ti4oIHALxUt1QqcUu42KaVbYqiUPTC44RWezadZKA3IjO95i3w6IgxpmkahVLmCg07ZhlAz/1+IpvNFgoFURSTyWSfTgEeCRUGLC0tkSLo5uaGZgNGWRHUaDSq1SrNcXm93jdv3rx69WrYRnUGTWZKkpRMJp2rcVxi8d1TqVQul6NnCH+Oka8vCEIikeBPBl4vRBOwbfs4u7eBMfbdd98xxiRJatW2gXoH0/PQfB/29vYMw0ilUuYAQBTFRCJBlWatysAs0OuDDuzqmnpJN345L+ptJe5/+fIlRD4AgNGE3tOsc2UOhzeYZ4yVSqVIJNI0x8/lqt2dxU7TWgJeqquqKlfpULo9HA63CnI0TWs6U98UqjQwTzLw8uJWSSzDMCzji6J4cXFhCahoz3w+XyqVXE7Wdw3lIBljOzs7giAgABhlgsFgMBgkRZCu6/V6nQLpUQsDhi737xWJRGJlZcWhGscsoexufNJVmp8AoVBod3fXUiEgCEIymdzb22O2WccBkEgk1tfXLQ9tug+ZTIZyGeZvo9HoyckJ5VOaCpPMcLHl1taWm4nTftNBAODs96OTDwBgLOCKAgcXkH9leaCbG96rqkrNZPb29iz1x3wH1tMAgM9dLCwsmLfHYjGaVqZ+oDw97zBrHw6HKQuuKAopZZs2SyWoEJD3A+XBg0N5cSgUssz1U/bd0l9VVVXeX5WqOJaXl+lbPtPSEwzDoAHtKzyAkWV1dVWSJFIEGYZxe3s7Uoqgu7u7m5sbkvsvLS3JsjzWi/s2dbV5BRGXUHYH10Na/vqaPh4pDqEJ2K7P2DWtTKIP5tlXgoRAhmE4C4G4+CcWiw04qmlF+wCgrd8PcT8AYGQhHQ4zFQPwDw5vl1ZSUTO0xNif//xnmhq2V9RRSrttDa57eE2CXcAqyzK515qm8fS8w2uGWnPQVdRqNUpuWZqlmuH9QHl0wdpdmqWLkSRJHz58oLNQeZ+maeT92xdr6/mLn0v/3fREAqMDKYLW1tb+67/+6/r6+ubmRtf158+fD1dXfH9/r+s6yf3n5+ffvHkzgnL/njjQ/FHzmBkzwzB4CZDL8JsCgFYnbXpdfZ3Tc+jkZhYCmRvHWWzj4p/Nzc3+2dkRLf+E3Pj9v/vd7yDuBwCMMrzdO/eGacaZNCr2RhPMpFl3k6cJhUK8H44ZLmjpYbKHrCJn2vIVL9WlRDtz1/2TSCQStGRYJpOxT38TvB8on8V20/3TDBX5meMH/rnfE+L5fJ6k/6OguwVd4Pf7v/32W94q9Pr6elitQhuNRq1Wq9Vq7MtyZqurqwO2wSWqqlLj4E7pSTUOLwFiXx7CiUTC4aFE4YqqqtR50/m8vZ0edDCJiqmurq6cQ6loNErPxqOjo1KpJAgCT9bwUivqs9yq9mAoWN13+P0AgHGEd1Y2b6SkOGNMlmXzVzyfnc1m7ToZvr6VZc1ae08JZnLKzRt5xstyXpc0fdlwz7upsMdcqkv2rK+vuz/j1tYWvVPtUxkErzTggU0XzjSfV6F/ckWT/RadnJx0OrgDdE9GRHcLuoYKA05PT09PT+v1er1e9/v9nS6y8RgMw9B1vdFoMMZWV1fX1tZGWe7fagKzlS9Lz0Nq+Pv4s1tKgARBaFoCxBhTFCWdTrvv2c9a9G9oGza4hO5DqVRy7gJnodFoUF7JssKg5T6MVOnR//fjHfx+j8cTCASorhd+PwBgNDEMY29vjy8oS6WlpLmkLhPmnWOxGOW8qb4tEonQG4Ucd3pky7Js6YZhaQWtaRqtGcz+sRkOzfZqnfTIt6AoysHBQTKZNFtFJXTUlrvpUbxUl31R7bs/Iy0AnMvlFEVpGhSxL+2G+P4dOdOkkqJ7wiOTlZUVGpCLgtg/XmwPMff0AGPN2toahQHlclnXdcMwBlAYMI5y/1a9bkiJZ9loqcYJhUILCwuPqcbhtVLsi0tdKBRKpZJlgXPecd9cAiQIAjemKZZ1EonDw0P3jQ1aUSqVDg8PKZcUCoXC4fDc3JwoimRqq6N4a/9YLEatF2hxA8bY+/fvaUaFMjhUajUimQjPxcWFQ74ffj8AYCzg7yrKe/FskCAI9tSvIAg7Ozu8wx2tC8tMubFQKGRWB/GVAY6Ojo6Ojiw7RyIRHhVQmzmOwxS8pmm0syzLlnZ1oihSXzm6FnP9HHWwaTogL9VlXQUeJAQyDIOEQPZXlPhlJTLWuvsnR1EU87XzS0gkErxZHkUR1PNbVVVRFPmcezKZzOfzHWXgHAiFQpD+TxK8MODHH3+kVqG6rr948aIfiqCHh4dqtUpyf7/fL8vyCMr9H0k/qnEsD5CtrS0qi6JGBbSR2g+IosgTN8OF7gNjLBQKbW9vm5+0DrMi5tb+XN/Pj+WlVqIoUoLm8PBwRIRAnrOzM8sm6HwAAGMHvUUof8+nWakXTdN0iyiKu7u7uVwun89TO2p+iH2JFkEQdnd3C4VCNpsldazDzo+HulAfHBzwaXEq87Ws9WuHAoBO1fn82K2tLcrTt3pF0bAO3T/NmF0HCh7sXb13dnaoeM5cMsGz9alUqtOraGrzI5cTAqOJ3+9/+/YtLwzQNK23hQHU4lPXdcYYtfhcW1vrycijBq/GsXj/vYXSBzyqV1WV1DLRaHQUvH/2pbsxY2xra8v9DCqfIXFOu5hnWV0uGtBv/s/Fh98PABhryNVLJpOqqlKXm7bucjwej8fjNEXb9hBZlmVZdrPzu3fv3LjI+/v7rRJLFJ/QuehEbV9I/C3ufGr70vSccDjs8Fpy0/2T/ePUf1tEUdze3m56S6PRqMOqYaIoOlwIQb/XY0YAo08wGJQk6ezsjAoD7u7uBEF4fGHAeMn9H8lgqnEs5VKtWi2zL6uS9eq87mnVq5Qxdn5+3vQQXprlpr8wn2V1s2jAAPDA7wcATBKk3ezfIV2M3zUdnSubzbJ23T8fA43PetrYlBjkLQWTB3Xj6VVhwN3dXa1WI83P0tLS69evJ0/zY4GvK5JOp/tRjUNDUb6flwDxbmO04i/fuVAo9GTqrwu4Sel0mqfzHe4DF/9QeVjb8WmW9eDggMZs2oNukHg2NjaGawEAwA2fP3+md9JIQVVxYOhomkbv1z6J3Xl3i+70RQD0GyoMkCTpv//7v6kwgBYOc68Ienh40HX99vaWRtvY2OjhKn6jDNWttqrGyWazXVQCNC0BMlfjCIIQiUTy+TzVC9HiX9QuMxKJ+Hy+nncCaEskEsnlcuTWU3ECbwlN8kjLvAQvC3av6Q+HwySFyuVy6+vrw018IOsPwHjAm08DYIe6jrpU53cBX+sArfTBKLO8vLy8vMwLA25vb93MA/Du/o1Gg+T+q6urE6z5sUBNEaghj7kaJ5lM8oUCOx3THDNQEwV7udTm5iY5+rwKKxQKUSMdcpG7v6SuoPtAxVd0H8hyKiQ7OTkxt/jkPaY7XVycei5TqVWrxdcHwxOSuAEARpbT09Nffvll2FY48erVq2AwOKyzU1e7UCg0Cq0VqLHP9va2mxovqgGIRqN8ZVxKm9n7AjnDD4zFYn1aZpJucjgcdljrHoDRQdd1UgTxLX/84x/tu5XLZV76yRiTJGljY2P0W3z2CZfVUP04KWOMJgEGc1JnKCAZ8H0YPAgAAADjjUPl1uChnJDLNxk1+THvzJfK6uitw3ue9u8OdGcYAMNF1/VisUj+pXMAMCVyfwA4CAAAAAAAMLGQIujt27dNvyoWi2tra6urq4M3DIAhggAAAAAAANOIruter3d65P4AcBAAAAAAAAAAMEXMDNsAAAAAAAAAwOBAAAAAAAAAAMAUgQAAAAAAAACAKQIBAAAAAAAAAFMEAgAAAAAAAACmCAQAAAAAAAAATBEIAAAAAAAAAJgiEAAAAAAAAAAwRSAAAAAAAAAAYIpAAAAAAAAAAMAUgQAAAAAAAACAKQIBAAAAAAAAAFMEAgAAAAAAAACmCAQAAAAAAAAATBEIAAAAAAAAAJgiEAAAAAAAAAAwRSAAAAAAAAAAYIpAAAAAAAAAAMAUgQAAAAAAAACAKQIBAAAAAAAAAFMEAgAAAAAAAACmiJnPnz83Go1GozFsSwAAAAAAAAB95/9mABADAAAAAAAAMPFAAgQAAAAAAMAUMcNMuX9MAgAAAAAAADDZYAYAAAAAAACAKcIaAGASAAAAAAAAgAnm/wcA8PsBAAAAAACYBiABAgAAAAAAYIr4f5VjE4G1ysr6AAAAAElFTkSuQmCC" alt="">
        </th>
    </tr>
</table>

<table style="width:30%;font-size:16px;line-height:1;margin:-50px auto 0">
    <tr>
        <th>
            <b>АКТ</b> <br>
            приймання-передачі
            гуманітарної допомоги
    </tr>
</table>

<table style="border:1px solid black;border-collapse:collapse;width:100%;font-size:14px;margin-bottom:5px;">
    <tr style="border:1px solid black;border-collapse:collapse;">
        <th style="width:30%;border:1px solid black;border-collapse:collapse;text-align:left;padding-left: 5px;">
            Відправник
        </th>
        <th style="border:1px solid black;border-collapse:collapse;">
            {{$data['sender']->name}}
        </th>
    </tr>
    <tr style="border:1px solid black;border-collapse:collapse;">
        <th style="width:30%;border:1px solid black;border-collapse:collapse;text-align:left;padding-left: 5px;">
            ЄДРПОУ відправника
        </th>
        <th style="border:1px solid black;border-collapse:collapse;">
            {{$data['sender']->id_code}}
        </th>
    </tr>
    <tr style="border:1px solid black;border-collapse:collapse;">
        <th style="width:30%;border:1px solid black;border-collapse:collapse;text-align:left;padding-left: 5px;">
            Адреса відправника
        </th>
        <th style="border:1px solid black;border-collapse:collapse;">
            {{ $data['sender']->getAddressToDocument() }}
        </th>
    </tr>
    <tr style="border:1px solid black;border-collapse:collapse;">
        <th style="width:30%;border:1px solid black;border-collapse:collapse;text-align:left;padding-left: 5px;">
            Уповноважена особа відправника: ПІБ
        </th>
        <th style="border:1px solid black;border-collapse:collapse;">
           {{$data['sender']->name}}
        </th>
    </tr>
    <tr style="border:1px solid black;border-collapse:collapse;">
        <th style="width:30%;border:1px solid black;border-collapse:collapse;text-align:left;padding-left: 5px;">
            Отримувач
        </th>
        <th style="border:1px solid black;border-collapse:collapse;">
            {{$data['recipient']->name}}
        </th>
    </tr>
    <tr style="border:1px solid black;border-collapse:collapse;">
        <th style="width:30%;border:1px solid black;border-collapse:collapse;text-align:left;padding-left: 5px;">
            ЄДРПОУ отримувача
        </th>
        <th style="border:1px solid black;border-collapse:collapse;">
            {{$data['recipient']->id_code}}
        </th>
    </tr>
    <tr style="border:1px solid black;border-collapse:collapse;">
        <th style="width:30%;border:1px solid black;border-collapse:collapse;text-align:left;padding-left: 5px;">
            Адеса отримувача
        </th>
        <th style="border:1px solid black;border-collapse:collapse;">
            {{ $data['recipient']->getAddressToDocument() }}
        </th>
    </tr>
    <tr style="border:1px solid black;border-collapse:collapse;">
        <th style="width:30%;border:1px solid black;border-collapse:collapse;text-align:left;padding-left: 5px;">
            Уповноважена особа отримувача: ПІБ
        </th>
        <th style="border:1px solid black;border-collapse:collapse;">
            {{$data['recipient']->founder->name}}
        </th>
    </tr>
</table>

<table style="border:1px solid black;border-collapse:collapse;width:100%;font-size:14px;margin-bottom:5px;text-align:left">
    <tr style="border:1px solid black;border-collapse:collapse;">
        <th style="border:1px solid black;border-collapse:collapse;;width:100%;text-align:center" colspan="4">
            <b>Вид допомоги / Type of assistance</b>
        </th>
    </tr>
    <tr>
        <th style="border:1px solid black;border-collapse:collapse;">№</th>
        <th style="border:1px solid black;border-collapse:collapse;">Код товару</th>
        <th style="border:1px solid black;border-collapse:collapse;">Товар</th>
        <th style="border:1px solid black;border-collapse:collapse;">Кількість</th>
    </tr>
    @foreach($data['products'] as $transferProduct)
        <tr>
            <th style="border:1px solid black;border-collapse:collapse;">{{$loop->index + 1}}</th>
            <th style="border:1px solid black;border-collapse:collapse;">{{$transferProduct->product->code}}</th>
            <th style="border:1px solid black;border-collapse:collapse;">{{$transferProduct->product->current()->first()->name}}</th>
            <th style="border:1px solid black;border-collapse:collapse;">{{$transferProduct->quantity .' '. $transferProduct->product->current()->first()->type}}</th>
        </tr>
    @endforeach
</table>
@if(isset($additionalInfo['description_product']) and $additionalInfo['description_product'])
    <div style="font-size: 14px; text-align: left; line-height: 1; margin-bottom: 7px">
        Додаткова інформація про товари вказана у Додатку 1.
    </div>
@endif
<div style="font-size: 14px; text-align: left; line-height: 1;">
    Підписанням цього Акту Відправник та Отримувач підтверджують факт поставки Гуманітарної допомоги. Цей Акт чинний із дати його підписання уповноваженими представниками обох Сторін.
</div>

<table style="width:100%;font-size:14px;margin-top: 20px;line-height: 1;">
    <tr>
        @if(isset($additionalInfo['confirmSmsSender']) and $additionalInfo['confirmSmsSender'])
            <th style="width: 40%;">
                <i style="font-size: 8px;">
                    Даний Акт підписан "Відправником" шляхом введення одноразового ідентифікатора (як аналог власноручного підпису є підтвердженням особи), який є у вигляді коду, отриманого на телефонний номер.
                </i><br>
                <div style="width: 80%; margin: auto; border-bottom: 1px solid black;"></div>
                Підпис відправника
            </th>
        @else
            <th style="width: 40%;">
                <div style="width: 80%; margin: auto; border-bottom: 1px solid black; margin-top:80px;"></div>
                Підпис отримувача
            </th>
        @endif
        @if(isset($additionalInfo['confirmSmsRecipient']) and $additionalInfo['confirmSmsRecipient'])
                <th style="width: 40%;">
                    <i style="font-size: 8px;">
                        Даний Акт підписан "Отримувачем" шляхом введення одноразового ідентифікатора (як аналог власноручного підпису є підтвердженням особи), який є у вигляді коду, отриманого на телефонний номер.
                    </i><br>
                    <div style="width: 80%; margin: auto; border-bottom: 1px solid black;"></div>
                    Підпис отримувача
                </th>
        @else
                <th style="width: 40%;">
                    <div style="width: 80%; margin: auto; border-bottom: 1px solid black; margin-top:80px;"></div>
                    Підпис отримувача
                </th>
        @endif
    </tr>
    <tr>
        @if(isset($additionalInfo['confirmSmsSender']) and $additionalInfo['confirmSmsSender'])
            <th style="width: 40%;padding-left: 15px;margin-left: auto">
                <br>
                <div style="width: 50%; display:inline-block; border-bottom: 1px solid black;">{{$transfer->created_at->format('d.m.Y')}}</div>
                <br>Дата</th>
        @else
            <th style="width: 40%;padding-left: 15px;margin-left: auto">
                <br>
                <div style="width: 50%; display:inline-block; border-bottom: 1px solid black;">__.__.20__</div>
                <br>Дата</th>
        @endif
        @if(isset($additionalInfo['confirmSmsRecipient']) and $additionalInfo['confirmSmsRecipient'])
            <th style="width: 40%;padding-left: 15px;margin-left: auto">
                    <br>
                <div style="width: 50%; display:inline-block; border-bottom: 1px solid black;">{{now()->format('d.m.Y')}}</div>
                <br>Дата</th>
        @else
                <th style="width: 40%;padding-left: 15px;margin-left: auto">
                    <br>
                    <div style="width: 50%; display:inline-block; border-bottom: 1px solid black;">__.__.20__</div>
                    <br>Дата</th>
        @endif
    </tr>
</table>

@if(isset($additionalInfo['description_product']) and $additionalInfo['description_product'])
    <div style="page-break-before: always;"></div>

    <table style="width:100%;font-size:18px;margin-bottom:5px">
        <tr>
            <th style="text-align: right; font-size: 15px;" valign="top">
               Додаток 1
            </th>
        </tr>
    </table>
    <table>
        <tr style="border-collapse:collapse;">
            <th style="width:20px;border-collapse:collapse;padding-left: 5px;font-weight:bold;"></th>
            <th style="text-align:left;border-collapse:collapse;font-weight:bold;box-sizing:border-box">
                <div style="display:inline-block;"></div>
                <div style="display:inline-block;width:100%;border-bottom: 1px solid black;margin-left:auto">
                    Додатковий опис товарів:
                </div>
            </th>
        </tr>
        <tr style="border-collapse:collapse;">
            <th style="width:20px;border-collapse:collapse;padding-left: 5px;font-weight:bold;"></th>
            <th style="text-align:left;border-collapse:collapse;box-sizing:border-box">
                <div style="display:inline-block;"> {{$additionalInfo['description_product']}}</div>
            </th>
        </tr>
    </table>


    <table style="width:100%;font-size:14px;margin-top: 20px;line-height: 1;">
        <tr>
            @if(isset($additionalInfo['confirmSmsSender']) and $additionalInfo['confirmSmsSender'])
                <th style="width: 40%;">
                    <i style="font-size: 8px;">
                        Даний Акт підписан "Відправником" шляхом введення одноразового ідентифікатора (як аналог власноручного підпису є підтвердженням особи), який є у вигляді коду, отриманого на телефонний номер.
                    </i><br>
                    <div style="width: 80%; margin: auto; border-bottom: 1px solid black;"></div>
                    Підпис відправника
                </th>
            @else
                <th style="width: 40%;">
                    <div style="width: 80%; margin: auto; border-bottom: 1px solid black;margin-top:80px;"></div>
                    Підпис отримувача
                </th>
            @endif
            @if(isset($additionalInfo['confirmSmsRecipient']) and $additionalInfo['confirmSmsRecipient'])
                <th style="width: 40%;">
                    <i style="font-size: 8px;">
                        Даний Акт підписан "Отримувачем" шляхом введення одноразового ідентифікатора (як аналог власноручного підпису є підтвердженням особи), який є у вигляді коду, отриманого на телефонний номер.
                    </i><br>
                    <div style="width: 80%; margin: auto; border-bottom: 1px solid black;"></div>
                    Підпис отримувача
                </th>
            @else
                <th style="width: 40%;">
                    <div style="width: 80%; margin: auto; border-bottom: 1px solid black;margin-top:80px;"></div>
                    Підпис отримувача
                </th>
            @endif
        </tr>
        <tr>
            @if(isset($additionalInfo['confirmSmsSender']) and $additionalInfo['confirmSmsSender'])
                <th style="width: 40%;padding-left: 15px;margin-left: auto">
                    <br>
                    <div style="width: 50%; display:inline-block; border-bottom: 1px solid black;">{{$transfer->created_at->format('d.m.Y')}}</div>
                    <br>Дата</th>
            @else
                <th style="width: 40%;padding-left: 15px;margin-left: auto">
                    <br>
                    <div style="width: 50%; display:inline-block; border-bottom: 1px solid black;">__.__.20__</div>
                    <br>Дата</th>
            @endif
            @if(isset($additionalInfo['confirmSmsRecipient']) and $additionalInfo['confirmSmsRecipient'])
                <th style="width: 40%;padding-left: 15px;margin-left: auto">
                    <br>
                    <div style="width: 50%; display:inline-block; border-bottom: 1px solid black;">{{now()->format('d.m.Y')}}</div>
                    <br>Дата</th>
            @else
                <th style="width: 40%;padding-left: 15px;margin-left: auto">
                    <br>
                    <div style="width: 50%; display:inline-block; border-bottom: 1px solid black;">__.__.20__</div>
                    <br>Дата</th>
            @endif
        </tr>
    </table>
@endif

</body>
</html>
