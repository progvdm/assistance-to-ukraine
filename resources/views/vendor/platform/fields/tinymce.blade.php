@component($typeForm, get_defined_vars())
    <script src="/vendor/tinymce/tinymce.min.js"
            referrerpolicy="origin"></script>
    <div
         data-quill-value='@json($value)'
         data-quill-groups="{{$attributes['groups'] ?? ''}}"
    >
        <textarea
            class="tinymce"
            {{$attributes}}
            type="text"
            placeholder="{{__('auth.form.text')}}"
            rows="10">
            {{$value}}
        </textarea>
    </div>
    <script>
        tinymce.init({
            selector: '.tinymce',
            plugins: 'preview importcss searchreplace autolink autosave save directionality code visualblocks visualchars fullscreen image link media template codesample table charmap pagebreak nonbreaking anchor insertdatetime advlist lists wordcount help charmap quickbars emoticons',
            toolbar1: "undo redo pastetext | blocks | normal bold italic forecolor backcolor fontselect fontsizeselect styleselect fontsize | alignleft aligncenter alignright alignjustify",
            toolbar2: 'bullist numlist outdent indent | link unlink image media fullscreen code appearance',

            tinycomments_mode: 'embedded',
            language: '{{app()->getLocale()}}',
            tinycomments_author: 'Author name',
            file_browser_callback: function (field_name, url, type, win) {
                console.log(field_name, url, type, win)
            },
            images_upload_url: '{{route('api.tinymce.photo.admin')}}',
            setup: function (editor) {
                editor.on('init change', function () {
                    editor.save();
                });
            }
        });
    </script>
@endcomponent
