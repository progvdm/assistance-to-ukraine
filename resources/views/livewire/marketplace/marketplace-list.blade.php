<div class="marketplace form js-import" data-form>
    <div class="_grid _spacer _spacer--md _mb-md">
        <div class="_cell _cell--12 _md:cell--3 _mb-df _df:mb-none js-import"  data-select2 data-initialize-select2="{{ json_encode((object)['Factory' => 'LivewireSelect']) }}">
            <label class="with-anim-underline">
                <select name="searchCategory" id="searchCategory" wire:model="searchCategory">
                    <option value="0">{{ __('aid-request.marketplace.All category') }}</option>
                    @foreach($categories as $category)
                        <option value="{{$category->id}}" {{ $searchCategory == $category->id ? 'selected' : ''  }}>{{$category->current->name}}</option>
                    @endforeach
                </select>
            </label>
        </div>

        <div class="_cell _cell--12 _md:cell--3 _mb-df _df:mb-none _flex-column">
            <label class="_flex _flex-row with-anim-underline">
                <div type="button">
                    @svg('marketplace', 'glass', [21, 19])
                </div>
                <input class="_ml-sm bg-unset" type="text" wire:model="searchQuery" placeholder="Search">
            </label>
        </div>
    </div>

    <div class="_grid _spacer _spacer--md _mb-md ">
            <div class="_cell _cell--12 _sm:cell--3">
                <div class="_flex _flex-column _xs:flex-row">
                    <label class="without-underline _flex _flex-row _cursor-pointer _mr-df">
                        <input class="_mr-xs _mb-xs _ml-none" checked type="radio" name="filter" value="all"
                               wire:model="tab">
                        {{__('aid-request.marketplace.All')}}
                    </label>
                    <label class="without-underline _flex _flex-row _cursor-pointer _mr-df">
                        <input class="_mr-xs _mb-xs _ml-none" type="radio" name="filter" value="offer"
                               wire:model="tab">
                        {{__('aid-request.marketplace.Offer')}}
                    </label>
                    <label class="without-underline _flex _flex-row _cursor-pointer">
                        <input class="_mr-xs _mb-xs _ml-none" type="radio" name="filter" value="need"
                               wire:model="tab">
                        {{__('aid-request.marketplace.Need')}}
                    </label>
                </div>
            </div>
            @if(auth()->user())
                <div class="_cell _cell--12 _md:cell--3 _mb-df _df:mb-none js-import"  data-select2 data-initialize-select2="{{ json_encode((object)['Factory' => 'LivewireSelect']) }}">
                    <label class="with-anim-underline">
                        <select name="searchOrganisation" id="searchOrganisation" wire:model="searchOrganisation">
                            <option value="0">{{ __('aid-request.marketplace.All organisation') }}</option>
                            @foreach($organisations as $organisation)
                                <option value="{{$organisation->id}}" {{ $searchOrganisation == $organisation->id ? 'selected' : ''  }}>{{$organisation->name}}</option>
                            @endforeach
                        </select>
                    </label>
                </div>
            @endif
        </div>

    <div class="_grid _spacer _spacer--xl">
        @if($list->isEmpty())
            <div class="_cell _cell--12 _sm:cell--6">
                <div class="title title--size-h6 _mb-xl">
                    {{ __('aid-request.marketplace.Nothing was found for your request! Try writing it differently') }}
                </div>
            </div>
        @endif
            <div class="_cell _cell--12 _sm:hide">
                <div class="title title--size-h6 title--white-text-shadow">
                    <div class="_flex _justify-between">
                        <div class="_flex _items-center text--weight-bold _cursor-pointer {{ ($tab == 'offer' or $tab == 'all') ? 'is-active' : ''}}"
                             wire:click="$set('tab', 'offer')">
                            @svg('marketplace', 'donor', [31, 31], '_mr-xs')
                            {{__('aid-request.marketplace.Donor offer')}}
                        </div>

                        <div class="_flex _items-center text--weight-bold _cursor-pointer {{ ($tab == 'need' or $tab == 'all') ? 'is-active' : '' }}"
                             wire:click="$set('tab', 'need')">
                            @svg('marketplace', 'recipient', [21, 27], '_mr-xs')
                            {{__('aid-request.marketplace.Recipient requests')}}
                        </div>
                    </div>
                </div>
            </div>

        @if($listOffer->isNotEmpty())
            <div class="_cell _cell--12 {{ ($tab == 'all' and $listNeed->isNotEmpty()) ? '_sm:cell--6' : ''}}">
                <div class="title title--size-h6 title--white-text-shadow _mb-xl _sm:show">
                    <div class="_flex _items-center text--weight-bold">
                        @svg('marketplace', 'donor', [31, 31], '_mr-xs')
                        {{__('aid-request.marketplace.Donor offer')}}
                    </div>
                </div>
                <div class="marketplace--min-height">
                    <div class="_grid _grid--1 _spacer _spacer--df {{ ($tab == 'all' and $listNeed->isNotEmpty()) ? '_md:grid--2' : ''}} {{ ($tab == 'offer' or $listNeed->isEmpty()) ? '_xs:grid--2 _md:grid--4' : ''}}">
                        @foreach($listOffer as $item)
                            @include('components.marketplace.card')
                        @endforeach
                    </div>
                </div>
            </div>
        @endif
        @if($listNeed->isNotEmpty())
            <div class="_cell _cell--12 {{ ($tab == 'all' and $listOffer->isNotEmpty()) ? '_sm:cell--6' : ''}}">
                <div class="title title--size-h6 title--white-text-shadow _mb-xl _sm:show">
                    <div class="_flex _items-center text--weight-bold">
                        @svg('marketplace', 'recipient', [21, 27], '_mr-xs')
                        {{__('aid-request.marketplace.Recipient requests')}}
                    </div>
                </div>
                <div class="marketplace--min-height">
                    <div class="_grid _grid--1 _spacer _spacer--df {{ ($tab == 'all' and $listOffer->isNotEmpty()) ? '_md:grid--2' : ''}} {{ ($tab == 'need' or $listOffer->isEmpty()) ? '_xs:grid--2 _md:grid--4' : ''}}">
                        @foreach($listNeed as $item)
                            @include('components.marketplace.card', ['class' => 'card--dark'])
                        @endforeach
                    </div>
                </div>
            </div>
        @endif

        <div class="_cell _cell--12 _pb-df">
            {{ $list->links() }}
        </div>
    </div>
    @include('layouts.preloader')
</div>
