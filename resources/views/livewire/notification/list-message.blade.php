<div class="ring">
    @if($list->count() > 0)
        <span class="ring__count" style="--fa-animation-iteration-count: 1">{{ $list->count() }}</span>
    @endif
    <i class="fa-solid fa-bell fa-shake fa-xl" style="--fa-animation-iteration-count: 1"></i>
    <div class="ring__block">
        @foreach($list as $notification)
            @if($notification->type == \App\Notifications\SignDocumentNotification::class )
                <a href="{{ \Illuminate\Support\Arr::get($notification->data, 'route') ? route(\Illuminate\Support\Arr::get($notification->data, 'route'), \Illuminate\Support\Arr::get($notification->data, 'route_params') + ['notification' =>$notification->id]) : '#' }}"
                   class="ring__item">
                    {{ __(\Illuminate\Support\Arr::get($notification->data, 'title')) }}
                    <br>
                    {{ Arr::get($notification->data, 'description') }}
                    <i class="fa-sharp fa-solid fa-circle-check _ml-xs"></i>
                </a>
            @else
                <a wire:click="clickMessage('{{$notification->id}}')"
                   class="ring__item">
                    {!!  __(Arr::get($notification->data, 'title'), Arr::get($notification->data, 'title_replace', [])) !!}
                    {{ __(Arr::get($notification->data, 'description'), Arr::get($notification->data, 'description_replace', [])) }}
                    <i class="fa-sharp fa-solid fa-circle-check _ml-xs"></i>
                </a>
            @endif
        @endforeach
        @if($list->count() == 0)
            <span class="ring__item">
                    {{ __('notifications.There are no messages!') }}
                </span>
        @endif
    </div>
</div>
