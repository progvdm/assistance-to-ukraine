<div>
    @if ($paginator->hasPages())
        <div class="_flex _justify-between _items-center _mt-md">
            <div class="pagination">
                <div class="_grid _spacer _spacer--sm">
                    @if ($paginator->onFirstPage())
                        <div class="_cell">
                            <div class="pagination__item pagination__item--withoutBorder" disabled="disabled">
                                < <span>{{__('default.Previous')}} </span>
                            </div>
                        </div>
                    @else
                        <div class="_cell">
                            <div class="pagination__item pagination__item--withoutBorder">
                                <a wire:click="previousPage('{{ $paginator->getPageName() }}')" wire:loading.attr="disabled" dusk="previousPage{{ $paginator->getPageName() == 'page' ? '' : '.' . $paginator->getPageName() }}.before" >< <span>{{__('default.Previous')}}</span> </a>
                            </div>
                        </div>
                    @endif
                        @foreach($elements as $links)
                            @if(is_array($links))
                                @foreach($links as $pageItem => $link)
                                    @if($pageItem != $paginator->currentPage() )
                                        <div class="_cell">
                                            <div class="pagination__item">

                                                <a wire:click="gotoPage({{ $pageItem }}, '{{ $paginator->getPageName() }}')"> {{$pageItem}} </a>
                                            </div>
                                        </div>
                                    @else
                                        <div class="_cell">
                                            <div class="pagination__item is-active">
                                                <span>{{ $paginator->currentPage() }}</span>
                                            </div>
                                        </div>
                                    @endif
                                @endforeach
                            @else
                                <div class="_cell">
                                    <div class="pagination__item">
                                        <a wire:click="gotoPage({{ $links }}, '{{ $paginator->getPageName() }}')"> {{$links}} </a>
                                    </div>
                                </div>
                            @endif
                        @endforeach
                    @if ($paginator->hasMorePages())
                        <div class="_cell">
                            <div class="pagination__item pagination__item--withoutBorder">
                                <a wire:click="nextPage('{{ $paginator->getPageName() }}')" wire:loading.attr="disabled" dusk="nextPage{{ $paginator->getPageName() == 'page' ? '' : '.' . $paginator->getPageName() }}.before" ><span>{{__('default.Next')}}</span> ></a>
                            </div>
                        </div>
                    @else
                        <div class="_cell">
                            <div class="pagination__item pagination__item--withoutBorder" disabled="disabled">
                                <span>{{__('default.Next')}}</span> >
                            </div>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    @endif
</div>
