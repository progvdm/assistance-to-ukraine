@if($thanks)
    @include('components.rating.thanks')
@else
    @include('components.rating.form')
@endif
