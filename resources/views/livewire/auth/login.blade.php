<div>
    <form class="form js-import _pt-df _px-lg" data-form wire:submit.prevent="submit">
        <div class="popup__title">Log in</div>
        <div class="_grid _spacer _spacer--md _md:spacer--lg _mb-df">
            <div class="_cell _cell--12">
                <div class="_flex _flex-column">
                    <label class="with-anim-underline">
                        <input wire:model.defer="email" type="text" placeholder="Log in / e-mail">
                    </label>
                    @include('components.form.error-field',['field' => 'email'])
                </div>
            </div>
            <div class="_cell _cell--12">
                <div class="_flex _flex-column">
                    <label class="with-anim-underline">
                        <input wire:model.defer="password" type="password" placeholder="{{__('auth.form.password')}}">
                    </label>
                    @include('components.form.error-field',['field' => 'password'])
                </div>
            </div>
            @include('components.form.error-field',['field' => 'incorrect'])

            <div class="_cell _cell--6 _text-left _md:text-right">
                <div>@include('components.form.error-field',['field' => 'verify'])</div>
            </div>

            <div class="_cell _cell--6 _text-right _md:text-left">
                @if($errors->has('verify'))
                    <button class="button button--transparent button--normal" wire:click="verifyLink">
                        <svg class="_mr-xs" width="19" height="14" viewBox="0 0 19 14" fill="none"
                             xmlns="http://www.w3.org/2000/svg">
                            <path
                                d="M2.24707 0.933594H16.1212C16.9496 0.933594 17.6212 1.60517 17.6212 2.43359V11.9995C17.6212 12.828 16.9496 13.4995 16.1212 13.4995H2.24707C1.41864 13.4995 0.74707 12.828 0.74707 11.9995V2.43359C0.74707 1.60517 1.41864 0.933594 2.24707 0.933594Z"
                                fill="#B0CAEF" stroke="#353535"/>
                            <path d="M8.56982 7.99121L16.942 1.21122" stroke="#353535"/>
                            <path d="M9.24365 8L1.00112 1.09739" stroke="#353535"/>
                        </svg>

                        {{__('auth.Verify')}}</button>
                @endif
            </div>

            <div class="_cell _cell--12 _md:text-center">
                <button type="submit" class="button button--main button--normal ">{{__('default.Confirm')}}</button>
            </div>
        </div>
    </form>
    <div class="button--restore _self-start _pl-none _pt-none _mt-xs"
             x-data="Alpine.plugins.openModal('auth.restore-password')"
             @click="open"

        >{{__('auth.Restore button')}}</div>
</div>
