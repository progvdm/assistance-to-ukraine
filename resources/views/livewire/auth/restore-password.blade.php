<form class="form js-import" data-form wire:submit.prevent="submit">
    <div class="popup__title">{{__('auth.Restore')}}</div>
    <div class="_grid _mb-xs _mb-md">
        <div class="_cell _cell--10 _md:m-auto">
            <div class="_flex _flex-column">
                <label class="with-anim-underline">
                    <input wire:model.defer="email" type="text" placeholder="e-mail">
                </label>
                @include('components.form.error-field',['field' => 'email'])
            </div>
        </div>
        @include('components.form.error-field',['field' => 'incorrect'])
    </div>

    <div class="_flex _justify-start _md:justify-center">
        <button wire:click="submit" type="submit"
                class="button button--main button--normal">{{__('default.Confirm')}}</button>
    </div>
</form>
