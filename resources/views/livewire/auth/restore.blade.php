<div class="section">
    <form wire:submit.prevent="submit">
        <div class="container container--white _mt-lg  _pt-lg _pb-xxl">
            <div class="popup__title _text-center _mt-xl">
                Restore password
            </div>
            <div class="_spacer _spacer--md _md:spacer--lg _px-hg">
                <div class="_cell _cell--12"></div>
                <div class="_flex _flex-column" style="max-width: 400px; margin: auto">
                    <div class="form js-import" data-form>
                        <div class="_flex _flex-column _mb-df">
                            <label class="with-anim-underline">
                                <input wire:model.defer="password" type="password" placeholder="{{__('auth.form.password')}}">
                            </label>
                            @include('components.form.error-field',['field' => 'password'])
                        </div>
                        @include('components.form.error-field',['field' => 'incorrect'])
                    </div>
                </div>
                <div class="_flex _flex-column" style="max-width: 400px; margin: auto">
                    <div class="form js-import" data-form>
                        <div class="_flex _flex-column _mb-df">
                            <label class="with-anim-underline">
                                <input wire:model.defer="password_confirmation" type="password"
                                       placeholder="{{__('auth.form.password_confirmation')}}">
                            </label>
                            @include('components.form.error-field',['field' => 'password_confirmation'])
                        </div>
                        @include('components.form.error-field',['field' => 'incorrect'])
                    </div>
                </div>

                <div class="_flex _justify-center _m-auto _mb-xl">
                    <button class="button button--main button--normal" wire:click="submit">{{__('default.Confirm')}}</button>
                </div>
            </div>
        </div>
    </form>
</div>
