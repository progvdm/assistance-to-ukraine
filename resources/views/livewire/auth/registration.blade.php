<form class="form js-import" data-form wire:submit.prevent="submit">
    <div class="popup__title">{{__('auth.Registration')}}</div>
    <div class="_grid _spacer _spacer--md _md:spacer--lg">
        <div class="_cell _cell--12 _md:cell--6 _mb-sm">
            <div class="input-indicator">
                <div class="popup__sub-title _mb-md">
                    {{__('auth.form.type_user')}}
                </div>
                <div wire:model.defer="organisation.type">
                    <div class="_flex _flex-column _mb-lg">
                        @foreach ($userTypes as $type => $name)
                            <label class="without-underline _flex _flex-row">
                                <input class="_mr-xs _mb-xs _ml-none"
                                       {{ $type_user == $type ? 'checked' :'' }} type="radio" name="organisation.type"
                                       value="{{$type}}">
                                {{$name}}
                            </label>
                        @endforeach
                        @include('components.form.error-field',['field' => 'organisation.type'])
                    </div>
                </div>
            </div>
            <div class="input-indicator">
                <div class="popup__sub-title _mb-md">
                    {{__('auth.Agent')}}
                </div>
                <div class="form__item _flex _flex-column _mb-df">
                    <label class="with-anim-underline" for="user.name">
                        {{__('auth.form.name')}}:
                    </label>
                    <input wire:model.defer="user.name" type="text" id="name">
                    @include('components.form.error-field',['field' => 'user.name'])
                </div>
                <div class="form__item _flex _flex-column _mb-df">
                    <label class="with-anim-underline" for="email">
                        {{__('auth.form.email')}}:
                    </label>
                    <input wire:model.defer="user.email" id="user.email" type="text">
                    @include('components.form.error-field',['field' => 'user.email'])
                </div>
                <div class="form__item _flex _flex-column _mb-df">
                    <label class="with-anim-underline" for="user.phone">
                        {{__('auth.form.phone')}}:
                    </label>
                    <input wire:model.defer="user.phone" id="user.phone" type="text">
                    @include('components.form.error-field',['field' => 'user.phone'])
                </div>
                <div class="form__item _flex _flex-column _mb-df">
                    <label class="with-anim-underline">
                        {{__('auth.form.password')}}:
                    </label>
                    <input wire:model.defer="user.password" id="user.password" type="password">
                    @include('components.form.error-field',['field' => 'user.password'])
                </div>
                <div class="form__item _flex _flex-column _mb-df">
                    <label class="with-anim-underline" for="password_confirmation">
                        {{__('auth.form.password_confirmation')}}:
                    </label>
                    <input wire:model.defer="user.password_confirmation" type="password"
                           id="password_confirmation">
                    @include('components.form.error-field',['field' => 'user.password_confirmation'])
                </div>
            </div>
        </div>
        <div class="_cell _cell--12 _md:cell--6 ">
            <div class="input-indicator">
                <div class="popup__sub-title _mb-md">
                    {{__('auth.General')}}
                </div>
                <div class="_flex _flex-column _mb-md">
                    <label class="_flex _flex-row with-anim-underline">
                        <input wire:model.defer="organisation.id_code" class="_mr-sm" type="text"
                               placeholder="{{__('auth.form.id_code')}}">
                        <div wire:click="substituteCompanyData" class="_cursor-pointer">
                            <svg fill="#000000" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 64 64" width="25px"
                                 height="25px">
                                <path
                                    d="M 24 2.8886719 C 12.365714 2.8886719 2.8886719 12.365723 2.8886719 24 C 2.8886719 35.634277 12.365714 45.111328 24 45.111328 C 29.036552 45.111328 33.664698 43.331333 37.298828 40.373047 L 52.130859 58.953125 C 52.130859 58.953125 55.379484 59.435984 57.396484 57.333984 C 59.427484 55.215984 58.951172 52.134766 58.951172 52.134766 L 40.373047 37.298828 C 43.331332 33.664697 45.111328 29.036548 45.111328 24 C 45.111328 12.365723 35.634286 2.8886719 24 2.8886719 z M 24 7.1113281 C 33.352549 7.1113281 40.888672 14.647457 40.888672 24 C 40.888672 33.352543 33.352549 40.888672 24 40.888672 C 14.647451 40.888672 7.1113281 33.352543 7.1113281 24 C 7.1113281 14.647457 14.647451 7.1113281 24 7.1113281 z"/>
                            </svg>
                        </div>
                    </label>
                    @include('components.form.error-field',['field' => 'organisation.id_code'])
                </div>
                <div class="form__item _flex _flex-column _mb-df">
                    <label class="with-anim-underline" for="organisation.zip_code">
                        {{__('auth.form.zip_code')}}:
                    </label>
                    <input wire:model.lazy="organisation.zip_code" class="_mr-sm" type="text"
                           id="zip_code">
                    @include('components.form.error-field',['field' => 'organisation.zip_code'])
                </div>
                <div class="form__item _flex _flex-column _mb-df">
                    <label class="with-anim-underline" for="organisation.name">
                        {{__('auth.form.Name organisation')}}:
                    </label>
                    <input wire:model.defer="organisation.name" type="text"
                           id="name">
                    @include('components.form.error-field',['field' => 'organisation.name'])
                </div>

            </div>
            <div class="input-indicator">
                <div class="popup__sub-title _mb-md">
                    {{__('auth.Address')}}
                </div>
                <div class="form__item _flex _flex-column _mb-df">
                    <label class="with-anim-underline" for="organisation.country">
                        {{__('auth.form.country')}}:
                    </label>
                    <input wire:model.defer="organisation.country" type="text" id="country">
                    @include('components.form.error-field',['field' => 'organisation.country'])
                </div>
                <div class="form__item _flex _flex-column _mb-df">
                    <label class="with-anim-underline" for="organisation.city">
                        {{__('auth.form.city')}}:
                    </label>
                    <input wire:model.defer="organisation.city" type="text" id="city">
                    @include('components.form.error-field',['field' => 'organisation.city'])
                </div>
                <div class="form__item _flex _flex-column _mb-df">
                    <label class="with-anim-underline" for="organisation.street">
                        {{__('auth.form.street')}}:
                    </label>
                    <input wire:model.defer="organisation.street" type="text" id="street">
                    @include('components.form.error-field',['field' => 'organisation.street'])
                </div>
                <div class="form__item _flex _flex-column _mb-lg">
                    <label class="with-anim-underline" for="organisation.build">
                        {{__('auth.form.build')}}:
                    </label>
                    <input wire:model.defer="organisation.build" type="text" id="build">
                    @include('components.form.error-field',['field' => 'organisation.build'])
                </div>
            </div>
        </div>
        <div class="_cell _cell--12 _md:cell--12 _mb-sm">
            <label class="without-underline _flex _flex-row">
                <input type="checkbox" wire:model.defer="oferta">
                <span class="">*{!!  __('auth.acquainted with the public offer contract', ['url' => "/documents-static/User agreement- " . app()->getLocale().'.pdf?v=3']) !!}</span>
            </label>
            @include('components.form.error-field',['field' => 'oferta'])
        </div>
        <div class="_cell _cell--12 _md:text-center _mb-sm">
                        <span wire:loading>
                            <i class="fa-solid fa-lg fa-spinner fa-spin"></i>
                            Processing...
                       </span>
        </div>
        <div class="_cell _cell--12 _md:text-center ">
            <button wire:click="submit" type="submit"
                    class="button button--main button--normal ">{{__('default.Confirm')}}</button>
        </div>

    </div>
</form>
