<div>
    <div class="title title--size-h1 _mb-md _df:mb-lg">
        {{ $h1 }}
    </div>

    <div class="form js-import _mb-xl" data-form>
        <div class="_grid _spacer _spacer--md _mb-md">
            <div class="_cell _cell--12 _md:cell--3 _mb-df _df:mb-none _flex-column">
                <label class="_flex _flex-row with-anim-underline">
                    <div type="button">
                        @svg('marketplace', 'glass', [21, 19])
                    </div>
                    <input class="_ml-sm bg-unset" type="text" wire:model="search" placeholder="Search">
                </label>
            </div>
        </div>
    </div>

    <div class="grid-masonry">
        @forelse($organisations as $organisation)
            <a href="{{routeLocale($route, ['organisation' => $organisation])}}" class="item" title="{{ $organisation->name }}">
                <span class="title @if(!$organisation->photo && $organisation->fileExists()) title--long @endif">{{ $organisation->name }}</span>
                @if($organisation->photo && $organisation->fileExists())
                    <img src="{{$organisation->getImage()}}" alt="">
                @endif
            </a>
        @empty
            <div>...</div>
        @endforelse
    </div>
</div>
