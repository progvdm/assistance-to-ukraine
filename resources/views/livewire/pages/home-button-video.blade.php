@if(!$hideBlock)
    <div class="video">
        <div class="video__btn">
            <div class="_flex inline-flex _items-center" wire:click="$set('viewVideo', true)">
                <svg width="62" height="62" viewBox="0 0 62 62" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <circle class="circle" cx="31" cy="31" r="30" fill="url(#paint0_linear_6294_896)" fill-opacity="0.6"
                            stroke-width="2"/>
                    <path class="triangle" d="M23.9131 39.957V22.0439L38.9566 31.0005L23.9131 39.957Z" fill="white"/>
                    <defs>
                        <linearGradient id="paint0_linear_6294_896" x1="31" y1="0" x2="31" y2="62"
                                        gradientUnits="userSpaceOnUse">
                            <stop stop-color="#4E82E8"/>
                            <stop offset="1" stop-color="#2457BA"/>
                        </linearGradient>
                    </defs>
                </svg>

                <span>{{ __('pages.How it works') }}</span>
            </div>
        </div>
        @if($viewVideo)
            <div class="popup">
                <div class="popup__wrapper _p-none">
                    <button wire:click="$set('viewVideo', false)" class="popup__close js-import" data-close-popup>
                        <i class="fa-xl fa-solid fa-circle-xmark"></i>
                    </button>

                    <div class="video__block js-import" data-video="play">
                        <video src="{{asset('static/How it works-'. app()->getLocale(). '.MP4')}}" controls
                               autobuffer></video>
                    </div>
                </div>
            </div>
        @endif
    </div>
@endif
