<div class="contact-us">
    <div class="form js-import" data-form>
        <div class="_grid _mb-df">
            <div class="_cell _cell--12 _md:cell--8">
                <div class="_grid _spacer _spacer--md _md:spacer--lg">
                    <div class="_cell _cell--12 _md:cell--6">
                        <label class="with-anim-underline">
                            <select wire:model.defer="categoryId">
                                <option value="1">
                                    {{ __('feedback.Technical support') }}
                                </option>
                                <option value="2">
                                    {{ __('feedback.Asisstance in registration and operational activities on the site') }}
                                </option>
                                <option value="3">
                                    {{ __('feedback.Cooperation with the aidmonitor project') }}
                                </option>
                                <option value="4">
                                    {{ __('feedback.Other questions') }}
                                </option>
                            </select>
                        </label>
                    </div>
                </div>
            </div>
        </div>

        <div class="_grid _mb-lg">
            <div class="_cell _cell--12 _md:cell--8">
                <div class="_grid _spacer _spacer--md _md:spacer--lg">
                    <div class="_cell _cell--12 _md:cell--6 _mb-none">
                        <div class="input-indicator">
                            <div class="form__item _flex _flex-column _mb-df">
                                <label class="with-anim-underline" for="name">
                                   {{ __('feedback.Name') }}:
                                </label>
                                <input wire:model.defer="name" type="text" id="name">
                                @include('components.form.error-field',['field' => 'name'])
                            </div>
                        </div>
                    </div>
                    <div class="_cell _cell--12 _md:cell--6 _mb-none">
                        <div class="input-indicator">
                            <div class="form__item _flex _flex-column _mb-df">
                                <label class="with-anim-underline" for="email">
                                    E-mail:
                                </label>
                                <input wire:model.defer="email" type="text" id="email">
                                @include('components.form.error-field',['field' => 'email'])
                            </div>
                        </div>
                    </div>
                    <div class="_cell _cell--12 _mb-sm">
                        <textarea class="full-width _mb-xs" wire:model.defer="text" name="textarea" rows="10"></textarea>

                        @include('components.form.error-field',['field' => 'text'])
                    </div>
                    <div class="_cell _m-none _cell--6">
                        <label class="with-anim-underline" data-text="{{ __('feedback.Upload file (png, jpg, jpeg, pdf, doc, docx)') }}">
                            <input type="file" accept=".png, .jpg, .jpeg, .pdf, .doc, .docx" wire:model.defer="file" placeholder="{{__('default.form.Choose file')}}" data-text="{{ __('feedback.Upload file (png, jpg, jpeg, pdf, doc, docx)') }}">
                        </label>
                        <div class="_flex _items-center _mt-sm">
                    <span wire:loading wire:target="file">
                        <i class="fa-solid fa-lg fa-spinner fa-spin"></i>
                        Uploading...
                    </span>
                        </div>
                        @if($file)
                            <div class="_flex _items-center _mt-xs _mb-xs">
                                <i class="fa-solid fa-xl fa-file-arrow-up"></i>
                                <span
                                    class="_ml-xs">{{$file->getClientOriginalName() ?? $file}}</span>
                            </div>
                        @endif
                        @include('components.form.error-field',['field' => 'file'])
                    </div>
                </div>
            </div>
        </div>

        <button class="button button--transparent button--normal _mt-xl" wire:loading.attr="disabled" wire:click="submit()">{{__('Send')}}</button>

        @include('livewire.flash-message')
        @include('layouts.preloader', ['target' => 'submit'])
    </div>
</div>
