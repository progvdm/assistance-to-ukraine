<div class="section _mt-xl">
    <div class="container container--white _pb-xl">
        @widget('breadcrumbs')
        <div class="title title--size-h1 _mb-md _df:mb-lg">
            <div class="title title--size-h2">
                {{ __('cargo.Approve') }} - {{$transfer->sender->name}}
            </div>
        </div>

        <div class="section">
            <div class="container">
                <div class="_grid _spacer _spacer--md">
                    <div class="_cell _cell--12 _mt-df">
                        <div class="_grid _spacer _spacer--md _md:spacer--lg _mb-xs _md:mb-sm">
                            <div class="_cell _cell--12 _md:cell--6">
                                <div class="form__table-title _p-xs">
                                    <div class="_grid _spacer _spacer--xs _items-end">
                                        <div class="_cell _cell--1 text-center">Code</div>
                                        <div class="_cell _cell--1 text-center"></div>
                                        <div class="_cell _cell--5">{{__('cargo.products.Name')}}</div>
                                        <div class="_cell _cell--2 _pl-none">{{__('cargo.products.Units')}}</div>
                                        <div class="_cell _cell--2 _pl-none ">{{__('cargo.products.Amount')}}</div>
                                    </div>
                                </div>
                                <div class="form__table-wrapper _p-xs _pr-sm">
                                    @foreach($transfer->productsStart as $index => $product)
                                        <div class="_grid _spacer _spacer--xs">
                                            <div class="_cell _cell--2">
                                               <span>{{$product->code}}</span>
                                            </div>
                                            <div class="_cell _cell--5">{{$product->getName()}}</div>
                                            <div class="_cell _cell--2">
                                                {{ $product->getType() }}
                                            </div>
                                            <div class="_cell _cell--3">
                                                {{ $product->quantity }}
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            </div>

                            @include('livewire.flash-message')
                            <div class="_cell _cell--12">
                                <div class="_flex _justify-between _items-end _m-none">
                                    <div>
                                        @include('livewire.cargo.elements.button-back')
                                        <button class="button button--main button--small" wire:click="submit(true)">
                                            <i class="_mr-xs fa-solid fa-check"></i>
                                            {{__('cargo.Accept')}}</button>
                                        <button class="button button--main button--small" wire:click="submit(false)">
                                            <i class="_mr-xs fa-solid fa-ban"></i>
                                            {{__('cargo.Refuse')}}</button>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


