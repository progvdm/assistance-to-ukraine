<div>
    <div class="_flex _justify-center _m-none _mb-md">
        @if($finish)
            @include('livewire.cargo.elements.button-back')
        @endif
        @if(!$finish)
           <button
               class="button button--main button--small button--uppercase _mr-sm"
               x-data="Alpine.plugins.openModal('popup.verify-sms-popup')"
               @click="open">{{__('default.Sign the document')}}</button>
        @endif
    </div>
        <div class="_flex _justify-center">
            {{-- для работы локально --}}
            {{--                    <iframe src="https://docs.google.com/viewer?url=https://test.aidmonitor.org/storage/2022/08/12/ee32040b12466153ea9ef95b364ceca7fae432bc.pdf&embedded=true"--}}
            {{--                            style="width: 100%; height: 600px;" frameborder="0">Ваш браузер не поддерживает фреймы</iframe>--}}
            <iframe src="https://docs.google.com/viewer?url={{ asset('storage/acts/'.$transferDocument->document) }}&embedded=true"
                    style="width: 100%; height: 600px;" frameborder="0">Your browser does not support frames</iframe>

        </div>
    @if($popupConfirmSms)
        @include('components.cargo.confirmSmsPopup')
    @endif
    @include('livewire.flash-message')
</div>
