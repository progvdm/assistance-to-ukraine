<div class="section" wire:init="checkGeoAndPhotoExists">
    @if(!$confirmGeoAndPhoto and $popupView)
        <button class="_hide"
                x-data="Alpine.plugins.openModal('popup.acceptance-info')"
                x-init="open"
        ></button>
    @endif
    <div class="container">
        <div class="_grid _spacer _spacer--md">
            <div class="_cell _cell--12 _mt-df">
                <div class="_grid _spacer _spacer--md _md:spacer--lg _mb-xs _md:mb-sm">
                    <div class="_cell _cell--12 _md:cell--6">
                        <div class="form__table-title _p-xs">
                            <div class="_grid _spacer _spacer--xs _items-end">
                                <div class="_cell _cell--1 text-center">{{__('cargo.products.code')}}</div>
                                <div class="_cell _cell--1 text-center"></div>
                                <div class="_cell _cell--5">{{__('cargo.products.Name')}}</div>
                                <div class="_cell _cell--2 _pl-none">{{__('cargo.products.Units')}}</div>
                                <div class="_cell _cell--2 _pl-none ">{{__('cargo.products.Amount')}}</div>
                            </div>
                        </div>
                        <div class="form__table-wrapper _p-xs _pr-sm">
                            @foreach($transfer->products as $index => $product)
                                <div class="_grid _spacer _spacer--xs">
                                    <div class="_cell _cell--2">
                                        <span>{{$product->product->code}}</span>
                                    </div>
                                    <div class="_cell _cell--5">{{$product->product->current->first()->name}}</div>
                                    <div class="_cell _cell--2">
                                        {{ $product->product->current->first()->type }}
                                    </div>
                                    <div class="_cell _cell--3">
                                        {{ $product->quantity }}
                                    </div>
                                </div>
                            @endforeach
                        </div>
                        <div class="form js-import" data-form>
                            <div class="form__sub-title _mb-df _mt-md _lg:mt-xl">
                                1. {{__('cargo.form.Photo')}}
                            </div>
                            @foreach($photos as $index => $photo)
                                @include('components.cargo.add-photo', ['index' => $index, 'photo' => $photo])
                            @endforeach

                            @if(end($photos) and isset(end($photos)['photo']) and end($photos)['photo'] != '' and end($photos)['photo']->getClientOriginalName())
                                <div class="_grid _spacer _spacer--lg _mb-df">
                                    <div class="_cell _m-none">
                                        <label class="with-anim-underline">
                                            <button class="add-btn" wire:click="addPhoto">
                                                +{{__('default.Add')}}</button>
                                        </label>
                                    </div>
                                </div>
                            @endif
                        </div>
                    </div>
                    <div class="_cell _cell--12 _md:cell--6">
                        <div class="form js-import" data-form>
                            <div class="form__sub-title _mb-df">
                                2. {{__('cargo.form.Video')}}
                            </div>
                            @foreach($videos as $index => $video)
                                @include('components.cargo.add-video', ['index' => $index, 'video' => $video])
                            @endforeach
                            @if(end($videos) and isset(end($videos)['photo']) and end($videos)['photo'] != '' and end($videos)['photo']->getClientOriginalName())
                                <div class="_grid _spacer _spacer--lg _mb-df">
                                    <div class="_cell _m-none">
                                        <label class="with-anim-underline">
                                            <button class="add-btn" type="button" wire:click="addVideo">
                                                +{{__('default.Add')}}</button>
                                        </label>
                                    </div>
                                </div>
                            @endif


                            <div class="form__sub-title _mb-df _mt-md _lg:mt-xl">
                                3. {{__('cargo.form.Documents')}}
                            </div>
                            @foreach($documents as $index => $document)
                                @include('components.cargo.add-document', ['index' => $index, 'document' => $document])
                            @endforeach
                            @if(end($documents) and isset(end($documents)['photo']) and end($documents)['photo'] != '' and end($documents)['photo']->getClientOriginalName())
                                <div class="_grid _spacer _spacer--lg _mb-df">
                                    <div class="_cell _m-none">
                                        <label class="with-anim-underline">
                                            <button class="add-btn" wire:click="addDocuments">
                                                +{{__('default.Add')}}</button>
                                        </label>
                                    </div>
                                </div>
                            @endif
                        </div>
                    </div>
                    @include('livewire.flash-message')
                    @include('livewire.cargo.elements.confirm-sms')
                    <div class="_cell _cell--12">
                        <div class="_flex _justify-start _items-end _m-none _mb-md">
                            @include('livewire.cargo.elements.button-back')
                            @if(!isset($finish) or (isset($finish) and $finish == false))
                                <button x-data="Alpine.plugins.openModal('popup.verify-sms-popup')"
                                        @click="open"
                                        class="button button--main button--small button--uppercase _mr-sm _ml-auto" {{ ((isset($confirmButton) and $confirmButton == true) or !isset($confirmButton)) ? '' : 'disabled' }} {{ ((isset($confirmSms) and $confirmSms !=0) or (!isset($confirmSms)) or (isset($isPeople) and $isPeople) ) ? '' : 'disabled' }}>{{__('default.Confirm')}}</button>
                            @endif
                            @if($transferId)
                                <a href="{{route('cabinet.download-act-accept', $transferId)}}" target="_blank"
                                   class="button button--transparent button--small button--uppercase _mr-sm">
                                    <i class="_mr-xs fa-solid fa-arrow-down"></i>
                                    {{__('default.Download act')}}
                                </a>
                            @endif
                        </div>

                    </div>

                    {{-- @if($popupConfirmSms)
                         @include('components.cargo.confirmSmsPopup')
                     @endif--}}
                </div>
            </div>
        </div>
    </div>
        @include('layouts.preloader')
</div>

