<div class="_cell _cell--12 _md:cell--6">
    <div class="_flex _flex-column _mb-df">
        <label class="_flex _flex-row _justify-between _items-start">
            <input {{ auth()->user()->phone ? '' : 'disabled' }}  {{ (isset($finish) and $finish) ? 'disabled' : '' }} type="checkbox" wire:model.defer="confirmSms" wire:change="setConfirmSms($event.target.checked)">
            <span class="form__checkbox-text _mr-df">*{{ __('cargo.form.confirmSms') }} </span>
        </label>
        @include('components.form.error-field',['field' => 'confirmSms'])
    </div>
</div>
