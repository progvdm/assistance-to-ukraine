<a href="{{ route('cabinet.home') }}"
   class="button button--transparent button--small">

    <i class="_mr-xs fa-solid fa-arrow-left"></i>
    {{ __('default.Back') }}
</a>

