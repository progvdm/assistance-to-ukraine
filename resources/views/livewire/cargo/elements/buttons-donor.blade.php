<div class="_flex _justify-between _items-end _m-none">
    <div>
        @include('livewire.cargo.elements.button-back')

        @if($transferId and $transfer->status_list and $transfer->status == \App\Enums\Cargo\CargoStatusesEnum::APPROVED)
            <button class="button button--main button--small" wire:click="sendDonate">{{__('Send')}}</button>
        @endif
        @if($transferId and $transfer->status != \App\Enums\Cargo\CargoStatusesEnum::SEND_APPROVE and !$transfer->status_list)
            <button class="button button--main button--small" wire:click="sendApprove">{{__('cargo.Send the list for approval')}}</button>
        @endif

        @if($transferId and $transfer->status == \App\Enums\Cargo\CargoStatusesEnum::SEND_CARGO)
            <a href="{{route('cabinet.download-act', $transferId)}}" target="_blank"
               class="button button--transparent button--small">
                <i class="_mr-xs fa-solid fa-arrow-down"></i>
                {{__('default.Download act')}}
            </a>
            <a href="{{route('cabinet.cargo-qr-code', $transferId)}}" target="_blank"
               class="button button--transparent button--small">
                <i class="_mr-xs fa-solid fa-print"></i>
                {{__('default.Print QR code')}}
            </a>
        @endif
    </div>

    @if(!$transferId or ($transferId and ($transfer->status == \App\Enums\Cargo\CargoStatusesEnum::NEW or $transfer->status == \App\Enums\Cargo\CargoStatusesEnum::NOT_APPROVED)))
        <button class="button button--main button--small button--uppercase" {{ ((isset($confirmSms) and $confirmSms !=0) or (!isset($confirmSms)) or (isset($isPeople) and $isPeople) ) ? '' : 'disabled' }} wire:click="submit">
            <i class="_mr-xs fa-solid fa-check"></i>
            {{__('default.Save')}}</button>
    @endif
</div>
