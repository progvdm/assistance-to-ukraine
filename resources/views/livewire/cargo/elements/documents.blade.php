<div class="form__sub-title _mb-df _mt-md _lg:mt-xl">
    {{ $index ? $index : '5' }}. {{__('cargo.form.Documents')}}
</div>
@if(isset($documentsSaved))
    @foreach($documentsSaved as $document)
    <div class="_cell _cell--1 _ml-auto">
        <div  wire:click="deleteFile({{$document['id']}}, 'document')" class="form__delete" title="Delete">
           <svg width="15" height="15" viewBox="0 0 34 31" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path class="cl1"
                      d="M32.7328 15.5C32.7328 23.761 25.6845 30.5 16.9367 30.5C8.18887 30.5 1.14062 23.761 1.14062 15.5C1.14062 7.23903 8.18887 0.5 16.9367 0.5C25.6845 0.5 32.7328 7.23903 32.7328 15.5Z"
                      stroke="#404040"></path>
                <path fill-rule="evenodd" clip-rule="evenodd"
                      d="M10.6734 22.2543L24.2172 9.3721L23.4194 8.61329L9.87559 21.4955L10.6734 22.2543Z"
                      fill="white"></path>
                <path fill-rule="evenodd" clip-rule="evenodd"
                      d="M23.2797 22.2543L9.73596 9.3721L10.5337 8.61329L24.0775 21.4955L23.2797 22.2543Z"
                      fill="white"></path>
            </svg>
        </div>
    </div>
    <div class="_flex _items-center _mt-xs _mb-xs">
        <a href="{{$document['url']}}" target="_blank">
            <i class="fa-solid fa-lg fa-file-arrow-up"></i>
            <span class="_ml-xs">{{ $document['name'] }}</span>
        </a>
    </div>
@endforeach
@endif
@foreach($documents as $index => $document)
        <div class="_grid _text-right  _nmr-xs _nmt-sm" style="{{ $document['document'] ? '' : 'display: none' }}">
            <div class="_cell _cell--1 _ml-auto">
                <div wire:click="delete('{{$index}}','documents')" class="form__delete" title="Delete">
                    <svg width="15" height="15" viewBox="0 0 34 31" fill="none"
                         xmlns="http://www.w3.org/2000/svg">
                        <path class="cl1"
                              d="M32.7328 15.5C32.7328 23.761 25.6845 30.5 16.9367 30.5C8.18887 30.5 1.14062 23.761 1.14062 15.5C1.14062 7.23903 8.18887 0.5 16.9367 0.5C25.6845 0.5 32.7328 7.23903 32.7328 15.5Z"
                              stroke="#404040"></path>
                        <path fill-rule="evenodd" clip-rule="evenodd"
                              d="M10.6734 22.2543L24.2172 9.3721L23.4194 8.61329L9.87559 21.4955L10.6734 22.2543Z"
                              fill="white"></path>
                        <path fill-rule="evenodd" clip-rule="evenodd"
                              d="M23.2797 22.2543L9.73596 9.3721L10.5337 8.61329L24.0775 21.4955L23.2797 22.2543Z"
                              fill="white"></path>
                    </svg>
                </div>
            </div>
        </div>
    @include('components.cargo.add-document', ['index' => $index, 'document' => $document])
@endforeach
@if(end($documents) and isset(end($documents)['document']) and end($documents)['document'] != '' and end($documents)['document']->getClientOriginalName())
    <div class="_grid _spacer _spacer--lg _mb-df">
        <div class="_cell _m-none">
            <label class="with-anim-underline">
                <button class="add-btn" wire:click="addDocument">+{{__('default.Add')}}</button>
            </label>
        </div>
    </div>
@endif
