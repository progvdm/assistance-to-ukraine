<div class="form__sub-title _mb-df">
    1. {{__('cargo.form.Send to')}}
</div>

<div class="_grid _spacer _spacer--lg _mb-df">
    <div class="_cell _cell--12 _m-none">
        <label class="with-anim-underline">
            <select wire:model.defer="recipient" {{ $finish ? 'disabled' : '' }}>
                <option value="0">
                    {{__('cargo.form.recipient')}}
                </option>
                @foreach ($recipients as $value)
                    <option value="{{$value['id']}}">{{$value['name']}}</option>
                @endforeach
            </select>
        </label>
        @include('components.form.error-field',['field' => 'recipient'])
    </div>
</div>
