<div class="_flex _justify-between _items-end">
   <div>
       @include('livewire.cargo.elements.button-back')

       @if($transferId)
           <a href="{{route('cabinet.download-act', $transferId)}}" target="_blank"
              class="button button--transparent button--small">
               <i class="_mr-xs fa-solid fa-arrow-down"></i>
               {{__('default.Download act')}}
           </a>
           <a href="{{route('cabinet.cargo-qr-code', $transferId)}}" target="_blank"
              class="button button--transparent button--small">
               <i class="_mr-xs fa-solid fa-print"></i>
               {{__('default.Print QR code')}}
           </a>
       @endif
   </div>

    @if(!isset($finish) or (isset($finish) and $finish == false))
        <button class="button button--main button--small" {{ ((isset($confirmButton) and $confirmButton == true) or !isset($confirmButton)) ? '' : 'disabled' }} {{ ((isset($confirmSms) and $confirmSms !=0) or (!isset($confirmSms)) or (isset($isPeople) and $isPeople) ) ? '' : 'disabled' }} wire:click="submit">
            <i class="_mr-xs fa-solid fa-check"></i>
            {{__('default.Confirm')}}
        </button>
    @endif
</div>
