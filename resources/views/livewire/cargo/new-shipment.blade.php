<div>
    @if(!$isPeople)
        @include('livewire.cargo.elements.confirm-sms')
    @endif
    <div class="_grid _spacer _spacer--md _md:spacer--lg _mb-xs _md:mb-sm">
        <div class="_cell _cell--12 _lg:cell--6">
            <div class="form js-import" data-form>
                @if(!$isPeople)
                    @include('livewire.cargo.elements.address')
                @endif
                <div class="form__sub-title _mb-df">
                    {{ $isPeople ? '1' :'2' }}. {{__('cargo.form.Products')}}
                </div>
                @include('components.cargo.add-product-in-warehouse')
            </div>
        </div>

        <div class="_cell _cell--12 _lg:cell--6">
            <div class="form js-import" data-form>
                @include('livewire.cargo.elements.photos', ['index'=> $isPeople ? '2' : '3'])
                @include('livewire.cargo.elements.videos', ['index'=> $isPeople ? '3' : '4'])
                @include('livewire.cargo.elements.documents', ['index'=> $isPeople ? '4' : '5'])

                <div class="form__sub-title _mb-df _mt-md _lg:mt-xl">
                    {{ $isPeople ? '5' : '6' }}. {{ __('cargo.Additional product description:') }}
                </div>
                <div class="_cell _cell--12 _mb-none">
                    <div class="input-indicator">
                        <div>
                            <label class="without-underline">
                                <textarea class="" wire:model.defer="additionalInfo.description_product" type="text"
                                          placeholder="{{ __('cargo.Additional product description:') }}"
                                          rows="7"></textarea>
                            </label>
                            @include('components.form.error-field',['field' => 'additionalInfo.description_product'])
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="_cell _cell--12">
            <hr class="hr-line">
        </div>

        @if($isPeople)
            <div class="_cell _cell--12 _lg:cell--6">
                <div class="js-import" data-form>
                    <label class="">
                        <input class="" type="checkbox" wire:model="additionalInfo.isOrganisation" {{ (isset($finish) and $finish) ? 'disabled' : '' }}>
                        <span class="form__sub-title">{{__('cargo.human_form.Organisation')}}</span>
                    </label>
                </div>
            </div>
        @endif
        <div class="_cell _cell--12">
            @if($isPeople)
                @if($additionalInfo and isset($additionalInfo['isOrganisation']) and $additionalInfo['isOrganisation'])
                    @include('components.cargo.donate-organisation-form')
                @else
                    @include('components.cargo.donate-human-form')
                @endif
            @endif
        </div>

        @include('livewire.flash-message')
        <div class="_cell _cell--12 _mb-none">
            @include('livewire.cargo.elements.buttons')
        </div>

        @if($popupConfirmSms and !$isPeople)
            <button wire:key="{{ rand() }}" class="_hide"
                    x-data="Alpine.plugins.openModal('popup.verify-sms-popup')"
                    x-init="open"
            ></button>
        @endif
    </div>
        @include('layouts.preloader')
</div>
