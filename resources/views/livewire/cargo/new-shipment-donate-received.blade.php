<div>
    <div class="title title--instruction _mb-df">
        {!! __('cargo.instruction') !!}
    </div>

    <div class="_grid _spacer _spacer--md _md:spacer--lg _mb-xs _md:mb-sm">
        <div class="_cell _cell--12 _md:cell--6">
            <div class="form js-import" data-form>
                @include('components.cargo.donors')
                @include('livewire.cargo.elements.address', ['finish' => true])

                <div class="form__sub-title _mb-df _mt-md _lg:mt-xl">
                    2. {{__('cargo.form.Products')}}
                </div>
                @foreach($formProducts as $index => $product)
                    @if($index > 0)
                        <div class="_grid _text-right  _nmr-xs _nmt-sm">
                            <div class="_cell _cell--1 _ml-auto">
                                <div wire:click="delete('{{$index}}','formProducts')" class="form__delete"
                                     title="Delete">
                                    <svg width="15" height="15" viewBox="0 0 34 31" fill="none"
                                         xmlns="http://www.w3.org/2000/svg">
                                        <path class="cl1"
                                              d="M32.7328 15.5C32.7328 23.761 25.6845 30.5 16.9367 30.5C8.18887 30.5 1.14062 23.761 1.14062 15.5C1.14062 7.23903 8.18887 0.5 16.9367 0.5C25.6845 0.5 32.7328 7.23903 32.7328 15.5Z"
                                              stroke="#404040"></path>
                                        <path fill-rule="evenodd" clip-rule="evenodd"
                                              d="M10.6734 22.2543L24.2172 9.3721L23.4194 8.61329L9.87559 21.4955L10.6734 22.2543Z"
                                              fill="white"></path>
                                        <path fill-rule="evenodd" clip-rule="evenodd"
                                              d="M23.2797 22.2543L9.73596 9.3721L10.5337 8.61329L24.0775 21.4955L23.2797 22.2543Z"
                                              fill="white"></path>
                                    </svg>
                                </div>
                            </div>
                        </div>
                    @endif
                    @include('components.cargo.add-product', ['index' => $index, 'product' => $product])
                @endforeach
                <div class="_grid _spacer _spacer--lg _mb-df">
                    <div class="_cell _m-none">
                        <label class="with-anim-underline">
                            <button type="button" class="add-btn" wire:click="addProduct">
                                + {{__('default.Add')}}</button>
                        </label>
                    </div>
                </div>
            </div>
        </div>

        <div class="_cell _cell--12 _md:cell--6">
            <div class="form js-import" data-form>
                @include('livewire.cargo.elements.photos')
                @include('livewire.cargo.elements.videos')
                @include('livewire.cargo.elements.documents')
            </div>
        </div>
        @include('livewire.flash-message')

        <div class="_cell _cell--12">
            @include('livewire.cargo.elements.buttons')
        </div>
    </div>
    @include('layouts.preloader')
</div>
