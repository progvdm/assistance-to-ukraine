<div>
    <a wire:click="$set('showPopup', true)">
        {{$name}}
    </a>
    @if($showPopup)
        @include('components.cargo.popupListProducts')
    @endif
</div>
