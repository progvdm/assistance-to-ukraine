<div>
    <div class="popup__header">
        <div class="_flex _justify-between _items-center">
            <div>
                {{__('cargo.history.Documents')}}:
            </div>
        </div>
    </div>
    <div class="popup__body _mb-md _mt-md">
        <div class="_flex _flex-column _mb-md js-import">
        <div class="_grid _spacer _spacer--md _items-center">
            <div class="_cell  @if($confirmPopupData['human']['file']) _cell--6 @else _cell--12 @endif ellipsis-text _justify-start" style="background-color: white">
                <div class="_flex">
                    <img class="_mr-xs" src="/images/svg/pdf-file.svg"
                         alt="" width="35" height="">
                    <div>
                        <a class="popup__file-link"
                           href="{{asset('storage/acts/'.$confirmPopupData['transfer']['document'])}}"
                           target="_blank" title="{{ $confirmPopupData['transfer']['document'] }}">
                            {{ 1 }}. {{ $confirmPopupData['transfer']['document'] }}
                        </a>
                        <div
                            class="popup__file-time _mt-xs">{{ \Carbon\Carbon::parse($confirmPopupData['transfer']['created_at'])->format('d.m.Y') }}</div>
                    </div>
                </div>
            </div>
            @if($confirmPopupData['human']['file'])
                <div class="_cell _cell--6 ellipsis-text _justify-start" style="background-color: white">
                    <div class="_flex">
                        <img class="_mr-xs" src="/images/svg/doc-file.svg"
                             alt="" width="35" height="">
                        <div>
                            <a class="popup__file-link"
                               href="{{asset('storage/humans/'.$confirmPopupData['human']['file'])}}"
                               target="_blank"
                               title="{{ str_replace('humans/','',$confirmPopupData['human']['file']) }}">
                                {{ 2 }}
                                . {{ str_replace('humans/','',$confirmPopupData['human']['file']) }}
                            </a>
                            <div
                                class="popup__file-time _mt-xs">{{ \Carbon\Carbon::parse($confirmPopupData['human']['created_at'])->format('d.m.Y') }}</div>
                        </div>
                    </div>
                </div>
            @endif
        </div>
    </div>

    @if(!$confirmPopupData['human']['file'] and $confirmPopupData['uploadAccess'])
        <div class="form _mt-df js-import _mb-md" data-form>
            <div class="_grid _spacer _spacer--lg _mb-xs">
                <div class="_cell _cell--12 _m-none">
                    <label class="with-anim-underline" data-text="{{ __('Download file') }}">
                        <input type="file" wire:model.defer="file"
                               placeholder="{{__('default.form.Choose img')}}"
                               data-text="{{ __('Download file') }}" accept=".png, .jpg, .jpeg, .pdf" onchange="if(this.files[0].size > '5000000'){ event.stopImmediatePropagation(); livewire.emit('addErrorEvent') }">
                    </label>
                    @include('components.form.error-field',['field' => 'file'])

                    <div class="_flex _items-center _mt-sm">
                        <span wire:loading wire:target="file">
                            <i class="fa-solid fa-lg fa-spinner fa-spin"></i>
                            Uploading...
                        </span>
                    </div>
                    @if(isset($file) and $file)
                        <div class="_flex _items-center _mt-xs _mb-xs">
                            <i class="fa-solid fa-lg fa-file-arrow-up"></i>
                            <span class="_ml-xs">{{$file->getClientOriginalName()}}</span>
                        </div>
                    @endif
                </div>
            </div>
            <button class="button button--main button--small button--uppercase _mr-sm"
                    wire:click="submit"
                    @if(!isset($file) or !$file) disabled="disabled" @endif>{{__('default.Confirm')}}</button>

        </div>
    @endif
    </div>
</div>
