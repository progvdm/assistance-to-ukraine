<div>
    <div class="popup__header">
        <div class="_flex _justify-between _items-center">
            <div>
                {{__('cargo.history.Products')}}:
            </div>
        </div>
    </div>
    <div class="recipient__list recipient__list--right _m-none _xs:m-md">
        <div class="_grid">
            <div class="_cell
                    _pl-xs _pr-xs _cell--9">{{__('dashboard.Name of products')}}</div>
            <div class="_cell _cell--3 _text-center">{{__('dashboard.Taken')}}</div>
        </div>
        @foreach($transferProducts as $transferProduct)
            <div class="_grid">
                <div class="_cell _cell--9 _pl-xs _pr-xs">
                    {{$transferProduct->product->current->first()->name}}
                </div>
                <div class="_cell _cell--3 _text-center">{{$transferProduct->quantity}}</div>
            </div>
        @endforeach
    </div>
</div>
