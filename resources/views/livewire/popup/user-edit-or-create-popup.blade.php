<div >
    <button wire:click="closePopup" class="popup__close js-import" data-close-popup>
        <i class="fa-xl fa-solid fa-circle-xmark"></i>
    </button>
    <div class="form _mt-df js-import" data-form>
        <div class="form__item _mt-df">
            <label class="with-anim-underline" for="userData.name">
                {{__('auth.form.name')}}:
            </label>
            <input wire:model="userData.name" id="userData.name" type="text">
            @include('components.form.error-field',['field' => 'userData.name'])
        </div>
        <div class="form__item _mt-df">
            <label class="with-anim-underline" for="userData.email">
                {{__('auth.form.email')}}:
            </label>
            <input wire:model="userData.email" id="userData.email" type="text">
            @include('components.form.error-field',['field' => 'userData.email'])
        </div>
        <div class="form__item _mt-df">
            <label class="with-anim-underline" for="userData.phone">
                {{__('auth.form.phone')}}:
            </label>

            <input wire:model="userData.phone" id="userData.phone" type="text" placeholder="">
            @include('components.form.error-field',['field' => 'userData.phone'])
        </div>
        <div class="_flex _items-center _mt-sm">
                        <span wire:loading wire:target="createUser, editUser">
                            <i class="fa-solid fa-lg fa-spinner fa-spin"></i>
                            Progress...
                        </span>
        </div>
        <div class="_flex _m-auto _mt-lg">
            <button class="button button--main button--small"
                    wire:click="{{isset($userData['id']) ? 'editUser('.$userData['id'].')' : 'createUser()'}}">
                <i class="_mr-xs fa-solid fa-check"></i>
                {{__('default.Confirm')}}
            </button>
        </div>
    </div>
</div>
