<div>
    <div class="popup__header">
        <div class="_flex _justify-between _items-center">
            <div>
                {{__('cargo.history.Photo')}}
                <button wire:click="$set('openFolderId', 0)"
                        class="button button--main button--small button--small--xs _ml-sm _pr-none" {{ $openFolderId == 0 ? 'disabled' : '' }}>
                    <svg width="22" height="16" viewBox="0 0 22 16" fill="none"
                         xmlns="http://www.w3.org/2000/svg">
                        <path
                            d="M0.292893 7.29289C-0.0976314 7.68342 -0.0976315 8.31658 0.292892 8.7071L6.65685 15.0711C7.04738 15.4616 7.68054 15.4616 8.07107 15.0711C8.46159 14.6805 8.46159 14.0474 8.07107 13.6569L2.41421 8L8.07107 2.34314C8.46159 1.95262 8.46159 1.31946 8.07107 0.928931C7.68054 0.538407 7.04738 0.538406 6.65686 0.928931L0.292893 7.29289ZM22 7L1 7L1 9L22 9L22 7Z"
                            fill="white"/>
                    </svg>
                </button>
            </div>
        </div>
    </div>
    <div class="popup__body js-import" data-mfp="gallery">
        <div class="_flex _flex-wrap _mb-md _nml-sm _nmr-sm">
            @foreach($folders as $folder)
                <div class="popup__folder" wire:click="$set('openFolderId', {{$folder->recipient_id}})">
                    <svg class="_mb-xs" width="77" height="64" viewBox="0 0 77 64" fill="none"
                         xmlns="http://www.w3.org/2000/svg">
                        <path d="M0.5 4V15.5L43 15L42 12.5L41.5 4.5L40.5 1.5L37.5 0.5H4.5L2 1.5L0.5 4Z"
                              fill="#F6F2E5"/>
                        <path
                            d="M1 15.2H0.7V15.5V59.5V59.5831L0.742752 59.6544L2.24275 62.1544L2.2877 62.2293L2.36584 62.2683L4.36584 63.2683L4.42918 63.3H4.5H72.5H72.5797L72.6488 63.2605L76.1488 61.2605L76.3 61.1741V61V18V17.8456L76.1744 17.7559L72.6744 15.2559L72.5961 15.2H72.5H1Z"
                            fill="url(#paint0_linear_6518_3128)" stroke="#D1D1D1" stroke-width="0.6"/>
                        <path
                            d="M0.5 5C0.5 2.51472 2.51472 0.5 5 0.5H37.2561C39.7414 0.5 41.7561 2.51472 41.7561 5V12.2353C41.7561 14.0658 43.2397 15.5588 45.0732 15.5588H72C74.4853 15.5588 76.5 17.5735 76.5 20.0588V59C76.5 61.4853 74.4853 63.5 72 63.5H5C2.51472 63.5 0.5 61.4853 0.5 59V5Z"
                            stroke="#878787"/>
                        <defs>
                            <linearGradient id="paint0_linear_6518_3128" x1="38.5" y1="16.5" x2="38.5"
                                            y2="63" gradientUnits="userSpaceOnUse">
                                <stop stop-color="#FDFCF4"/>
                                <stop offset="1" stop-color="#EBE5D2"/>
                            </linearGradient>
                        </defs>
                    </svg>
                    <div>
                        {{ $folder->name }}
                    </div>
                </div>
            @endforeach
        </div>
        <div class="_grid _spacer _spacer--md _items-center">
            @foreach($photos as $photo)
                <div class="_cell _cell--4">
                    <a class="tab__item tab__item--small" target="_blank" href="{{$photo->url}}"
                       data-description="" style="background-image: url('{{$photo->url}}')">
                    </a>
                </div>
            @endforeach
        </div>
    </div>
    <style>
        .mfp-with-zoom .mfp-container,
        .mfp-with-zoom.mfp-bg {
            opacity: 0;
            -webkit-backface-visibility: hidden;
            /* ideally, transition speed should match zoom duration */
            -webkit-transition: all 0.3s ease-out;
            -moz-transition: all 0.3s ease-out;
            -o-transition: all 0.3s ease-out;
            transition: all 0.3s ease-out;
        }

        .mfp-with-zoom.mfp-ready .mfp-container {
            opacity: 1;
        }

        .mfp-with-zoom.mfp-ready.mfp-bg {
            opacity: 0.8;
        }

        .mfp-with-zoom.mfp-removing .mfp-container,
        .mfp-with-zoom.mfp-removing.mfp-bg {
            opacity: 0;
        }
    </style>
</div>


