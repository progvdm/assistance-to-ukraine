<div>
    @if($thank_you)
        <div class="form js-import _pt-df _px-lg">
            <div class="popup__title _mx-md">{{ __('aid-request.popup.Thank you!') }}</div>
            <div
                class="popup__sub-title _text-center _mb-df">{{ __('aid-request.popup.Your application has been sent') }}</div>
            <div class="_grid _spacer _spacer--md _md:spacer--lg">
                <div class="_cell _cell--12 _md:text-center">
                    <button class="button button--main button--normal " type="button" x-el:button-close hidden
                            @click="close($event)">
                        Ok
                    </button>
                </div>
            </div>
        </div>
    @else
        <div class="form js-import _pt-df _px-lg">
            <div
                class="popup__title">{{ __('aid-request.popup.To receive information you need:') }}</div>
            <div class="_grid _spacer _spacer--md _md:spacer--df" wire:Loading.class="disabled">
                <div class="_cell _cell--12">
                    <div class="popup__text">
                        1.<span class="_cursor-pointer" x-data="Alpine.plugins.openModal('auth.login')"
                                @click="open">
                            <u>{{ __('aid-request.popup.Log in') }}</u>
                        </span>
                        {{ __('aid-request.popup.or') }}
                        <span class="_cursor-pointer" x-data="Alpine.plugins.openModal('auth.registration')"
                              @click="open">
                            <u>{{ __('aid-request.popup.register') }}</u>
                        </span>
                        {{ __('aid-request.popup.on this web platform') }}
                    </div>
                </div>
                <div class="_cell _cell--12">
                    <div class="popup__text">
                        2. {{ __('aid-request.popup.Or fill out the following form:') }}
                    </div>
                </div>
                <div class="_cell _cell--12">
                    <div class="_flex _flex-column">
                        <label class="with-anim-underline">
                            <input wire:model.defer="name" type="text" placeholder="*{{ __('aid-request.popup.Name of the applicant') }}">
                        </label>
                        @include('components.form.error-field',['field' => 'name'])
                    </div>
                </div>
                <div class="_cell _cell--12">
                    <div class="_flex _flex-column">
                        <label class="with-anim-underline">
                            <input wire:model.defer="name_organisation" type="text" placeholder="{{ __('aid-request.popup.Name of the organization') }}">
                        </label>
                        @include('components.form.error-field',['field' => 'name_organisation'])
                    </div>
                </div>
                <div class="_cell _cell--12">
                    <div class="_flex _flex-column">
                        <label class="with-anim-underline">
                            <input wire:model.defer="email" type="text" placeholder="*E-mail">
                        </label>
                        @include('components.form.error-field',['field' => 'email'])
                    </div>
                </div>
                <div class="_cell _cell--12">
                    <label class="without-underline">
                                        <textarea type="text" wire:model.defer="message"
                                                  placeholder="{{ __('aid-request.popup.Please describe what you need') }}"
                                                  rows="10"></textarea>
                    </label>
                    @include('components.form.error-field',['field' => 'message'])

                </div>
                <div class="_cell _cell--12 _md:text-center">
                    <button type="button"
                            wire:click="sendLeed()"
                            class="button button--main button--normal">{{__('aid-request.popup.Confirm')}}</button>
                </div>
            </div>
        </div>
    @endif
</div>
