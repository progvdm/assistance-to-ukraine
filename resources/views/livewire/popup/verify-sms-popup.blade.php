<div>
    <form class="form js-import _pt-df" data-form wire:submit.prevent="submit">
        <div class="popup__title">{{ __('popup.Sms verification') }}</div>
        <div class="_grid _spacer _spacer--md _md:spacer--lg _mb-df">
            <div class="_cell _cell--12 _mb-df _pb-df">
                <div class="_flex _flex-column">
                    <label class="with-anim-underline">
                        <input type="number" wire:model="sms" name="sms" placeholder="{{__('popup.Sms code')}}:">
                    </label>
                    @include('components.form.error-field',['field' => 'sms'])
                </div>
            </div>
            <div class="_cell _cell--12 _text-center">
                @if(!$reSendSms)
                    <button class="button button--small button--transparent" wire:click="reSendSms">
                        {{__('popup.Resend sms')}}
                    </button>
                @else
                    <span class="text--italic _mt-xs">{{ __('popup.SMS sent!') }}</span>
                @endif
            </div>
            <div class="_cell _cell--12 _text-center">
                @if($telegram)
                    <span class="text--italic _mt-xs">{{ __('popup.Receive SMS in Telegram') }}</span>
                    <a class="text--color-yellow text--weight-bold"
                       href="https://t.me/aid_monitor_bot?start={{ $telegramToken }}" target="_blank"
                       style="font-size: 16px;">
                        @aid_monitor_bot
                    </a>
                @endif
            </div>
            <div class="_cell _cell--12 _md:text-center">
                <button type="submit" class="button button--main button--normal ">{{__('default.Confirm')}}</button>
            </div>
        </div>
    </form>
</div>


