<div>
    <div class="_flex _justify-between _items-center _mb-df">
        <div class="title title--size-h2">
            {{__('dashboard.Dashboard')}} - {{ Organisation::getOrganisationName()}}
        </div>
    </div>

    <div class="_flex _justify-between _items-center _mb-df">
        <div class="title title--size-h6">
            {{ Organisation::getAuthUser()->name }}
        </div>
    </div>

    <div class="_cell _cell--12 _mb-xl">
        <div class=" _mb-df">
            @include('components.cabinet.sidebar')
        </div>
    </div>
    @if(Organisation::isRecipient())
        <div class="_cell _cell--12 _mb-md">
            <div class="tabs">
                <div class="_flex _mt-xs">
                    <span class="tabs__item {{ $typeTransfer == 'accepted' ? 'is-active' : '' }}" wire:click="changeTab('accepted')">
                        {{__('dashboard.Aid accepted')}}
                    </span>
                    <span class="tabs__item {{ $typeTransfer == 'handed_over' ? 'is-active' : '' }}" wire:click="changeTab('handed_over')">
                        {{__('dashboard.Aid given')}}
                    </span>
                    <span class="tabs__item {{ $typeTransfer == 'provided' ? 'is-active' : '' }}" wire:click="changeTab('provided')">
                        {{__('dashboard.Provided to final beneficiaries')}}
                    </span>
                </div>
            </div>
        </div>
    @endif

    <div class="form__table-title--big _mb-md">
        <div class="_grid">
            <div class="_cell _cell--3 text--shadow">
                @if(Organisation::isDonor())
                    {{__('dashboard.First recipient of assistance')}}:
                @else
                    @if($typeTransfer == 'handed_over')
                        {{__('dashboard.Passed on')}}
                    @elseif($typeTransfer == 'provided')
                        {{__('dashboard.We have provided assistance')}}
                    @else
                        {{__('dashboard.Received from')}}

                    @endif
                @endif
            </div>
            <div class="_cell _cell--5 text--shadow">
                @if(Organisation::isDonor() or $typeTransfer == 'handed_over' or $typeTransfer == 'provided')
                    {{__('dashboard.Composition of humanitarian aid')}}:
                @else
                    {{__('dashboard.Assistance received')}}
                @endif
            </div>
            <div class="_cell _cell--4 text--shadow">
                @if(Organisation::isDonor())
                    {{__('dashboard.Assistance is transferred from the first recipient to the following')}}
                @elseif($typeTransfer == 'provided')
                @else
                    {{__('dashboard.Assistance provided to')}}:
                @endif
            </div>
        </div>
    </div>

    <div class="js-import" data-recipient-table>
        @foreach($transfers as $item)
            @livewire('cabinet.dashboard-item', ['transfer' => $item, 'typeTransfer' => $typeTransfer, 'numList' => $transfers->total() - $loop->index - (($transfers->currentPage() > 1) ? (($transfers->currentPage()-1)*12) : 0)], key($item->id))
        @endforeach
    </div>
    {{ $transfers->links() }}

    <div class="delivery__files">
        @if($popupVideo)
            @include('components.cargo.transfer-videos', ['videos' => $transferInfo['videos']])
        @endif
    </div>
    @include('layouts.preloader', ['target' => 'gotoPage, nextPage, previousPage, changeTab'])
    <div class="_mt-xl">
        @widget('color-info')
    </div>
    @widget('social-icons')
    @include('livewire.flash-message')
</div>
