@if($open)
    @include('components.dashboard.open-row')
@else
    @include('components.dashboard.close-row')
@endif
