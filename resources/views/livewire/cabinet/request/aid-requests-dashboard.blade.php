<div>
    <div class="_flex _justify-between _items-center _mb-df">
        <div class="title title--size-h2">
            {{__('aid-request.dashboard.Own applications')}} - {{ Organisation::getOrganisationName()}}
        </div>
    </div>
    <div class="_flex _justify-between _items-center _mb-df">
        <div class="title title--size-h6">
            {{ Organisation::getAuthUser()->name }}
        </div>
    </div>

    <div class="_cell _cell--12 _mb-xl">
        <div class=" _mb-df">
            @include('components.cabinet.sidebar-requests')
        </div>
    </div>

    <div class="user__list _mt-df">
        @if($collection->count())
            <div class="_grid">
                <div class="_cell _cell--4 _pl-xs _pr-xs">{{ __('aid-request.dashboard.Product') }}</div>
                <div class="_cell _cell--1 _pl-xs _pr-xs">{{ __('aid-request.dashboard.Type') }}</div>
                <div class="_cell _cell--1 _pl-xs _pr-xs">{{ __('aid-request.dashboard.Number in the system') }}</div>
                <div class="_cell _cell--2 _pl-xs _pr-xs">{{ __('aid-request.dashboard.Publication date') }}</div>
                <div
                    class="_cell _cell--1 _pl-xs _pr-xs">{{ __('aid-request.dashboard.Application validity date') }}</div>
                <div class="_cell _cell--3 _text-center"></div>
            </div>

            @foreach($collection as $item)
                <div class="_grid @if($openId) is-active @endif">
                    <div class="_cell _cell--4 _pl-xs _pr-xs">
                        <div class="_flex">
                            <div class="_mr-xs">{{ $item->own_number }}.</div>
                            <div>{{ $item->description }}</div>
                        </div>

                    </div>
                    <div class="_cell _cell--1 _justify-start _pl-xs _pr-xs">
                        {{ \App\Enums\AidRequest\AidRequestTypesEnum::get($item->type) }}
                    </div>
                    <div class="_cell _cell--1 _justify-start _pl-xs _pr-xs">
                        {{ $item->numberSystem()}}
                    </div>
                    <div class="_cell _cell--2 _justify-start _pl-xs _pr-xs">
                        {{ $item->publication_date->format('d.m.Y') }}
                    </div>
                    <div class="_cell _cell--1 _justify-start _pl-xs _pr-xs">
                        {{ $item->active_date->format('d.m.Y') }}
                    </div>

                    <div class="_cell _cell--3 _text-right _flex _justify-end">

                        @if($item->leeds->isNotEmpty())
                            <div class="ring _mr-xs _cursor-pointer not_hover" wire:click="$set('openId',{{$item->id == $openId ? 0 : $item->id}})">
                                @if($item->leeds->where('status', 'new')->count())
                                    <span class="ring__count" style="--fa-animation-iteration-count: 1">{{ $item->leeds->where('status', 'new')->count() }}</span>
                                @endif
                                <i class="fa-solid fa-envelope  @if($openId == $item->id) is-active @endif" style="--fa-animation-iteration-count: 1"
                                   title="Show all requests"></i>
                            </div>

                        @endif
                        <i class="fa-solid fa-eye _mr-xs _cursor-pointer" title="{{ __('aid-request.dashboard.Application preview') }}"
                           x-data="Alpine.plugins.openModal('cabinet.request.aid-request-preview',  {{ json_encode(['aidId' => $item->id]) }})"
                           @click="open"
                           @mouseenter="open"
                        ></i>
                        <i class="fa-solid fa-pen-to-square _mr-xs _cursor-pointer" title="Edit"
                           wire:click="edit({{$item->id}})"></i>
                        <i class="fa-solid fa-trash _cursor-pointer _mr-xs _cursor-pointer" title="Delete"
                           wire:click="delete({{$item->id}})"></i>
                    </div>

                    @if($openId == $item->id)
                        <div class="_cell _cell--12 opacity-height _pl-xxl _pr-xs">
                            <div class="user__lines">
                                <div class="_grid">
                                    <div class="_cell _cell--2 _pl-lg _pr-xs" title="{{ __('aid-request.dashboard.Click on the applicant\'s name to see their message') }}">{{ __('aid-request.dashboard.Name of the applicant') }}</div>
                                    <div class="_cell _cell--3" title="{{ __('aid-request.dashboard.Click on the applicant\'s name to see their message') }}">{{ __('aid-request.dashboard.Name of the organization') }}</div>
                                    <div class="_cell _cell--2">{{ __('aid-request.dashboard.Email') }}</div>
                                    <div class="_cell _cell--1">{{ __('aid-request.dashboard.Status') }}</div>
                                    <div class="_cell _cell--4">{{ __('aid-request.dashboard.Message text')}}</div>
                                </div>
                                @foreach($item->leeds->sortByDesc('id') as $leed)
                                    <div class="_grid"  x-data="{ opened: {{ $selectLeed == $leed->id ? 'true' : 'false'}} }" :class="{'is-active-item' : opened == true}">
                                        {{--
                                        true | false
                                        $leed->isNew() -- новая
                                        $leed->isApprove() -- апрувнул
                                        $leed->isRejected() -- отклонена
                                        --}}
                                        <div class="_cell _cell--2 _pl-xs _pr-xs _cursor-pointer"
                                             @click="opened = ! opened">
                                            <div class="_flex" >
                                                <div class="_mr-xs">{{ $loop->iteration }}.</div>
                                                <div>{{ $leed->name }}</div>
                                            </div>
                                        </div>
                                        <div class="_cell _cell--3 _cursor-pointer"
                                             @click="opened = ! opened">
                                            <div class="_flex" >
                                                <div>{{ $leed->name_organisation != '' ? $leed->name_organisation : '-----' }}</div>
                                            </div>
                                        </div>
                                        <div class="_cell _cell--2"  @click="opened = ! opened">
                                            <a href="mailto:{{ $leed->email }}">{{ $leed->email }}</a>
                                        </div>
                                        <div class="_cell _cell--1 _cursor-pointer" @click="opened = ! opened">
                                            {{ __('aid-request.statuses.'.$leed->status) }}
                                        </div>
                                        <div class="_cell _cell--4" x-show="opened" x-collapse.duration.500ms >
                                            {{ $leed->message }}
                                            <br>
                                            @if($leed->isNew())
                                                <button class="button button--transparent button--small _mt-md"
                                                        wire:click="approveLeed({{$leed->id}})">{{ __('aid-request.dashboard.Accept') }}</button>
                                                <button class="button button--transparent button--small _mt-md"
                                                        wire:click="rejectedLeed({{$leed->id}})">{{ __('aid-request.dashboard.Refuse') }}</button>
                                            @endif
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    @endif
                </div>
            @endforeach
        @else
            <div class="_grid _justify-center">
                <div
                    class="_cell _cell--6 _pl-xs _pr-xs _text-center ">{{ __('aid-request.dashboard.Add-ons have not been created yet!') }}</div>
            </div>
        @endif
    </div>

    {{ $collection->links() }}
    @include('livewire.flash-message')
    @include('layouts.preloader', ['target' => 'approveLeed, rejectedLeed'])
</div>
