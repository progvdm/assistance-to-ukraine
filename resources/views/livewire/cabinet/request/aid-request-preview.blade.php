<div>
    <div class="popup__header">
        <div class="_flex _justify-between _items-center">
            <div>
                {{__('aid-request.form.Preview')}}
            </div>
        </div>
    </div>
    <div class="popup__body marketplace">
        <div class="_grid _spacer _spacer--xxl _mt-xl justify-center">
            <div class="_cell _cell--12 _md:cell--6">
                @include('components.marketplace.card', ['item' => $aidRequest, 'class' => $aidRequest->type == 'offer' ? '' : 'card--dark'])
            </div>
        </div>
    </div>
</div>


