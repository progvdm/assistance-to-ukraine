<div class="form js-import" data-form>
    <div class="_flex _justify-between _items-center _nmt-df">
        <div class="title title--size-h4 _mb-md">
            {{ $edit ? __('aid-request.form.Editing the application') : __('aid-request.form.Creating an application')}}
        </div>
    </div>
    <div class="_flex _justify-between _items-center _mt-xs">
        <div class="title">
            № {{ $idSystem }} ({{ $own_number }})
        </div>
    </div>
    <div class="_grid _spacer _spacer--xl _mt-xl ">
        <div class="_cell _cell--12 _md:cell--6 _mb-none">
            <div class="_grid _spacer _spacer--lg _mb-df">
                <div class="_cell _cell--12">
                    <div class="form__sub-title">
                        1. {{__('Основні дані')}}
                    </div>
                </div>
                <div class="_cell _cell--12">
                    <label class="without-underline _flex _flex-row">
                        <span class="_mr-sm">*{{__('aid-request.Application type')}}</span>
                        <div wire:model="type">
                            <div class="_flex">
                                @foreach (\App\Enums\AidRequest\AidRequestTypesEnum::all() as $key => $name)
                                    <label class="without-underline _flex _flex-row">
                                        <input class="_mr-xs"
                                               {{ $key == $type ? 'checked' : '' }} type="radio"
                                               name="type" value="{{$key}}">
                                        {{$name}}
                                    </label>
                                @endforeach
                                @include('components.form.error-field',['field' => 'type', 'class' => '_ml-xs'])
                            </div>
                        </div>
                    </label>
                </div>
                <div class="_cell _cell--12">
                    <div class="js-import" wire:ignore data-select2
                         data-initialize-select2="{{ json_encode((object)['Factory' => 'LivewireSelect']) }}">
                        <label class="">
                            <span class="form__sub-title _mb-df">*{{__('aid-request.form.Type organisation')}}</span>

                            <select wire:model="type_organisation_id" class="_mt-xs _pr-md">
                                <option value="">
                                    {{__('aid-request.form.No select')}}
                                </option>
                                @foreach ($organisationTypes as $item)
                                    <option
                                        value="{{ $item->id }}" {{ $item->id == $type_organisation_id ? 'selected' : ''}}>{{$item->name_formatted}}</option>
                                @endforeach
                            </select>
                        </label>
                    </div>
                    @include('components.form.error-field',['field' => 'type_organisation_id'])
                </div>
            </div>

            <div class="_grid _spacer _spacer--lg _mb-df">
                <div class="_cell _cell--12">
                    <div class="form__sub-title ">
                        2. {{__('Продукт')}}
                    </div>
                </div>
                <div class="_cell _cell--12">
                    <div class="js-import" wire:ignore data-select2
                         data-initialize-select2="{{ json_encode((object)['Factory' => 'LivewireSelect']) }}">
                        <label class="with-anim-underline">
                            <span class="form__sub-title _mb-df">*{{__('aid-request.form.Group product')}}</span>

                            <select wire:model="category_id" class="_mt-xs _pr-md">
                                <option value="">
                                    {{__('aid-request.form.No select')}}
                                </option>
                                @foreach ($categories as $item)
                                    <option
                                        value="{{ $item->id }}" {{ $item->id == $category_id ? 'selected' : ''}}>{{$item->current->name}}</option>
                                @endforeach
                            </select>
                        </label>
                    </div>
                    @include('components.form.error-field',['field' => 'category_id'])
                </div>
                <div class="_cell _cell--12 _df:cell--6">
                    <label class="with-anim-underline">
                        <span class="form__sub-title">*{{__('aid-request.form.Name product')}} UA</span>
                        <input class="_mt-xs" type="text"
                               wire:model="product_name.uk">
                    </label>
                    @include('components.form.error-field',['field' => 'product_name.uk'])
                </div>
                <div class="_cell _cell--12 _df:cell--6">
                    <label class="with-anim-underline">
                        <span class="form__sub-title">*{{__('aid-request.form.Name product')}} EN</span>
                        <input class="_mt-xs" type="text"
                               wire:model="product_name.en">
                    </label>
                    @include('components.form.error-field',['field' => 'product_name.en'])
                </div>
                <div class="_cell _cell--12 _df:cell--6">
                    <label class="">
                        <span class="form__sub-title">*{{__('aid-request.form.Count')}}</span>
                        <input class="_mt-xs" type="number" onkeypress='return event.charCode >= 48 && event.charCode <= 57'
                               wire:model="count">
                    </label>
                    @include('components.form.error-field',['field' => 'count'])
                </div>
                <div class="_cell _cell--12 _df:cell--6">
                    <label class="">
                        <span class="form__sub-title _mb-xs">*{{__('aid-request.form.Unit of measurement')}}</span>
                        <select wire:model="count_type">
                            <option value="">
                                {{__('aid-request.form.No select')}}
                            </option>
                            @foreach ($productTypes as $key => $name)
                                <option value="{{$key}}">{{$name}}</option>
                            @endforeach
                        </select>
                    </label>
                    @include('components.form.error-field',['field' => 'count_type'])
                </div>
            </div>
        </div>
        <div class="_cell _cell--12 _md:cell--6">
            <div class="_grid _spacer _spacer--lg _mb-df _df:mb:none">
                <div class="_cell _cell--12">
                    <div class="form__sub-title">
                        3. {{__('Публікація')}}
                    </div>
                </div>
                <div class="_cell _cell--12">
                    <label class="without-underline _flex _flex-row">
                        <input type="checkbox" wire:model="published">
                        <span class="">{{__('aid-request.form.Published')}}</span>
                    </label>
                </div>
                <div class="_cell _cell--12">
                    <label class="">
                        <span class="form__sub-title">*{{__('aid-request.form.Publication date')}}</span>
                        <input class="_mt-xs" type="date" min="{{ date('Y-m-d') }}" wire:model="publication_date">
                    </label>
                    @include('components.form.error-field',['field' => 'publication_date'])
                </div>
                <div class="_cell _cell--12">
                    <label class="">
                        <span class="form__sub-title">*{{__('aid-request.form.Application validity date')}}</span>
                        <input class="_mt-xs" type="date"
                               min="{{ $publication_date ? date('Y-m-d', strtotime($publication_date)) : date('Y-m-d') }}"
                               wire:model="active_date">
                    </label>
                    @include('components.form.error-field',['field' => 'active_date'])
                </div>
            </div>
            <div class="input-indicator">
                <div class="_grid _spacer _spacer--lg">
                    <div class="_cell _cell--12">
                        <div class="form__sub-title _mt-md _lg:mt-xxl">
                            4. {{__('aid-request.form.Contacts')}}
                        </div>
                    </div>
                    <div class="_cell _cell--12">
                        <div class="form__item _flex _flex-column">
                            <label class="" for="contact_email">
                                *{{__('aid-request.form.Email')}}:
                            </label>
                            <input wire:model="contact_email" id="contact_email" type="text">
                            @include('components.form.error-field',['field' => 'contact_email'])
                        </div>
                    </div>
                    <div class="_cell _cell--12">
                        <div class="form__item _flex _flex-column">
                            <label class="" for="contact_phone">
                                {{__('aid-request.form.Phone')}}:
                            </label>
                            <input wire:model="contact_phone" id="contact_phone" type="text">
                            @include('components.form.error-field',['field' => 'contact_phone'])
                        </div>
                    </div>
                    <div class="_cell _cell--12">
                        <div class="form__item _flex _flex-column _mb-df">
                            <label class="" for="contact_social">
                                {{__('aid-request.form.Social')}}:
                            </label>
                            <input wire:model="contact_social" type="text"
                                   id="name">
                            @include('components.form.error-field',['field' => 'contact_social'])
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="_grid _spacer _spacer--xl _mb-md _df:mb-xl">
        <div class="_cell _cell--12">
            <div class="text--italic">
                @if($category_id or $type_organisation_id or $product_name or $count_type)
                    <div class="_mb-sm">
                        UA: {{ $buildDescription['uk'] }}
                    </div>
                    <div>
                        EN: {{$buildDescription['en']}}
                    </div>
                @else
                    {{ __('aid-request.form.Enter the parameters to load the text of the application') }}
                @endif
            </div>
        </div>
    </div>
    <div class="_flex _justify-between">
        <a href="{{ route('cabinet.aid-requests.index') }}"
           class="button button--transparent button--small">

            <i class="_mr-xs fa-solid fa-arrow-left"></i>
            {{ __('default.Back') }}
        </a>
        <button class="button button--main button--small"
                wire:click="submit" {{ $showConfirm ? '' : 'disabled' }}>
            <i class="_mr-xs fa-solid fa-check"></i>
            {{__('aid-request.Save')}}
        </button>
    </div>
    @include('livewire.flash-message')
    @include('layouts.preloader', ['target' => 'submit'])
</div>


