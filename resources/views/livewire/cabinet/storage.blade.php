<div>
    <div class="_flex _justify-between _items-center _mb-df">
        <div class="title title--size-h2" wire:ignore>
            {{ Seo::meta()->getH1() }}
        </div>
    </div>
    <br>
    <div class="form js-import" data-form>
        <div class="_grid _spacer _spacer--xs _mb-md _md:mb-none">
            <div class="_cell _cell--12 _md:cell--8">
                <div class="input-indicator">
                    <div class="form__item _flex _flex-column _mb-df">
                        <label class="with-anim-underline" for="filter">
                            {{__('storage.filter.name')}}:
                        </label>
                        <input wire:model="filter.name" id="filter" type="text">
                    </div>
                </div>
            </div>
            <div class="_cell _cell--12 _md:cell--4">
                <label class="with-anim-underline">
                    <select wire:model="filter.category" name="filter.category" id="filter.category">
                        <option value="0">No select</option>
                        @foreach($categories as $category)
                            <option value="{{ $category->id }}">{{ $category->current->name }}</option>
                        @endforeach
                    </select>
                </label>
            </div>
        </div>
    </div>
    <div class="recipient__list recipient__list--inner">
        <div class="_grid">
            <div class="_cell _cell--1 _pl-xs _pr-xs">{{ __('storage.Code') }}</div>
            <div class="_cell _cell--3 _pl-xs _pr-xs">{{ __('storage.Product') }}</div>
            <div class="_cell _cell--1 _text-center">{{ __('storage.Units') }}</div>
            <div class="_cell _cell--1 _text-center">{{ __('storage.quantity') }}</div>
            <div class="_cell _cell--1 _text-center">{{ __('storage.Residue') }}</div>
            <div class="_cell _cell--3 _text-center">{{ __('storage.Received from:') }}</div>
            <div class="_cell _cell--2 _text-center">{{ __('storage.Category') }}</div>
        </div>

        @foreach($products as $product)
            <div class="_grid">
                <div class="_cell _cell--1 _pl-xs _pr-xs">{{ $product->product->product->code }}</div>
                <div
                    class="_cell _cell--3 _justify-start _pl-xs _pr-xs">{{ $product->product->product->getName() }}</div>
                <div class="_cell _cell--1 _text-center">{{ $product->product->product->getType() }}</div>
                <div class="_cell _cell--1 _text-center">{{ $product->original_quantity }}</div>
                <div class="_cell _cell--1 _text-center">{{ $product->quantity }}</div>
                <div class="_cell _cell--3 _pl-xs _pr-xs _justify-start">{{ $product->product->transfer->sender->name }}</div>
                <div class="_cell _cell--2 _pl-xs _pr-xs _justify-start">{{ $product->product->product->getCategoryName() }}</div>
            </div>
        @endforeach
    </div>
</div>
