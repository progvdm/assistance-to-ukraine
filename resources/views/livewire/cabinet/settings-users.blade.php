<div>
    <div class="_flex _justify-between _items-center _mb-df">
        <div class="title title--size-h2" wire:ignore>
            {{ Seo::meta()->getH1() }}
        </div>
    </div>
    <br>
    {{--<div class="form js-import" data-form>
        <div class="_grid _spacer _spacer--xs _mb-md _md:mb-none">
            <div class="_cell _cell--12 _md:cell--8">
                <div class="input-indicator">
                    <div class="form__item _flex _flex-column _mb-df">
                        <label class="with-anim-underline" for="filter">
                            {{__('storage.filter.name')}}:
                        </label>
                        <input wire:model="filter.name" id="filter" type="text">
                    </div>
                </div>
            </div>
            <div class="_cell _cell--12 _md:cell--4">
                <label class="with-anim-underline">
                    <select wire:model="filter.category" name="filter.category" id="filter.category">
                        <option value="0">No select</option>
                        @foreach($categories as $category)
                            <option value="{{ $category->id }}">{{ $category->current->name }}</option>
                        @endforeach
                    </select>
                </label>
            </div>
        </div>
    </div>--}}
    <div class="_flex _justify-end _m-auto _mt-df">
        <button class="button button--main button--small"
                x-data="Alpine.plugins.openModal('popup.user-edit-or-create-popup',  {{ json_encode(['userId' => null]) }})"
                @click="open"
                @mouseenter="open">
            <i class="_mr-xs fa-solid fa-plus"></i>
            {{__('users.Add new')}}
        </button>
    </div>
    <div class="user__list _mt-df">
        @if($users->count())
            <div class="_grid">
                <div class="_cell _cell--4 _pl-xs _pr-xs">{{ __('users.Name') }}</div>
                <div class="_cell _cell--4 _pl-xs _pr-xs">{{ __('users.Email') }}</div>
                {{--
                            <div class="_cell _cell--3 _text-center">{{ __('users.User type') }}</div>
                --}}
                <div class="_cell _cell--2 _text-center">{{ __('users.Status') }}</div>
                <div class="_cell _cell--2 _text-center"></div>
            </div>

            @foreach($users as $user)
            <div class="_grid">
                <div class="_cell _cell--4 _pl-xs _pr-xs">{{ $user->name }}</div>
                <div
                    class="_cell _cell--4 _justify-start _pl-xs _pr-xs">{{ $user->email }}</div>
{{--
                <div class="_cell _cell--3 _text-center">{{ $user->type_user }}</div>
--}}
                <div class="_cell _cell--2 _text-center">{{ $user->email_verified_at ? 'active' : 'not active'}}</div>
                <div class="_cell _cell--2 _text-center">
                    <button class="button button--transparent"
                            x-data="Alpine.plugins.openModal('popup.user-edit-or-create-popup',  {{ json_encode(['userId' => $user->id]) }})"
                            @click="open"
                            @mouseenter="open"
                            >
                        <i class="_mr-xxs fa-xs fa-solid fa-pen-to-square"></i>
                        Edit</button>
                    <button class="button button--transparent" wire:click="deleteUser({{$user->id}})">
                        <i class="_mr-xxs fa-xs fa-solid fa-trash"></i>
                        Delete</button>
                </div>
            </div>
        @endforeach
        @else
            <div class="_grid _justify-center">
                <div class="_cell _cell--6 _pl-xs _pr-xs _text-center ">{{ __('users.No users have registered yet!') }}</div>
            </div>
        @endif
    </div>

    @include('livewire.flash-message')
</div>
