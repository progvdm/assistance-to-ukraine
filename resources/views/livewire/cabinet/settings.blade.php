<form class="form js-import _mt-xl" wire:submit.prevent="submit" data-form>
    <div wire:ignore>
        <script src="/vendor/tinymce/tinymce.min.js"
                referrerpolicy="origin"></script>
    </div>
    <div class="_grid _spacer _spacer--md _md:spacer--lg">
        <div class="_cell _cell--12 _md:cell--3">
            <div class="form__sub-title">
                1. {{__('auth.Agent')}}
            </div>
        </div>
        <div class="_cell _cell--12 _md:cell--9">
            <div class="form__item _flex _flex-column _mb-df">
                <label class="with-anim-underline" for="user.name">
                    {{__('auth.form.name')}}:
                </label>
                <input wire:model="user.name" id="user.name" type="text">
                @include('components.form.error-field',['field' => 'user.name'])
            </div>
            <div class="form__item _flex _flex-column _mb-df">
                <label class="with-anim-underline" for="user.email">
                    {{__('auth.form.email')}}:
                </label>
                <input wire:model="user.email" id="user.email" type="text">
                @include('components.form.error-field',['field' => 'user.email'])
            </div>
            <div class="_grid _spacer _spacer--none _md:spacer--lg">
                <div class="_cell _cell--12 _md:cell--6">
                    <div class="form__item _flex _flex-column _mb-df">
                        <label class="with-anim-underline" for="user.phone">
                            {{__('auth.form.phone')}}:
                        </label>

                        <input wire:model="user.phone" id="user.phone" type="text" placeholder="">
                        @include('components.form.error-field',['field' => 'user.phone'])
                    </div>
                </div>
                <div class="_cell _cell--12 _md:cell--6">
                    <div class="form__item _flex _flex-column _mb-df">
                        <label class="with-anim-underline" for="user.notification_phone">
                            {{__('auth.form.additional_phone')}}:
                        </label>

                        <input wire:model="user.notification_phone" id="user.notification_phone" type="text" placeholder="">
                        @include('components.form.error-field',['field' => 'user.notification_phone'])
                    </div>
                </div>
            </div>
            <div class="_grid _spacer _spacer--none _md:spacer--lg">
                <div class="input-indicator _cell _cell--12 _md:cell--6">
                    <div class="_mb-md">
                        {{__('auth.setting.Phone for notifications')}}
                    </div>
                    <div class="_flex _flex-column  _mb-df" wire:model="user.notification_phone_check">
                        <label class="without-underline _flex _flex-row">
                            <input class="_mr-xs _mb-xs _ml-none" type="radio" name="user.notification_phone_check" {{ $user['notification_phone_check'] == 'phone' ? 'checked' : '' }} value="phone">
                            {{__('auth.setting.Basic')}}
                        </label>

                        <label class="without-underline _flex _flex-row">
                            <input class="_mr-xs _mb-xs _ml-none" {{ $user['notification_phone'] == '' ? 'disabled' : ''}} {{ $user['notification_phone_check'] == 'notification_phone' ? 'checked' : '' }} type="radio" name="user.notification_phone_check" value="notification_phone">
                            {{__('auth.setting.Additional')}}
                        </label>
                        @include('components.form.error-field',['field' => 'user.notification_phone_check'])
                    </div>
                </div>
                <div class="_cell _cell--12 _md:cell--6 _grid _spacer _spacer--none">
                    <div class="text">
                        {{__('auth.setting.Digital signature of documents using Telegram bot')}}

                        <div class="text--italic _mt-xs">
                            {{ __('auth.setting.Follow the link to get digital codes') }}
                            <a class="text--color-yellow text--weight-bold" href="https://t.me/aid_monitor_bot?start={{ $user['telegramToken'] }}" target="_blank" style="font-size: 16px;">
                                @aid_monitor_bot
                            </a>
                        </div>
                    </div>

                    @if($user['telegram_user_name'])
                        <div class="_cell _cell--12">
                            <div class="form__item _flex _flex-column ">
                                <label class="with-anim-underline" for="user.telegram_user_name">
                                    {{__('auth.setting.Telegram login')}}:
                                </label>
                                <input wire:model="user.telegram_user_name" id="user.telegram_user_name" disabled type="text" placeholder="">
                            </div>
                        </div>
                    @endif
                </div>
                <div class="_cell _cell--12 _md:cell--12 _mb-sm">
                    <label class="without-underline _flex _flex-row">
                        <input type="checkbox" checked disabled>
                        <span class="">*{!!  __('auth.acquainted with the public offer contract', ['url' => "/documents-static/User agreement- " . app()->getLocale().'.pdf?v=3']) !!}</span>
                    </label>
                    @include('components.form.error-field',['field' => 'oferta'])
                </div>
            </div>

        </div>
        <div class="_cell _cell--12 _md:cell--3">
            <div class="form__sub-title _pt-none _md:pt-xs">
                2. {{__('auth.General')}}
            </div>
        </div>
        <div class="_cell _cell--12 _md:cell--9">
            @include('components.tabs.tabs', ['tabsList' => $locales, 'tabContainer' => 'organisationInfo'])

            @foreach($locales as $key => $tab)
                @include('components.tabs.tabs-container-start', ['key' => $key, 'tabContainer' => 'organisationInfo'])
                <div class="form__item _flex _flex-column _mb-df">
                    <label class="with-anim-underline" for="organisation.{{$key}}.name">
                        {{__('auth.form.Name organisation')}}:
                    </label>
                    <input wire:model="organisation.{{$key}}.name" id="organisation.{{$key}}.name" type="text">
                    @include('components.form.error-field',['field' => 'organisation.'.$key.'name'])
                </div>
                @include('components.tabs.tabs-container-end')
            @endforeach
            <div class="_grid _spacer _spacer--none _md:spacer--lg">
                <div class="_cell _cell--12 _md:cell--6">
                    <div class="form__item _flex _flex-column _mb-df">
                        <label class="_flex with-anim-underline" for="organisation.id_code">
                            {{__('auth.form.id_code')}}:
                        </label>
                        <input wire:model.lazy="organisation.id_code" id="organisation.id_code" class="_mr-sm" type="text">
                        @include('components.form.error-field',['field' => 'organisation.id_code'])
                    </div>
                </div>
                <div class="_cell _cell--12 _md:cell--6">
                    <div class="form__item _flex _flex-column _mb-df">
                        <label class="with-anim-underline" for="organisation.zip_code">
                            {{__('auth.form.zip_code')}}:
                        </label>
                        <input wire:model.lazy="organisation.zip_code" id="organisation.zip_code" class="_mr-sm" type="text">
                        @include('components.form.error-field',['field' => 'organisation.zip_code'])
                    </div>
                </div>
            </div>
            <div class="_flex _flex-column _mb-df">
                <label class="with-anim-underline _flex-row _justify-between">
                    <span class="form__checkbox-text _mr-df">{{__('auth.verification.sanction')}}</span>

                    <input class="width-a" disabled @if($verification['sanction']) checked @endif type="checkbox">
                </label>
                @include('components.form.error-field',['field' => 'validation.sanction'])
            </div>
            <div class="_flex _flex-column _mb-df">
                <label class="with-anim-underline _flex-row _justify-between">
                    <span class="form__checkbox-text _mr-df">{{__('auth.verification.founderRu')}}</span>
                    <input class="width-a" disabled @if($verification['founderRu']) checked @endif type="checkbox">
                </label>
                @include('components.form.error-field',['field' => 'validation.founderRu'])
            </div>
            <div class="_flex _flex-column _mb-df">
                <label class="with-anim-underline _flex-row _justify-between">
                    <span class="form__checkbox-text _mr-df">{{__('auth.verification.criminalCourts')}}</span>
                    <input class="width-a" disabled @if($verification['criminalCourts']) checked
                           @endif type="checkbox">
                </label>
                @include('components.form.error-field',['field' => 'validation.criminalCourts'])
            </div>
        </div>

        <div class="_cell _cell--12 _md:cell--3">
            <div class="form__sub-title _pt-none _md:pt-xs">
                {{ Organisation::isRecipient() ? '3.1.' : '3.' }} {{__('auth.Address')}}
            </div>
        </div>
        <div class="_cell _cell--12 _md:cell--9">
            @include('components.tabs.tabs', [
                        'tabsList' => $locales,
                        'tabContainer' => 'organisationAddress',
                        'field' => 'organisation',
                        'fieldsArr' => ['country', 'city', 'street', 'build']])

            @foreach($locales as $key => $tab)
                @include('components.tabs.tabs-container-start', ['key' => $key, 'tabContainer' => 'organisationAddress'])
                <div class="_grid _spacer _spacer--none _md:spacer--lg">
                    <div class="_cell _cell--12 _md:cell--6">
                        <div class="form__item _flex _flex-column _mb-df">
                            <label class="with-anim-underline" for="organisation.{{$key}}.country">
                                {{__('auth.form.country')}}:
                            </label>
                            <input wire:model="organisation.{{$key}}.country" id="organisation.{{$key}}.country" type="text">

                            @include('components.form.error-field',['field' => 'organisation.'.$key.'.country'])
                        </div>
                    </div>
                    <div class="_cell _cell--12 _md:cell--6">
                        <div class="form__item _flex _flex-column _mb-df">
                            <label class="with-anim-underline">
                                {{__('auth.form.city')}}:
                            </label>
                            <input wire:model="organisation.{{$key}}.city" id="organisation.{{$key}}.city" type="text">
                            @include('components.form.error-field',['field' => 'organisation.'.$key.'.city'])
                        </div>
                    </div>
                </div>
                <div class="_grid _spacer _spacer--none _md:spacer--lg">
                    <div class="_cell _cell--12 _md:cell--6">
                        <div class="form__item _flex _flex-column _mb-df">
                            <label class="with-anim-underline" for="organisation.{{$key}}.street">
                                {{__('auth.form.street')}}:
                            </label>
                            <input wire:model="organisation.{{$key}}.street" id="organisation.{{$key}}.street" type="text">
                            @include('components.form.error-field',['field' => 'organisation.'.$key.'.street'])
                        </div>
                    </div>
                    <div class="_cell _cell--12 _md:cell--6">
                        <div class="form__item _flex _flex-column _mb-df">
                            <label class="with-anim-underline" for="organisation.{{$key}}.build">
                                {{__('auth.form.build')}}:
                            </label>
                            <input wire:model="organisation.{{$key}}.build" id="organisation.{{$key}}.build" type="text">

                            @include('components.form.error-field',['field' => 'organisation.'.$key.'.build'])
                        </div>
                    </div>
                </div>

                @include('components.tabs.tabs-container-end')
            @endforeach
        </div>
        @if(Organisation::isRecipient())
            <div class="_cell _cell--12 _md:cell--3">
                <div class="form__sub-title">
                    3.2. {{__('auth.Warehouse address')}}
                </div>
            </div>
            <div class="_cell _cell--12 _md:cell--9">
                @include('components.tabs.tabs', [
                            'tabsList' => $locales,
                            'tabContainer' => 'organisationAddressStorage',
                            'field' => 'organisation',
                            'fieldsArr' => ['storage_city', 'storage_street', 'storage_build']])

                @foreach($locales as $key => $tab)
                    @include('components.tabs.tabs-container-start', ['key' => $key, 'tabContainer' => 'organisationAddressStorage'])
                    @if($key == 'uk')
                        <div class="form__item _flex _flex-column _mb-df newPost-select" wire:ignore>
                            <label class="with-anim-underline">
                                {{__('auth.form.city')}}:
                            </label>
                            <livewire:cabinet.select-cities
                                name="organisation.{{$key}}.storage_city"
                                value="{{ $organisation['storage_city'] }}"
                                placeholder=""
                                :searchable="true"
                            />
                        </div>
                    @else
                        <div class="form__item _flex _flex-column _mb-df newPost-select">
                            <label class="with-anim-underline" for="organisation.{{$key}}.storage_city">
                                {{__('auth.form.city')}}:
                            </label>
                            <input wire:model="organisation.{{$key}}.storage_city"
                                   id="organisation.{{$key}}.storage_city" type="text">
                            @include('components.form.error-field',['field' => 'organisation.'.$key.'.storage_city'])
                        </div>
                    @endif
                    <div class="_grid _spacer _spacer--none _md:spacer--lg">
                        <div class="_cell _cell--12 _md:cell--6">
                            <div class="form__item _flex _flex-column _mb-df">
                                <label class="with-anim-underline" for="organisation.{{$key}}.storage_street">
                                    {{__('auth.form.street')}}:
                                </label>
                                <input wire:model="organisation.{{$key}}.storage_street" id="organisation.{{$key}}.storage_street" type="text">
                                @include('components.form.error-field',['field' => 'organisation.'.$key.'.storage_street'])
                            </div>
                        </div>
                        <div class="_cell _cell--12 _md:cell--6">
                            <div class="form__item _flex _flex-column _mb-df">
                                <label class="with-anim-underline" for="organisation.{{$key}}.storage_build">
                                    {{__('auth.form.build')}}:
                                </label>
                                <input wire:model="organisation.{{$key}}.storage_build" id="organisation.{{$key}}.storage_build" type="text">

                                @include('components.form.error-field',['field' => 'organisation.'.$key.'.storage_build'])
                            </div>
                        </div>
                    </div>
                    @include('components.tabs.tabs-container-end')
                @endforeach
            </div>
        @endif
        <div class="_cell _cell--12 _md:cell--3">
            <div class="form__sub-title _pt-none _md:pt-xs">
                4. {{__('auth.Company information')}}
            </div>
        </div>
        <div class="_cell _cell--12 _md:cell--9">
            <div class="_mb-df">
                <label class="with-anim-underline" data-text="Upload your logo">
                    <input wire:model="organisation.photoUpload" type="file" placeholder="{{__('auth.form.file')}}"
                           data-text="Upload your logo">
                </label>

                @if(isset($organisation['photoUpload']) and $organisation['photoUpload'])
                    <div class="_flex _items-center _mt-xs _mb-xs">
                        <i class="fa-solid fa-xl fa-file-arrow-up"></i>
                        <span class="_ml-xs">{{$organisation['photoUpload']->getClientOriginalName()}}</span>
                    </div>

                    <div class="_grid _mt-df">
                        <div class="_cell _cell--6">
                            <img src="{{ $organisation['photoUpload']->temporaryUrl() }}" alt="" loading="lazy">
                        </div>
                    </div>
                @endif
                @if(isset($organisation['photo']) and $organisation['photo'] != '' and !isset($organisation['photoUpload']))
                    <div class="_grid _mt-df">
                        <div class="_cell _cell--6">
                            <img src="{{Organisation::getOrganisation()->getImage()}}" alt="123">
                        </div>
                    </div>
                @endif
                @include('components.form.error-field',['field' => 'photo'])
            </div>


            <div class="form__item _flex _flex-column _mb-df">
                <label class="with-anim-underline" for="organisation.additional_phone">
                    {{__('auth.setting.Public phone')}}:
                </label>

                <input wire:model="organisation.additional_phone" id="organisation.additional_phone" type="text" placeholder="">
                @include('components.form.error-field',['field' => 'organisation.additional_phone'])
            </div>
            <div class="form__item _flex _flex-column _mb-df">
                <label class="with-anim-underline" for="organisation.additional_email">
                    {{__('auth.form.additional_email')}}:
                </label>
                <input wire:model="organisation.additional_email" id="organisation.additional_email" type="text">
                @include('components.form.error-field',['field' => 'organisation.additional_email'])
            </div>
            <div class="form__item _flex _flex-column _mb-df">
                <label class="with-anim-underline" for="organisation.website">
                    {{__('auth.form.website')}}:
                </label>
                <input wire:model="organisation.website" id="organisation.website" type="text">
                @include('components.form.error-field',['field' => 'organisation.website'])
            </div>

            <div class="_grid _spacer _spacer--none _md:spacer--lg">
                <div class="_cell _cell--12 _md:cell--6">
                    <div class="form__item _flex _flex-column _mb-df">
                        <label class="with-anim-underline" for="organisation.facebook">
                            {{__('auth.form.facebook')}}:
                        </label>
                        <input wire:model="organisation.facebook" id="facebook" type="text">
                        @include('components.form.error-field',['field' => 'organisation.facebook'])
                    </div>
                </div>
                <div class="_cell _cell--12 _md:cell--6">
                    <div class="form__item _flex _flex-column _mb-df">
                        <label class="with-anim-underline" for="organisation.instagram">
                            {{__('auth.form.instagram')}}:
                        </label>
                        <input wire:model="organisation.instagram" id="organisation.instagram" type="text">
                        @include('components.form.error-field',['field' => 'organisation.instagram'])
                    </div>
                </div>
            </div>
        </div>

        {{--<div class="_cell _cell--12 _mb-none">
            <div class="input-indicator">
                <div>
                    <label class="with-anim-underline _mb-df" for="text_short">
                        {{__('auth.form.text_short')}}:
                    </label>
                    @include('components.tabs.tabs', ['tabsList' => $locales, 'tabContainer' => 'organisationTextShort'])

                    @foreach($locales as $key => $tab)
                        @include('components.tabs.tabs-container-start', ['key' => $key, 'tabContainer' => 'organisationTextShort'])
                        <label class="without-underline">
                        <textarea class="" wire:model="organisation.{{$key}}.text_short" type="text" placeholder="{{__('auth.form.text_short')}}"
                                  rows="7"></textarea>
                        </label>
                        @include('components.form.error-field',['field' => 'organisation.'.$key.'.text_short'])
                        @include('components.tabs.tabs-container-end')
                    @endforeach
                </div>
            </div>
        </div>--}}
        <div class="_cell _cell--12">
            <div class="input-indicator">
                <div class="_mt-df" >
                    <label class="with-anim-underline _mb-df" for="text">
                        {{__('auth.form.text')}}:
                    </label>
                    @include('components.tabs.tabs', ['tabsList' => $locales, 'tabContainer' => 'organisationText'])

                    @foreach($locales as $key => $tab)
                        @include('components.tabs.tabs-container-start', ['key' => $key, 'tabContainer' => 'organisationText'])
                        <label class="without-underline" wire:ignore>
                        <textarea class="tinymce" data-name="organisation.{{$key}}.text" en_US
                                  wire:model="organisation.{{$key}}.text"
                                  type="text"
                                  placeholder="{{__('auth.form.text')}}"
                                  rows="10"></textarea>
                        </label>
                        @include('components.form.error-field',['field' => 'organisation.'.$key.'.text'])
                        @include('components.tabs.tabs-container-end')
                    @endforeach
                </div>
            </div>
        </div>
        <div class="_cell _cell--12">
            <div class="_flex _justify-between">
                <a href="/cabinet" class="button button--transparent button--small _mr-xs">

                    <svg class="_mr-xs" style="transform: rotate(90deg)" width="10" height="15" viewBox="0 0 10 15"
                         fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path
                            d="M4.54038 14.4596C4.79422 14.7135 5.20578 14.7135 5.45962 14.4596L9.59619 10.323C9.85003 10.0692 9.85003 9.65765 9.59619 9.40381C9.34235 9.14997 8.9308 9.14997 8.67696 9.40381L5 13.0808L1.32304 9.40381C1.0692 9.14997 0.657647 9.14997 0.403806 9.40381C0.149965 9.65765 0.149965 10.0692 0.403806 10.323L4.54038 14.4596ZM4.35 0L4.35 14H5.65L5.65 0L4.35 0Z"
                            fill="#353535"/>
                    </svg>

                    {{ __('default.Back') }}
                </a>


                    <button wire:click="submit" type="submit" @if(!$showConfirm) disabled @endif
                            class="button button--main button--small">
                        <i class="_mr-xs fa-solid fa-check"></i>
                        {{__('default.Confirm')}}</button>

            </div>
        </div>
    </div>
    <script>
        let lang = '{{app()->getLocale()}}';

        tinymce.init({
            selector: '.tinymce',
            plugins: 'preview importcss searchreplace autolink autosave save directionality code visualblocks visualchars fullscreen image link media template codesample table charmap pagebreak nonbreaking anchor insertdatetime advlist lists wordcount help charmap quickbars emoticons',
            toolbar1: "undo redo pastetext | blocks | normal bold italic forecolor backcolor fontselect fontsizeselect styleselect fontsize | alignleft aligncenter alignright alignjustify",
            toolbar2: 'bullist numlist outdent indent | link unlink image media fullscreen code appearance',

            file_browser_callback: function(field_name, url, type, win) {
                console.log(field_name, url, type, win)
            },
            images_upload_url: '{{route('api.tinymce.photo.user')}}',
            language: lang,
            setup: function (editor) {
                editor.on('init change', function () {
                    editor.save();
                });

                editor.on('change', function (e) {
                    let field = e.target.targetElm.dataset.name;
                    Livewire.emit('changeTiny', field, editor.getContent());
                });
            }
        });
    </script>
</form>
