@if(isset($message['error']))
    <div class="noty">
        <p class="noty__message noty__message--error">{{ $message['error'] }}</p>
    </div>
@elseif(isset($message['success']))
    <div class="noty">
        <p class="noty__message noty__message--success">{{ $message['success'] }}</p>
    </div>
@endif
@if(isset($message['info']))
    <div class="noty">
        <p class="noty__message noty__message--info">{{ $message['info'] }}</p>
    </div>
@endif
@if(isset($errors) and $errors->any())
    <div class="noty">
        <p class="noty__message noty__message--error">{{ __('form.messages.Validation error') }}</p>
    </div>
@endif
