@php
/**
 * @var $name string
 * @var $label string|null
 * @var $placeholder string|null
 * @var $value string|null
 * @var $attributes \Illuminate\View\ComponentAttributeBag
 */

    $inputId = uniqid($name . '-input');
@endphp
<div {{ $attributes->merge(['class' => 'form-item form-item--input']) }}>
    @if($label)
        <label for="{{ $inputId }}" class="form-item__label">{{ $label }}</label>
    @endif
{{--        {{ $attributes->get('required') ? '*' : null }}--}}

    <input
        id="{{ $inputId }}"
        class="form-item__control js-dmi js-input-mask {{ $errors->has($name) ? 'with-error' : null }}"
        type="tel"
        inputmode="tel"
        placeholder="{{ $placeholder ?? '' }}"
        name="{{ $name }}"
        value="{{ remove_phone_mask($value) ? $value : '' }}"
    >

    @error($name)
        <label id="{{ $inputId }}-error" class="has-error" for="{{ $inputId }}" class="form-error">{{ $message }}</label>
    @enderror

    {!! $slot !!}
</div>

