@if ($paginator->hasPages())
    <div class="_flex _justify-between _items-center _mt-md">
        <div class="pagination">
            <div class="_grid _spacer _spacer--sm">
                @if ($paginator->onFirstPage())
                    <div class="_cell">
                        <div class="pagination__item pagination__item--withoutBorder">
                            < <span>{{__('default.Previous')}} </span>
                        </div>
                    </div>
                @else
                    <div class="_cell">
                        <div class="pagination__item pagination__item--withoutBorder">
                            <a href="{{$paginator->previousPageUrl()}}">< <span>{{__('default.Previous')}}</span> </a>
                        </div>
                    </div>
                @endif
                @if($paginator->currentPage() != 1)
                    <div class="_cell">
                        <div class="pagination__item">
                            <a href="{{routeLocale(\Illuminate\Support\Facades\Route::current()->getName())}}">1 ...</a>
                        </div>
                    </div>
                @endif

                <div class="_cell">
                    <div class="pagination__item is-active">
                        <span>{{ $paginator->currentPage() }}</span>
                    </div>
                </div>
                @if($paginator->currentPage() + 1 <= $paginator->lastPage())
                    <div class="_cell">
                        <div class="pagination__item">
                            <a href="{{routeLocale(\Illuminate\Support\Facades\Route::current()->getName(), ['page' => $paginator->currentPage() + 1])}}"> {{$paginator->currentPage() + 1}} </a>
                        </div>
                    </div>
                @endif
                @if($paginator->currentPage() + 2 <= $paginator->lastPage())
                    <div class="_cell">
                        <div class="pagination__item">
                            <a href="{{routeLocale(\Illuminate\Support\Facades\Route::current()->getName(), ['page' => $paginator->currentPage() + 2])}}"> {{$paginator->currentPage() + 2}} </a>
                        </div>
                    </div>
                @endif
                @if($paginator->currentPage() != $paginator->lastPage())
                    <div class="_cell">
                        <div class="pagination__item">
                            <a href="{{routeLocale(\Illuminate\Support\Facades\Route::current()->getName(), ['page' => $paginator->lastPage()])}}">... {{$paginator->lastPage()}}</a>
                        </div>
                    </div>
                @endif
                @if ($paginator->hasMorePages())
                    <div class="_cell">
                        <div class="pagination__item pagination__item--withoutBorder">
                            <a href="{{$paginator->nextPageUrl()}}"><span>{{__('default.Next')}}</span> ></a>
                        </div>
                    </div>
                @else
                    <div class="_cell">
                        <div class="pagination__item pagination__item--withoutBorder">
                            <span>{{__('default.Next')}}</span> >
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </div>
@endif
