<div class="section section--marketplace">
    <div class="_flex _flex-column">
        @if($listOffer->isNotEmpty())

            <div class="marketplace__line _pb-xs">
                <div class="container">
                    <div class="_flex _items-center">
                        <div class="marketplace__title">
                            @svg('marketplace', 'offer', [28, 25])
                            {{ __('aid-request.marketplace.We offer') }}
                        </div>

                        <div class="marketplace__run-line js-import" data-marquee="15000">
                            <a href="{{ route('page.marketplace') }}">
                                @foreach($listOffer as $item)
                                    <span>{{ $item->description }}</span>
                                @endforeach
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        @endif

        @if($listNeed->isNotEmpty())
            <div class="marketplace__line _pt-xs">
                <div class="container">
                    <div class="_flex _items-center">
                        <div class="marketplace__title">
                            @svg('marketplace', 'required', [21, 27])
                            {{ __('aid-request.marketplace.Required') }}
                        </div>

                        <div class="marketplace__run-line js-import" data-marquee="18000">
                            <a href="{{ route('page.marketplace') }}">
                                @foreach($listNeed as $item)
                                    <span>{{ $item->description }}</span>
                                @endforeach
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        @endif
    </div>
</div>
