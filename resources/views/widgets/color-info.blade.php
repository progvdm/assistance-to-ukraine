<div>
    <div class="color-info _flex-wrap _mb-df">
        <div class="color-info__icon _mr-df _mb-sm">
            @svg('dashboard', 'color-new', 14)
            {{__('dashboard.New undivided assistance')}}
        </div>
        <div class="color-info__icon _mr-df _mb-sm">
            @svg('dashboard', 'color-all', 14)
            {{__('dashboard.All done')}}
        </div>
        <div class="color-info__icon _mr-df _mb-sm">
            @svg('dashboard', 'color-process', 14)
            {{__('dashboard.In process')}}
        </div>
    </div>
    <div class="color-info _flex-wrap _mb-df">
        <div class="color-info__icon _mr-df _mb-sm">
            @svg('dashboard', 'color-approval', 12)
            {{ __('cargo.statuses.List approval') }}
        </div>
        <div class="color-info__icon _mr-df _mb-sm">
            @svg('dashboard', 'color-no-confirmed', 12)
            {{ __('cargo.statuses.List not confirmed') }}
        </div>
        <div class="color-info__icon _mr-df _mb-sm">
            @svg('dashboard', 'color-confirmed', 12)
            {{ __('cargo.statuses.List confirmed') }}
        </div>
        <div class="color-info__icon _mr-df _mb-sm">
            @svg('dashboard', 'color-delivery-no-confirmed', 12)
            {{ __('cargo.statuses.Delivery not confirmed') }}
        </div>
        <div class="color-info__icon _mr-df _mb-sm">
            @svg('dashboard', 'color-delivery-confirmed', 12)
            {{ __('cargo.statuses.Delivery confirmed') }}
        </div>
    </div>
</div>
