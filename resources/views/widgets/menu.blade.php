<div class="menu _m-auto _pl-xs _pr-xs">
    <div class="_flex _justify-between _spacer _spacer--xxl _df:spacer--md _lg:spacer--xl _xl:spacer--xl">
        <div class="menu__item">
            <a href="{{ route('page.about') }}" class="menu__link @if(stripos(url()->current(), route('page.about')) !== false) is-active @endif">
                {{__('default.About us')}}</a>
        </div>
       {{-- <div class="menu__item">
            <a href="{{ route('page.whatIsDone') }}" class="menu__link @if(stripos(url()->current(), route('page.whatIsDone')) !== false) is-active @endif">
                {{__('default.What is done')}}
            </a>
        </div>--}}
        <div class="menu__item">
            <a href="{{ route('page.donors') }}" class="menu__link @if(stripos(url()->current(), route('page.donors')) !== false) is-active @endif">
                {{__('default.Donors')}}
            </a>
        </div>
        <div class="menu__item">
            <a href="{{ route('page.recipients') }}" class="menu__link @if(stripos(url()->current(), route('page.recipients')) !== false) is-active @endif">
                {{__('default.Recipients')}}
            </a>
        </div>
        <div class="menu__item">
            <a href="{{ route('page.marketplace') }}" class="menu__link  @if(stripos(url()->current(), route('page.marketplace')) !== false) is-active @endif"> {{__('default.Marketplace')}}</a>
        </div>
        <div class="menu__item">
            <a href="{{ route('page.feedback') }}" class="menu__link  @if(stripos(url()->current(), route('page.feedback')) !== false) is-active @endif"> {{__('default.Feedback')}}</a>
        </div>
        <div class="menu__item">
            <a href="{{ route('home') }}#faq" class="menu__link anchor" data-anchor="faq">FAQ</a>
        </div>
        <div class="menu__item">
            <a href="{{ route('page.documents') }}" class="menu__link  @if(stripos(url()->current(), route('page.documents')) !== false) is-active @endif"> {{__('default.Documents')}}</a>
        </div>
    </div>
</div>
