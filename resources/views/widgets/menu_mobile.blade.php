<div class="menu__hamburger _lg:hide _ml-md js-import" data-hamburger>
    <svg class="close" width="22" height="17" viewBox="0 0 22 17" fill="none" xmlns="http://www.w3.org/2000/svg">
        <rect y="14" width="22" height="3" rx="1.5" fill=""/>
        <rect y="7" width="22" height="3" rx="1.5" fill=""/>
        <path d="M8 1.5C8 0.671573 8.67157 0 9.5 0H20.5C21.3284 0 22 0.671573 22 1.5C22 2.32843 21.3284 3 20.5 3H9.5C8.67157 3 8 2.32843 8 1.5Z" fill=""/>
    </svg>

    <svg class="open" width="21" height="22" viewBox="0 0 21 22" fill="none" xmlns="http://www.w3.org/2000/svg">
        <path class="cl1" d="M0.954055 20.545C0.563531 20.1545 0.563531 19.5213 0.954055 19.1308L18.6317 1.45311C19.0222 1.06259 19.6554 1.06259 20.0459 1.45311C20.4365 1.84364 20.4365 2.4768 20.0459 2.86733L2.36827 20.545C1.97774 20.9355 1.34458 20.9355 0.954055 20.545Z" fill=""/>
        <path class="cl1" d="M19.799 20.5445C20.1895 20.154 20.1895 19.5208 19.799 19.1303L2.12133 1.45263C1.7308 1.0621 1.09764 1.0621 0.707114 1.45263C0.31659 1.84315 0.31659 2.47632 0.707114 2.86684L18.3848 20.5445C18.7753 20.935 19.4085 20.935 19.799 20.5445Z" fill=""/>
    </svg>
</div>

<div class="menu menu--mobile">
    <div class="menu__wrapper">
        <div class="menu__item menu__item--fixed _xs:hide _pt-md _pb-md">
            @widget('langSwitcher')
        </div>
        <div class="menu__item">
            <a href="{{ route('page.about') }}" class="menu__link @if(stripos(url()->current(), route('page.about')) !== false) is-active @endif">
                {{__('default.About us')}}</a>
        </div>
        {{-- <div class="menu__item">
             <a href="{{ route('page.whatIsDone') }}" class="menu__link @if(stripos(url()->current(), route('page.whatIsDone')) !== false) is-active @endif">
                 {{__('default.What is done')}}
             </a>
         </div>--}}
        <div class="menu__item">
            <a href="{{ route('page.donors') }}" class="menu__link @if(stripos(url()->current(), route('page.donors')) !== false) is-active @endif">
                {{__('default.Donors')}}
            </a>
        </div>
        <div class="menu__item">
            <a href="{{ route('page.recipients') }}" class="menu__link @if(stripos(url()->current(), route('page.recipients')) !== false) is-active @endif">
                {{__('default.Recipients')}}
            </a>
        </div>
        <div class="menu__item">
            <a href="{{ route('page.marketplace') }}" class="menu__link  @if(stripos(url()->current(), route('page.marketplace')) !== false) is-active @endif"> {{__('default.Marketplace')}}</a>
        </div>
        <div class="menu__item">
            <a href="{{ route('page.feedback') }}" class="menu__link  @if(stripos(url()->current(), route('page.feedback')) !== false) is-active @endif"> {{__('default.Feedback')}}</a>
        </div>
        <div class="menu__item">
            <a href="{{ route('home') }}#faq" class="menu__link anchor" data-anchor="faq">FAQ</a>
        </div>
        <div class="menu__item">
            <a href="{{ route('page.documents') }}" class="menu__link  @if(stripos(url()->current(), route('page.documents')) !== false) is-active @endif"> {{__('default.Documents')}}</a>
        </div>
    </div>
</div>
