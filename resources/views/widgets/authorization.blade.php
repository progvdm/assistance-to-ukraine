<div class="authorization">
    <div class="_flex _justify-between">
        @svg('content', 'user', [16, 19], 'authorization__icon _xs:show _mr-xs')

        @auth
            <a href="{{ route('cabinet.home') }}" class="authorization__link
                                            @if(stripos(url()->current(), 'cabinet')) is-active @endif">
                {{__('auth.Cabinet')}}
            </a>
            <span class="authorization__slash">/</span>
            <a href="{{ route('logout') }}" class="authorization__link">{{__('auth.Logout')}}</a>
        @else
            <span
                x-data="Alpine.plugins.openModal('auth.login')"
                @click="open"
                class="authorization__link">{{__('auth.Login')}}</span>
            <span class="authorization__slash">/</span>
            <span
                x-data="Alpine.plugins.openModal('auth.registration')"
                @click="open"
                class="authorization__link">{{__('auth.Register')}}</span>
        @endauth
    </div>
</div>


