<div class="breadcrumbs">
    <div class="breadcrumbs__list">
        @foreach(Seo::breadcrumbs()->elements() as $element)
            @if($element->getUrl() != '#')
            <a href="{{ $element->getUrl() }}" class="breadcrumbs__link">
                {{ $element->getTitle() }}
            </a>
            <span> / </span>
            @else
                <span class="breadcrumbs__link">
                    {{ $element->getTitle() }}
                </span>
            @endif
        @endforeach
    </div>
</div>
