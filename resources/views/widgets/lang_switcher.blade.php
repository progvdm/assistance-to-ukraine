<div class="language-switcher _justify-around _xs:justify-start">
    @foreach($locales as $locale)
            @if($locale['active'])
                <span class="language-switcher__item">
                     {{ $locale['name'] }}
                </span>
            @else
                <a class="language-switcher__item" rel="alternate" hreflang="{{ $locale['link'] }}" href="{{ $locale['link'] }}">
                    {{ $locale['name'] }}
                </a>
            @endif
    @endforeach
</div>
