<div class="">
    <div class="">
        <div class="_flex _flex-column _mb-md">
            <div class="recipient__table _mb-df">
                <div class="_grid">
                    <div class="_cell _cell--8 _p-xxs _pr-xs">{{__('pages.users.popup.name')}}</div>
                    <div class="_cell _cell--4 _p-xxs _pl-xs">{{$user->name}}</div>
                    <div class="_cell _cell--8 _p-xxs _pr-xs">{{__('pages.users.popup.location')}}</div>
                    <div class="_cell _cell--4 _p-xxs _pl-xs">
                        {{$user->country}},
                        {{$user->city}},
                        {{$user->street}},
                        {{$user->build}}</div>
                    <div class="_cell _cell--8 _p-xxs _pr-xs">{{__('pages.users.popup.number_cells')}}</div>
                    <div class="_cell _cell--4 _p-xxs _pl-xs _text-right">{{$user->number_cells}}</div>
                    <div class="_cell _cell--8 _p-xxs _pr-xs">{{__('pages.users.popup.contacts')}}</div>
                    <div class="_cell _cell--4 _p-xxs _pl-xs _text-right"></div>
                    <div class="_cell _cell--8 _p-xxs _pr-xs">{{__('pages.users.popup.phone')}}</div>
                    <div class="_cell _cell--4 _p-xxs _pl-xs _text-right">{{$user->phone}}</div>
                    <div class="_cell _cell--8 _p-xxs _pr-xs">Email</div>
                    <div class="_cell _cell--4 _p-xxs _pl-xs _text-right">
                        @if($user->additional_email == '')
                            {{$user->email}}
                        @else
                            {{$user->additional_email}}
                        @endif
                    </div>
                    <div class="_cell _cell--8 _p-xxs _pr-xs">{{__('pages.users.popup.facebook')}}</div>
                    <div class="_cell _cell--4 _p-xxs _pl-xs _text-right">{{$user->facebook}}</div>
                    <div class="_cell _cell--8 _p-xxs _pr-xs">{{__('pages.users.popup.instagram')}}</div>
                    <div class="_cell _cell--4 _p-xxs _pl-xs _text-right">{{$user->instagram}}</div>
                    <div class="_cell _cell--8 _p-xxs _pr-xs">{{__('pages.users.popup.website')}}</div>
                    <div class="_cell _cell--4 _p-xxs _pl-xs">{{$user->website}}</div>
                    <div class="_cell _cell--8 _p-xxs _pr-xs">{{__('pages.users.popup.sanction')}}</div>
                    <div class="_cell _cell--4 _p-xxs _pl-xs">
                        @if($user->getVerification() &&
                            $user->getVerification()['sanction'] == '1' || $user->getVerification()['founderRu'] == '1')
                            {{__('pages.users.popup.not_verify')}}
                        @else
                            {{__('pages.users.popup.verify')}}
                        @endif</div>
                    <div class="_cell _cell--8 _p-xxs _pr-xs">{{__('pages.users.popup.criminal')}}</div>
                    <div class="_cell _cell--4 _p-xxs _pl-xs">
                        @if($user->getVerification() &&
                            $user->getVerification()['criminalCourts'] == '1')
                            {{__('pages.users.popup.not_verify')}}
                        @else
                            {{__('pages.users.popup.verify')}}
                        @endif
                    </div>
                </div>
            </div>
            @if(!auth()->user() && !$user->open_info)
                <span></span>
            @else
                <div>
                    @if($user->description_file != '')
                        <a href="{{asset('storage/'.$user->description_file)}}" target="_blank"><i class="fa-solid fa-xl fa-file-arrow-up"></i></a>
                    @endif
                </div>
                <div>
                    {{$user->text}}
                </div>
            @endif
        </div>
    </div>
</div>
