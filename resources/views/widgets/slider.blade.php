<div class="_flex _justify-between _items-center _mb-md _md:mb-xl">
    <div>
        <div class="title title--size-h1 _mb-md _df:mb-lg">
            What is done
        </div>

        <div class="title title--size-h3">
            The latest updates
        </div>
    </div>
</div>

<div class="_mb-xl">
    <div class="_flex _flex-column-reverse _sm:flex-row">
        <div class="news-text">
            <div class="news-card__text-title"></div>
        </div>
        <div class="news-slider _overflow">
            <div class="news-slider__slider swiper-container js-import"
                 data-swiper-slider='{"containerClass":"news-slider__slider","type":"NewsSlider","navigation":{"type":"default-outside","outside":true}}'>
                <div class="swiper-wrapper">
                    @foreach($transfers as $transfer)
                        @if($transfer->getImages()->count())
                            <div class="news-card swiper-slide">
                                <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAADUlEQVR42mP8z8BQDwAEhQGAhKmMIQAAAABJRU5ErkJggg=="
                                    data-src="{{$transfer->getImages()->first()->getUrl()}}" loading="lazy" class="swiper-lazy news-card__image">
                                <div class="news-card__text _hide">
                                    <div class="news-card__text-title">
                                        From {{$transfer->sender->city}}
                                        <span>{{$transfer->created_at->toDateString()}}</span>
                                    </div>
                                    {{--                        text--}}
                                </div>
                            </div>
                        @else
                            <div class="news-card swipper-slide">
                                <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAADUlEQVR42mP8z8BQDwAEhQGAhKmMIQAAAABJRU5ErkJggg=="
                                    data-src="{{asset('/images/noimage.jpg')}}" loading="lazy" class="swiper-lazy news-card__image">
                            </div>
                        @endif
                    @endforeach
                </div>
            </div>
        </div>
    </div>

    <div class="slider-navigation slider-navigation--under-slider">
        <div class="slider-navigation__button slider-navigation__button--prev swiper-button-prev swiper-button-disabled">
            @svg('slider', 'prev', [40, 41])
        </div>
        <div class="slider-navigation__button slider-navigation__button--next swiper-button-next">
            @svg('slider', 'next', [40, 41])
        </div>
    </div>
</div>
