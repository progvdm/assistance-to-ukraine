@if($list->count())
<div class="_flex _justify-between _items-center _mb-md _md:mb-xl" id="faq">
    <div>
        <div class="title title--size-h1 _mb-md _df:mb-lg">
            FAQ
        </div>

        <div class="title title--size-h3">
            {{ __('faq.We will answer all questions') }}
        </div>
    </div>
</div>

    @foreach($list as $faq)
        <div class="faq _mb-md _md:mb-df">
            <div class="faq__question js-import" data-accordion="faq">
                <div class="_flex">
                    <div class="_mr-xs">{{ $loop->iteration }}.</div>
                    {!! $faq->question !!}
                </div>
                <svg class="_md:show" width="23" height="13" viewBox="0 0 23 13" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <line y1="-1" x2="15" y2="-1" transform="matrix(-0.707107 0.707107 0.707107 0.707107 22.4556 2)" stroke="white" stroke-width="2"/>
                    <line x1="1.55574" y1="1.29387" x2="12.1623" y2="11.9005" stroke="white" stroke-width="2"/>
                </svg>
                <svg class="_md:hide" width="13" height="7" viewBox="0 0 13 7" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M1.242 1.64359C0.949102 1.3507 0.949103 0.875823 1.242 0.58293C1.53489 0.290036 2.00976 0.290036 2.30266 0.58293L7.07514 5.35541C7.36803 5.64831 7.36803 6.12318 7.07514 6.41607C6.78225 6.70897 6.30737 6.70897 6.01448 6.41607L1.242 1.64359Z" fill="white"/>
                    <path d="M11.9689 1.64359C12.2618 1.3507 12.2618 0.875823 11.9689 0.58293C11.676 0.290036 11.2012 0.290036 10.9083 0.58293L6.1358 5.35541C5.84291 5.64831 5.84291 6.12318 6.1358 6.41607C6.42869 6.70897 6.90357 6.70897 7.19646 6.41607L11.9689 1.64359Z" fill="white"/>
                </svg>

            </div>
            <div class="faq__answer">
                {!! $faq->answer !!}
            </div>
        </div>
    @endforeach
@endif
