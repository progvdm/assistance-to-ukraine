<div class="_flex _flex-column _sm:flex-row _items-start">
    <div  x-data="Alpine.plugins.openModal('auth.registration', {{json_encode(['type' => 'donor'])}})"
          @click="open"
          @mouseenter="open"
          class="button button--normal button--main _mr-df _mb-md _sm:mb-none">
        <span class="_flex _items-end">
            <span>Become a donor</span>
            <i class="_ml-sm _mt-xxs fa-solid fa-arrow-right"></i>
        </span>
    </div>
    <div
        x-data="Alpine.plugins.openModal('auth.registration', {{json_encode(['type' => 'recipient'])}})"
        @click="open"
        @mouseenter="open"
        class="button button--normal button--main">
        <span class="_flex _items-end">
            <span>Ask for help</span>
            <i class="_ml-sm _mt-xxs fa-solid fa-arrow-right"></i>
        </span>
    </div>
</div>
