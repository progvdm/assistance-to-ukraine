<div class="popup"
     x-data="Alpine.plugins.modal()"
     @open-modal.window="open"
     @preload-modal.window="preload"
     @close-modal.window="close"
     @keydown.escape.window="close"
     x-show.transition.opacity.duration.300ms="isOpen"
     :class="{ 'is-show': isOpen }"
     x-init="$watch('isOpen', (val) => console.log())"
     x-ref="modal"
     x-cloak
     tabindex="-1">
    <div
        x-ref="content"
        :class="{ 'is-show': isShow }"
        class="popup__wrapper"
    ></div>
    <button class="popup__close" type="button" x-el:button-close hidden @click="close($event)">
        <i class="fa-xl fa-solid fa-circle-xmark"></i>
    </button>
</div>
