<?php
return [
    // Verify SMS popup
    'Receive SMS in Telegram' => 'Receive SMS in Telegram',
    'Resend sms' => 'Resend sms',
    'Sms verification' => 'Sms verification',
    'Sms code' => 'Sms code',
    'SMS sent!' => 'SMS sent!',

];
