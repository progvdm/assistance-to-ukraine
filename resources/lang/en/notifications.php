<?php
return [
    'You need to sign the document!' => 'You need to sign the document!',
    'There are no messages!' => 'There are no messages!',
    'You have received feedback on the application №:number' => 'You have received feedback on the application №:number',
    'New aid requests!' => 'New applications on the marketplace! <br>Offers - :offer. <br>Need - :need.',
];
