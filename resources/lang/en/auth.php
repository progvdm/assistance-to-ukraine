<?php
return [
    'Registration title' => 'Registration title',
    'Login' => 'Login',
    'Register' => 'Register',
    'Logout' => 'Logout',
    'Cabinet' => 'Cabinet',
    'Donor' => 'Donor',
    'General' => 'General',
    'Registration' => 'Registration',
    'Address' => 'Address',
    'Agent' => 'Agent',
    'Company information' => 'Company information',
    'Restore' => 'Reset password',
    'Restore email subject' => 'Password reset',
    'Restore password message' => 'To reset your password, follow the link: ',
    'Restore button' => 'Forgot your password?',
    'Success restore password' => 'You have successfully changed your password!',
    'send_restore_email' => 'A password reset link has been sent to your email!',
    'send_verify_email' => 'A confirmation link has been sent to you!',
    'No verify email' => 'Email not verified!',
    'Charity organization' => 'Charity organization',
    'Recipient' => 'Recipient',
    'Verify email subject' => 'Email confirmation',
    'Verify email message' => 'You have successfully registered! To verify your email, follow this link:',
    'Verify' => 'Verify',
    'Success verify email' => 'Your email has been successfully verified!',
    'acquainted with the public offer contract' => "acquainted with the <a class=\"_cursor-pointer text--italic\" target=\"_blank\" style=\"border-bottom: 1px solid black;\" href=\":url\">public offer contract</a>",
    'login' => [
        'incorrect' => 'Login and / or password entered incorrectly!'
    ],
    'verification' => [
        'sanction' => 'Availability in sanctions lists',
        'founderRu' => 'Russian beneficiaries',
        'criminalCourts' => 'Criminal courts',
    ],
    'form' => [
        'email' => 'Email',
        'password' => 'Password',
        'password_confirmation' => 'Password Confirmation',
        'zip_code' => 'Zip code',
        'id_code' => 'ID code',
        'Name organisation' => 'Organization Name',
        'country' => 'Country',
        'city' => 'City',
        'street' => 'Street',
        'build' => 'Build',
        'name' => 'Full Name',
        'type_user' => 'User Type',
        'text' => 'Description of the organization',
        'text_short' => 'Short info',
        'photo' => 'Logo',
        'phone' => 'Phone',
        'additional_phone' => 'Additional phone',
        'number_cells' => 'Number of cells in Ukraine',
        'website' => 'Website',
        'facebook' => 'Facebook',
        'instagram' => 'Instagram',
        'additional_email' => 'Additional email',
        'messages' => [
            'confirmed_password' => 'Passwords do not match',
            'email' => 'Enter a valid email',
            'Email already exists' => 'Email already exists',
            'Email not found' => 'Email not found',
            'Enter valid :attribute!' => 'Enter valid :attribute!',
            'required' => [
                'email' => 'Email field is required',
                'phone' => 'Phone field is required',
                'password' => 'Password required',
                'password_confirmation' => '',
                'zip_code' => 'Zip code field is required',
                'name' => 'Organization Name field is required',
                'country' => 'Country required field',
                'city' => 'City required field',
                'street' => 'Street field required',
                'build' => 'Build filed required',
                'type_user' => 'User type field is required',
                'id_code' => 'EDRPOU field is required',
            ],
            'unique' => [
                'id_code' => 'The organization with such EDRPOU is already registered!'
            ]
        ],
        'Open information to all users' => 'Open information to all users'
    ],
    'Warehouse address' => 'Warehouse address',
    'setting' => [
        'Phone for notifications' => 'Phone for notifications',
        'Basic' => 'Basic',
        'Additional' => 'Additional',
        'Public phone' => 'Public phone',
        'Digital signature of documents using Telegram bot' => 'Digital signature of documents using Telegram bot.',
        'Follow the link to get digital codes' => 'Follow the link to get digital codes.',
        'Telegram login' => 'Telegram login',
    ],
    'No organization with such EDRPOU was found!' => 'No organization with such EDRPOU was found!',
    'The organization with such EDRPOU is already registered!' => 'The organization with such EDRPOU is already registered!',
];
