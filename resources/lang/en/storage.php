<?php
return [
    'Product' => 'Product',
    'Name of products' => 'Name of products',
    'Received from:' => 'Received from:',
    'Units' => 'Units',
    'quantity' => 'Quantity',
    'Residue' => 'Residue',
    'Category' => 'Category',
    'Code' => 'Code',
    'filter' => [
        'name' => 'Name/code search',
        'category' => 'Category',
    ]
];
