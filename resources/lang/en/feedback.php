<?php
return [
    'Categories of questions:' => 'Categories of questions:',
    'Technical support' => 'Technical support',
    'Asisstance in registration and operational activities on the site' => 'Asisstance in registration and operational activities on the site',
    'Cooperation with the aidmonitor project' => 'Cooperation with the aidmonitor project',
    'Other questions' => 'Other questions',
    'Question sent! Our specialist will contact you soon.' => 'Question sent! Our specialist will contact you soon.',
    'Fill in the text of the appeal!' => 'Fill in the text of the appeal!',
    'The text must be at least 10 characters!' => 'The text must be at least 10 characters!',
    'Your question has already been sent. Try again later.' => 'Your question has already been sent. Try again later.',
    'Email not valid!' => 'Email not valid!',
    'Name' => 'Name',
    'Upload file (png, jpg, jpeg, pdf, doc, docx)' => 'Upload file (png, jpg, jpeg, pdf, doc, docx)',
];
