<?php
return [
      'Users of the organization' => 'Users of the organization',
      'Name' => 'Name',
      'Email' => 'Email',
      'User type' => 'User type',
      'Status' => 'Status',
      'Add new' => 'Add new',

      'User updated!' => 'User updated!',
      'User created!' => 'User created!',
      'No users have registered yet!' => 'No users have registered yet!',
];
