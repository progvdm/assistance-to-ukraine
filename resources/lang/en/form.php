<?php
return [
        'messages' => [
          'Required' => 'This field is required!',
          'Success' => 'Success',
          'Error' => 'Error',
          'Validation error' => 'Validation error',
          'Info' => 'Info',
        ],
];
