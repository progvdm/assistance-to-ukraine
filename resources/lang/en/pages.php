<?php
    return [
        'Manual' => 'Manual',
        'How it works' => 'How it works',
        'How it works on the platform' => 'How it works on the platform',
        'This way of organizing the provision and distribution of humanitarian aid makes these processes transparent, as each step is displayed in the donor\'s and recipient\'s workspace. The scheme resembles a necklace in which, after the first donor, each new recipient who receives humanitarian aid is added to the chain of supply, reporting and verification. The donor can see the remains of the humanitarian cargo provided by him in the warehouses of each individual recipient, as well as the entire network formed as a result of transfers to other recipients and final beneficiaries. No recipient report is possible without GPS verification from the place of receipt of the humanitarian cargo and without a corresponding photo report.' => 'This way of organizing the provision and distribution of humanitarian aid makes these processes transparent, as each step is displayed in the donor\'s and recipient\'s dashboard. The scheme resembles a necklace in which, after the first donor, each new recipient who receives humanitarian aid is added to the chain of supply, reporting and verification. The donor can see the remains of the humanitarian cargo provided by him in the warehouses of each individual recipient, as well as the entire network formed as a result of transfers to other recipients and final beneficiaries. No recipient report is possible without GPS verification from the place of receipt of the humanitarian cargo and without a corresponding photo report.',
        'The platform automatically generates reports, sends 5-digit codes for digital signatures via SMS messages, which is similar to SMS confirmations of the banking system and complies with the current legislation of Ukraine.' => 'The platform automatically generates reports, sends 5-digit codes for digital signatures via SMS messages, which is similar to SMS confirmations of the banking system and complies with the current legislation of Ukraine.',
        //'The platform is fully functional in the version for mobile phones, and an application has been developed<br> for scanning QR codes, GPS verification, adding photos and video reports' => 'The platform is fully functional in the version for mobile phones, and an application has been developed<b> for scanning QR codes, GPS verification, adding photos and video reports',
        'The platform is fully functional in the version for mobile phones, and an application has been developed<br> for scanning QR codes, GPS verification, adding photos and video reports' => 'The platform is fully functional and in the version for mobile phones as well. The <a target="_blank" href="https://app.aidmonitor.org">app.aidmonitor.org</a> application is designed <b>for scanning QR codes, GPS verification, adding photos and video reports.</b> This application works in tandem with the main platform, expanding and supplementing the main functionality. ',
        'users' => [
            'popup' => [
                'name' => 'Name',
                'location' => 'Location',
                'number_cells' => 'Number of cells in Ukraine',
                'contacts' => 'Contacts',
                'phone' => 'Phone',
                'facebook' => 'Facebook',
                'instagram' => 'Instagram',
                'website' => 'Website',
                'sanction' => 'Checked for the presence of beneficiaries from Russia, sanctions lists',
                'criminal' => 'Checking for criminal cases against related parties',
                'verify' => 'check passed',
                'not_verify' => 'check failed',
            ]
        ]
    ];
