<?php
return [
    'Good day :name. There are currently no messages.' => 'Good day :name. There are currently no messages.',
    'Hello! \nIndicate the mail for identification. \nMail registered in our system.' => "Hello! \nIndicate the mail for identification. \nMail registered in our system.",
    'Greetings :name. You have successfully subscribed to the message.' => 'Greetings :name. You have successfully subscribed to the message.',
    'User not found! Try again:' => 'User not found! Try again:',
];
