<?php
return [
     'Cabinet' => 'Кабінет',
     'All deliveries' => 'Усі поставки',
     'New shipment' => 'Нова допомога',
     'Donate product' => 'Віддати продукти',
     'Cargos is empty' => 'Допомоги поки що немає!',
    'Received shipments' => 'Отримана допомога',
    'Sent cargos' => 'Надіслана допомога',
    'Donate products' => 'Віддана допомога',
    'Success settings' => 'Ви успішно змінили дані!',
    'Settings' => 'Налаштування профілю',
    'Waiting shipments' => 'Очікувана допомога',
    'Provide assistance' => 'Надати допомогу',
    'Received assistance' => 'Отримана допомога',
    'Give help' => 'Передати допомогу',
    'Assist an individual' => 'Надати допомогу фіз. особі',
    'Choose the category of cargo, the cargo itself, and transfer it to an individual' => 'Обрати категорію вантажу, сам вантаж, та передати саме фізичній особі',
    'Select the cargo category, the cargo itself, and pass it on to another user' => 'Виберіть категорію вантажу, сам вантаж і передайте його іншому користувачеві',
    'Previously received help from an unregistered donor on the site' => 'Раніше отримана допомога від незареєстрованого на сайті донора',
    'Assistance provide to individual' => 'Надати допомогу фіз. особі',
    'Assistance to' => 'Передати допомогу',
    'Warehouse' => 'Склад',
    'System users' => 'Користувачі системи',
];
