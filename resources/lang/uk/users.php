<?php
return [
      'Users of the organization' => 'Користувачі організації',
      'Name' => 'Ім\'я',
      'Email' => 'Email',
      'User type' => 'Тип користувача',
      'Status' => 'Статус',
      'Add new' => 'Додати нового',

      'User updated!' => 'Користувач оновлений!',
      'User created!' => 'Користувач створений!',
      'No users have registered yet!' => 'Користувачів ще не зареєстровано!',
    ];
