<?php
return [
    'messages' => [
        'Required' => 'Це поле обов\'язкове!',
        'Success' => 'Успіх!',
        'Error' => 'Помилка',
        'Validation error' => 'Помилка валідації',
        'Info' => 'Информація',
    ],
];

