<?php
return [
    // Verify SMS popup
    'Receive SMS in Telegram' => 'Надіслати смс у Telegram',
    'Resend sms' => 'Повторно відправити SMS',
    'Sms verification' => 'SMS верифікація',
    'Sms code' => 'SMS код',
    'SMS sent!' => 'SMS відправлено!',
];
