<?php
return [
    'Categories of questions:' => 'Категорії запитань:',
    'Technical support' => 'Технічна підтримка',
    'Asisstance in registration and operational activities on the site' => 'Допомога у реєстрації і операційній діяльності на сайті',
    'Cooperation with the aidmonitor project' => 'Питання співпраці з проектом aidmonitor',
    'Other questions' => 'Інші питання',
    'Question sent! Our specialist will contact you soon.' => 'Запитання відправлено! Незабаром з вами зв\'яжеться наш спеціаліст.',
    'Fill in the text of the appeal!' => 'Заповніть текст звернення!',
    'The text must be at least 10 characters!' => 'Текст має бути не меньше 10 символів!',
    'Your question has already been sent. Try again later.' => 'Ваше запитання вже було відправлено! Спробуйте пізніше.',
    'Email not valid!' => 'Електронна адреса недійсна!',
    'Name' => 'Ім\'я',
    'Upload file (png, jpg, jpeg, pdf, doc, docx)' => 'Завантажити файл (png, jpg, jpeg, pdf, doc, docx)',
];
