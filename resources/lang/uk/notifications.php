<?php
return [
    'You need to sign the document!' => 'Вам треба підписати документ!',
    'There are no messages!' => 'Повідомлень немає!',
    'You have received feedback on the application №:number' => 'Ви отримали відгук на заявку №:number',
    'New aid requests!' => 'На маркетплейсі нові заявки! <br>Пропозиції - :offer. <br>Потреби - :need.',
];
