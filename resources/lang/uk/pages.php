<?php
return [
    'Manual' => 'Інструкція',
    'How it works' => 'Як це працює',
    'How it works on the platform' => 'Як це працює на платформі',
    'This way of organizing the provision and distribution of humanitarian aid makes these processes transparent, as each step is displayed in the donor\'s and recipient\'s workspace. The scheme resembles a necklace in which, after the first donor, each new recipient who receives humanitarian aid is added to the chain of supply, reporting and verification. The donor can see the remains of the humanitarian cargo provided by him in the warehouses of each individual recipient, as well as the entire network formed as a result of transfers to other recipients and final beneficiaries. No recipient report is possible without GPS verification from the place of receipt of the humanitarian cargo and without a corresponding photo report.' => 'Такий спосіб організації надання та розподілу гуманітарної допомоги дозволяє зробити ці процеси прозорими, оскільки кожен крок відображається у робочому кабінеті донора і реципієнта. Схема нагадує намисто у якому за першим донором до ланцюгу постачання, звітності та верифікації додається кожен новий реципієт який отримує гуманітарну допомогу. Донор може бачити залишки наданого ним гуманітарного вантажу по складам як кожного окремого реципієнта, так і усієї мережі, що утворюється у результаті передачі іншим реципієнтам та кінцевим бенефіціарам. Жоден звіт реципієнта не можливий без GPS-верифікації з місця отримання гуманітарного вантажу та без відповідного фотозвіту.',
    'The platform automatically generates reports, sends 5-digit codes for digital signatures via SMS messages, which is similar to SMS confirmations of the banking system and complies with the current legislation of Ukraine.' => 'Платформа автоматично генерує звіти, надсилає за допомогою смс повідомлень 5 значні коди для цифрових підписів, що є аналогічним до смс підтверджень банківської системи та відповідає діючому законодавству України.',
    'The platform is fully functional in the version for mobile phones, and an application has been developed<br> for scanning QR codes, GPS verification, adding photos and video reports' => 'Платформа повнофункціональна і у версії для мобільних телефонів, а <b>для сканування QR кодів, GPS-верифікації, додавання фото та відеозвітів, розроблено додаток <a target="_blank" href="https://app.aidmonitor.org">app.aidmonitor.org</a></b>',
    'users' => [
        'popup' => [
            'name' => 'Назва',
            'location' => 'Розташування',
            'number_cells' => 'Кількість осередквів в Україні',
            'contacts' => 'Контактні дані',
            'phone' => 'Телефон',
            'facebook' => 'Facebook',
            'instagram' => 'Instagram',
            'website' => 'Веб-адреса',
            'sanction' => 'Перевірено на наявність бенефіціарів із росії, саенкціонних списках',
            'criminal' => 'Перевірка на наявність кримінальних справ на пов\'язаних осіб',
            'verify' => 'перевірку пройдено',
            'not_verify' => 'перевірку не пройдено',
        ]
    ]
];
