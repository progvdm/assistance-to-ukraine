<?php
return [
    'Product' => 'Товар',
    'Name of products' => 'Найменування продуктів',
    'Received from:' => 'Отримано від:',
    'Units' => 'Одиниці',
    'quantity' => 'Кількість',
    'Residue' => 'Залишок',
    'Category' => 'Категорія',
    'Code' => 'Код',
    'filter' => [
        'name' => 'Пошук ім`я/код',
        'category' => 'Категорія',
    ]
];
