export function togglePassword() {

    return {
        isActive: false,

        toggleShow(element) {
            this.isActive = !this.isActive;

            if (element.getAttribute('type') === 'password') {
                element.setAttribute('type', 'text')
            } else {
                element.setAttribute('type', 'password')
            }
        }
    };
}
