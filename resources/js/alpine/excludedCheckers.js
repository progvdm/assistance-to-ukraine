export function excludedCheckers () {
	return {
		watchPromocodes() {
			const promoChecker = this.$refs.promo;
			window.promoChecker = promoChecker;
			promoChecker.addEventListener('change', () => {
				if (promoChecker.checked) {
					window.bonusChecker.checked = false;
					window.bonusChecker.dispatchEvent(new Event('change'));
				}
			});
		},

		watchBonuses() {
			const bonusChecker = this.$refs.bonus;
			window.bonusChecker = bonusChecker;
			bonusChecker.addEventListener('change', () => {
				if (bonusChecker.checked) {
					window.promoChecker.checked = false;
					window.promoChecker.dispatchEvent(new Event('change'));
				}
			});
		}
	}
}
