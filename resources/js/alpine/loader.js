export function loader() {

    return {
        isActive: false,

        setActive() {
            this.isActive = !this.isActive;

            setTimeout( () => {
                this.isActive = !this.isActive;
            }, 300)
        }
    };
}
