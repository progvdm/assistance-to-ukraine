export function multipleAccordion() {
	return {
		isOpen: false,
		get ref() {
			return this.$refs.text;
		},
		open() {
			this.isOpen = !this.isOpen;

			if (this.isOpen) {
				this.ref.style.maxHeight = this.ref.scrollHeight + 'px';
			} else {
				this.ref.style.maxHeight = 0;
			}
		},
		resize() {
			if (this.isOpen) {
				this.ref.style.maxHeight = '100%';
			}
		}
	};
}
