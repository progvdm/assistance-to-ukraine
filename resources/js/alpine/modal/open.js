import { MODAL_EVENT_OPEN, MODAL_EVENT_PRELOAD, MODAL_EVENT_CLOSE } from 'js@/alpine/modal/constants';
import { dispatch } from 'js@/utils/dispatch';
import {livewire} from 'js@/livewire';
import {HOOKS} from 'js@/livewire/constants';
import {isTextarea} from 'js@/utils/element-type';

export function openModal(name, params = {}, config = {}) {
    livewire.on('closePopup', () => {
        dispatch(window, MODAL_EVENT_CLOSE)
    })

	livewire.hook(HOOKS.elementUpdated, (el, component) => {
		if (el.id === 'login-form' || el.id === 'register-form') {
			new MutationObserver(() => {
				dispatch(window, 'update');
				dispatch(window, 'resize');
			}).observe(el, { characterData: true, childList: true, subtree: true, attributes: true })
		}
	});

	return {
		detail() {
			return {
				el: this.$el,
				name,
				config,
				params: {
					...params
				}
			};
		},

		dispatchModalEvent(e, detail) {
		    if(e && e.type){
			    detail.eventName = e.type.toLowerCase();
            }

			if (detail.eventName === 'mouseenter') {
				dispatch(window, MODAL_EVENT_PRELOAD, detail);
			} else {
				dispatch(window, MODAL_EVENT_OPEN, detail);
			}
		},

		open(e) {
			this.dispatchModalEvent(e, this.detail());
		},

		forceOpen(e) {
			const detail = this.detail();

			detail.force = true;
			this.dispatchModalEvent(e, detail);
		},

		// Open after load page
		openOnLoad() {
			const listener = () => {
				dispatch(window, MODAL_EVENT_OPEN, this.detail());
				window.removeEventListener('load', listener);
			};

			window.addEventListener('load', listener);
		}
	};
}
