export const isNotEqual = (obj1, obj2) => {
	if (!obj2) {
		return true;
	} else if (obj1) {
		if (obj1.force) {
			return true;
		}

		return !(obj1.name === obj2.name && JSON.stringify(obj1.params) === JSON.stringify(obj2.params));
	} else {
		return true;
	}
};
