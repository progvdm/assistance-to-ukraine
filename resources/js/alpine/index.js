import { modal } from './modal';
import { openModal } from './modal/open';
import { singleAccordion } from './single-accordion';
import { tabs } from './tabs';
import { excludedCheckers } from './excludedCheckers';

export const scriptsAlpine = {
	modal,
	openModal,
	// singleAccordion,
	// tabs,
	// excludedCheckers
};
