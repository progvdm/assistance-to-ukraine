import {livewire} from "js@/livewire";
import { HOOKS } from 'js@/livewire/constants';

function noty() {
    if ($(".noty__message" ).length)  {
        let notyWrapper = $( ".noty" ),
            timer = 3000;

        setTimeout(function (){
            notyWrapper.css( 'right', '-500px' )
        }, timer)

        setTimeout(function (){
            notyWrapper.empty()
        }, timer+1500)
    }
}

noty()


livewire.hook('element.updated', (el, component) => {
    noty();
})

livewire.hook('element.initialized', (el, component) => {
    noty();
})
