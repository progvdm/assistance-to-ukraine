// -----------------------------------------------------------------------------
// Constants
// -----------------------------------------------------------------------------

export const JS_STATIC_SELECTOR = '.js-static';

import lazyloadRunMobule from 'js@/modules/lazyload';
import configLazyload from 'js@/modules/lazyload/config';
/**
 * Static Module Runner
 */
export const SMR = {
	/**
	 * @param {JQuery} [$root = $('body')]
	 */
	runAll($root = $('body')) {
		const $elements = $root.find(JS_STATIC_SELECTOR);
        lazyloadRunMobule($elements.filter(configLazyload.filterSelector));
    }
};
