import arrayFrom from 'js@/var/array-from';
import { addClass, removeClass } from 'js@/utils/classes';
import { dispatch } from 'js@/utils/dispatch';

export const create = ({ modules, selector, debug, pendingCssClass, loadedCssClass, errorCssClass }) => {
	const _cache = new Set();

	const _log = (message) => {
		if (debug) {
			console.log('[DMI]: ' + message);
		}
	};

	const _getElements = (container) => {
		const elements = container.querySelectorAll(selector);

		if (elements.length === 0) {
			_log(`No import elements with selector "${selector}"`);
		}

		return elements;
	};

	const _resolveWithErrors = (moduleName, message) => {
		console.warn(`[DMI]: Module "${moduleName}" resolved width errors!!!`);
		console.error(message);

		return Promise.resolve();
	};

	const _markAsPending = (elements, stats) => {
		removeClass(elements, [DMI.loadedEvent, DMI.errorCssClass]);
		addClass(elements, DMI.pendingCssClass);
		dispatch(elements, DMI.pendingEvent, { stats });
	};

	const _markAsLoaded = (elements, stats) => {
		removeClass(elements, [DMI.pendingCssClass, DMI.errorCssClass]);
		addClass(elements, DMI.loadedCssClass);
		dispatch(elements, DMI.loadedEvent, { stats });
	};

	const _markAsError = (elements, stats) => {
		removeClass(elements, [DMI.pendingCssClass, DMI.loadedCssClass]);
		addClass(elements, DMI.errorCssClass);
		dispatch(elements, DMI.errorEvent, { stats });
	};

	const _importFn = (moduleName, elements, container, useImportCondition) => {
		if (!modules.hasOwnProperty(moduleName)) {
			return _resolveWithErrors(moduleName, `Undefined moduleName "${moduleName}"`);
		}

		elements = arrayFrom(elements);

		const module = modules[moduleName];
		const moduleElements = elements.filter((element) => element.matches(module.filter));

		if (moduleElements.length === 0) {
			return Promise.resolve();
		}

		const stats = { moduleName };

		if (useImportCondition && !_cache.has(moduleName) && typeof module.importCondition === 'function') {
			const allowed = module.importCondition(moduleElements, container, stats);

			if (allowed !== true) {
				_log(`module "${moduleName}" skipped by ".importCondition()"`);

				return Promise.resolve();
			}
		}

		_cache.add(moduleName);
		_markAsPending(moduleElements, stats);

		return module
			.importFn(stats)
			.then(({ default: _default }) => {
				if (typeof _default !== 'function') {
					_markAsError(moduleElements, stats);

					return _resolveWithErrors(
						moduleName,
						`imported module "${moduleName}" - must export default method`
					);
				}

				_markAsLoaded(moduleElements, stats);
				_log(`module "${moduleName}" is loaded`);
				_default(moduleElements);

				return Promise.resolve();
			})
			.catch((err) => {
				return _resolveWithErrors(moduleName, err);
			});
	};

	const DMI = {
		get modules() {
			return modules;
		},

		get debug() {
			return debug === true;
		},

		get selector() {
			return selector;
		},

		get pendingCssClass() {
			return pendingCssClass || '_dmi-is-pending';
		},

		get pendingEvent() {
			return 'dmi:pending';
		},

		get loadedCssClass() {
			return loadedCssClass || '_dmi-is-loaded';
		},

		get loadedEvent() {
			return 'dmi:loaded';
		},

		get errorCssClass() {
			return errorCssClass || '_dmi-has-error';
		},

		get errorEvent() {
			return 'dmi:error';
		},

		/**
		 * @param {string} moduleName
		 * @param {HTMLElement} container
		 * @param {func} ignoreImportCondition
		 * @returns {Promise<void>|*}
		 */
		importModule(moduleName, container = document.body, ignoreImportCondition) {
			const elements = _getElements(container);

			if (elements.length === 0) {
				_log('No elements');

				return Promise.resolve();
			}

			return _importFn(moduleName, elements, container, ignoreImportCondition !== true);
		},

		/**
		 * @param {HTMLElement} container
		 * @param {Boolean} awaitAll
		 * @param {func} ignoreImportCondition
		 * @returns {Promise<void>|Promise<void[]>}
		 */
		importAll(container = document.body, awaitAll = false, ignoreImportCondition) {
			const elements = _getElements(container);

			if (elements.length === 0) {
				_log('No elements');

				return Promise.resolve();
			}

			const _getAll = () =>
				Object.keys(modules).map((moduleName) =>
					_importFn(moduleName, elements, container, ignoreImportCondition !== true)
				);

			if (awaitAll) {
				return Promise.all(_getAll());
			} else {
				_getAll();
				return Promise.resolve();
			}
		}
	};

	DMI.importModule.bind(DMI);

	return DMI;
};
