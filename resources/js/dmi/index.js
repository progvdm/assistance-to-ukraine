// -----------------------------------------------------------------------------
// Deps
// -----------------------------------------------------------------------------


// NPM
import { create } from '@wezom/dynamic-modules-import/dist/es6';
// Modules configs
import configBurger from 'js@/modules/burger/config';
import configSwiperCarousel from 'js@/modules/swiper-carousel/config';
import configAccordion from 'js@/modules/accordion/config';
import configFilter from 'js@/modules/filter/config';
import configMagnificPopup from 'js@/modules/magnific-popup/config';
import configDelivery from 'js@/modules/delivery/config';
import configAutoRedirect from 'js@/modules/auto-redirect/config';
import configRecipientTable from 'js@/modules/recipient-table/config';
import configForm from 'js@/modules/form-smooth/config';
import configMap from 'js@/modules/google-map/config';
import configGeoAccess from 'js@/modules/geo-access/config';
import configVideo from 'js@/modules/video/config';
import configMarquee from 'js@/modules/marquee/config';
import configSelect2 from 'js@/modules/select2/config';


// -----------------------------------------------------------------------------
// Constants
// -----------------------------------------------------------------------------

export const JS_IMPORT_CLASS_NAME = 'js-import';
export const JS_IMPORT_SELECTOR = '.js-import';
export const JS_IMPORT_INITIALIZED_KEYS = [
    configBurger.initializedKey,
    configSwiperCarousel.initializedKey,
    configAccordion.initializedKey,
    configFilter.initializedKey,
    configMagnificPopup.initializedKey,
    configDelivery.initializedKey,
    configAutoRedirect.initializedKey,
    configRecipientTable.initializedKey,
    configForm.initializedKey,
    configMap.initializedKey,
    configGeoAccess.initializedKey,
    configVideo.initializedKey,
    configMarquee.initializedKey,
    configSelect2.initializedKey
];

// -----------------------------------------------------------------------------
// Initialize
// -----------------------------------------------------------------------------

/**
 * Dynamic Module Import
 */
export const DMI = create({
    selector: JS_IMPORT_SELECTOR,
    debug: false,
    modules: {
        swiperCarousel: {
            filter: configSwiperCarousel.filterSelector,
            importFn (stats) {
                return import('js@/modules/swiper-carousel');
            }
        },
        burger: {
            filter: configBurger.filterSelector,
            importFn () {
                return import('js@/modules/burger');
            }
        },
        filter: {
            filter: configFilter.filterSelector,
            importFn () {
                return import('js@/modules/filter');
            }
        },
        accordion: {
            filter: configAccordion.filterSelector,
            importFn () {
                return import('js@/modules/accordion');
            }
        },
        magnificPopup: {
            filter: configMagnificPopup.filterSelector,
            importFn() {
                return import('js@/modules/magnific-popup');
            }
        },
        delivery: {
            filter: configDelivery.filterSelector,
            importFn() {
                return import('js@/modules/delivery');
            }
        },
        reload: {
            filter: configAutoRedirect.filterSelector,
            importFn() {
                return import('js@/modules/auto-redirect');
            }
        },
        recipientTable: {
            filter: configRecipientTable.filterSelector,
            importFn() {
                return import('js@/modules/recipient-table');
            }
        },
        form: {
            filter: configForm.filterSelector,
            importFn() {
                return import('js@/modules/form-smooth');
            }
        },
        map: {
            filter: configMap.filterSelector,
            importFn() {
                return import('js@/modules/google-map');
            }
        },
        geoAccess: {
            filter: configGeoAccess.filterSelector,
            importFn() {
                return import('js@/modules/geo-access');
            }
        },
        video: {
            filter: configVideo.filterSelector,
            importFn() {
                return import('js@/modules/video');
            }
        },
        marquee: {
            filter: configMarquee.filterSelector,
            importFn() {
                return import('js@/modules/marquee');
            }
        },
        select2: {
            filter: configSelect2.filterSelector,
            importFn (stats) {
                return import('js@/modules/select2');
            }
        },
        // inputMask: {
        //     filter: configMask.filterSelector,
        //     importFn: () => import('js@/modules/mask'),
        // },
    }
});
