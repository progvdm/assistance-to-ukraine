import $ from 'jquery';
import 'custom-jquery-methods/fn/has-inited-key';
import { DMI } from 'js@/dmi';
import { HOOKS } from 'js@/livewire/constants';
import { dispatch } from 'js@/utils/dispatch';
import { isTextarea } from 'js@/utils/element-type';
import { livewire } from 'js@/livewire';
import { JS_IMPORT_SELECTOR, JS_IMPORT_INITIALIZED_KEYS } from 'js@/dmi';
import { JS_STATIC_SELECTOR } from 'js@/smr';
import { reInitScripts } from "js@/utils/re-init-scripts";

let textareaStyles;

/**
 * A message has been fully received and implemented (DOM updates, etc...)
 */
livewire.hook(HOOKS.messageProcessed, (message, component) => {
	dispatch(component.el, HOOKS.messageProcessed, {
		message,
		component
	});

    const $container = $(`[wire\\:id="${component.id}"]`);
    if ($container.length > 0) {
        const $elements = $container.find(`${JS_STATIC_SELECTOR}, ${JS_IMPORT_SELECTOR}`);
        if ($elements.length > 0) {
            $elements.each((i, element) => {
                const $el = $(element);
                JS_IMPORT_INITIALIZED_KEYS.forEach((key) => {
                    $el.removeInitedKey(key);
                })
            });

            reInitScripts($container);
        }
    }

    if (message.response.serverMemo.errors) { // if livewire memo still has errors
        const $root = $('html, body');
        const $firstErrorParent = $container.find('.js-error').eq(0); // first error in dom
        const headerHeight = $('.js-header').height();

        if (!$container.closest('.modal').length && $container.height() > $(window).height() && $firstErrorParent.length) {
            $root.animate(
                {
                    scrollTop: firstErrorParent.offset().top - headerHeight - 50
                },
                500
            );
        }
    }
});
/*

/**
 * A new Livewire message was just sent to the server
 */
livewire.hook(HOOKS.messageSent, (message, component) => {
	dispatch(component.el, HOOKS.messageSent, {
		message,
		component
	});
});

/**
 * A new element has been initialized
 */
livewire.hook(HOOKS.elementInitialized, (el, component) => {

});

/**
 * An element is about to be updated after a Livewire request
 */
livewire.hook(HOOKS.elementUpdating, (fromEl, toEl, component) => {
	// Save text area styles before Livewire request
	if (isTextarea(fromEl) && fromEl.hasAttribute('style')) {
		textareaStyles = fromEl.getAttribute('style');
	}
});

/**
 * An element has just been updated from a Livewire request
 */
livewire.hook(HOOKS.elementUpdated, (el, component) => {
	dispatch(el, HOOKS.elementUpdated, {
		component,
		el
	});

	// Set text area styles after Livewire request
	if (isTextarea(el) && textareaStyles) {
		el.setAttribute('style', textareaStyles);
		textareaStyles = null;
	}
});

/**
 * This Livewire private hooks, this is not in the documentation
 */
livewire.hook('beforePushState', (state) => {
	state.reload = true;
});

livewire.hook('beforeReplaceState', (state) => {
	state.reload = true;
});

// livewire.hook('component.initialized', (component) => {})
// livewire.hook('element.initialized', (el, component) => {})
// livewire.hook('element.updating', (fromEl, toEl, component) => {})
// livewire.hook('element.updated', (el, component) => {})
// livewire.hook('element.removed', (el, component) => {})
// livewire.hook('message.sent', (message, component) => {})
// livewire.hook('message.failed', (message, component) => {})
// livewire.hook('message.received', (message, component) => {})
// livewire.hook('message.processed', (message, component) => {})
