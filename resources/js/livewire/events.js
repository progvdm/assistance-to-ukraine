import { livewire } from 'js@/livewire';
import { serverResponse } from 'js@/modules/server-response';
// import { updateLanguageSwitchers } from 'js@/modules/language-switcher';

const events = {
	jsResponse(...params) {
		serverResponse(params[0]);
	},

	changeUrl(url, title) {
		document.title = title;

		try {
			window.history.pushState({ reload: true }, title, url);
		} catch (e) {
			console.error(e);
		}
	},

	changeTitle(title) {
		document.title = title;
	},

	// updateLangSwitcher(html) {
	// 	updateLanguageSwitchers(html);
	// }
};

for (const key in events) {
	livewire.on(key, events[key]);
}
