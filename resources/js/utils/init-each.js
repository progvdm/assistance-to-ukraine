'use strict';

// ----------------------------------------
// Imports
// ----------------------------------------

import $ from 'jquery';
import 'custom-jquery-methods/fn/has-inited-key';

// ----------------------------------------
// Public
// ----------------------------------------

/**
 * @param {jQuery} $elements
 * @param {string} key
 * @param {Function} fn
 */
export function initEach($elements, key, fn) {
	$elements.each((i, element) => {
		const $element = $(element);

        if ($element.data(key)) {
			return true;
		} else {
			fn($element, element);
		}
	});
}
