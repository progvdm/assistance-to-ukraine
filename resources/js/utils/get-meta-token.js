/**
 * @return {string|null}
 */
export const getMetaToken = () => {
	const metaToken = document.querySelector('meta[name="csrf-token"]');

	if (metaToken) {
		return metaToken.getAttribute('content');
	}

	return null;
};
