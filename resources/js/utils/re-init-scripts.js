/**
 * @module
 */

// ----------------------------------------
// Imports
// ----------------------------------------


// import wsTabs from 'wezom-standard-gallery-tabs';
import { DMI } from 'js@/dmi';
import { SMR } from 'js@/smr';

// ----------------------------------------
// Inner data
// ----------------------------------------

// code

// ----------------------------------------
// Public
// ----------------------------------------

export const reInitScripts = ($container = $(document), awaitAll = true) => {
	$.type = window.jqueryType;
	// wsTabs.init($container);
	SMR.runAll($container);
	return DMI.importAll($container, awaitAll);
};
