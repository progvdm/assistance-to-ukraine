import arrayFrom from 'js@/var/array-from';

/**
 * @param {HTMLCollection|HTMLElement[]} elements
 * @returns {Array}
 */
export const serializeArray = (elements) => {
	return elements
		.filter((field) => {
			if (['checkbox', 'radio'].includes(field.type)) {
				return field.checked;
			} else if (field.type === 'select-one') {
				return arrayFrom(field.options).find((option) => {
					return option.selected && !option.disabled && option.hasAttribute('value') && Boolean(option.value);
				});
			}

			return field.value !== '';
		})
		.reduce((result, element) => {
			result.push({
				name: element.name,
				value: element.value
			});
			return result;
		}, []);
};
