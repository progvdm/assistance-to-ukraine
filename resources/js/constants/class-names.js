export const CLASS_NAMES = {
	isOpen: 'is-open',
	isActive: 'is-active',
	isFired: 'is-fired',
	hidden: 'is-hidden',
	show: 'is-show'
};
