'use strict';

// ----------------------------------------
// Imports
// ----------------------------------------

import {initEach} from 'js@/utils/init-each';
import CONFIG from './config';
import $ from "jquery";
import marquee from "js@/lib/jquery.marquee.min";
// ----------------------------------------
// Exports
// ----------------------------------------

/**
 * @param {jQuery} $elements
 */
export default function ($elements) {
    initEach($elements, CONFIG.initializedKey, _init);
}

// ----------------------------------------
// Private
// ----------------------------------------

/**
 * @private
 * @param $element
 */
function _init($element) {
    $element.each(function () {
        let $this = $(this);
        $(this).show().marquee({
                duration: $this.data('marquee'),
                gap: 3,
                startVisible: true,
                pauseOnHover: true,
                delayBeforeStart: 1500,
                direction: 'left',
                duplicated: true
            });
    });
}
