/**
 * @type {ModuleConfig}
 */
module.exports = {
	initializedKey: 'marqueeInitialized',
	filterSelector: '[data-marquee]'
};
