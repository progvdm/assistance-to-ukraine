export const sweetAlertGeneralOptions = {
	confirmButtonColor: '#00bccc',
	showClass: {
		popup: 'animated fade-in-down'
	},
	hideClass: {
		popup: 'animated fade-out-up'
	}
};

/**
 * @param {SweetAlertOptions} options
 */
export const mergeSweetAlertOptions = (options) => {
	return Object.assign({}, sweetAlertGeneralOptions, options);
};
