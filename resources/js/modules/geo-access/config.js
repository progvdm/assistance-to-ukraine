/**
 * @type {ModuleConfig}
 */
module.exports = {
    initializedKey: 'geoAccessInitialized',
    filterSelector: '[data-geo-access]'
};
