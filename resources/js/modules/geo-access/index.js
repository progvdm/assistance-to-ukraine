'use strict';

// ----------------------------------------
// Imports
// ----------------------------------------

import { initEach } from 'js@/utils/init-each';
import CONFIG from './config';

// ----------------------------------------
// Exports
// ----------------------------------------

/**
 * @param {jQuery} $elements
 */
export default function ($elements) {
    initEach($elements, CONFIG.initializedKey, _init);
}

// ----------------------------------------
// Private
// ----------------------------------------

/**
 * @private
 * @param $element
 */
function _init ($element) {
    function showLocation( position ) {
        let latitude = position.coords.latitude;
        let longitude = position.coords.longitude;

        $.ajax({
            url: $element[0].dataset.url,
            dataType: "json",
            method: "POST",
            data: {
                "_token" : $element[0].dataset.crf,
                "latitude": latitude,
                "longitude": longitude
            },

            success: function(data) {
                console.log(data);
                if(data.redirect){
                    window.location.assign(data.redirect);
                }
            }
        });
    }

    function errorHandler( err ) {
        if (err.code == 1) {
            console.log(err)
        }
    }

    function getLocation() {
        let geolocation = navigator.geolocation;
        geolocation.getCurrentPosition(showLocation, errorHandler);
    }

    getLocation();
}
