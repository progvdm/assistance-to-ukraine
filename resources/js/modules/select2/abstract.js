// ----------------------------------------
// Imports
// ----------------------------------------

import $ from 'jquery';
import { WebPluginInterface } from 'web-plugin-interface';
import 'select2/dist/js/select2.full';

// ----------------------------------------
// Public
// ----------------------------------------

/**
 * @implements WebPluginInterface
 */
export class AbstractSelect2 extends WebPluginInterface {
	/**
	 * @param {jQuery} $container
	 * @param {Object} [clientSettings={}]
	 * @param {Object} [clientProps={}]
	 */
	constructor ($container, clientSettings = {}, clientProps = {}) {
		super();
		this.$container = $container;
		this.$select = this.$container.find('select');

		/**
		 * @type {Select2Props}
		 */
		this.props = {};
		this.clientProps = clientProps;

		/**
		 * @type {Select2Settings}
		 */
		this.settings = {};
		this.clientSettings = clientSettings;
	}

	/**
	 * @type {Select2Props}
	 */
	get defaultProps () {
		return super.defaultProps;
	}

	/**
	 * @type {Select2Settings}
	 */
	get defaultSettings () {
		return super.defaultSettings;
	}

	/**
	 * @return {Promise}
	 * @protected
	 */
	_setup () {
		this.props = $.extend({}, this.defaultProps, this.clientProps);
		this.settings = $.extend({}, this.defaultSettings, this.clientSettings);

        const language = this.props.language || window.app.language;

        if (language) {
			return import('select2/src/js/select2/i18n/' + language).then(({ default: language }) => {
				$.extend(this.settings, { language });
			});
		} else {
			return Promise.resolve();
		}
	}

	/**
	 * @protected
	 */
	_beforeInitialize () {
		super._beforeInitialize();
	}

	/**
	 * @protected
	 */
	_initialize () {
		this.$select.select2(this.settings);
	}

	/**
	 * @protected
	 */
	_afterInitialize () {
		super._afterInitialize();
	}

	/**
	 * @return {*|Promise<any | never>}
	 */
	initialize () {
		return this._setup().then(() => {
			this._beforeInitialize();
			this._initialize();
			this._afterInitialize();
		});
	}
}

// ----------------------------------------
// Definitions
// ----------------------------------------

/**
 * @typedef {Object} Select2Props
 * @property {string} [language]
 */

/**
 * @typedef {Object} Select2Settings
 * @property {string} [containerCssClass]
 * @property {string} [dropdownCssClass]
 * @property {number} [minimumResultsForSearch]
 * @property {string|number} [width]
 */
