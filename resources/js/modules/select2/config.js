/**
 * @type {ModuleConfig}
 */
module.exports = {
	initializedKey: 'select2InitializedKey',
	filterSelector: '[data-select2]'
};
