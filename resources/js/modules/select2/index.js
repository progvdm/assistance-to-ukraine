'use strict';

// ----------------------------------------
// Imports
// ----------------------------------------

import 'custom-jquery-methods/fn/has-inited-key';
import { initEach } from 'js@/utils/init-each';
import CONFIG from './config';
import createNewFactory from '../../utils/create-new-factory';
import { AbstractSelect2 } from './abstract';
import * as factory from './factory';
// import(
//     /* webpackPreload: true */
//     /* webpackChunkName: "select2" */
//     './sass/index.scss'
// );

// ----------------------------------------
// Exports
// ----------------------------------------

/**
 * @param {jQuery} $elements
 */
export default function ($elements) {
	initEach($elements, CONFIG.initializedKey, _init);
}

// ----------------------------------------
// Private
// ----------------------------------------

/**
 * @param {jQuery} $container
 * @private
 */
function _init ($container) {
	const data = $container.data('initialize-select2');
	/** @type {AbstractSelect2} */
	const instance = createNewFactory(AbstractSelect2, factory, $container, data);
	instance.initialize();
}
