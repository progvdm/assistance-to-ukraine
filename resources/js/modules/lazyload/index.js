// ----------------------------------------
// Imports
// ----------------------------------------

import zzLoad from 'zz-load';
import { initEach } from 'js@/utils/init-each';
import CONFIG from './config';


// ----------------------------------------
// Exports
// ----------------------------------------

/**
 * @param {jQuery} $elements
 */
export default function ($elements) {
    initEach($elements, CONFIG.initializedKey, _init);
}

// ----------------------------------------
// Private
// ----------------------------------------

/**
 * @param {JQuery<HTMLPictureElement>} $element
 * @param {HTMLPictureElement} element
 */

function _init ($element, element) {
    if (element.__zzLoadObserver === undefined || element.__zzLoadObserver === null) {
        element.__zzLoadObserver = zzLoad(element, {
            rootMargin: '20% 0px',
            threshold: 0.1,
            onLoad() {
                element.__zzLoadObserver = null;
            }
        });
        element.__zzLoadObserver.observe();
    }
}
