/**
 * @type {ModuleConfig}
 */
module.exports = {
	initializedKey: 'imageLazyLoad',
	filterSelector: '[data-lazy-load]'
};
