'use strict';

// ----------------------------------------
// Imports
// ----------------------------------------

import { initEach } from 'js@/utils/init-each';
import CONFIG from './config';
import $ from "js@/lib/jquery";

// ----------------------------------------
// Exports
// ----------------------------------------

/**
 * @param {jQuery} $elements
 */
export default function ($elements) {
    initEach($elements, CONFIG.initializedKey, _init);
}

// ----------------------------------------
// Private
// ----------------------------------------

/**
 * @private
 * @param $element
 */
function _init ($element) {
    let playBtn = $('.video__play'),
        stopBtn = $('.popup__close'),
        video = $('video');

    if ($element.data('video') === 'play') {
        video.get(0).play()

        stopBtn.on('click', () => {
            video.get(0).pause()
        })
    }

    if ($('.video__play').length) {
        playBtn.on('click', () => {
            playBtn.hide()
            video.attr('controls', 'true').get(0).play()
        })
    }
}
