/**
 * @type {ModuleConfig}
 */
module.exports = {
	initializedKey: 'videoInitialized',
	filterSelector: '[data-video]'
};
