/**
 * @type {ModuleConfig}
 */
module.exports = {
	initializedKey: 'recipientTableInitialized',
	filterSelector: '[data-recipient-table]'
};
