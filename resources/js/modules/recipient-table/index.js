'use strict';

// ----------------------------------------
// Imports
// ----------------------------------------
import { initEach } from 'js@/utils/init-each';
import CONFIG from './config';

// ----------------------------------------
// Exports
// ----------------------------------------

/**
 * @param {jQuery} $elements
 */
export default function ($elements) {
    initEach($elements, CONFIG.initializedKey, _init);
}

// ----------------------------------------
// Private
// ----------------------------------------

/**
 * @private
 * @param $element
 */
function _init ($element) {
    function setActive(line, el) {
        if (line.closest('._nmt-sm').next().find('[data-donor*="/'+ el +'/"]').length) {
            line.addClass('is-active').closest('._cell').next()
                .find('[data-donor*="/'+ el +'/"]').addClass('is-active');
        }
    }

    function setTopPosition(line, el, topPositionSiblings) {
        if (line.closest('._nmt-sm').next().find('[data-donor*="/'+ el +'/"]').length) {
            line.addClass('is-active')
                .closest('._nmt-sm').next().find('[data-donor*="/'+ el +'/"]').each(function () {
                topPositionSiblings.push($(this).position().top + $(this).outerHeight() / 2)
            })
        }
    }

    function calcPseudoLine(line, delta) {
        line.closest('._cell').find('.recipient__pseudo-line')
            .css('height', Math.abs(delta))
    }

    $($element).on('mouseover', '.recipient__list [data-donor]', function (e){
        let $this = $(this),
            idDonor = e.currentTarget.getAttribute("data-donor"),
            topPositionParent = $this.position().top + $this.outerHeight() / 2,
            topPositionSiblings = [],
            delta;

        $('[data-donor]').removeClass('is-active');

        idDonor = idDonor.split('/');

        $.each(idDonor,function(index, value){
            if (value.length > 0) {
                setActive($this, value);
                setTopPosition($this, value, topPositionSiblings);
            }
        })

        if (topPositionSiblings.length > 1) {
            if (topPositionParent <= topPositionSiblings[0]) {
                delta =  topPositionSiblings[topPositionSiblings.length - 1] - topPositionParent;
                $this.closest('._cell').find('.recipient__pseudo-line')
                    .css('top', ($this.outerHeight() / 2 ) )
           } else {
                if (topPositionParent > topPositionSiblings[topPositionSiblings.length - 1]) {
                    delta = topPositionSiblings[0] - topPositionParent;
                } else {
                    delta = topPositionSiblings[topPositionSiblings.length - 1] - topPositionSiblings[0];
                }

                $this.closest('._cell').find('.recipient__pseudo-line')
                    .css('top', (topPositionSiblings[0] - topPositionParent + $this.outerHeight() / 2))
            }

            calcPseudoLine($this, delta)
        } else {
            delta = topPositionParent - topPositionSiblings[0];

            if (topPositionParent <= topPositionSiblings[0]) {
                $this.closest('._cell').find('.recipient__pseudo-line')
                    .css('top', $this.outerHeight()/2)
            } else {
                $this.closest('._cell').find('.recipient__pseudo-line')
                    .css('top', $this.outerHeight()/2 - delta)
            }

            calcPseudoLine($this, delta)
        }
    })

    $($element).on('mouseout', '[data-donor]', function (){
        $('.recipient__list .is-active').removeClass('is-active')
    });
}





