/**
 * @type {ModuleConfig}
 */
module.exports = {
	initializedKey: 'imask',
	filterSelector: '[data-mask]'
};
