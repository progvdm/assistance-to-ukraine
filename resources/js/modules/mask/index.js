import Inputmask from 'inputmask';
import { HOOKS } from 'js@/livewire/constants';
import { subscribeHook, sendInputValueToLivewire } from 'js@/livewire/utils';
import {wrapInit} from "js@/utils/wrap-init";

const {
    mask: { phone }
} = window.app;

/**
 * @param {HTMLInputElement} input
 */
function inputMask(input) {
    const mask = input.dataset.mask || phone;

    let isComplete = false;

    const inputmask = new Inputmask(mask, {
        // refreshValue: true,
        showMaskOnHover: true,
        oncomplete: function () {
            isComplete = true;
            sendInputValueToLivewire(this);
        },
        onincomplete: function () {
            sendInputValueToLivewire(this);
        }
    });

    inputmask.mask(input);

    const handlerPaste = () => {
        sendInputValueToLivewire(input);
    };

    const handlerChange = () => {
        if (isComplete) {
            isComplete = false;

            return;
        }

        sendInputValueToLivewire(input);
    };

    const removeEvents = () => {
        input.removeEventListener('paste', handlerPaste, false);
        input.removeEventListener('change', handlerChange, false);
    };

    const addEvents = () => {
        input.addEventListener('paste', handlerPaste, false);
        input.addEventListener('change', handlerChange, false);
    };

    if (input.value === input.inputmask.getemptymask()) {
        input.setAttribute('value', '');
        input.inputmask.setValue('');
    }

    removeEvents();
    addEvents();

    subscribeHook(
        input,
        HOOKS.elementUpdated,
        () => {
            removeEvents();
        },
        true
    );
}

export default (elements) => {
    wrapInit(elements, inputMask);
};
