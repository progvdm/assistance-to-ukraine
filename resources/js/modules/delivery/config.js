/**
 * @type {ModuleConfig}
 */
module.exports = {
	initializedKey: 'deliveryInitialized',
	filterSelector: '[data-delivery]'
};
