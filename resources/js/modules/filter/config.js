/**
 * @type {ModuleConfig}
 */
module.exports = {
	initializedKey: 'filterInitialized',
	filterSelector: '[data-filter]'
};
