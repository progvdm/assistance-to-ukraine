'use strict';

// ----------------------------------------
// Imports
// ----------------------------------------

import { initEach } from 'js@/utils/init-each';
import CONFIG from './config';

// ----------------------------------------
// Exports
// ----------------------------------------

/**
 * @param {jQuery} $elements
 */
export default function ($elements) {
    initEach($elements, CONFIG.initializedKey, _init);
}

// ----------------------------------------
// Private
// ----------------------------------------

/**
 * @private
 * @param $element
 */
function _init ($element) {
    let contentBlock = $('.delivery__wrapper');

    $element.on('click', '.filter__icon', function () {
        let type = $(this).data('filter');

        $('.filter__icon').removeClass('is-active')
        $(this).addClass('is-active')

        if (type === 'grid') {
            contentBlock.removeClass('delivery__wrapper--table').addClass('delivery__wrapper--grid _spacer _spacer--md')
        } else {
            contentBlock.removeClass('delivery__wrapper--grid _spacer _spacer--md').addClass('delivery__wrapper--table')
        }

    })
}
