/**
 * @type {ModuleConfig}
 */
module.exports = {
	initializedKey: 'autoRedirectInitialized',
	filterSelector: '[data-auto-redirect]'
};

