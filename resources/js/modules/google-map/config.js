/**
 * @type {ModuleConfig}
 */
module.exports = {
    initializedKey: 'deliveryMapInitialized',
    filterSelector: '[data-delivery-map]'
};
