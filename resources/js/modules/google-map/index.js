'use strict';

// ----------------------------------------
// Imports
// ----------------------------------------

import { initEach } from 'js@/utils/init-each';
import CONFIG from './config';

// ----------------------------------------
// Exports
// ----------------------------------------

/**
 * @param {jQuery} $elements
 */
export default function ($elements) {
    initEach($elements, CONFIG.initializedKey, _init);
}

// ----------------------------------------
// Private
// ----------------------------------------

/**
 * @private
 * @param $element
 */
function _init ($element) {
    let google = window.google;
    let map;
    let holder = $element;
    let bounds = new google.maps.LatLngBounds();
    let infowindow = new google.maps.InfoWindow();
    let address;
    let service = new google.maps.DirectionsService();

    let mapOptions = {
        zoom: 10,
    };

    let _createMarker = (obj) => {
        let marker = new google.maps.Marker({
            position: new google.maps.LatLng(obj.lat, obj.lng),
            map: map,
            icon: '/images/geo-icon.png',
            title: obj.name,
            label: {
                text:  obj.number,
                className: 'marker__label'
            },
        });

        google.maps.event.addListener(marker, 'click', function () {
            infowindow.setContent(`
			<div class="marker__popup">
				<div class="marker__title _mb-xs">${obj.name}</div>
				<div class="marker__sub-title _mb-xs">Comment:</div>
				<div class="marker__comment _mb-xs">${obj.comment}</div>
			</div>
		`);
            /*' <svg class="marker__photo" id="${obj.number}" width="30" height="30" viewBox="0 0 23 23" fill="none"
                     xmlns="http://www.w3.org/2000/svg">
                    <circle cx="11.5002" cy="11.5" r="10.85" fill="white" stroke="#353535"
                            stroke-width="1.3"/>
                    <path fill="black" fill-rule="evenodd" clip-rule="evenodd"
                          d="M9.5609 7.375L8.28057 8.54864C8.13992 8.67757 7.94916 8.75 7.75024 8.75L5.50024 8.75V15.625H17.5002V8.75H15.2502C15.0513 8.75 14.8606 8.67757 14.7199 8.54864L13.4396 7.375H9.5609ZM11.5002 10.4688C10.6718 10.4688 10.0002 11.0844 10.0002 11.8437C10.0002 12.6031 10.6718 13.2187 11.5002 13.2187C12.3287 13.2187 13.0002 12.6031 13.0002 11.8437C13.0002 11.0844 12.3287 10.4688 11.5002 10.4688ZM8.50024 11.8437C8.50024 10.325 9.84339 9.09375 11.5002 9.09375C13.1571 9.09375 14.5002 10.325 14.5002 11.8437C14.5002 13.3625 13.1571 14.5937 11.5002 14.5937C9.84339 14.5937 8.50024 13.3625 8.50024 11.8437Z"
                          class="cl1"/>
                    <path d="M6.00024 8.5V7.5H7.50024L7.00024 8.5H6.00024Z" class="cl1"/>
                </svg>'*/
            // ${obj.phones.map(item => `<div class="text-b14"><a href="tel:${item.link}" class="link-block text-b14">${item.text}</a></div>`).join('')}

            infowindow.open(map, marker);
        });
    };

    let _getAddress = () => new Promise((resolve) => {
        $.ajax({url: $element[0].dataset.urlJson, dataType: 'json'}).done(function (data) {
            resolve(data);
        });
    });

    let _setMarkers = () => {
        let promise = _getAddress();
        promise.then(
            result => {
                address = result.data;
                for (let k in address) {
                    _createMarker(address[k]);
                    bounds.extend(new google.maps.LatLng(address[k].lat, address[k].lng));
                }
                map.fitBounds(bounds);
            }
        );
    };

    let _setRoad = () => {
        let promise = _getAddress();

        promise.then(
            result => {
                address = result.data;

                for (let i = 0; i < Object.keys(address).length; i++) {
                    if ((i + 1) < Object.keys(address).length) {
                        let src = Object.keys(address)[i];
                        let des = Object.keys(address)[i + 1];

                        service.route({
                            origin: src,
                            destination: des,
                            travelMode: google.maps.DirectionsTravelMode.WALKING
                        }, function(result, status) {
                            if (status == google.maps.DirectionsStatus.OK) {
                                var path = new google.maps.MVCArray();
                                var poly = new google.maps.Polyline({
                                    map: map,
                                    strokeColor: '#4986E7'
                                });
                                poly.setPath(path);
                                for (var i = 0, len = result.routes[0].overview_path.length; i < len; i++) {
                                    path.push(result.routes[0].overview_path[i]);
                                }
                            }
                        });
                    }
                }

            }
        );
    };

    map = new google.maps.Map(holder[0], mapOptions);

    _setMarkers();
    _setRoad();

    $('body').on('click', 'marker__photo', function () {
        let $this = $(this),
            id = $this.attr('id');

        $('photo-popup[id=""]').show();
    })
}
