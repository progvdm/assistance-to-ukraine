// ----------------------------------------
// Imports
// ----------------------------------------

import $ from 'jquery';
import 'custom-jquery-methods/fn/has-inited-key';
import CONFIG from './config';
import {
    Swiper,
    Navigation,
    EffectFade,
    Pagination,
    Autoplay,
    Thumbs,
    Lazy
} from 'swiper';
import { initEach } from "js@/utils/init-each";
// import(
//     /* webpackPreload: true */
//     /* webpackChunkName: "swiper" */
//     './sass/index.scss'
// );

Swiper.use([ Navigation, EffectFade, Pagination, Autoplay, Thumbs, Lazy]);


// ----------------------------------------
// Exports
// ----------------------------------------

/**
 * @param {jQuery} $elements
 */
export default function ($elements) {
    initEach($elements, CONFIG.initializedKey, _init);
}

// ----------------------------------------
// Private
// ----------------------------------------

/**
 * @param {jQuery} $element
 * @private
 */
function _init ($element) {
    const params = $element.data('swiper-slider');

    switch (params.type) {
        case 'NewsSlider':
            {
                const slider = new Swiper($element[0], { // eslint-disable-line no-unused-vars
                    loop: false,
                    slidesPerView: 'auto',
                    watchSlidesVisibility: true,
                    lazy: true,
                    autoplay: true,
                    speed: 1000,
                    updateOnWindowResize: true,
                    navigation: {
                        nextEl: '.swiper-button-next',
                        prevEl: '.swiper-button-prev',
                    },
                    on: {
                        afterInit: (instance) => {
                            if ($(instance.navigation.nextEl).hasClass('swiper-button-disabled') &&
                                $(instance.navigation.prevEl).hasClass('swiper-button-disabled')) {
                                $(instance.navigation.prevEl).parent().hide();
                            }

                            changeText()
                        },
                        slideChange: (instance) => {
                            changeText()
                        }
                    }
                });
            }
            break;
        case 'GallerySlider':
            {
                const sliderThumbs = new Swiper('.swiper-thumbs', {
                    spaceBetween: 20,
                    slidesPerView: 'auto',
                    touchRatio: 0.2,
                    watchOverflow: true,
                    slideToClickedSlide: true,
                    loop: false
                });

                const slider = new Swiper($element[0], { // eslint-disable-line no-unused-vars
                    loop: false,
                    slidesPerView: 1,
                    speed: 1000,
                    setWrapperSize: true,
                    watchOverflow: true,
                    updateOnWindowResize: true,
                    spaceBetween: 30,
                    navigation: {
                        nextEl: '.swiper-button-next',
                        prevEl: '.swiper-button-prev',
                    },
                    on: {
                        afterInit: (instance) => {
                            if ($(instance.navigation.nextEl).hasClass('swiper-button-disabled') &&
                                $(instance.navigation.prevEl).hasClass('swiper-button-disabled')) {
                                $(instance.navigation.prevEl).parent().hide();
                            }
                        }
                    },
                    thumbs: {
                        swiper: sliderThumbs,
                        slideThumbActiveClass: 'is-active',
                        thumbsContainerClass: 'swiper-thumbs'
                    }
                });
            }
            break;
        case 'SidebarSlider':
            {
                const slider = new Swiper($element[0], { // eslint-disable-line no-unused-vars
                    loop: false,
                    slidesPerView: "auto",
                    speed: 1000,
                    setWrapperSize: true,
                    watchOverflow: true,
                    updateOnWindowResize: true,
                    autoHeight: true,
                    navigation: {
                        nextEl: '.swiper-button-next',
                        prevEl: '.swiper-button-prev',
                    },
                    on: {
                        afterInit: (instance) => {
                            if ($(instance.navigation.nextEl).hasClass('swiper-button-disabled') &&
                                $(instance.navigation.prevEl).hasClass('swiper-button-disabled')) {
                                $(instance.navigation.prevEl).parent().hide();
                            }
                        }
                    }
                });
            }
            break;
        case 'PartnersSlider':
            {
                const slider = new Swiper($element[0], { // eslint-disable-line no-unused-vars
                    loop: false,
                    slidesPerView: "auto",
                    autoplay: true,
                    speed: 1000,
                    setWrapperSize: true,
                    watchOverflow: true,
                    updateOnWindowResize: true,
                });
            }
            break;
    }
}

function changeText() {
    let textBlock = $('.news-text');

    $('.swiper-slide-active').each(function () {
        let text = $(this).find('.news-card__text').html()
        textBlock.html(text)
    })
}
