/**
 * @type {ModuleConfig}
 */
module.exports = {
	initializedKey: 'swiperCarouselInitialized',
	filterSelector: '[data-swiper-slider]'
};
