/**
 * @type {ModuleConfig}
 */
module.exports = {
	initializedKey: 'accordionInitialized',
	filterSelector: '[data-accordion]'
};
