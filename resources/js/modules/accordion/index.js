'use strict';

// ----------------------------------------
// Imports
// ----------------------------------------

import {initEach} from 'js@/utils/init-each';
import CONFIG from './config';
import $ from "jquery";

// ----------------------------------------
// Exports
// ----------------------------------------

/**
 * @param {jQuery} $elements
 */
export default function ($elements) {
    initEach($elements, CONFIG.initializedKey, _init);
}

// ----------------------------------------
// Private
// ----------------------------------------

/**
 * @private
 * @param $element
 */
function _init($element) {
    $element.each(function () {
        switch ($element.data('accordion')) {
            case 'faq': {
                $(this).on('click', function () {
                    let $this = $(this);

                    if (!$this.hasClass("is-active")) {
                        $(".faq__question").removeClass("is-active").next().slideUp(400);
                        $(".accordion__title")
                    }

                    $this.toggleClass("is-active");
                    $this.stop().next().slideToggle();
                })
            }
                break;
            default: {
                $(this).on('click', function () {
                    $(this).toggleClass('is-active').next().stop().slideToggle();
                })
            }
                break;
        }
    });
}
