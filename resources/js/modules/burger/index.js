'use strict';

// ----------------------------------------
// Imports
// ----------------------------------------

import { initEach } from 'js@/utils/init-each';
import CONFIG from './config';

// ----------------------------------------
// Exports
// ----------------------------------------

/**
 * @param {jQuery} $elements
 */
export default function ($elements) {
    initEach($elements, CONFIG.initializedKey, _init);
}

// ----------------------------------------
// Private
// ----------------------------------------

/**
 * @private
 * @param $element
 */
function _init ($element) {
    $element.each(function () {
        const $burgerBtn = $(this);
        const $mobileMenu = $('.menu--mobile');

        $burgerBtn.on('click', function () {
            $burgerBtn.toggleClass('is-active')
            $mobileMenu.toggleClass('is-active')
        })

        $mobileMenu.on('click', function (e) {
            if ($(e.target).hasClass('menu--mobile')) {
                $burgerBtn.trigger('click')
            }
        })
    });
}
