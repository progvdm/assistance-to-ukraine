/**
 * @type {ModuleConfig}
 */
module.exports = {
    initializedKey: 'burgerInitialized',
    filterSelector: '[data-hamburger]'
};
