/**
 * @type {ModuleConfig}
 */
module.exports = {
	initializedKey: 'formInitialized',
	filterSelector: '[data-form]'
};
