'use strict';

// ----------------------------------------
// Imports
// ----------------------------------------

import { initEach } from 'js@/utils/init-each';
import CONFIG from './config';
import $ from "js@/lib/jquery";

// ----------------------------------------
// Exports
// ----------------------------------------

/**
 * @param {jQuery} $elements
 */
export default function ($elements) {
    initEach($elements, CONFIG.initializedKey, _init);
}

// ----------------------------------------
// Private
// ----------------------------------------

/**
 * @private
 * @param $element
 */
function _init ($element) {
    inputChange($element)
}

function inputChange(el) {
    el.find('input[type="text"], ' +
        'input[type="password"], ' +
        'input[type="email"], ' +
        'input[type="number"], ' +
        'button[class^="newPost"]').each(function () {
        let $this = $(this);

        if ($this.attr('id') != undefined && !$this.attr('id').includes("storage_city")) {
            $this.css('padding-left', $this.prev().css('width', 'auto').width() + 10)
            $this.prev().css('width', '100%')
        } else {
            let padding = $this.closest('.newPost-select').find('.with-anim-underline').css('width', 'auto').width() + 10;
            $this.css('padding-left', padding);
            $('#storage_city-selected, .newPost-select__option--selected').css('padding-left', padding);
            $this.closest('.newPost-select').find('.with-anim-underline').css('width', '100%')
        }

        function setLine() {
            if ($this.val().length > 0) {
                if ($this.attr('id')) {
                    $this.prev().addClass('with-value');
                } else {
                    $this.closest('label').addClass('with-value');
                }
            } else {
                if ($this.attr('id')) {
                    $this.prev().removeClass('with-value');
                } else {
                    $this.closest('label').removeClass('with-value');
                }
            }
        }

        setLine();
        $this.on('change', setLine);
    })
}
