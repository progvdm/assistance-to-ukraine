/**
 * @type {ModuleConfig}
 */

module.exports = {
	initializedKey: 'magnificPopupInitialized',
	filterSelector: '[data-mfp]'
};
