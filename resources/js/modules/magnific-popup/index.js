'use strict';

// ----------------------------------------
// Imports
// ----------------------------------------

import $ from 'jquery';
import 'magnific-popup';

// ----------------------------------------
// Exports
// ----------------------------------------

export default function () {
    $('[data-mfp="gallery"]').magnificPopup({
        delegate: '.tab__item',
        type: 'image',
        tLoading: 'Loading image #%curr%...',
        gallery: {
            enabled: true,
            navigateByImgClick: true,
            preload: [0,1]
        },
        image: {
            tError: '<a href="%url%">The image #%curr%</a> could not be loaded.',
            titleSrc: function(item) {
                return item.el.attr('data-description');
            }
        }
    });
}
