declare interface ModuleConfig {
	initializedKey: string;
	filterSelector: string;
}
