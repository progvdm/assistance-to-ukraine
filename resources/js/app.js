/**
 * Polyfills for older browsers are described in the "browserslist legacy".
 * In final build for "browserslist modern", these polyfills will not be included
 */
import 'regenerator-runtime/runtime';
import $ from 'js@/lib/jquery';

import 'alpine-magic-helpers/dist/component';
import 'alpine-magic-helpers/dist/screen';
import Alpine from 'alpinejs'
import collapse from '@alpinejs/collapse'
import { scriptsAlpine } from 'js@/alpine';

import 'js@/livewire/events';
import 'js@/livewire/hooks';
// import 'js@/livewire/on-error';

import { serverResponse } from 'js@/modules/server-response';

import { SMR } from 'js@/smr';
import { DMI } from 'js@/dmi';

import 'js@/noty';

window.serverResponse = serverResponse;
window.Alpine = Alpine

Object.assign(Alpine, {
    plugins: {
        ...scriptsAlpine
    }
});

Alpine.plugin(collapse)
Alpine.start()

document.addEventListener('DOMContentLoaded', () => {
    const $root = $('html, body');
    const $body = $('body');
    const $window = $(window);

    $body.addClass('is-ready');
    SMR.runAll($root);
    DMI.importAll($root);

    function checkBrowser() {
        let browser = "";
        let c = navigator.userAgent.search("Chrome");
        let f = navigator.userAgent.search("Firefox");

        if (c > -1) {
            browser = "chrome";
        } else if (f > -1) {
            browser = "firefox";
        }

        $('body').addClass(browser)
    }

    function smoothScrollingTo(target){
        $('html,body').animate({scrollTop:$(target).offset().top}, 700);
    }

    checkBrowser()

    if (location.hash) {
        smoothScrollingTo(location.hash);
    }

    $('a[href*="#"]').on('click', function(event){
        if (location.hash === this.hash) {
            event.preventDefault();
            smoothScrollingTo(this.hash);
        } else {
            smoothScrollingTo(this.hash);
        }
    });

    if ($('.soc-widget').length) {
        let socialIcons = $('.soc-widget');

        $(window).on('scroll', function() {
            if($(window).scrollTop() + $(window).height() >= $(document).height()){
                socialIcons.addClass('is-hide');
            } else {
                socialIcons.removeClass('is-hide');
            }
        });
    }

    if ($('.road-map:visible').length) {
        let lastStep = $('.road-map .path').length;

        const delay = (ms) => new Promise((resolve) => {
            setTimeout(resolve, ms);
        });

        (async () => {
            for (var i = 1; i <= lastStep; i++) {
                $('.path--' + i).css('opacity', 1)
                $('.star--' + i).css('opacity', 1)
                $('.circle--' + i).css('opacity', 1)
                $('.city--' + i).css('opacity', 1)

                await delay(1000);
            }
        })()
    }
});

document.addEventListener('livewire:update', function () {
    const $body = $('body');

    $('.form').find('input[type="text"], ' +
        'input[type="password"], ' +
        'input[type="email"], ' +
        'input[type="number"], ' +
        'button[class^="newPost"]').each(function () {
        let $this = $(this);

        if ($this.attr('id') != undefined && !$this.attr('id').includes("storage_city")) {
            $this.css('padding-left', $this.prev().css('width', 'auto').width() + 10)
            $this.prev().css('width', '100%')
        } else {
            let padding = $this.closest('.newPost-select').find('.with-anim-underline').css('width', 'auto').width() + 10;
            $this.css('padding-left', padding);
            $('#storage_city-selected, .newPost-select__option--selected').css('padding-left', padding);
            $this.closest('.newPost-select').find('.with-anim-underline').css('width', '100%')
        }

        function setLine() {
            if ($this.val().length > 0) {
                if ($this.attr('id')) {
                    $this.prev().addClass('with-value');
                } else {
                    $this.closest('label').addClass('with-value');
                }
            } else {
                if ($this.attr('id')) {
                    $this.prev().removeClass('with-value');
                } else {
                    $this.closest('label').removeClass('with-value');
                }
            }
        }

        setLine();
        $this.on('change', setLine);
    })
})
