<?php

namespace Database\Seeders;

use App\Models\Pages\SystemPage;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;

class CreateMarketplacePage extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $page = new SystemPage();
        $page->slug = 'marketplace';
        $page->published = true;
        $page->save();
        foreach (LaravelLocalization::getSupportedLocales() as $locale => $item) {
            $page->translateOrNew($locale)->fill([
                'name' => 'Aid Marketplace',
                'title' => 'Aid Marketplace',
                'description' => 'Aid Marketplace',
                'h1' => 'Aid Marketplace',
                'system_page_id' => $page->id
            ])->save();
        }
    }
}
