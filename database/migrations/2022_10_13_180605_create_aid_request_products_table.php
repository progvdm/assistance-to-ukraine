<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('aid_request_products', function (Blueprint $table) {
            $table->id();
            $table->foreignId('aid_request_id')->constrained('aid_requests')->cascadeOnDelete();
            $table->foreignId('list_product_id')->constrained('list_products')->cascadeOnDelete();
            $table->string('type')->nullable();
            $table->integer('quantity')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('aid_request_products');
    }
};
