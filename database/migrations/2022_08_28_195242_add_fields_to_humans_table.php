<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('humans', function (Blueprint $table) {
            $table->string('address')->after('phone')->nullable();
            $table->string('id_code')->after('address')->nullable();
            $table->string('responsible_person')->after('id_code')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('humans', function (Blueprint $table) {
            $table->dropColumn('address');
            $table->dropColumn('id_code');
            $table->dropColumn('responsible_person');
        });
    }
};
