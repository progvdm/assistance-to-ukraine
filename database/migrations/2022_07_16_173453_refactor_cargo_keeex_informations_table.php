<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::rename('cargo_keeex_informations', 'cargo_informations');

        Schema::table('cargo_informations', function (Blueprint $table) {
            $table->string('type')->default('keeex')->after('cargo_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::rename('cargo_informations', 'cargo_keeex_informations');

    }
};
