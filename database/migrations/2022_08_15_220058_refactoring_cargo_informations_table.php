<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');

        Schema::rename('cargo_informations', 'transfer_informations');

        Schema::table('transfer_informations', function (Blueprint $table) {
            $table->dropConstrainedForeignId('cargo_id');
            $table->dropColumn('cargo_id');
            $table->foreignId('transfer_id')->change()->constrained('transfers')->cascadeOnDelete();
        });
        DB::statement('SET FOREIGN_KEY_CHECKS = 1');

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cargo_informations', function (Blueprint $table) {
            $table->string('signed')->default(0)->after('signed');
            $table->dropColumn('signed_sender');
            $table->dropColumn('signed_recipient');
        });
    }
};
