<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('system_page_translations', function (Blueprint $table) {
            $table->id();
            $table->foreignId('system_page_id')->constrained('system_pages')->cascadeOnDelete();
            $table->string('locale');
            $table->string('name');
            $table->string('h1');
            $table->text('text')->nullable();
            $table->text('title')->nullable();
            $table->text('description')->nullable();
            $table->text('keywords')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('system_page_translations');
    }
};
