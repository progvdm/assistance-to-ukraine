<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            //$table->dropConstrainedForeignId('organisation_id');
            //$table->dropColumn('organisation_id');
            $table->foreignId('organisation_id')->after('type_user')->nullable()->constrained('organisations')->nullOnDelete()->cascadeOnUpdate();
        });

        $users = \App\Models\User::all();

        foreach ($users as $user){
            $data = [
                'id' => $user->id,
                'published' => 1,
                'view_site' => $user->status_view,
                'view_system' => $user->status_view,
                'type' => $user->type_user,
                'founder_id' => $user->id,
                'number_cells' => $user->number_cells ?? 0,
                'zip_code' => $user->zip_code,
                'id_code' => $user->id_code,
                'photo' => $user->photo,
                'website' => $user->website,
                'facebook' => $user->facebook,
                'instagram' => $user->instagram,
                'open_info' => $user->open_info,
                'description_file' => $user->description_file,
                'additional_phone' => $user->additional_phone,
                'additional_email' => $user->additional_email,
            ];
            $dataLang = [
                'name' => $user->name_organization ?? 'No Name',
                'text' => $user->text,
                'text_short' => $user->text_short,
                'country' => $user->country,
                'city' => $user->city,
                'street' => $user->street,
                'build' => $user->build,
                'storage_city' => $user->storage_city,
                'storage_street' => $user->storage_street,
                'storage_build' => $user->storage_build,
            ];
            $organisation =  new \App\Models\Organisation\Organisation();
            $organisation->fill($data);
            $organisation->save();

            foreach (LaravelLocalization::getSupportedLocales() as $locale => $supportedLocale) {
                $organisationLang =  new \App\Models\Organisation\OrganisationTranslation();
                $organisationLang->fill($dataLang);
                $organisationLang->locale = $locale;
                $organisationLang->organisation_id = $organisation->id;
                $organisationLang->save();
            }
            $user->organisation_id = $user->id;
            $user->save();
        }
        Schema::table('transfers', function (Blueprint $table) {
            $table->dropForeign('transfers_recipient_id_foreign');
            $table->dropForeign('transfers_sender_id_foreign');
            $table->foreign('sender_id')->references('id')->on('organisations');
            $table->foreign('recipient_id')->references('id')->on('organisations');
        });
        Schema::table('warehouses', function (Blueprint $table) {
            $table->renameColumn('user_id', 'organisation_id');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropConstrainedForeignId('organisation_id');
        });
        Schema::table('transfers', function (Blueprint $table) {
            $table->dropForeign('transfers_recipient_id_foreign');
            $table->dropForeign('transfers_sender_id_foreign');
            $table->foreign('sender_id')->references('id')->on('users');
            $table->foreign('recipient_id')->references('id')->on('users');
        });
        Schema::table('warehouses', function (Blueprint $table) {
            $table->renameColumn('organisation_id', 'user_id');
        });

        \App\Models\Organisation\Organisation::all()->each(function ($query){
            $query->delete();
        });
    }
};
