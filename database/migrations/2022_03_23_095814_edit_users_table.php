<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('type_user')->after('id');
            $table->string('second_name')->nullable()->after('name');
            $table->string('zip_code')->nullable()->after('second_name');
            $table->string('name')->after('zip_code');
            $table->string('country')->after('name');
            $table->string('city')->nullable()->after('country');
            $table->string('street')->nullable()->after('city');
            $table->string('build')->nullable()->after('street');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('type_user');
            $table->dropColumn('second_name');
            $table->dropColumn('zip_code');
            $table->dropColumn('name');
            $table->dropColumn('country');
            $table->dropColumn('city');
            $table->dropColumn('street');
            $table->dropColumn('build');
        });
    }
};
