<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('human_passports', function (Blueprint $table) {
            $table->id();
            $table->foreignId('human_id')->constrained('humans')->cascadeOnDelete();
            $table->string('series')->nullable();
            $table->string('number')->nullable();
            $table->string('date')->nullable();
            $table->string('issued')->nullable();
            $table->string('registration_address')->nullable();
            $table->timestamps();

            $table->unique(['series', 'number'], 'series_number_unique');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('human_passports');
    }
};
