<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('list_products', function (Blueprint $table) {
            $table->string('import_id')->nullable()->change();
            $table->foreignId('parent_id')->after('list_category_id')->nullable()->constrained('list_products')->cascadeOnDelete();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('list_products', function (Blueprint $table) {
            $table->dropConstrainedForeignId('parent_id');
        });
    }
};
