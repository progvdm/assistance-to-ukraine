<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('what_is_done_translations', function (Blueprint $table) {
            $table->id();
            $table->foreignId('what_is_done_id')->constrained('what_is_dones')->cascadeOnDelete();
            $table->string('locale');
            $table->string('name');
            $table->string('country')->nullable();
            $table->text('text_short')->nullable();
            $table->text('text')->nullable();
            $table->string('h1');
            $table->text('title')->nullable();
            $table->text('description')->nullable();
            $table->text('keywords')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('what_is_done_translates');
    }
};
