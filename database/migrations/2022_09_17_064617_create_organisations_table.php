<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('organisations');
        Schema::create('organisations', function (Blueprint $table) {
            $table->id();
            $table->boolean('published')->default(1);
            $table->boolean('view_site')->default(1);
            $table->boolean('view_system')->default(1);
            $table->string('type');
            $table->foreignId('founder_id')->nullable()->constrained('users')->nullOnDelete();

            $table->integer('number_cells')->default(0);
            $table->string('zip_code')->nullable();
            $table->string('id_code')->nullable();
            $table->string('photo')->nullable();
            $table->string('website')->nullable();
            $table->string('facebook')->nullable();
            $table->string('instagram')->nullable();
            $table->boolean('open_info')->default(0);
            $table->string('description_file')->nullable();
            $table->string('additional_phone')->nullable();
            $table->string('additional_email')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('organisations');
    }
};
