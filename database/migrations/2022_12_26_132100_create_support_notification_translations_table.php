<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('support_notification_translations', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('support_notification_id')->unsigned();
            $table->foreign('support_notification_id', 'support_notification_foreign')->references('id')->on('support_notifications')->cascadeOnDelete();
            //$table->foreignId('support_notification_id')->constrained('support_notifications')->cascadeOnDelete();
            $table->string('locale');
            $table->string('subject');
            $table->text('text');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('support_notification_translations');
    }
};
