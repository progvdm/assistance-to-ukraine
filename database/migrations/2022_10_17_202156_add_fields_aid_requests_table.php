<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('aid_requests', function (Blueprint $table) {
            $table->foreignId('organisation_type_id')->after('category_id')->nullable()->constrained('organisation_types')->cascadeOnUpdate();
            $table->integer('count')->after('organisation_type_id')->nullable();
            $table->string('count_type')->after('count')->nullable();
        });
        Schema::table('aid_request_translations', function (Blueprint $table) {
            $table->string('name')->after('locale')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('aid_requests', function (Blueprint $table) {
            $table->dropConstrainedForeignId('organisation_type_id');
            $table->dropColumn('count');
            $table->dropColumn('count_type');
        });
        Schema::table('aid_request_translations', function (Blueprint $table) {
            $table->dropColumn('name');
        });
    }
};
