<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('nova_poshta_cities', function (Blueprint $table) {
            $table->id();
            $table->integer('cityID');
            $table->string('ref');
            $table->string('description');
            $table->string('areaRef');
            $table->string('areaDescription');
            $table->string('settlementTypeDescription');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('nova_poshta_cities');
    }
};
