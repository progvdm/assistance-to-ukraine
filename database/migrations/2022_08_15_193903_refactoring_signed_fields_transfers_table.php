<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('transfers', function (Blueprint $table) {
            $table->string('signed_sender')->default(0)->after('signed');
            $table->string('signed_recipient')->default(0)->after('signed_sender');
            $table->dropColumn('signed');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('transfers', function (Blueprint $table) {
            $table->string('signed')->default(0)->after('signed');
            $table->dropColumn('signed_sender');
            $table->dropColumn('signed_recipient');
        });
    }
};
