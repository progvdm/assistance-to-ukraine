<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        Schema::table('transfers', function (Blueprint $table) {
            $table->dropConstrainedForeignIdFor(\App\Models\Cargos\Cargo::class);
            $table->dropColumn('name_donor');
            $table->dropColumn('document');

            $table->string('status')->default('new');
            $table->boolean('status_list')->default(false);
            $table->string('keeex_url')->nullable();
        });
        Schema::table('transfer_documents', function (Blueprint $table) {
            $table->foreignId('user_id')->constrained('users')->cascadeOnDelete();
            $table->string('type')->nullable();
        });

        Schema::table('warehouse_products', function (Blueprint $table) {
            $table->dropColumn('product_id');
            $table->foreignId('transfer_product_id')->constrained('transfer_products')->cascadeOnDelete();
        });
        Schema::table('products', function (Blueprint $table) {
            $table->dropConstrainedForeignIdFor(\App\Models\Cargos\Cargo::class);
            $table->foreignId('transfer_start_id')->constrained('transfers')->cascadeOnDelete();
        });

        Schema::dropIfExists('cargo_transfer');
        Schema::dropIfExists('cargo_parents');
        Schema::dropIfExists('cargos');
        Schema::dropIfExists('transfer_videos');
        Schema::dropIfExists('transfer_photos');
        DB::statement('SET FOREIGN_KEY_CHECKS = 1');
    }
};
