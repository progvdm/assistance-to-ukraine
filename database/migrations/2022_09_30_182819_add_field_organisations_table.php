<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('organisations', function (Blueprint $table) {
           $table->json('verification')->nullable();
        });

        /** @var App\Models\Organisation\Organisation $organisation */
        foreach (App\Models\Organisation\Organisation::all() as $organisation){
            $organisation->verification = $organisation->founder->verification;
            $organisation->save();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('organisations', function (Blueprint $table) {
            $table->dropColumn('verification');
        });
    }
};
