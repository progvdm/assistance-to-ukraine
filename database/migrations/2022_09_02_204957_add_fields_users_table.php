<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
           $table->string('notification_phone')->after('additional_phone')->nullable();
           $table->string('storage_city')->after('build')->nullable();
           $table->string('storage_street')->after('storage_city')->nullable();
           $table->string('storage_build')->after('storage_street')->nullable();
           $table->string('notification_phone_check')->after('phone')->default('phone')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('notification_phone');
            $table->dropColumn('storage_city');
            $table->dropColumn('storage_street');
            $table->dropColumn('storage_build');
            $table->dropColumn('notification_phone_check');
        });
    }
};
