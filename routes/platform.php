<?php

declare(strict_types=1);

use App\Orchid\Screens\Examples\ExampleCardsScreen;
use App\Orchid\Screens\Examples\ExampleChartsScreen;
use App\Orchid\Screens\Examples\ExampleFieldsAdvancedScreen;
use App\Orchid\Screens\Examples\ExampleFieldsScreen;
use App\Orchid\Screens\Examples\ExampleLayoutsScreen;
use App\Orchid\Screens\Examples\ExampleScreen;
use App\Orchid\Screens\Examples\ExampleTextEditorsScreen;
use App\Orchid\Screens\Faq\FaqEditScreen;
use App\Orchid\Screens\Faq\FaqListScreen;
use App\Orchid\Screens\Organisation\OrganisationListScreen;
use App\Orchid\Screens\Pages\SystemPage\SystemPageEditScreen;
use App\Orchid\Screens\Pages\SystemPage\SystemPageListScreen;
use App\Orchid\Screens\Pages\WhatIsDone\WhatIsDoneEditScreen;
use App\Orchid\Screens\Pages\WhatIsDone\WhatIsDoneListScreen;
use App\Orchid\Screens\PlatformScreen;
use App\Orchid\Screens\Role\RoleEditScreen;
use App\Orchid\Screens\Role\RoleListScreen;
use App\Orchid\Screens\User\UserEditScreen;
use App\Orchid\Screens\User\UserListScreen;
use App\Orchid\Screens\User\UserProfileScreen;
use Illuminate\Support\Facades\Route;
use Tabuna\Breadcrumbs\Trail;

/*
|--------------------------------------------------------------------------
| Dashboard Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the need "dashboard" middleware group. Now create something great!
|
*/

// Main
Route::screen('/main', PlatformScreen::class)
    ->name('platform.main');

// Platform > Profile
Route::screen('profile', UserProfileScreen::class)
    ->name('platform.profile')
    ->breadcrumbs(function (Trail $trail) {
        return $trail
            ->parent('platform.index')
            ->push(__('Profile'), route('platform.profile'));
    });

// Platform > System > Users
Route::screen('users/{user}/edit', UserEditScreen::class)
    ->name('platform.systems.users.edit')
    ->breadcrumbs(function (Trail $trail, $user) {
        return $trail
            ->parent('platform.systems.users')
            ->push(__('Administrator'), route('platform.systems.users.edit', $user));
    });

// Platform > System > Users > Create
Route::screen('users/create', UserEditScreen::class)
    ->name('platform.systems.users.create')
    ->breadcrumbs(function (Trail $trail) {
        return $trail
            ->parent('platform.systems.users')
            ->push(__('Create'), route('platform.systems.users.create'));
    });

// Platform > System > Users > User
Route::screen('users', UserListScreen::class)
    ->name('platform.systems.users')
    ->breadcrumbs(function (Trail $trail) {
        return $trail
            ->parent('platform.index')
            ->push(__('Administrators'), route('platform.systems.users'));
    });

// Platform > System > organisations > List
Route::screen('organisations', OrganisationListScreen::class)
    ->name('platform.systems.organisations')
    ->breadcrumbs(function (Trail $trail) {
        return $trail
            ->parent('platform.index')
            ->push(__('Організації'), route('platform.systems.organisations'));
    });

// System Pages
Route::screen('system-pages', SystemPageListScreen::class)
    ->name('platform.pages.system')
    ->breadcrumbs(function (Trail $trail) {
        return $trail
            ->parent('platform.index')
            ->push(__('System pages'), route('platform.pages.system'));
    });

Route::screen('system-page/{page}/edit', SystemPageEditScreen::class)
    ->name('platform.pages.system.edit')
    ->breadcrumbs(function (Trail $trail, $page) {
        return $trail
            ->parent('platform.pages.system')
            ->push(__('Page'), route('platform.pages.system.edit', $page));
    });

// Notifications
Route::screen('support/notifications', \App\Orchid\Screens\Notification\NotificationsList::class)
    ->name('platform.support.notifications')
    ->breadcrumbs(function (Trail $trail) {
        return $trail
            ->parent('platform.index')
            ->push(__('Support notification'), route('platform.support.notifications'));
    });

Route::screen('support/notifications/create', \App\Orchid\Screens\Notification\NotificationCreate::class)
    ->name('platform.support.notifications.create')
    ->breadcrumbs(function (Trail $trail) {
        return $trail
            ->parent('platform.support.notifications')
            ->push(__('Notification'), route('platform.support.notifications.create'));
    });


// what-is-done
Route::screen('what-is-done', WhatIsDoneListScreen::class)
    ->name('platform.pages.what-is-done')
    ->breadcrumbs(function (Trail $trail) {
        return $trail
            ->parent('platform.index')
            ->push(__('What is done list'), route('platform.pages.what-is-done'));
    });

Route::screen('what-is-done/create', WhatIsDoneEditScreen::class)
    ->name('platform.pages.what-is-done.create')
    ->breadcrumbs(function (Trail $trail) {
        return $trail
            ->parent('platform.pages.what-is-done')
            ->push(__('Create'), route('platform.pages.what-is-done.create'));
    });
Route::screen('what-is-done/{page}/edit', WhatIsDoneEditScreen::class)
    ->name('platform.pages.what-is-done.edit')
    ->breadcrumbs(function (Trail $trail, $page) {
        return $trail
            ->parent('platform.pages.what-is-done')
            ->push(__('Page'), route('platform.pages.what-is-done.edit', $page));
    });

// FAQ
Route::screen('faq', FaqListScreen::class)
    ->name('platform.faq')
    ->breadcrumbs(function (Trail $trail) {
        return $trail
            ->parent('platform.index')
            ->push(__('FAQ '), route('platform.faq'));
    });

Route::screen('faq/create', FaqEditScreen::class)
    ->name('platform.faq.create')
    ->breadcrumbs(function (Trail $trail) {
        return $trail
            ->parent('platform.faq')
            ->push(__('Create'), route('platform.faq.create'));
    });
Route::screen('faq/{page}/edit', FaqEditScreen::class)
    ->name('platform.faq.edit')
    ->breadcrumbs(function (Trail $trail, $page) {
        return $trail
            ->parent('platform.faq')
            ->push(__('Edit'), route('platform.faq.edit', $page));
    });


// Platform > System > Roles > Role
Route::screen('roles/{role}/edit', RoleEditScreen::class)
    ->name('platform.systems.roles.edit')
    ->breadcrumbs(function (Trail $trail, $role) {
        return $trail
            ->parent('platform.systems.roles')
            ->push(__('Role'), route('platform.systems.roles.edit', $role));
    });

// Platform > System > Roles > Create
Route::screen('roles/create', RoleEditScreen::class)
    ->name('platform.systems.roles.create')
    ->breadcrumbs(function (Trail $trail) {
        return $trail
            ->parent('platform.systems.roles')
            ->push(__('Create'), route('platform.systems.roles.create'));
    });

// Platform > System > Roles
Route::screen('roles', RoleListScreen::class)
    ->name('platform.systems.roles')
    ->breadcrumbs(function (Trail $trail) {
        return $trail
            ->parent('platform.index')
            ->push(__('Roles'), route('platform.systems.roles'));
    });

// Example...
Route::screen('example', ExampleScreen::class)
    ->name('platform.example')
    ->breadcrumbs(function (Trail $trail) {
        return $trail
            ->parent('platform.index')
            ->push('Example screen');
    });

Route::screen('example-fields', ExampleFieldsScreen::class)->name('platform.example.fields');
Route::screen('example-layouts', ExampleLayoutsScreen::class)->name('platform.example.layouts');
Route::screen('example-charts', ExampleChartsScreen::class)->name('platform.example.charts');
Route::screen('example-editors', ExampleTextEditorsScreen::class)->name('platform.example.editors');
Route::screen('example-cards', ExampleCardsScreen::class)->name('platform.example.cards');
Route::screen('example-advanced', ExampleFieldsAdvancedScreen::class)->name('platform.example.advanced');

//Route::screen('idea', Idea::class, 'platform.screens.idea');
