<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});
Route::group(['prefix' => 'app', 'as' => 'api.app.'/*, 'middleware' => 'auth.check'*/], function() {
    Route::post('scan-qr-code', 'App\Http\Controllers\Api\AppController@scanQrCode')->name('scanQrCode');
    Route::post('upload/photo', 'App\Http\Controllers\Api\AppController@uploadPhoto')->name('upload.photo');
});

Route::post('keeex/info/', 'App\Http\Controllers\Api\ApiKeeexController@uploadInfo')->name('api.keeex');
Route::post('keeex/info/fileupload', 'App\Http\Controllers\Api\ApiKeeexController@uploadFile')->name('api.keeex.fileupload');

Route::post('telegram-bot', 'App\Http\Controllers\Api\TelegramController@telegram')->name('telegram');
Route::post('tinymce/photo', 'App\Http\Controllers\Api\TinymceController@photoUpload')->name('api.tinymce.photo.user');
Route::post('tinymce/admin/photo', 'App\Http\Controllers\Api\TinymceController@photoUploadAdmin')->name('api.tinymce.photo.admin');
