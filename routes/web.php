<?php

use App\Http\Controllers\Auth\LogoutController;
use App\Http\Controllers\Auth\RestorePasswordController;
use App\Http\Controllers\Cabinet\CabinetController;
use App\Http\Controllers\Cabinet\CargoController;
use App\Http\Controllers\Cabinet\GeoLocationsController;
use App\Http\Controllers\Home\HomeController;
use App\Http\Controllers\Page\PageController;
use App\Http\Controllers\Pages\AboutController;
use App\Http\Controllers\Pages\ContactsController;
use App\Http\Controllers\Pages\DocumentsController;
use App\Http\Controllers\Pages\DonorsController;
use App\Http\Controllers\Pages\FeedbackController;
use App\Http\Controllers\Pages\InstructionController;
use App\Http\Controllers\Pages\MarketplaceController;
use App\Http\Controllers\Pages\RecipientsController;
use App\Http\Controllers\Pages\WhatIsDoneController;
use App\Models\User;
use App\Notifications\CustomAidRequestNotification;
use App\Notifications\EveryDayNewAidКequestNotification;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Facades\Route;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;

Route::get('/', function () {
    return redirect(app()->getLocale());
});

Route::group(['prefix' => LaravelLocalization::setLocale()], function()
{
    Route::view('render-component', 'livewire.component')->name('render-component');

    //  Cabinet
    Route::group(['prefix' => 'cabinet', 'as' => 'cabinet.', 'middleware' => 'auth.check'], function() {
        Route::get('/', [CabinetController::class, 'index'])
            ->name('home');
        Route::get('/regenerate-files/asyfdvgVGShgVSHGVDHSTGVhgVSHFcVHSGVHGSVhjBSJGvHGSjb', [CabinetController::class, 'regenerateFiles'])
            ->name('regenerateFiles');
        /*Route::get('/user/{user?}', [CabinetController::class, 'index'])
            ->name('home.user');*/
        Route::get('/new-shipment', [CargoController::class, 'newShipment'])
            ->name('new-shipment');
        Route::get('/new-shipment/received', [CargoController::class, 'newShipmentReceived'])
            ->name('new-shipment-received');

        Route::get('/sign-document/{transferDocument}', [CargoController::class, 'signDocument'])
            ->name('sign-document');

        Route::get('/accept-cargo/{transfer}', [CargoController::class, 'accept'])
            ->name('accept-cargo');
        Route::get('/approve/{transfer}', [CargoController::class, 'approve'])
            ->name('approve');
        Route::get('/locations/{transfer}', [GeoLocationsController::class, 'index'])
            ->name('locations.cargo');
        Route::get('/locations/{transfer}/json', [GeoLocationsController::class, 'indexJson'])
            ->name('locations.cargo.json');
        Route::get('/cargo-document', [CargoController::class, 'document'])
            ->name('cargo-document');
        Route::get('/{transfer}/cargo-qr-code', [CargoController::class, 'printQrCode'])
            ->name('cargo-qr-code');
        Route::get('/{transfer}/document-test', [CargoController::class, 'testDocument'])
            ->name('cargo-doc-test');
        Route::get('/donate-products/{transfer?}', [CargoController::class, 'donate'])
            ->name('donate-product');
        Route::get('/recipient/donate-products', [CargoController::class, 'donateRecipient'])
            ->name('donate-product-recipient');
        Route::get('/people/donate-products', [CargoController::class, 'donatePeople'])
            ->name('donate-product-people');
        Route::get('/download-act/{transfer}', [CargoController::class, 'downloadAct'])
            ->name('download-act');
        Route::get('/download-act-accept/{transfer}', [CargoController::class, 'downloadActAccept'])
            ->name('download-act-accept');
        Route::get('/settings', [CabinetController::class, 'settings'])
            ->name('settings');
        Route::get('/users', [CabinetController::class, 'users'])
            ->name('users');
        Route::get('/warehouse', [CabinetController::class, 'warehouse'])
            ->name('warehouse');

        Route::get('/aid-requests', [\App\Http\Controllers\Cabinet\AidRequest\AidRequestController::class, 'index'])
            ->name('aid-requests.index');
        Route::get('/aid-request/create', [\App\Http\Controllers\Cabinet\AidRequest\AidRequestController::class, 'create'])
            ->name('aid-requests.create');
        Route::get('/aid-request/{id}/update', [\App\Http\Controllers\Cabinet\AidRequest\AidRequestController::class, 'update'])
            ->name('aid-requests.update');

    });

    Route::get('/cabinet/cargo/{transfer}', [CargoController::class, 'cargo'])
        ->name('cabinet.cargo');
    Route::post('/cargo/{transfer}/save-location', [CargoController::class, 'cargoLocation'])
        ->name('cabinet.cargo.save-location');


    Route::get('/cargo/{cargo}/keeex-url', [CargoController::class, 'redirectKeeexUrl'])
        ->name('redirectKeeexUrl');

    Route::get('/verify-email/{token}', [CabinetController::class, 'verify'])
        ->name('verify-email');

    // Auth
    Route::get('/login/is-admin/{id}', \App\Http\Controllers\Auth\LoginController::class)
        ->middleware(['admin', 'platform'])
        ->name('login.admin');
    Route::get('/logout', LogoutController::class)
        ->name('logout');
    Route::get('/restore-password/{token}', RestorePasswordController::class)
        ->name('restore-password');

    // Home
    Route::get('/', HomeController::class)->name('home');

    // Pages
    Route::get('/country-photos/{country}', [CargoController::class, 'countryPhotos'])
        ->name('country-photos');

    Route::get('about-us', [AboutController::class, 'index'])->name('page.about');
    Route::get('marketplace', [MarketplaceController::class, 'index'])->name('page.marketplace');
    Route::get('documents', [DocumentsController::class, 'index'])->name('page.documents');
    Route::get('documents/{pdf}', [DocumentsController::class, 'document'])->name('page.documents.pdf');
    //Route::get('contacts', [ContactsController::class, 'index'])->name('page.contacts');
    Route::get('feedback', [FeedbackController::class, 'index'])->name('page.feedback');
    Route::get('what-is-done', [WhatIsDoneController::class, 'index'])->name('page.whatIsDone');
    Route::get('5tguI7ftfvCjFGCmgvn', [CabinetController::class, 'reloadData']);
    Route::get('what-is-done/{slug}', [WhatIsDoneController::class, 'itemView'])->name('page.whatIsDone.page');
    Route::get('donors', [DonorsController::class, 'index'])->name('page.donors');
    Route::get('donors/{organisation}', [DonorsController::class, 'show'])->name('page.donors.show');
    Route::get('recipients', [RecipientsController::class, 'index'])->name('page.recipients');
    Route::get('recipients/{organisation}', [RecipientsController::class,'show'])->name('page.recipients.show');
    Route::get('instruction', [InstructionController::class, 'index'])->name('instruction');
    //Route::get('{page}', PageController::class)->name('page');

});
