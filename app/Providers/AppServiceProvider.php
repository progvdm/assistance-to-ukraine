<?php

namespace App\Providers;

use App\AlpineComponents\PhoneInput;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Blade;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(Ui::class);
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->views();
    }

    /**
     * Load module views.
     */
    protected function views()
    {
        Blade::directive('svg', function ($arguments) {
            return '<?php echo svg(' . $arguments . '); ?>';
        });

        $this->loadViewComponentsAs('ui', [
            PhoneInput::class,
        ]);
    }
}
