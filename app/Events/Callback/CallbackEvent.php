<?php

namespace App\Events\Callback;

use App\Models\Callbacks\Callback;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class CallbackEvent
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public ?Callback $callback = null;
    public string|null $email = null;

    /**
     * Create a new event instance.
     *
     * @param $callback
     * @param $email
     */
    public function __construct($callback, $email)
    {
        $this->callback = $callback;
        $this->email = $email;
    }
}
