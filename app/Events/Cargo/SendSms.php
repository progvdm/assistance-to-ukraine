<?php

namespace App\Events\Cargo;

use App\Models\User;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class SendSms
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public string $phone;
    public string $text;

    /**
     * Create a new event instance.
     *
     * @param $phone
     * @param $text
     */
    public function __construct($phone, $text)
    {
        logger('Send SMS phone:'.$phone . ' text:' . $text);
        $this->phone = $phone;
        $this->text = $text;
    }
}
