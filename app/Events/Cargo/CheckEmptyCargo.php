<?php

namespace App\Events\Cargo;

use App\Models\Transfers\Transfer;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class CheckEmptyCargo
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public Transfer $transfer;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($transfer)
    {
        $this->transfer = $transfer;
    }
}
