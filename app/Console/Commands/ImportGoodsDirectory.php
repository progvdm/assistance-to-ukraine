<?php

namespace App\Console\Commands;

use App\Imports\ImportGroupsList;
use App\Imports\ImportProductsList;
use Illuminate\Console\Command;
use Maatwebsite\Excel\Facades\Excel;

class ImportGoodsDirectory extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:products-list';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import products list excel';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        Excel::import(new ImportGroupsList(), 'storage/ImportFile/import-groups.xlsx');
        Excel::import(new ImportProductsList(), 'storage/ImportFile/import-products.xlsx');

        return Command::SUCCESS;
    }
}
