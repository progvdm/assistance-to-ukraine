<?php

namespace App\Console\Commands;

use \App\Models\Notifications\SupportNotification as SupportNotificationModel;
use App\Models\User;
use App\Notifications\SupportNotification;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Notification;

class SupportNotificationSend extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'support-notification:send';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Отправка уведомлений с админки';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $notifications = SupportNotificationModel::whereSendStatus(false)->get();
        foreach ($notifications as $notification) {
            if(array_search('all', $notification->users) !== false){
                $users = User::all();
            }else{
                $users = User::whereIn('id', $notification->users)->get();
            }

            Notification::send($users, new SupportNotification($notification));
            $notification->send_status = true;
            $notification->save();
        }
        return Command::SUCCESS;
    }
}
