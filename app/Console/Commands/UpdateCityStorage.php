<?php

namespace App\Console\Commands;

use App\Models\NovaPoshta\NovaPoshtaCity;
use App\Services\NovaPoshta\NovaPoshtaService;
use Illuminate\Console\Command;

class UpdateCityStorage extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'novaPoshta:updateCities';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update cities by local storage';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $service = new NovaPoshtaService();

        $service->getCities('');
        $cities = [];
        foreach ($service->getCities('', 1000000) as $city){
            $cities[] =[
                'cityID' => $city->CityID,
                'ref' => $city->Ref,
                'description' => $city->Description,
                'areaRef' => $city->Area,
                'areaDescription' => $city->Area,
                'settlementTypeDescription' => $city->SettlementTypeDescription,
            ];
        }
        if($cities){
            NovaPoshtaCity::truncate();
            NovaPoshtaCity::upsert($cities, ['cityID']);
            $this->info('Cities storage updated!');
        }else{
            $this->error('Cities storage not updated! Failed to get cities with api.');
        }

        return true;
    }
}
