<?php

namespace App\Foundation;

class FlashMessage
{
    const ERROR = 'error-message';
    const INFO = 'info-message';
    const SUCCESS = 'success-message';

    public static function setMessage($key, $message)
    {
        session()->flash($key, $message);
    }

    /**
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public static function getMessage($key)
    {
        return session()->get($key);
    }
}
