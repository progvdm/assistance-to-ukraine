<?php


namespace App\Dto\Cargo;

use Illuminate\Support\Collection;

class TransferActsFilesDto
{

    public static function makeByCollection($collection) :collection
    {
        $result = collect();
        foreach ($collection as $item) {
            $result->push((object)[
                'url' => $item->getUrl(),
                'name' => $item->document,
                'created_at' => $item->created_at,
            ]);
        }

        return $result;
    }
}
