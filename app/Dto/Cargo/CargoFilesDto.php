<?php


namespace App\Dto\Cargo;
use Illuminate\Contracts\Support\Arrayable;

class CargoFilesDto
{

    public static function makeByModel($list) :array
    {
        $result = [];
        foreach ($list as $item) {
            $itemArr['url'] = $item->getUrl();
            $itemArr['id'] = $item->id;
            $itemArr['name'] = $item->getName();
            $result[$item->id] = $itemArr;
        }
        return $result;
    }
}
