<?php


namespace App\Dto\Cargo;

class TransferDto
{

    public static function makeBylivewire($data) :array
    {
        return [
            'sender_id' => $data['sender_id'],
            'cargo_id' => $data['cargo_id'],
            'human_id' => $data['human_id'],
            'parent' => $data['parent'],
            'type' => $data['type'],
            'name_donor' => $data['name_donor'],
            'additional_info' => json_encode($data['additionalInfo']),
        ];
    }
}
