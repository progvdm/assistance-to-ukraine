<?php


namespace App\Dto\Cargo;
use Illuminate\Contracts\Support\Arrayable;

class ProductsDto
{

    public static function makeByModel($list) :array
    {
        $result = [];
        foreach ($list as $item) {
            $itemArr['category_id'] = $item->category_id;
            $itemArr['name'] = $item->getName();
            $itemArr['code'] = $item->code;
            $itemArr['quantity'] = $item->quantity;
            $itemArr['original_quantity'] = $item->original_quantity;
            $itemArr['volume'] = $item->volume;
            $itemArr['type'] = $item->getType();

            $result[] = $itemArr;
        }
        return $result;
    }
}
