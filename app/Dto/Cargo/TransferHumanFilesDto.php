<?php


namespace App\Dto\Cargo;

use Illuminate\Support\Collection;

class TransferHumanFilesDto
{

    public static function makeByCollection($collection) :collection
    {
        $result = collect();
        foreach ($collection as $item) {
            $result->push((object)[
                'url' => asset('storage/humans/'.$item->human->file),
                'name' => $item->human->file,
                'created_at' => $item->created_at,
                'human' => true,
            ]);
        }

        return $result;
    }
}
