<?php

namespace App\Mail\Callback;

use App\Models\Callbacks\Callback;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class CallbackEmail extends Mailable
{
    use Queueable, SerializesModels;

    public ?Callback $callback;

    /**
     * Create a new message instance.
     *
     * @param Callback $callback
     */
    public function __construct(Callback $callback)
    {
        $this->callback = $callback;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $result =  $this
            ->subject('Нова заявка: ' . $this->callback->category)
            ->view('messages.send-callback', ['callback' => $this->callback]);

        if($this->callback->file){
            $result->attachFromStorage('public/callback/'.$this->callback->file);
        }
        return $result;
    }
}
