<?php

namespace App\Traits;

use Illuminate\Support\Arr;
use Illuminate\Support\Facades\File;

trait ImageAttachable
{
    public function fileExists(): bool
    {
        $filePath = $this->getPath();

        return File::exists($filePath);
    }

    public function getImage(): string
    {
        $settings = Arr::get($this->fileSettings(), $this->field);
        $directory = Arr::get($settings, 'directory');

        return asset('storage/'.$directory.$this->photo);
    }

    protected function getPath(): string
    {
        $settings = Arr::get($this->fileSettings(), $this->field);
        $path = Arr::get($settings, 'path');
        $directory = Arr::get($settings, 'directory');

        return storage_path($path.$directory.$this->{$this->field});
    }

    public function deleteImage(): bool
    {
        if(File::exists($this->getPath())){
            File::delete($this->getPath());
        }
        $this->photo = "";
        $this->save();

        return true;
    }
}
