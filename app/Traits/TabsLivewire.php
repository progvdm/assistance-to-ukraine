<?php

namespace App\Traits;



trait TabsLivewire
{
    public array $tabs = [];

    public function selectTab($container, $tab){
        $this->tabs[$container] = $tab;
    }
}
