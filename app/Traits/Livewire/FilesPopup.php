<?php

namespace App\Traits\Livewire;



use App\Dto\Cargo\TransferActsFilesDto;
use App\Dto\Cargo\TransferFilesDto;
use App\Dto\Cargo\TransferHumanFilesDto;
use App\Models\Transfers\Transfer;
use App\Models\Transfers\TransferDocument;
use App\Models\Transfers\TransferFiles;

trait FilesPopup
{
    public int $transferId = 0;
    public int $openFolderId = 0;
    public bool $showPopup = true;
    public string $typeMedia = '';


    private function getMedia(){
        if($this->showPopup){
            $transfer = Transfer::findOrFail($this->transferId);
            $transfersAllow = [];
            $productsAllow = [];
            $productsAllowT = [];
            foreach ($transfer->products as $transferProduct) {
                $productsAllow[$transferProduct->id] = $transferProduct->id;
                $productsAllowT[$transferProduct->product_id] = $transferProduct->product_id;
            }
            $folders = $this->getTransferFolders($transfer, $productsAllow, $productsAllowT);

            if($this->openFolderId != 0){
                $ids = $folders->where('recipient_id', '=', $this->openFolderId)->first()->ids;
                $transfers = Transfer::whereIn('id', array_values($ids))->get();
               // dd($transfers);
                foreach ($transfers as $item){
                    $productsAllow = [];
                    foreach ($item->products as $transferProduct) {
                        $productsAllow[$transferProduct->id] = $transferProduct->id;
                    }
                    $transfersAllow[$item->id] = $item->id;
                    $this->getNextTransfer($item->recipient_id, $productsAllow, $this->openFolderId != $transfer->recipient_id, $transfersAllow);
                }
            }
            if($this->openFolderId == $transfer->recipient_id){
                $transfersAllow[$transfer->id] = $transfer->id;
            }

            if($this->openFolderId != 0){
                $folders = [];
            }


            $files = TransferFilesDto::makeByCollection(Transfer::getFilesByIds($transfersAllow, $this->typeMedia));
            if($this->typeMedia == TransferFiles::FILE){
                $transferActs = TransferActsFilesDto::makeByCollection(TransferDocument::whereIn('transfer_id', $transfersAllow)->get());

                $humansFile = Transfer::whereIn('id',array_values($transfersAllow))->where('human_id', '!=', null)->whereHas('human', function ($query){
                    $query->where('file', '!=', '');
                })->get();
                $transferActsHuman = TransferHumanFilesDto::makeByCollection($humansFile);
                $acts = $transferActs->merge($transferActsHuman);
                $acts = $acts->sortBy('created_at');
                $files = [
                    'files' => $files,
                    'acts' => $acts,
                    'humanActs' => $transferActsHuman,

                ];
            }
        }
        return [$files ?? [], $folders ?? []];
    }

    private function getNextTransfer($id, $productsAllow, $allowNext, &$transfersAllow){
        $nextTransfers = Transfer::getNextTransfer($id, $productsAllow);
        foreach ($nextTransfers as $nextCargo) {
            if($allowNext or $this->typeMedia == TransferFiles::FILE){
                if($nextCargo->human_id != null){
                    $transfersAllow[$nextCargo->id] = $nextCargo->id;
                }elseif ($this->openFolderId != 0) {
                    $transfersAllow[$nextCargo->id] = $nextCargo->id;
                }
            }elseif ($nextCargo->human_id){
                $transfersAllow[$nextCargo->id] = $nextCargo->id;
            }
            if($nextCargo->recipient_id != $nextCargo->sender_id and $this->openFolderId != 0 and $allowNext){
                $productsAllowNext = [];
                foreach ($nextCargo->products as $transferProduct) {
                    $productsAllowNext[$transferProduct->id] = $transferProduct->id;
                }
                $this->getNextTransfer($nextCargo->recipient_id, $productsAllowNext, $allowNext, $transfersAllow);
            }
        }
    }

    private function getTransferFolders($transferOpenFolder, $productsAllow, $productsAllowT){
        $nextTransfers = Transfer::getNextTransfer($transferOpenFolder->recipient_id, $productsAllow);
        $folders = collect([]);
        if($transferOpenFolder->id == $this->transferId and !$folders->firstWhere('recipient_id', $transferOpenFolder->recipient->id) and ($transferOpenFolder->transferFiles()->whereType($this->typeMedia)->count() or $this->typeMedia == TransferFiles::FILE)){
            $folders->push((object)['ids' => [$transferOpenFolder->id], 'recipient_id'=>$transferOpenFolder->recipient->id, 'name' => $transferOpenFolder->recipient->name]);
        }
        foreach ($nextTransfers as $transfer){
            if($transfer->human_id == null){
                $typeMedia = $this->typeMedia;
                $transfersM = Transfer::whereSenderId($transfer->recipient_id)->whereHas('products', function ($query) use ($productsAllowT){
                    $query->whereIn('product_id', $productsAllowT);
                })->whereHas('transferFiles', function ($query) use ($typeMedia){
                    $query->whereType($typeMedia);
                })->count();

                if(!$folders->firstWhere('recipient_id', $transfer->recipient->id) and ($transfer->transferFiles()->whereType($this->typeMedia)->count() or $this->typeMedia == TransferFiles::FILE or $transfersM)){
                    $folders->push((object)['ids' => [$transfer->id], 'recipient_id'=>$transfer->recipient->id, 'name' => $transfer->recipient->name]);
                }elseif($folders->firstWhere('recipient_id', $transfer->recipient->id)){
                    $ids = $folders->firstWhere('recipient_id', $transfer->recipient->id)->ids;
                    $ids[] = $transfer->id;
                    $folders->firstWhere('recipient_id', $transfer->recipient->id)->ids = $ids;
                }
            }

        }
        return $folders;
    }

}
