<?php

declare(strict_types=1);

namespace App\Orchid\Layouts\Organisation;

use App\Models\Organisation\Organisation;
use Orchid\Screen\Actions\Button;
use Orchid\Screen\Actions\DropDown;
use Orchid\Screen\Actions\Link;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Layouts\Table;
use Orchid\Screen\TD;

class OrganisationListLayout extends Table
{
    /**
     * @var string
     */
    public $target = 'organisations';

    /**
     * @return TD[]
     */
    public function columns(): array
    {
        return [
            TD::make('name', __('Name'))
                ->sort()
                ->cantHide(),

            TD::make('email', __('Email'))
                ->sort()
                ->cantHide()
                ->render(function (Organisation $organisation) {
                    return $organisation->founder->email;
                }),
            /*TD::make('email', __('Email'))
                ->sort()
                ->cantHide()
                ->filter(Input::make())
                ->render(function (Organisation $user) {
                    return ModalToggle::make($user->email)
                        ->modal('asyncEditUserModal')
                        ->modalTitle($user->presenter()->title())
                        ->method('saveUser')
                        ->asyncParameters([
                            'user' => $user->id,
                        ]);
                }),*/

            TD::make('created_at', __('Зареэстровані'))
                ->sort()
                ->render(function (Organisation $user) {
                    return $user->created_at ? $user->created_at->toDateTimeString() : '';
                }),

            TD::make(__('Actions'))
                ->align(TD::ALIGN_CENTER)
                ->width('100px')
                ->render(function (Organisation $user) {
                    return DropDown::make()
                        ->icon('options-vertical')
                        ->list([

                            Link::make(__('login'))
                                ->target('blank')
                                ->route('login.admin', $user->id)
                                ->icon('login'),
                        ]);
                }),
        ];
    }
}
