<?php

namespace App\Orchid\Layouts\Organisation;

use App\Orchid\Filters\OrganisationNameFilter;
use Orchid\Filters\Filter;
use Orchid\Screen\Layouts\Selection;

class OrganisationFiltersLayout extends Selection
{
    /**
     * @return string[]|Filter[]
     */
    public function filters(): array
    {
        return [
            OrganisationNameFilter::class,
        ];
    }
}
