<?php

declare(strict_types=1);

namespace App\Orchid\Layouts\Notifications;

use App\Models\Notifications\SupportNotification;
use App\NotificationChannels\TelegramChannel;
use Orchid\Platform\Models\User;
use Orchid\Screen\Actions\Button;
use Orchid\Screen\Actions\DropDown;
use Orchid\Screen\Actions\Link;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Layouts\Table;
use Orchid\Screen\TD;

class NotificationsListLayout extends Table
{
    /**
     * @var string
     */
    public $target = 'list';

    /**
     * @return TD[]
     */
    public function columns(): array
    {
        return [
            TD::make('subject', __('Subject'))
                ->sort()
                ->cantHide()
                ->filter(Input::make()),

            TD::make('send_status', __('Status'))
                ->sort()
                ->cantHide()
                ->render(function (SupportNotification $model) {
                   return $model->send_status == 1 ? 'Відправлено' : 'Очікується відправка';
                }),
            TD::make('users', __('Users'))
                ->render(function (SupportNotification $model) {
                    $users = User::whereIn('id', array_values($model->users))->get();
                    $users = $users->map(function (User $user){
                        return [$user->id => $user->name .' | '. $user->email];
                    })->collapse()->toArray();
                    if(array_search('all', $model->users) !== false){
                        $users[] = 'All users';
                    }
                    return implode(' | ', $users);
                }),
            TD::make('channels', __('Channels'))
                ->render(function (SupportNotification $model) {
                    $channels =
                        [
                            'mail'   => 'Email',
                            'database'   => 'Notification',
                            TelegramChannel::class   => 'Telegram',
                        ];
                    $result = [];
                    foreach ($model->channels as $val){
                        $result[] =  $channels[$val];
                    }
                    return implode(' | ', $result);
                }),

            TD::make('updated_at', __('Last edit'))
                ->sort()
                ->render(function (SupportNotification $model){
                    return $model->updated_at ? $model->updated_at->format('Y.m.d H:i:s') : $model->created_at->format('Y.m.d H:i:s');
                }),
            TD::make(__('Actions'))
                ->align(TD::ALIGN_CENTER)
                ->width('100px')
                ->render(function (SupportNotification $page) {
                    return DropDown::make()
                        ->icon('options-vertical')
                        ->list([

                            Link::make(__('View'))
                                ->route('platform.faq.edit', $page->id)
                                ->icon('view'),
                            Button::make(__('Delete'))
                                ->method('remove', [
                                    'id' => $page->id,
                                ])
                                ->icon('trash'),
                        ]);
                }),
        ];
    }
}
