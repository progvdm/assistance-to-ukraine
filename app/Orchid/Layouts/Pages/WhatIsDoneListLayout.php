<?php

declare(strict_types=1);

namespace App\Orchid\Layouts\Pages;

use App\Models\Pages\SystemPage;
use App\Models\WhatIsDone\WhatIsDone;
use Orchid\Platform\Models\User;
use Orchid\Screen\Actions\Button;
use Orchid\Screen\Actions\DropDown;
use Orchid\Screen\Actions\Link;
use Orchid\Screen\Actions\ModalToggle;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Layouts\Persona;
use Orchid\Screen\Layouts\Table;
use Orchid\Screen\TD;

class WhatIsDoneListLayout extends Table
{
    /**
     * @var string
     */
    public $target = 'list';

    /**
     * @return TD[]
     */
    public function columns(): array
    {
        return [
            TD::make('name', __('Name'))
                ->sort()
                ->cantHide()
                ->filter(Input::make()),

            TD::make('slug', __('Slug'))
                ->sort()
                ->cantHide()
                ->filter(Input::make()),

            TD::make('updated_at', __('Last edit'))
                ->sort(),

            TD::make(__('Actions'))
                ->align(TD::ALIGN_CENTER)
                ->width('100px')
                ->render(function (WhatIsDone $page) {
                    return DropDown::make()
                        ->icon('options-vertical')
                        ->list([

                            Link::make(__('Edit'))
                                ->route('platform.pages.what-is-done.edit', $page->id)
                                ->icon('pencil'),
                            Button::make(__('Delete'))
                                ->method('remove', [
                                    'id' => $page->id,
                                ])
                                ->icon('trash'),
                        ]);
                }),
        ];
    }
}
