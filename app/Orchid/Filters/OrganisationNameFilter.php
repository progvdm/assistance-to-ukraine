<?php

declare(strict_types=1);

namespace App\Orchid\Filters;

use Illuminate\Database\Eloquent\Builder;
use Orchid\Filters\Filter;
use Orchid\Platform\Models\Role;
use Orchid\Screen\Field;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Fields\Select;

class OrganisationNameFilter extends Filter
{
    /**
     * @return string
     */
    public function name(): string
    {
        return __('Name');
    }

    /**
     * The array of matched parameters.
     *
     * @return array|null
     */
    public function parameters(): ?array
    {
        return ['name'];
    }

    /**
     * @param Builder $builder
     *
     * @return Builder
     */
    public function run(Builder $builder): Builder
    {
        return $builder->whereHas('translations', function (Builder $query) {
            $query->where('name', 'LIKE', '%'.$this->request->get('name').'%');
        });
    }

    /**
     * @return Field[]
     */
    public function display(): array
    {
        return [
            Input::make('name')
                ->empty()
                ->value($this->request->get('name'))
                ->title(__('Name')),
        ];
    }
}
