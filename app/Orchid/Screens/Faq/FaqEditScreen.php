<?php

declare(strict_types=1);

namespace App\Orchid\Screens\Faq;

use App\Models\Faq\Faq;
use App\Orchid\Screens\Fields\Ttinymce;
use Illuminate\Http\Request;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;
use Orchid\Screen\Action;
use Orchid\Screen\Actions\Button;
use Orchid\Screen\Actions\Link;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Fields\Quill;
use Orchid\Screen\Fields\Radio;
use Orchid\Screen\Screen;
use Orchid\Support\Color;
use Orchid\Support\Facades\Layout;
use Orchid\Support\Facades\Toast;

class FaqEditScreen extends Screen
{
    /**
     * @var Faq
     */
    public Faq $page;

    /**
     * Query data.
     *
     * @param Faq $page
     *
     * @return array
     */
    public function query(Faq $page): iterable
    {
        return [
            'page' => $page,
        ];
    }

    /**
     * Display header name.
     *
     * @return string|null
     */
    public function name(): ?string
    {
        return $this->page->exists ? 'Edit ' : 'Create ';
    }

    /**
     * Display header description.
     *
     * @return string|null
     */
    public function description(): ?string
    {
        return '';
    }

    /**
     * @return iterable|null
     */
    public function permission(): ?iterable
    {
        return [
            'platform.pages.system',
        ];
    }

    /**
     * Button commands.
     *
     * @return Action[]
     */
    public function commandBar(): iterable
    {
        return [
            Link::make('Close')
                ->route('platform.faq')
                ->icon('close')
                ->type(Color::DANGER()),

            Button::make(__('Save'))
                ->icon('check')
                ->type(Color::SUCCESS())
                ->method('save'),

        ];
    }

    /**
     * @return \Orchid\Screen\Layout[]
     */
    public function layout(): iterable
    {
        $tabsLocales = [];
        foreach (LaravelLocalization::getSupportedLocales() as $locale => $supportedLocale) {
            $tabsLocales[$supportedLocale['name']] = Layout::columns([
                Layout::rows([

                    Input::make($locale.'[question]')
                        ->title('Question	:')
                        ->placeholder('Enter question')
                        ->value($this->page->translate($locale)->question ?? '')
                        ->required()
                        ->help('Please enter question'),

                    Ttinymce::make($locale.'[answer]')
                        ->title('Answer')
                        ->value($this->page->translate($locale)->answer ?? ''),
                ])

            ]);
        }
        return [
            Layout::columns([
                Layout::rows([
                    Radio::make('published')
                        ->placeholder('Yes')
                        ->value(1)
                        ->title('Published')
                        ->checked($this->page->published == 1 ? true : false),

                    Radio::make('published')
                        ->placeholder('No')
                        ->value(0)
                        ->checked($this->page->published == 0 ? true : false),
                ]),
                Layout::rows([
                    Input::make('sort')
                        ->title('Sort:')
                        ->required()
                        ->min(0)
                        ->type('sort')
                        ->value($this->page->sort),
                ]),
            ]),
            Layout::tabs($tabsLocales),
        ];
    }

    /**
     * @param Faq $page
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function save(Faq $page, Request $request)
    {
        $page->fill($request->all());
        $page->save();
        foreach (LaravelLocalization::getSupportedLocales() as $locale => $supportedLocale) {
            $page->translateOrNew($locale)->fill($request->get($locale)+ ['faq_id' =>$page->id ])->save();
        }
        $page->save();
        Toast::info(__('FAQ updated.'));

        return redirect()->route('platform.faq.edit', $page);
    }

}
