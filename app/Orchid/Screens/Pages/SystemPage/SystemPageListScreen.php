<?php

declare(strict_types=1);

namespace App\Orchid\Screens\Pages\SystemPage;

use App\Models\Pages\SystemPage;
use App\Orchid\Layouts\Pages\SystemPageListLayout;
use Orchid\Screen\Actions\Link;
use Orchid\Screen\Screen;

class SystemPageListScreen extends Screen
{
    /**
     * Query data.
     *
     * @return array
     */
    public function query(): iterable
    {
        return [
            'pages' => SystemPage::paginate(),
        ];
    }

    /**
     * Display header name.
     *
     * @return string|null
     */
    public function name(): ?string
    {
        return 'System page';
    }

    /**
     * Display header description.
     *
     * @return string|null
     */
    public function description(): ?string
    {
        return '';
    }

    /**
     * @return iterable|null
     */
    public function permission(): ?iterable
    {
        return [
            'platform.pages.system',
        ];
    }

    /**
     * Button commands.
     *
     * @return \Orchid\Screen\Action[]
     */
    public function commandBar(): iterable
    {
        return [
            /*Link::make(__('Add'))
                ->icon('plus')
                ->route('platform.systems.users.create'),*/
        ];
    }

    /**
     * Views.
     *
     * @return string[]|\Orchid\Screen\Layout[]
     */
    public function layout(): iterable
    {
        return [
            //UserFiltersLayout::class,
            SystemPageListLayout::class,


        ];
    }

}
