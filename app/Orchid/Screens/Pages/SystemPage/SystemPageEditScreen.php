<?php

declare(strict_types=1);

namespace App\Orchid\Screens\Pages\SystemPage;

use App\Models\Pages\SystemPage;
use App\Orchid\Screens\Fields\Ttinymce;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rule;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;
use Orchid\Platform\Models\User;
use Orchid\Screen\Action;
use Orchid\Screen\Actions\Button;
use Orchid\Screen\Actions\Link;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Fields\Label;
use Orchid\Screen\Fields\Quill;
use Orchid\Screen\Fields\Radio;
use Orchid\Screen\Fields\TextArea;
use Orchid\Screen\Fields\Upload;
use Orchid\Screen\Screen;
use Orchid\Support\Color;
use Orchid\Support\Facades\Layout;
use Orchid\Support\Facades\Toast;

class SystemPageEditScreen extends Screen
{
    /**
     * @var SystemPage
     */
    public SystemPage $page;

    /**
     * Query data.
     *
     * @param SystemPage $page
     *
     * @return array
     */
    public function query(SystemPage $page): iterable
    {
        return [
            'page' => $page,
        ];
    }

    /**
     * Display header name.
     *
     * @return string|null
     */
    public function name(): ?string
    {
        return $this->page->exists ? 'Edit Page' : 'Create Page';
    }

    /**
     * Display header description.
     *
     * @return string|null
     */
    public function description(): ?string
    {
        return '';
    }

    /**
     * @return iterable|null
     */
    public function permission(): ?iterable
    {
        return [
            'platform.pages.system',
        ];
    }

    /**
     * Button commands.
     *
     * @return Action[]
     */
    public function commandBar(): iterable
    {
        return [
            Link::make('Close')
                ->route('platform.pages.system')
                ->icon('close')
                ->type(Color::DANGER()),

            Button::make(__('Save'))
                ->icon('check')
                ->type(Color::SUCCESS())
                ->method('save'),

        ];
    }

    /**
     * @return \Orchid\Screen\Layout[]
     */
    public function layout(): iterable
    {
        $tabsLocales = [];
        foreach (LaravelLocalization::getSupportedLocales() as $locale => $supportedLocale) {
            $tabsLocales[$supportedLocale['name']] = Layout::columns([
                Layout::rows([

                    Input::make($locale.'[name]')
                        ->title('Name:')
                        ->placeholder('Enter name')
                        ->value($this->page->translate($locale)->name ?? '')
                        ->required()
                        ->help('Please enter name'),
                    Input::make($locale.'[h1]')
                        ->title('H1:')
                        ->placeholder('Enter H1')
                        ->value($this->page->translate($locale)->h1 ?? '')
                        ->required()
                        ->help('Please enter H1'),

                    Ttinymce::make($locale.'[text]')
                        ->title('Text')
                        ->value($this->page->translate($locale)->text ?? ''),

                    TextArea::make($locale.'[title]')
                        ->title('Title (seo)')
                        ->value($this->page->translate($locale)->title ?? '')
                        ->rows(2),
                    TextArea::make($locale.'[description]')
                        ->title('Description (seo)')
                        ->value($this->page->translate($locale)->description ?? '')
                        ->rows(6),
                    TextArea::make($locale.'[keywords]')
                        ->title('Keywords (seo)')
                        ->value($this->page->translate($locale)->keywords ?? '')
                        ->rows(6)
                ])

            ]);
        }
        return [
            Layout::columns([
                Layout::rows([
                    Radio::make('published')
                        ->placeholder('Yes')
                        ->value(1)
                        ->title('Published')
                        ->checked($this->page->published == 1 ? true : false),

                    Radio::make('published')
                        ->placeholder('No')
                        ->value(0)
                        ->checked($this->page->published == 0 ? true : false),
                ]),
                Layout::rows([
                    Label::make('static')
                        ->title('Slug:')
                        ->value($this->page->slug),
                ]),
            ]),
            Layout::tabs($tabsLocales),
            $this->getFieldsByPage($this->page->slug),

        ];
    }

    private function getFieldsByPage(string $alias){
      switch ($alias){
          case 'documents':
              return
                  Layout::columns([
                      Layout::rows([
                          Upload::make('files')
                              ->title('Upload with page')
                              ->media()
                              ->groups('documents')
                              ->closeOnAdd()
                              ->value($this->page->files)
                              ->acceptedFiles('application/pdf, .doc, .docx')
                              ->horizontal(),
                      ]),
                  ]);
              break;
          default:
              return [];
      }
    }

    /**
     * @param SystemPage $page
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function save(SystemPage $page, Request $request)
    {
        $page->fill($request->all());
        foreach (LaravelLocalization::getSupportedLocales() as $locale => $supportedLocale) {
            $page->translateOrNew($locale)->fill($request->get($locale)+ ['system_page_id' => $page->id ])->save();
        }
        $page->save();
        $page->files()->sync($request->get('files'));

        Toast::info(__('Page updated.'));

        return redirect()->route('platform.pages.system.edit', $page);
    }

}
