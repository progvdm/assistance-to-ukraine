<?php

declare(strict_types=1);

namespace App\Orchid\Screens\Pages\WhatIsDone;

use App\Models\WhatIsDone\WhatIsDone;
use App\Orchid\Screens\Fields\Ttinymce;
use Illuminate\Http\Request;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;
use Orchid\Screen\Action;
use Orchid\Screen\Actions\Button;
use Orchid\Screen\Actions\Link;
use Orchid\Screen\Fields\Cropper;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Fields\Label;
use Orchid\Screen\Fields\Picture;
use Orchid\Screen\Fields\Quill;
use Orchid\Screen\Fields\Radio;
use Orchid\Screen\Fields\SimpleMDE;
use Orchid\Screen\Fields\TextArea;
use Orchid\Screen\Fields\Upload;
use Orchid\Screen\Screen;
use Orchid\Support\Color;
use Orchid\Support\Facades\Layout;
use Orchid\Support\Facades\Toast;

class WhatIsDoneEditScreen extends Screen
{
    /**
     * @var WhatIsDone
     */
    public WhatIsDone $page;

    /**
     * Query data.
     *
     * @param WhatIsDone $page
     *
     * @return array
     */
    public function query(WhatIsDone $page): iterable
    {
        return [
            'page' => $page,
        ];
    }

    /**
     * Display header name.
     *
     * @return string|null
     */
    public function name(): ?string
    {
        return $this->page->exists ? 'Edit Page' : 'Create Page';
    }

    /**
     * Display header description.
     *
     * @return string|null
     */
    public function description(): ?string
    {
        return '';
    }

    /**
     * @return iterable|null
     */
    public function permission(): ?iterable
    {
        return [
            'platform.pages.system',
        ];
    }

    /**
     * Button commands.
     *
     * @return Action[]
     */
    public function commandBar(): iterable
    {
        return [
            Link::make('Close')
                ->route('platform.pages.what-is-done')
                ->icon('close')
                ->type(Color::DANGER()),

            Button::make(__('Save'))
                ->icon('check')
                ->type(Color::SUCCESS())
                ->method('save'),

        ];
    }

    /**
     * @return \Orchid\Screen\Layout[]
     */
    public function layout(): iterable
    {
        $tabsLocales = [];
        foreach (LaravelLocalization::getSupportedLocales() as $locale => $supportedLocale) {
            $tabsLocales[$supportedLocale['name']] = Layout::columns([
                Layout::rows([

                    Input::make($locale.'[name]')
                        ->title('Name:')
                        ->placeholder('Enter name')
                        ->value($this->page->translate($locale)->name ?? '')
                        ->required()
                        ->help('Please enter name'),
                    Input::make($locale.'[h1]')
                        ->title('H1:')
                        ->placeholder('Enter H1')
                        ->value($this->page->translate($locale)->h1 ?? '')
                        ->required()
                        ->help('Please enter H1'),

                    Input::make($locale.'[country]')
                        ->title('Country:')
                        ->placeholder('Enter country')
                        ->value($this->page->translate($locale)->country ?? '')
                        ->help('Please enter country'),

                    TextArea::make($locale.'[text_short]')
                        ->title('Short text')
                        ->value($this->page->translate($locale)->text_short ?? '')
                        ->rows(6),
                    Ttinymce::make($locale.'[text]')
                        ->title('Text')
                        ->value($this->page->translate($locale)->text ?? ''),

                    TextArea::make($locale.'[title]')
                        ->title('Title (seo)')
                        ->value($this->page->translate($locale)->title ?? '')
                        ->rows(2),
                    TextArea::make($locale.'[description]')
                        ->title('Description (seo)')
                        ->value($this->page->translate($locale)->description ?? '')
                        ->rows(6),
                    TextArea::make($locale.'[keywords]')
                        ->title('Keywords (seo)')
                        ->value($this->page->translate($locale)->keywords ?? '')
                        ->rows(6)
                ])

            ]);
        }
        return [
            Layout::columns([
                Layout::rows([
                    Radio::make('published')
                        ->placeholder('Yes')
                        ->value(1)
                        ->title('Published')
                        ->checked($this->page->published == 1 ? true : false),

                    Radio::make('published')
                        ->placeholder('No')
                        ->value(0)
                        ->checked($this->page->published == 0 ? true : false),
                ]),
                Layout::rows([
                    Input::make('slug')
                        ->title('Slug:')
                        ->required()
                        ->type('slug')
                        ->value($this->page->slug),
                ]),
            ]),
            Layout::tabs($tabsLocales),
            Layout::rows([
                Upload::make('images')
                    ->title('Upload with page')
                    ->media()
                    ->groups('images')
                    ->closeOnAdd()
                    ->value($this->page->images)
                    ->acceptedFiles('image/*')
                    ->horizontal(),

            ])->title('Images upload'),
        ];
    }

    /**
     * @param WhatIsDone $page
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function save(WhatIsDone $page, Request $request)
    {
        $page->fill($request->all());
        $page->save();
        foreach (LaravelLocalization::getSupportedLocales() as $locale => $supportedLocale) {
            $page->translateOrNew($locale)->fill($request->get($locale)+ ['what_is_done_id' =>$page->id ])->save();
        }
        $page->save();
        $page->images()->sync($request->get('images'));
        Toast::info(__('Page updated.'));

        return redirect()->route('platform.pages.what-is-done.edit', $page);
    }

}
