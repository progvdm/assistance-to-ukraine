<?php

declare(strict_types=1);

namespace App\Orchid\Screens\Fields;

use Orchid\Screen\Field;

class Ttinymce extends Field
{
    /**
     * @var string
     */
    protected $view = 'platform::fields.tinymce';

    /**
     * All attributes that are available to the field.
     *
     * @var array
     */
    protected $attributes = [
        'value'   => null,
        'toolbar' => ['text', 'color', 'quote', 'header', 'list', 'format', 'media'],
        'height'  => '300px',
        'base64'  => false,
    ];

    /**
     * Attributes available for a particular tag.
     *
     * @var array
     */
    protected $inlineAttributes = [
        'accesskey',
        'autocomplete',
        'autofocus',
        'checked',
        'disabled',
        'form',
        'formaction',
        'formenctype',
        'formmethod',
        'formnovalidate',
        'formtarget',
        'name',
        'placeholder',
        'readonly',
        'required',
        'step',
        'tabindex',
        'height',
        'groups',
    ];
}
