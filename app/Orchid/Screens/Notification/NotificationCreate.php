<?php

namespace App\Orchid\Screens\Notification;

use App\Models\Notifications\SupportNotification;
use App\Models\User;
use App\NotificationChannels\TelegramChannel;
use Illuminate\Http\Request;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;
use Orchid\Screen\Actions\Button;
use Orchid\Screen\Actions\Link;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Fields\Select;
use Orchid\Screen\Fields\TextArea;
use Orchid\Screen\Screen;
use Orchid\Support\Color;
use Orchid\Support\Facades\Layout;
use Orchid\Support\Facades\Toast;

class NotificationCreate extends Screen
{
    /**
     * Query data.
     *
     * @return array
     */
    public function query(): iterable
    {
        return [];
    }

    /**
     * Display header name.
     *
     * @return string|null
     */
    public function name(): ?string
    {
        return 'Create notification';
    }

    /**
     * Button commands.
     *
     * @return \Orchid\Screen\Action[]
     */
    public function commandBar(): iterable
    {
        return [Link::make('Close')
            ->route('platform.pages.what-is-done')
            ->icon('close')
            ->type(Color::DANGER()),

            Button::make(__('Save'))
                ->icon('check')
                ->type(Color::SUCCESS())
                ->method('save'),
            ];
    }

    /**
     * @return \Orchid\Screen\Layout[]
     */
    public function layout(): iterable
    {
        $tabsLocales = [];
        foreach (LaravelLocalization::getSupportedLocales() as $locale => $supportedLocale) {
            $tabsLocales[$supportedLocale['name']] = Layout::columns([
                Layout::rows([
                    Input::make($locale.'[subject]')
                        ->title('Subject:')
                        ->value('')
                        ->required()
                        ->help('Please enter subject'),

                    TextArea::make($locale.'[text]')
                        ->title('Text message')
                        ->value('')
                        ->rows(6),
                ])
            ]);
        }
        $users = [
            'all' => 'All'
        ];
        User::all()->each(function (User $user) use (&$users){
            $users[$user->id] = $user->name .' | '. $user->email;
        });
        return [
            Layout::columns([
                Layout::rows([
                    Select::make('users')
                        ->options($users)
                        ->multiple()
                        ->required()
                        ->title('Users')
                        ->help('Allow search bots to index'),
                ]),
                Layout::rows([
                    Select::make('channels')
                        ->options([
                            'mail'   => 'Email',
                            'database'   => 'Notification',
                            TelegramChannel::class   => 'Telegram',
                        ])
                        ->multiple()
                        ->required()
                        ->title('Channels')
                        ->help('Allow search bots to index'),
                ]),
            ]),
            Layout::tabs($tabsLocales),
        ];
    }

    /**
     * @param WhatIsDone $page
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function save(Request $request)
    {
        $supportNotificationModel = new SupportNotification();
        $supportNotificationModel->fill($request->all());
        $supportNotificationModel->save();
        foreach (LaravelLocalization::getSupportedLocales() as $locale => $supportedLocale) {
            $supportNotificationModel->translateOrNew($locale)->fill($request->get($locale)+ ['support_notification_id' =>$supportNotificationModel->id ])->save();
        }
        $supportNotificationModel->save();
        Toast::info(__('Notification created.'));

        return redirect()->route('platform.support.notifications');
    }

}
