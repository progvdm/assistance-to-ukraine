<?php

namespace App\Orchid\Screens\Notification;

use App\Models\Notifications\SupportNotification;
use App\Orchid\Layouts\Notifications\NotificationsListLayout;
use Illuminate\Http\Request;
use Orchid\Screen\Actions\Link;
use Orchid\Screen\Screen;
use Orchid\Support\Facades\Toast;

class NotificationsList extends Screen
{
    /**
     * Query data.
     *
     * @return array
     */
    public function query(): iterable
    {
        return [
            'list' => SupportNotification::all(),
        ];
    }

    /**
     * Display header name.
     *
     * @return string|null
     */
    public function name(): ?string
    {
        return 'Support notification';
    }

    /**
     * Button commands.
     *
     * @return \Orchid\Screen\Action[]
     */
    public function commandBar(): iterable
    {
        return [
            Link::make(__('Add'))
                ->icon('plus')
                ->route('platform.support.notifications.create'),
        ];
    }

    /**
     * Views.
     *
     * @return \Orchid\Screen\Layout[]|string[]
     */
    public function layout(): iterable
    {
        return [
            NotificationsListLayout::class
        ];
    }
    /**
     * @param Request $request
     */
    public function remove(Request $request): void
    {
        SupportNotification::findOrFail($request->get('id'))->delete();

        Toast::info(__('Notify was removed'));
    }
}
