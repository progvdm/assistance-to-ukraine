<?php

declare(strict_types=1);

namespace App\Orchid;

use Orchid\Platform\Dashboard;
use Orchid\Platform\ItemPermission;
use Orchid\Platform\OrchidServiceProvider;
use Orchid\Screen\Actions\Menu;
use Orchid\Support\Color;

class PlatformProvider extends OrchidServiceProvider
{
    /**
     * @param Dashboard $dashboard
     */
    public function boot(Dashboard $dashboard): void
    {
        parent::boot($dashboard);

        // ...
    }

    /**
     * @return Menu[]
     */
    public function registerMainMenu(): array
    {
        return [
            /*Menu::make('Example screen')
                ->icon('monitor')
                ->route('platform.example')
                ->title('Navigation')
                ->badge(function () {
                    return 6;
                }),*/

            Menu::make('Контент ')
                ->icon('code')
                ->list([
                    Menu::make('Системны сторінки')->route('platform.pages.system')->icon('docs'),
/*                    Menu::make('What is done list')->route('platform.pages.what-is-done')->icon('docs'),*/
                    Menu::make('FAQ ')
                        ->icon('faq')
                        ->route('platform.faq'),
                   // Menu::make('Sub element item 2')->icon('heart'),
                ]),
            Menu::make('Повідомлення користувачам')->route('platform.support.notifications')->icon('notifications'),

            /*Menu::make('Basic Elements')
                ->title('Form controls')
                ->icon('note')
                ->route('platform.example.fields'),

            Menu::make('Advanced Elements')
                ->icon('briefcase')
                ->route('platform.example.advanced'),

            Menu::make('Text Editors')
                ->icon('list')
                ->route('platform.example.editors'),

            Menu::make('Overview layouts')
                ->title('Layouts')
                ->icon('layers')
                ->route('platform.example.layouts'),

            Menu::make('Chart tools')
                ->icon('bar-chart')
                ->route('platform.example.charts'),

            Menu::make('Cards')
                ->icon('grid')
                ->route('platform.example.cards')
                ->divider(),

            Menu::make('Documentation')
                ->title('Docs')
                ->icon('docs')
                ->url('https://orchid.software/en/docs'),

            Menu::make('Changelog')
                ->icon('shuffle')
                ->url('https://github.com/orchidsoftware/platform/blob/master/CHANGELOG.md')
                ->target('_blank')
                ->badge(function () {
                    return Dashboard::version();
                }, Color::DARK()),*/

            Menu::make(__('Організації'))
                ->icon('user')
                ->route('platform.systems.organisations')
                //->permission('platform.systems.users')
                ,

            Menu::make('Адміністрування ')
                ->icon('code')
                ->list([
                    Menu::make(__('Користувачі системи'))
                        ->icon('user')
                       // ->route('platform.systems.users')
                        //->permission('platform.systems.users')
                        ->title(__('Система')),
                    Menu::make(__('Roles'))
                        ->icon('lock')
                        ->route('platform.systems.roles')
                        ->permission('platform.systems.roles')
                    // Menu::make('Sub element item 2')->icon('heart'),
                ]),

        ];
    }

    /**
     * @return Menu[]
     */
    public function registerProfileMenu(): array
    {
        return [
            Menu::make('Profile')
                ->route('platform.profile')
                ->icon('user'),
        ];
    }

    /**
     * @return ItemPermission[]
     */
    public function registerPermissions(): array
    {
        return [
            ItemPermission::group(__('System'))
                ->addPermission('platform.systems.roles', __('Roles'))
                ->addPermission('platform.systems.users', __('Users')),
            ItemPermission::group('Pages')
                ->addPermission('platform.pages.system', __('System page'))
                ->addPermission('platform.pages.what-is-done', __('What is done'))
        ];
    }
}
