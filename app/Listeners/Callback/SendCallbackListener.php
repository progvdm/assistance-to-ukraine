<?php

namespace App\Listeners\Callback;

use App\Events\Callback\CallbackEvent;
use App\Foundation\FlashMessage;
use App\Mail\Callback\CallbackEmail;
use App\Services\Telegram\TelegramBotService;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class SendCallbackListener implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Handle the event.
     *
     * @param CallbackEvent $event
     * @return void
     */
    public function handle(CallbackEvent $event)
    {
        $this->sendEmail($event);
        $this->sendTelegram($event);
    }

    private function sendEmail($event){
        try {
            Mail::to($event->email ?? env('MAIL_SUPERADMIN'))
                ->send(new CallbackEmail($event->callback));
        }catch (\Exception $e){
            FlashMessage::setMessage(FlashMessage::ERROR, __('default.Some error'));
            Log::error($e->getMessage());
        }
    }

    private function sendTelegram($event){
        $telegram = new TelegramBotService();
        $telegram->sendMessageCallback($event->callback);
    }
}
