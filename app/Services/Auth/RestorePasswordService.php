<?php

namespace App\Services\Auth;

use App\Events\Auth\RestorePassword;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class RestorePasswordService
{
    public function send($user)
    {
        $token = Str::random(60);
        DB::table('password_resets')->insert([
            'email' => $user->email,
            'token' => $token,
            'created_at' => Carbon::now()
        ]);

        RestorePassword::dispatch($user, $token);
    }

    public function restore($token)
    {
        $tokenData = DB::table('password_resets')
            ->where('token', $token)->first();

        if(!$tokenData){
            return null;
        }

        $user = User::whereEmail($tokenData->email)->first();

        return $user->id;
    }

    public function deleteRestoresUser($user)
    {
        DB::table('password_resets')->where('email', $user->email)
            ->delete();
    }
}
