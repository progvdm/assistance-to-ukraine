<?php

namespace App\Services\NovaPoshta;

use NovaPoshta\ApiModels\Address;
use NovaPoshta\Config;
use NovaPoshta\MethodParameters\Address_getCities;
use NovaPoshta\MethodParameters\Address_getSettlements;
use NovaPoshta\MethodParameters\Address_getWarehouses;
use stdClass;
use App\Interfaces\NovaPoshtaServiceInterface;

/**
 * Class NovaPoshtaService
 *
 * @package WezomCms\Orders\Services
 */
class NovaPoshtaService implements NovaPoshtaServiceInterface
{
    /**
     * NovaPoshtaService constructor.
     *
     */
    public function __construct()
    {
        Config::setApiKey(env('NOVA_POSHTA_API2_KEY'));
        Config::setFormat(Config::FORMAT_JSONRPC2);
        Config::setLanguage('ua');
    }

    /**
     * Get all areas
     *
     * @return array
     */
    public function getAreas(): array
    {
        return Address::getAreas()->data;
    }

    /**
     * Get settlements using findByString attribute
     *
     * @param  string  $query
     * @param  bool|null  $hasWarehouses
     * @return array
     */
    public function getSettlements(string $query, bool $hasWarehouses = null): array
    {
        $key = 'nova-poshta-settlements';
        $data = new Address_getSettlements();
        if ($query) {
            $data->setFindByString($query);
            $key .= '-' . urlencode(strtolower($query));
        }
        if (!is_null($hasWarehouses)) {
            $data->Warehouse = $hasWarehouses;
            $key .= '-' . $hasWarehouses;
        }

        return Address::getSettlements($data)->data;
    }

    /**
     * Get city using ref attribute
     *
     * @param  string  $ref
     * @return stdClass|null
     */
    public function getCity(string $ref): ?stdClass
    {
        $data = new Address_getCities();
        $data->setRef($ref);
        $data->Limit = 1;

        $response = Address::getCities($data);
        return $response->data ? current($response->data) : null;
    }

    /**
     * Get cities using findByString attribute
     *
     * @param  string  $query
     * @param  int  $Limit
     * @return array
     */
    public function getCities(string $query = '', int $Limit = 10): array
    {
        $data = new Address_getCities();
        if($query){
            $data->setFindByString($query);
        }
        $data->setPage(1);
        $data->Limit = $Limit;


        return Address::getCities($data)->data;
    }

    /**
     * Get city warehouses using city ref
     *
     * @param  string  $cityRef
     * @return array
     */
    public function getCityWarehouses(string $cityRef): array
    {
        $data = new Address_getWarehouses();
        $data->setCityRef($cityRef);

        return Address::getWarehouses($data)->data;
    }
}
