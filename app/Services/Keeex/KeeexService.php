<?php

namespace App\Services\Keeex;

use App\Helper\FileUploader;
use App\Models\Cargos\TransferInformations;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Psr7\Request;

class KeeexService
{
    //protected FileUploader $uploader;

    private string $apiUrl = 'https://photoproofpro.keeex.me/';
    private string $scenarioId = '281';
    private string $apiToken;
    private string $userToken;
    private string $routeResult;
    private string $requestToken;

    public function __construct()
    {
        $this->apiToken = env('KEEEX_API_TOKEN', '');
        $this->userToken = env('KEEEX_API_USER_TOKEN', '');
        $this->routeResult = route('api.keeex');
        //$this->routeResult = 'https://e214-65-108-213-52.ngrok.io/api/keeex/info';
        $this->requestToken = env('KEEEX_API_REQUEST_TOKEN', '');
    }

    public function getUrlData($cargoId){
        try {
            $client = new Client();
            $headers = [
                'Authorization' => $this->apiToken,
                'Content-Type' => 'application/json'
            ];
            $body = '{
                          "token": "'.$this->userToken.'",
                          "scenarioId": 281,
                          "data": {
                            "internalId": "'.$cargoId.'",
                            "url": "'.$this->routeResult.'"
                          }
                        }';
            $request = new Request('POST', $this->apiUrl.'/api/v1/backoffice/qrcode/sign', $headers, $body);
            $response = $client->sendAsync($request)->wait()->getBody()->getContents();
            return json_decode($response);
        } catch (GuzzleException $e) {
            logger()->error($e->getMessage());
            return null;
        }
    }

    public function saveInfo(\Illuminate\Http\Request $request){

        $data  = [];

        if($request->has('data')){
            foreach (json_decode($request->get('data')) as $datum) {
                $data[$datum->key] = $datum->value;
            }
            logger()->info('uploadFile Callback info :', $data);
            $info = new TransferInformations();
            $info->type = 'keeex';
            $info->status = $data['status'] ?? '';
            $info->cargo_id = (int)$data['internalId'];
            $info->comment = $data['comment'] ?? '';
            $info->data = $request->get('data');
            $info->save();

            if($request->hasFile('file')) {
                $file = $request->file('file');
                $fileName = $request->file('file')->getClientOriginalName();
                $file->move(public_path() . '/storage/keeex/',$fileName);
                $info->photo = $fileName;
                $info->geo_mark = $this->getLocation($fileName);
                $info->save();

                logger()->info('uploadFile File :', [$fileName]);
            }
        }
    }

    public function getLocation($fileName){
        $file = public_path() . '/storage/keeex/'.$fileName;
        $dataExif = exif_read_data($file, 'IFD0');
        $resultGeo = null;
        if(isset($dataExif['GPSLatitude']) and isset($dataExif['GPSLongitude'])){
            $n1 = explode('/', $dataExif['GPSLatitude'][0])[0] / explode('/', $dataExif['GPSLatitude'][0])[1] ;
            $n2 = explode('/', $dataExif['GPSLatitude'][1])[0] / explode('/', $dataExif['GPSLatitude'][1])[1] ;
            $n3 = explode('/', $dataExif['GPSLatitude'][2])[0] / explode('/', $dataExif['GPSLatitude'][2])[1] ;

            $e1 = explode('/', $dataExif['GPSLongitude'][0])[0] / explode('/', $dataExif['GPSLatitude'][0])[1] ;
            $e2 = explode('/', $dataExif['GPSLongitude'][1])[0] / explode('/', $dataExif['GPSLatitude'][1])[1] ;
            $e3 = explode('/', $dataExif['GPSLongitude'][2])[0] / explode('/', $dataExif['GPSLatitude'][2])[1] ;

            $lat = round($n1 + $n2/60 + $n3/3600, 6);
            $lng = round($e1 + $e2/60 + $e3/3600, 6);
            $resultGeo = $lat .','.$lng;
        }
        return $resultGeo;
    }


}
