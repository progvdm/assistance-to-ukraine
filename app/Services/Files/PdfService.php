<?php

namespace App\Services\Files;

use Barryvdh\DomPDF\Facade\Pdf;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;

class PdfService
{
    const EXTENSION = '.pdf';
    const NAME_SIZE = '13';

    protected string $filename = '';
    protected string $path     = '';

    public function __construct($settings, $fieldName, $filename = null, $regenerate = false)
    {
        if($filename){
            if($regenerate){
                $this->filename =$filename . self::EXTENSION;
            }else{
                $this->filename = preg_replace('/[^ a-zа-яё\d]/ui', '',$filename ).self::EXTENSION;
            }
        }else{
            $this->filename = Str::random(self::NAME_SIZE).self::EXTENSION;
        }
        $this->path = filePath($settings, $fieldName);

        if(!File::isDirectory($this->path )){
            File::makeDirectory($this->path , 0777, true, true);

        }
    }

    public function html($html): string
    {
        $savePath = $this->path . $this->filename;

        $pdf = PDF::loadHTML($html);
        $pdf->render();
        $pdf->save($savePath);

        return $this->filename;
    }
    public function download($html)
    {
        $savePath = $this->path . $this->filename;

        $pdf = PDF::loadHTML($html);
        $pdf->render();

        return $pdf->download();
    }
}
