<?php

namespace App\Services\Telegram;

use App\Models\Callbacks\Callback;
use TelegramBot\Api\HttpException;

class TelegramBotService
{
    private $bot;
    private $group_support_id;

    public function __construct()
    {
        $this->bot = new \TelegramBot\Api\Client(env('TELEGRAM_BOT_TOKEN'));
        $this->group_support_id = env('TELEGRAM_GROUP_SUPPORT_ID');
    }

    public function sendMessage($chatId, $message){
        try {
            $this->bot->sendMessage($chatId,$message);
        }catch (HttpException $error){
            logger()->error($error->getMessage());
        }
    }

    public function sendMessageCallback(Callback $callback){
        $message = "----------------------------------". "\n";
        $message .= "New application!". "\n";
        $message .= "\n";
        $message .= "Category: ". $callback->category . "\n";
        $message .= "Ім'я: ". $callback->name . "\n";
        $message .= "Email: ". $callback->email . "\n";
        $message .= "\n";
        $message .= $callback->text . "\n";
        $message .= "----------------------------------". "\n";

        $this->sendMessage($this->group_support_id, $message);
        if($callback->file){
            $data = (array)explode('.', $callback->file);
            $extension = end($data);
            if(in_array($extension,['png', 'jpg', 'jpeg'])){
                $media = new \TelegramBot\Api\Types\InputMedia\ArrayOfInputMedia();
                $media->addItem(new \TelegramBot\Api\Types\InputMedia\InputMediaPhoto(asset('storage/callback/'.$callback->file)));
                $this->bot->sendMediaGroup($this->group_support_id, $media);
            }else{
                try {
                    $document = new \CURLFile(asset('storage/callback/'.$callback->file));
                    $this->bot->sendDocument($this->group_support_id, $document);
                } catch (\TelegramBot\Api\HttpException $e) {
                    logger()->error($e->getMessage());
                    $this->bot->sendMessage($this->group_support_id, 'Файл на жаль не підгружено в Telegram.');
                }

            }
        }

    }
    public function sendMessageDebugSMS($notifiable, $text){
        $message = "--------------------". "\n";
        $message .= "SMS DEBUG! " ."\n";
        $message .= "User phone: ". $notifiable->getPhoneNotification() ."\n";
        $message .= "User name: ". $notifiable->name ."\n";
        $message .= "User Email: ". $notifiable->email ."\n";
        $message .= $text . "\n";
        $message .= "--------------------". "\n";

        $this->sendMessage($this->group_support_id, $message);
    }

}
