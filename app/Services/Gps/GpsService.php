<?php

namespace App\Services\Gps;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Support\Facades\Log;
use JetBrains\PhpStorm\ArrayShape;

class GpsService
{
    private string $apiKey = '';
    private string $baseUrl = 'https://nominatim.openstreetmap.org';

    public function getCityByGeo(float $lat, float $lon){
        $city = '';
        $result = $this->request($lat, $lon);
        if(isset($result[0]) and $result[0]->osm_id){
            $result = $this->getData($result[0]->osm_id);
            $result = $result[0] ?? null;
            if($result and $result->address and (isset($result->address->village) or isset($result->address->town) or isset($result->address->city))){
                if(isset($result->address->city) and $result->address->city != ''){
                    $city =  $result->address->city;
                }
                if(isset($result->address->town) and $result->address->town != ''){
                    $city =  $result->address->town;
                }
                if(isset($result->address->village) and $result->address->village != ''){
                    $city =  $result->address->village;
                }
            }
        }

        return $city;
    }

    private function request(float $lat, float $lon)
    {
        $client = new Client();
        try {
            $request = $client->get(
                $this->baseUrl . '/search.php' . '?format=json&q=' . $lat . ',' . $lon
            );
            $response = $request->getBody()->getContents();
            return json_decode($response);
        } catch (GuzzleException $e) {
            Log::error($e->getMessage());
            return null;
        }
    }

    private function getData($osm_id)
    {
        $client = new Client();
        try {
            $request = $client->get(
                $this->baseUrl . '/lookup' . '?format=json&extratags=1&osm_ids=W' . $osm_id
            );
            $response = $request->getBody()->getContents();

            return json_decode($response);
        } catch (GuzzleException $e) {
            Log::error($e->getMessage());
            return null;
        }
    }
}
