<?php

namespace App\Services\Mail;

use App\Models\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class EmailTokenService
{
    public function token($user)
    {
        $token = Str::random(60);
        DB::table('email_tokens')->insert([
            'email' => $user->email,
            'token' => $token,
            'created_at' => Carbon::now()
        ]);

        return $token;
    }

    public function restore($token)
    {
        $tokenData = DB::table('email_tokens')
            ->where('token', $token)->first();

        if(!$tokenData){
            return null;
        }

        $user = User::whereEmail($tokenData->email)->first();

        return $user->id;
    }

    public function deleteTokens($user)
    {
        DB::table('email_tokens')->where('email', $user->email)
            ->delete();
    }
}
