<?php

namespace App\Services\OpenDataBot;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Support\Facades\Log;
use JetBrains\PhpStorm\ArrayShape;

class OpenDataBotService
{
    private string $apiKey = '';
    private string $baseUrl = 'https://opendatabot.com/api/v3/';
    private string $baseUrlV2 = 'https://opendatabot.com/api/v2/';

    public function __construct()
    {
        if (env('OPEN_DATA_BOT_API_KEY')) {
            $this->apiKey = env('OPEN_DATA_BOT_API_KEY', 'Ue3NA9KvbXrA');
        }
    }

    /**
     * @param string $zip_code
     * @return object|null
     */
    public function getCompanyData(string $zip_code, string $court_type = 'undefined'): object|null
    {
        if (!$responseData = $this->companyRequest($zip_code)) {
            return null;
        }
        if(isset($responseData->data) and isset($responseData->data->registry)){
            $data = [];
            $data['name'] = $responseData->data->registry->shortName ?? '';
            $data['user_name'] = $responseData->data->registry->ceoName ?? '';
            $data['location'] = $this->parseLocationString($responseData->data->registry->address);
            $data['sanction'] = 0;
            $data['founderRu'] = 0;
            $data['count_courts'] = 0;

            foreach ($responseData->data->factors as $factor) {
                if ($factor->factorGroup == 'edr' && $factor->type == 'ruFounders') {
                    $data['founderRu'] = 1;
                }
                if ($factor->factorGroup == 'sanction' && $factor->type == 'sanction') {
                    $data['sanction'] = 1;
                }
                if ($factor->factorGroup == 'court' && isset($factor->items)) {
                    foreach ($factor->items as $item) {
                        if ($item->type == $court_type) {
                            $data['count_courts'] = 1;
                        }
                    }
                }
            }

            return (object)$data;
        }
        return null;
    }

    public function getCompanyCourtsRequest(string $id_code, string $type)
    {
        $client = new Client();
        try {
            $request = $client->get(
                $this->baseUrlV2 . 'company-courts/' . $type . '?apiKey=' . $this->apiKey . '&code=' . $id_code
            );
            $response = $request->getBody()->getContents();
            return json_decode($response);
        } catch (GuzzleException $e) {
            Log::error($e->getMessage());
            return null;
        }
    }

    /**
     * @param string $zip_code
     * @return mixed
     */
    protected function companyRequest(string $zip_code): mixed
    {
        $client = new Client();
        try {
            $request = $client->get($this->baseUrl . 'full-company/' . $zip_code . '?apiKey=' . $this->apiKey);
            $response = $request->getBody()->getContents();
            return json_decode($response);
        } catch (GuzzleException $e) {
            Log::error($e->getMessage());
            return null;
        }
    }

    #[ArrayShape([
        'country' => "string",
        'city' => "string",
        'street' => "string",
        'build' => "string",
        'zip' => "string"
    ])] private function parseLocationString($location): array
    {
        $address = explode(',', $location->parts->atu ?? '');
        $city = end($address) ?? '';
        $city = str_replace(['с.', 'село', 'місто', 'м.', 'селище міського типу'], '', $city);
        $street = $location->parts->street ?? '';
        $street = str_replace(['вул.', 'вул', 'ВУЛИЦЯ', 'вулиця'], '', $street);
        return [
            'country' => $location->country ? trim($location->country) : '',
            'city' => trim($city),
            'street' => trim($street),
            'build' => $location->parts->house ? trim($location->parts->house) : '',
            'zip' => $location->zip ? trim($location->zip) : '',
        ];
    }
}
