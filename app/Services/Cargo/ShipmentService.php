<?php

namespace App\Services\Cargo;

use App\Enums\Cargo\CargoStatusesEnum;
use App\Enums\Cargo\ProductTypeEnum;
use App\Models\Categories\Category;
use App\Models\Humans\Human;
use App\Models\Organisation\Organisation;
use App\Models\Products\Product;
use App\Models\Transfers\TransferDocument;
use App\Models\Transfers\TransferProduct;
use App\Models\User;
use App\Models\Warehouses\Warehouse;
use App\Models\Warehouses\WarehouseProduct;
use App\Models\Cargos\Cargo;
use App\Models\Transfers\Transfer;
use App\Services\Transfer\DocumentService;
use App\Services\Transfer\PhotoService;
use App\Services\Transfer\VideoService;


class ShipmentService
{
    private $livewire;

    public function __construct($livewire)
    {
        $this->livewire = $livewire;
    }

    public function uploadDataForm($ignoreAuthUser = false){
        $this->livewire->productTypes = ProductTypeEnum::all();
        $this->livewire->recipients = Organisation::getRecipientsForSelect($ignoreAuthUser);
        $this->livewire->donors = Organisation::getDonorsForSelect();
        $this->livewire->categories = Category::getForSelect();
    }

    public function setProducts(&$data)
    {
        $data['products'] = $data['formProducts'];
        unset($data['formProducts']);
    }
    public function createPhotos(array $photos, int $transfer_id)
    {
        $photoService = new PhotoService();
        $photoService->upload($photos, $transfer_id);
    }

    public function createVideos(array $videos, int $transfer_id)
    {
        $videoService = new VideoService();
        $videoService->upload($videos, $transfer_id);
    }

    public function createDocuments(array $documents, int $transfer_id)
    {
        $documentService = new DocumentService();
        $documentService->upload($documents, $transfer_id);
    }

    public function createAct(Transfer $transfer, $additionalInfo = [], $filename = null): bool
    {
        $generateService = new GenerateDocumentService($this->livewire->transfer);
        $filename = $generateService->upload($this->livewire->isPeople ? 'donate' : 'child', $additionalInfo ?? $this->livewire->additionalInfo, $filename);

        $document = new TransferDocument();
        $document->type = CargoStatusesEnum::SEND_CARGO;
        $document->document = $filename;
        $document->user_id = auth()->user()->id;
        $document->transfer_id = $transfer->id;
        $document->save();

        return true;
    }
    public function createProducts(int $transfer_id, array $products)
    {
        $transferProducts = [];
        foreach($products as $product){
            $model = new Product();
            $model->new($transfer_id, $product);

            $transferProducts[] = [
                'transfer_id' => $transfer_id,
                'product_id' => $model->id,
                'transfer_product_id' => null,
                'quantity' => $product['quantity'],
            ];
        }

        TransferProduct::insert($transferProducts);
    }
    public function createHuman()
    {
        return Human::new($this->livewire->additionalInfo);
    }

    public function attachCargoParent(Cargo $cargo, array $data)
    {
        $cargosIds = [];
        $productsIds = [];

        foreach ($data['products'] as $product){
            $productsIds[] = $product['id'];
        }

        $products = Product::select('cargo_id')->whereIn('id', $productsIds)->get();
        foreach ($products as $product){
            $cargosIds[] = $product->cargo_id;
        }

        $cargo->parents()->attach($cargosIds);
    }

    public function productsToWarehouse($transfer)
    {
        $products = [];
        if(!\Organisation::getWarehouse()){
            $warehouse = new Warehouse();
            $warehouse->organisation_id = \Organisation::getOrganisationId();
            $warehouse->save();
        }else{
            /** @var Warehouse $warehouse */
            $warehouse = \Organisation::getWarehouse();
        }

        foreach($transfer->products as $product){
            $products[] = [
                'warehouse_id' => $warehouse->id,
                'transfer_product_id' => $product->id,
                'quantity' => $product->quantity,
                'original_quantity' => $product->quantity,
            ];
        }
        WarehouseProduct::insert($products);
    }
    public function warehouseProducts(): bool
    {
        /** @var Warehouse $warehouse */
        $warehouse = \Organisation::getWarehouse();
        $productsWarehouse = $warehouse->warehouseProducts;
        if(!$productsWarehouse){
            return false;
        }
        if(isset($this->livewire->isDonor) and $this->livewire->isDonor){
            return true;
        }
        foreach($productsWarehouse as $warehouseProduct){
            if($warehouseProduct->product){
                $this->livewire->formProducts[$warehouseProduct->product->id] = [
                    'id' => $warehouseProduct->product->id,
                    'name' => $warehouseProduct->product->product->current->first()->name,
                    'name_sender' => $warehouseProduct->product->transfer->sender->name,
                    'original_quantity' => $warehouseProduct->original_quantity,
                    'quantity_storage' => $warehouseProduct->quantity,
                    'quantity_residue' => $warehouseProduct->quantity,
                    'quantity' => 0,
                    'type' => $warehouseProduct->product->product->current->first()->type,
                ];
            }
        }
        return true;
    }

    public function updateProducts(int $transfer_id, array $products)
    {
        $warehouseId = \Organisation::getWarehouse()->id;

        $transferProducts = [];
        foreach($products as $product){
            $productTransfer = TransferProduct::find($product['id']);
            $transferProducts[] = [
                'transfer_id' => $transfer_id,
                'product_id' => $productTransfer->product_id,
                'transfer_product_id' => $productTransfer->id,
                'quantity' => $product['quantity'],
            ];
            $warehouseProduct = WarehouseProduct::where('transfer_product_id', $product['id'])
                ->where('warehouse_id', $warehouseId)->first();
            $warehouseProduct->decrement('quantity', $product['quantity']);
        }
        TransferProduct::insert($transferProducts);
    }
}
