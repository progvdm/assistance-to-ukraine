<?php

namespace App\Services\Cargo;

use App\Enums\Transfer\TransferTypeEnum;
use App\Services\Files\PdfService;
use App\Helper\QrCodeGenerator;
use App\Models\Cargos\Cargo;
use App\Models\Transfers\Transfer;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;

class GenerateDocumentService
{
    public ?Transfer $transfer = null;

    public function __construct($transfer)
    {
        $this->transfer = $transfer;
    }

    public function upload(string $type = '', $additionalInfo = [], $filename = null, $regenerate = false): string
    {
        $qr = QrCodeGenerator::generate(route('cabinet.cargo', $this->transfer->id));
        switch ($type){
            case 'donate':
                $html = $this->donate($qr, $additionalInfo, $regenerate);
                break;
            case 'childs':
                $html = $this->declaration($qr, $additionalInfo);
                break;
            default:
                $html = $this->actOfAcceptance2($qr, $additionalInfo);
                break;
        }
        /*if($acceptance){
            $html = $this->actOfAcceptance($qr, $additionalInfo);
        }elseif($donate){
            $additionalInfoM = [
                'phone' => $additionalInfo['phone'] ?? '',
                'name' => $additionalInfo['name'],
                'passport' => [
                    'series' => $additionalInfo['passport']['series'] ?? '',
                    'number' => $additionalInfo['passport']['number'] ?? '',
                    'date' => $additionalInfo['passport']['date'] ?? '',
                    'issued' => $additionalInfo['passport']['issued'] ?? '',
                    'registration_address' => $additionalInfo['passport']['registration_address'] ?? ''
                ]
            ];
            $html = $this->donate($qr, $additionalInfoM);
        }elseif($this->transfer->child->isEmpty()){
            $html = $this->declaration($qr, $additionalInfo);
        }else{
            $html = $this->actOfAcceptance2($qr, $additionalInfo);
        }*/

        $pdf = new PdfService(Transfer::fileSettings(), 'document', $filename, $regenerate);
        return $pdf->html($html);
    }

    protected function declaration($qr, $additionalInfo): Factory|View|Application
    {
        $products = $this->transfer->productsStart()->with(['current','category', 'category.currents'])->get();
        $productsArray = [];
        foreach ($products as $product){
            $categoryName = '';
            foreach ($product->category->currents as $category){
                $categoryName .= $category->name . ' / ';
            }
            $productsArray[trim($categoryName, ' / ')][] = [
                'id' => $product->id,
                'name' => $product->current()->first()->name,
                'type' => $product->current()->first()->type,
                'quantity' => $product->quantity,
                'volume' => $product->volume,
            ];
        }

        return view('documents.declaration', [
            'transfer' => $this->transfer,
            'products' => $productsArray,
            'qr' => $qr,
            'additionalInfo' => $additionalInfo
        ]);
    }

    public function donate($qr, $additionalInfo, $regenerate = false): Factory|View|Application
    {
        $transfer = $this->transfer;
        $data = [
            'products' => $transfer->products,
            'donorName' => $transfer->sender->name,
        ];
        if(isset($additionalInfo['isOrganisation']) and $additionalInfo['isOrganisation']){
            $additionalInfoM = [
                'name' => $additionalInfo['name'],
                'phone' => $additionalInfo['phone'] ?? '',
                'address' => $additionalInfo['address'] ?? '',
                'id_code' => $additionalInfo['id_code'] ?? '',
                'responsible_person' => $additionalInfo['responsible_person'] ?? '',
                'description_product' => $additionalInfo['description_product'] ?? '',

            ];
            return view('documents.donate-organisation', ['data' => $data, 'additionalInfo' => $additionalInfoM]);
        }else{
            $additionalInfo = (array)$additionalInfo;
            $additionalInfo['passport'] = isset($additionalInfo['passport']) ? (array)$additionalInfo['passport'] : [];
            $date = isset($additionalInfo['passport']['date']) ? $additionalInfo['passport']['date'] : '';
            if(isset($additionalInfo['passport']['date']) and !$regenerate){
                $date = date('d/m/Y', strtotime($additionalInfo['passport']['date']));
            }
            $additionalInfoM = [
                'phone' => $additionalInfo['phone'] ?? '',
                'name' => $additionalInfo['name'],
                'passport' => [
                    'series' => $additionalInfo['passport']['series'] ?? '',
                    'number' => $additionalInfo['passport']['number'] ?? '',
                    'date' => $date,
                    'issued' => $additionalInfo['passport']['issued'] ?? '',
                    'registration_address' => $additionalInfo['passport']['registration_address'] ?? '',
                ],
                'description_product' => $additionalInfo['description_product'] ?? ''
            ];
            return view('documents.donate', ['data' => $data, 'additionalInfo' => $additionalInfoM]);
        }
    }

    protected function actOfAcceptance($qr, $additionalInfo): Factory|View|Application
    {
        $products = $this->transfer->products;
        return view('documents.act-of-acceptance', ['transfer' => $this->transfer, 'products' => $products, 'qr' => $qr, 'additionalInfo' => $additionalInfo]);
    }

    protected function actOfAcceptance2($qr, $additionalInfo): Factory|View|Application
    {
        $transfer = $this->transfer;
        $data = [
          'sender' => $transfer->sender,
          'recipient' => $transfer->recipient,
          'products' => $transfer->products,
        ];

        return view('documents.act-of-acceptance2', ['data' => $data, 'transfer' => $transfer, 'qr' => $qr, 'additionalInfo' => $additionalInfo]);
    }
}
