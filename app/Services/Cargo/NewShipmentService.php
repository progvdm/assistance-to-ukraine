<?php

namespace App\Services\Cargo;

use App\Dto\Cargo\CargoFilesDto;
use App\Dto\Cargo\ProductsDto;
use App\Enums\Cargo\CargoStatusesEnum;
use App\Enums\Cargo\ProductTypeEnum;
use App\Enums\Transfer\TransferTypeEnum;
use App\Events\Cargo\SendSms;
use App\Models\Categories\Category;
use App\Models\Humans\Human;
use App\Models\Organisation\Organisation;
use App\Models\Products\Product;
use App\Models\Transfers\TransferDocument;
use App\Models\Transfers\TransferFiles;
use App\Models\Transfers\TransferProduct;
use App\Models\Transfers\TransferVideo;
use App\Models\User;
use App\Models\Cargos\Cargo;
use App\Models\Transfers\Transfer;
use App\Services\Transfer\DocumentService;
use App\Services\Transfer\PhotoService;
use App\Services\Transfer\VideoService;


class NewShipmentService
{
    public Transfer|null $transfer;

    public function __construct($transferId = null)
    {
        if($transferId){
            $this->transfer = Transfer::find($transferId);
        }else{
            $this->transfer = null;
        }
    }

    public function uploadDataForm($livewire, $ignoreAuthUser = false){
        $livewire->productTypes = ProductTypeEnum::all();
        $livewire->recipients = Organisation::getRecipientsForSelect($ignoreAuthUser);
        $livewire->donors = Organisation::getDonorsForSelect();
        $livewire->categories = Category::getForSelect();
        if($this->transfer){
            $livewire->transfer = $this->transfer;
            if($this->transfer->status == CargoStatusesEnum::SEND_CARGO or $this->transfer->status == CargoStatusesEnum::SEND_APPROVE or $this->transfer->status == CargoStatusesEnum::APPROVED  or $this->transfer->status == CargoStatusesEnum::DELIVERY_CONFIRMED or $this->transfer->status == CargoStatusesEnum::DELIVERY_NOT_CONFIRMED){
                $livewire->finish = true;
            }

            $livewire->transferId = $this->transfer->id;
            $livewire->formProducts = ProductsDto::makeByModel($this->transfer->productsStart);
            $livewire->recipient = $this->transfer->recipient_id;
            $livewire->additionalInfo = $this->transfer->getAdditionalInfo();

            //Files
            $livewire->photosSaved = CargoFilesDto::makeByModel($this->transfer->getImages());
            $livewire->videosSaved = CargoFilesDto::makeByModel($this->transfer->getVideos());
            $livewire->documentsSaved = CargoFilesDto::makeByModel($this->transfer->getFiles());

        }
    }

    public function createOrUpdate($livewire, $data, $humanId): ?Transfer
    {
        /** @var User $user */
        $user = auth()->user();

        if(!$user)
        {
            return null;
        }

        if(!$this->transfer){
            $transfer = new Transfer();
            $transfer->sender_id = \Organisation::getOrganisationId();
            $transfer->recipient_id = $data['recipient'];
            $transfer->human_id = $humanId;
            $transfer->parent = null;
            $transfer->status = CargoStatusesEnum::NEW;
            $transfer->type = 'send';
            $transfer->additional_info = json_encode($data['additionalInfo'] ?? []);
            $transfer->save();
            $this->transfer = $transfer;
        }else{
            $this->transfer->update([
                'additional_info' => json_encode($data['additionalInfo'] ?? []),
                'recipient_id' => $data['recipient']
            ]);
        }

        $this->createOrUpdateProducts($this->transfer->id, $data['products']);
        $this->createPhotos($data['photos'], $this->transfer->id);
        $this->createVideos($data['videos'], $this->transfer->id);
        $this->createDocuments($data['documents'], $this->transfer->id);

        $livewire->transfer = $this->transfer ?? null;
        return $this->transfer;
    }

    public function sendCargo($livewire){

        $transfer = $this->transfer;

        $this->createAct($transfer);
        $text = 'Вам '.$transfer->recipient->name.' відправлено вантаж. Увійдіть до особистого кабінету Aidmonitor.';
        $this->transfer->status = CargoStatusesEnum::SEND_CARGO;
        $this->transfer->save();
        $livewire->transferId = $transfer->id;
        $livewire->transfer = $this->transfer;
        $this->sendSmsRecipient($text);
    }
    public function sendApprove($livewire){

        $transfer = $this->transfer;
        $this->transfer->status = CargoStatusesEnum::SEND_APPROVE;
        $this->transfer->save();
        $livewire->transfer = $this->transfer;
        $livewire->finish = true;

        $text = 'Вам '.$transfer->recipient->name.' сформовано вантаж. Увійдіть до особистого кабінету Aidmonitor для узгодження списку товарів.';
        $this->sendSmsRecipient($text);
    }

    public function setProducts(&$data)
    {
        $data['products'] = $data['formProducts'];
        unset($data['formProducts']);
    }
    public function createPhotos(array $photos, int $transfer_id)
    {
        $photoService = new PhotoService();
        $photoService->upload($photos, $transfer_id);
    }

    public function createVideos(array $videos, int $transfer_id)
    {
        $videoService = new VideoService();
        $videoService->upload($videos, $transfer_id);
    }

    public function createDocuments(array $documents, int $transfer_id)
    {
        $documentService = new DocumentService();
        $documentService->upload($documents, $transfer_id);
    }

    public function createAct(Transfer $transfer)
    {
        $filename = $transfer->sender->name. ' to '.$transfer->recipient->name;
        $filename = mb_substr($filename, 0, 120). ' '. date('Y-m-d His');

        $generateService = new GenerateDocumentService($this->transfer);
        $filename = $generateService->upload('childs', $transfer->getAdditionalInfo(), $filename);
        $document = new TransferDocument();
        $document->type = CargoStatusesEnum::SEND_CARGO;
        $document->document = $filename;
        $document->user_id = auth()->user()->id;
        $document->transfer_id = $transfer->id;
        $document->save();
    }


    public function createOrUpdateProducts(int $transfer_id, array $products)
    {
        Product::where('transfer_start_id', $transfer_id)->delete();
        TransferProduct::where('transfer_id', $transfer_id)->delete();
        $transferProducts = [];
        foreach($products as $product){
            $model = new Product();
            $model->new($transfer_id, $product);

            $transferProducts[] = [
                'transfer_id' => $transfer_id,
                'product_id' => $model->id,
                'transfer_product_id' => null,
                'quantity' => $product['quantity'],
            ];
        }

        TransferProduct::insert($transferProducts);
    }
    public function createHuman($additionalInfo)
    {
        return Human::new($additionalInfo);
    }

    public function deleteFile($livewire, $id, $name){
        switch ($name) {
            case 'photo':
                $photo = TransferFiles::find($id);
                if($photo){
                    $photo->deleteFile();
                    $photo->delete();
                    unset($livewire->photosSaved[$id]);
                }
                break;
            case 'video':
                $video = TransferFiles::find($id);
                if($video){
                    $video->deleteFile();
                    $video->delete();
                    unset($livewire->videosSaved[$id]);
                }
                break;
            case 'document':
                $document = TransferFiles::find($id);
                if($document){
                    $document->deleteFile();
                    $document->delete();
                    unset($livewire->documentsSaved[$id]);
                }
                break;
        }
    }



    public function sendSmsRecipient($text){
        $transfer = $this->transfer;
        $transfer->recipient->notification(\App\Notifications\MessageNotification::class, $text);
    }
    public function sendSmsDonor($text){
        $transfer = $this->transfer;
        $transfer->sender->notification(\App\Notifications\MessageNotification::class, $text);
    }
}
