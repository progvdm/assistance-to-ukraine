<?php

namespace App\Services\Transfer;

use App\Helper\ImageUploader;
use App\Models\Transfers\TransferFiles;
use Illuminate\Support\Facades\Storage;

class PhotoService
{
    protected ImageUploader $uploader;

    public function __construct()
    {
        $this->uploader = new ImageUploader();
    }

    public function upload(array $photos, int $transfer_id)
    {
        foreach ($photos as $photo){
            if($photo['photo'] == ''){
                continue;
            }
            $model = new TransferFiles();
            $model->transfer_id = $transfer_id;
            $name = $this->uploader->uploadOne($photo['photo'], $model, 'images');
            $model->type = TransferFiles::IMAGE;
            $model->file = $name;
            $model->save();
        }
    }
    public function uploadBase64($photo, int $transfer_id)
    {
        preg_match("/data:image\/(.*?);/",$photo,$image_extension); // extract the image extension
        $photo = preg_replace('/data:image\/(.*?);base64,/','',$photo); // remove the type part
        $photo = str_replace(' ', '+', $photo);
        $imageName = 'api_' . time() . '.' . $image_extension[1]; //generating unique file name;
        Storage::disk('public')->put('images/'.$imageName,base64_decode($photo));

        $model = new TransferFiles();
        $model->transfer_id = $transfer_id;
        $model->type = TransferFiles::IMAGE;
        $model->file = $imageName;
        $model->save();
    }
}
