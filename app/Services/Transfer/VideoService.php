<?php

namespace App\Services\Transfer;

use App\Helper\FileUploader;
use App\Models\Transfers\TransferFiles;

class VideoService
{
    protected FileUploader $uploader;

    public function __construct()
    {
        $this->uploader = new FileUploader();
    }

    public function upload(array $videos, int $transfer_id)
    {
        foreach ($videos as $video){
            if($video['video'] == ''){
                continue;
            }
            $model = new TransferFiles();
            $model->transfer_id = $transfer_id;
            $name = $this->uploader->uploadOne($video['video'], 'videos');
            $model->type = TransferFiles::VIDEO;
            $model->file = $name;
            $model->save();
        }
    }
}
