<?php

namespace App\Services\Transfer;

use App\Helper\FileUploader;
use App\Models\Humans\Human;
use App\Models\Transfers\TransferFiles;

class DocumentService
{
    protected FileUploader $uploader;

    public function __construct()
    {
        $this->uploader = new FileUploader();
    }

    public function upload(array $documents, int $transfer_id)
    {
        foreach ($documents as $document){
            if($document['document'] == ''){
                continue;
            }
            $model = new TransferFiles();
            $model->transfer_id = $transfer_id;
            $name = $this->uploader->uploadOne($document['document'], 'documents');
            $model->type = TransferFiles::FILE;
            $model->file = $name;
            $model->save();
        }
    }
    public function uploadHuman($document, int $id)
    {
        $model = Human::find($id);
        if($document and $model){
            $name = $this->uploader->uploadOne($document, 'humans', 'public', $model->name.' - signed '. date('Y-m-d His'));
            $model->confirmed = true;
            $model->file = $name;
            $model->save();
        }
    }
}
