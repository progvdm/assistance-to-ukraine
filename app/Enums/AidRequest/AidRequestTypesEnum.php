<?php

namespace App\Enums\AidRequest;

use JetBrains\PhpStorm\ArrayShape;

class AidRequestTypesEnum
{
    const OFFER = 'offer';
    const NEED = 'need';


    /**
     *
     * @param $status
     * @return mixed
     */
    public static function get( $status ): mixed
    {
        if( self::is( $status ) )
            return self::all()[$status];
        else
            return false;
    }

    /**
     *
     * @param $status
     * @return bool
     */
    public static function is( $status ): bool
    {
        return( ! empty( self::all()[$status] ) );
    }


    #[ArrayShape([
        self::OFFER => "mixed",
        self::NEED => "mixed",
    ])]
    public static function all(): array
    {
        return [
            self::OFFER => __('aid-request.type.Offer'),
            self::NEED => __('aid-request.type.Need'),
        ];
    }
    /**
     * @return string[]
     */
    public static function keys(): array
    {
        return [
            self::OFFER,
            self::NEED,
        ];
    }
}
