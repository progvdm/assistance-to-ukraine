<?php

namespace App\Enums\Transfer;

use JetBrains\PhpStorm\ArrayShape;

class TransferTypeEnum
{
    public const SENDING      = "sending";
    public const RECEIVING    = "receiving ";
    public const DONATE       = "donate ";

    /**
     * @param $type
     * @return mixed
     */
    public static function get( $type ): mixed
    {
        if( self::is( $type ) )
            return self::all()[$type];
        else
            return false;
    }

    /**
     *
     * @param $type
     * @return bool
     */
    public static function is( $type ): bool
    {
        return( ! empty( self::all()[$type] ) );
    }


    #[ArrayShape([self::SENDING => "mixed", self::RECEIVING => "mixed", self::DONATE => "mixed"])]
    public static function all(): array
    {
        return [
            self::SENDING => __('cargo.transfer.SENDING'),
            self::RECEIVING => __('cargo.transfer.RECEIVING'),
            self::DONATE => __('cargo.transfer.DONATE')
        ];
    }

    /**
     * @return string[]
     */
    public static function keys(): array
    {
        return [
            self::SENDING,
            self::RECEIVING,
            self::DONATE
        ];
    }
}
