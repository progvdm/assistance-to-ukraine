<?php

namespace App\Enums\Users;

use JetBrains\PhpStorm\ArrayShape;

class OrganisationTypeEnum
{
    public const DONOR      = "donor";
    public const RECIPIENT  = "recipient";

    /**
     *
     * @param $type
     * @return mixed
     */
    public static function get( $type ): mixed
    {
        if( self::is( $type ) )
            return self::all()[$type];
        else
            return false;
    }

    /**
     *
     * @param $type
     * @return bool
     */
    public static function is( $type ): bool
    {
        return( ! empty( self::all()[$type] ) );
    }


    #[ArrayShape([
        self::DONOR => "mixed",
        self::RECIPIENT => "mixed"])]
    public static function all(): array
    {
        return [
            self::DONOR => __('auth.Donor'),
            self::RECIPIENT => __('auth.Recipient')
        ];
    }

    /**
     *
     * @return string[]
     */
    public static function keys(): array
    {
        return [
            self::DONOR,
            self::CHARITY,
            self::ENTITY
        ];
    }
}
