<?php

namespace App\Enums\Cargo;

use JetBrains\PhpStorm\ArrayShape;

class CargoStatusesEnum
{
    const NEW = 'new';
    const SEND_APPROVE = 'send_approve';
    const APPROVED = 'approved';
    const NOT_APPROVED = 'not_approved';
    const SEND_CARGO = 'send_cargo';
    const ARRIVED_IN_THE_CITY = 'arrived_in_the_city';
    const DELIVERY_NOT_CONFIRMED= 'delivery_not_confirmed';
    const DELIVERY_CONFIRMED= 'delivery_confirmed';

    /**
     *
     * @param $status
     * @return mixed
     */
    public static function get( $status ): mixed
    {
        if( self::is( $status ) )
            return self::all()[$status];
        else
            return false;
    }
    /**
     *
     * @param $status
     * @return mixed
     */
    public static function getClass( $status ): mixed
    {
        if( self::is( $status ) )
            return self::allClass()[$status];
        else
            return false;
    }
    /**
     *
     * @param $status
     * @return mixed
     */
    public static function getIconSvg( $status ): mixed
    {
        if( self::is( $status ) )
            return self::allIcon()[$status];
        else
            return false;
    }

    /**
     *
     * @param $status
     * @return bool
     */
    public static function is( $status ): bool
    {
        return( ! empty( self::all()[$status] ) );
    }


    #[ArrayShape([
        self::NEW => "mixed",
        self::SEND_APPROVE => "mixed",
        self::APPROVED => "mixed",
        self::NOT_APPROVED => "mixed",
        self::SEND_CARGO => "mixed",
        self::ARRIVED_IN_THE_CITY => "mixed",
        self::DELIVERY_NOT_CONFIRMED => "mixed",
        self::DELIVERY_CONFIRMED => "mixed",
    ])]
    public static function all(): array
    {
        return [
            self::NEW => __('cargo.statuses.New'),
            self::SEND_APPROVE => __('cargo.statuses.List approval'),
            self::APPROVED => __('cargo.statuses.List confirmed'),
            self::NOT_APPROVED => __('cargo.statuses.List not confirmed'),
            self::SEND_CARGO => __('cargo.statuses.Sent the goods'),
            self::ARRIVED_IN_THE_CITY => __('cargo.statuses.Arrived in the city'),
            self::DELIVERY_NOT_CONFIRMED => __('cargo.statuses.Delivery not confirmed'),
            self::DELIVERY_CONFIRMED => __('cargo.statuses.Delivery confirmed'),

        ];
    }
    #[ArrayShape([
        self::NEW => "mixed",
        self::SEND_APPROVE => "mixed",
        self::APPROVED => "mixed",
        self::NOT_APPROVED => "mixed",
        self::SEND_CARGO => "mixed",
        self::ARRIVED_IN_THE_CITY => "mixed",
        self::DELIVERY_NOT_CONFIRMED => "mixed",
        self::DELIVERY_CONFIRMED => "mixed",
        ])]
    public static function allClass(): array
    {
        return [
            self::NEW => 'recipient__status--new',
            self::SEND_APPROVE => 'recipient__status--list-send-confirm',
            self::APPROVED => 'recipient__status--list-confirmed',
            self::NOT_APPROVED => 'recipient__status--list-not-confirmed',
            self::SEND_CARGO => 'recipient__status--delivery-send',
            self::ARRIVED_IN_THE_CITY => 'recipient__status--delivery-send',
            self::DELIVERY_NOT_CONFIRMED => 'recipient__status--delivery-not-confirmed',
            self::DELIVERY_CONFIRMED => 'recipient__status--delivery-confirmed',

        ];
    }
    #[ArrayShape([
        self::NEW => "mixed",
        self::SEND_APPROVE => "mixed",
        self::APPROVED => "mixed",
        self::NOT_APPROVED => "mixed",
        self::SEND_CARGO => "mixed",
        self::ARRIVED_IN_THE_CITY => "mixed",
        self::DELIVERY_NOT_CONFIRMED => "mixed",
        self::DELIVERY_CONFIRMED => "mixed",
        ])]
    public static function allIcon(): array
    {
        return [
            self::NEW => '<svg width="12" height="16" viewBox="0 0 12 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path fill-rule="evenodd" clip-rule="evenodd" d="M5.79318 0.205025C6.06885 0.478392 6.06885 0.921608 5.79318 1.19497L4.88055 2.1H5.99993C9.2839 2.1 11.9999 4.7934 11.9999 8.05C11.9999 8.5 11.6248 8.5 11.4999 8.5C11.375 8.5 10.5882 8.55521 10.5882 8.05C10.5882 5.5666 8.5042 3.5 5.99993 3.5H4.88055L5.79318 4.40503C6.06885 4.67839 6.06885 5.12161 5.79318 5.39497C5.51752 5.66834 5.07058 5.66834 4.79492 5.39497L2.67727 3.29497C2.4016 3.02161 2.4016 2.57839 2.67727 2.30503L4.79492 0.205025C5.07058 -0.0683418 5.51752 -0.0683418 5.79318 0.205025Z" fill="#353535"/>
                        <path fill-rule="evenodd" clip-rule="evenodd" d="M6.20676 15.2969C5.93109 15.0236 5.93109 14.5803 6.20676 14.307L7.11939 13.402H6.00001C2.71604 13.402 7.62939e-06 10.7086 7.62939e-06 7.45195C0 7.00195 0.375111 7.00195 0.5 7.00195C0.624889 7.00195 1.41177 6.94674 1.41177 7.45195C1.41177 9.93535 3.49574 12.002 6.00001 12.002H7.11939L6.20676 11.0969C5.93109 10.8236 5.93109 10.3803 6.20676 10.107C6.48242 9.83361 6.92936 9.83361 7.20502 10.107L9.32267 12.207C9.59833 12.4803 9.59833 12.9236 9.32267 13.1969L7.20502 15.2969C6.92936 15.5703 6.48242 15.5703 6.20676 15.2969Z" fill="#353535"/>
                    </svg>',
            self::SEND_APPROVE => '<svg width="12" height="16" viewBox="0 0 12 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path fill-rule="evenodd" clip-rule="evenodd" d="M5.79318 0.205025C6.06885 0.478392 6.06885 0.921608 5.79318 1.19497L4.88055 2.1H5.99993C9.2839 2.1 11.9999 4.7934 11.9999 8.05C11.9999 8.5 11.6248 8.5 11.4999 8.5C11.375 8.5 10.5882 8.55521 10.5882 8.05C10.5882 5.5666 8.5042 3.5 5.99993 3.5H4.88055L5.79318 4.40503C6.06885 4.67839 6.06885 5.12161 5.79318 5.39497C5.51752 5.66834 5.07058 5.66834 4.79492 5.39497L2.67727 3.29497C2.4016 3.02161 2.4016 2.57839 2.67727 2.30503L4.79492 0.205025C5.07058 -0.0683418 5.51752 -0.0683418 5.79318 0.205025Z" fill="#4070b5"/>
                        <path fill-rule="evenodd" clip-rule="evenodd" d="M6.20676 15.2969C5.93109 15.0236 5.93109 14.5803 6.20676 14.307L7.11939 13.402H6.00001C2.71604 13.402 7.62939e-06 10.7086 7.62939e-06 7.45195C0 7.00195 0.375111 7.00195 0.5 7.00195C0.624889 7.00195 1.41177 6.94674 1.41177 7.45195C1.41177 9.93535 3.49574 12.002 6.00001 12.002H7.11939L6.20676 11.0969C5.93109 10.8236 5.93109 10.3803 6.20676 10.107C6.48242 9.83361 6.92936 9.83361 7.20502 10.107L9.32267 12.207C9.59833 12.4803 9.59833 12.9236 9.32267 13.1969L7.20502 15.2969C6.92936 15.5703 6.48242 15.5703 6.20676 15.2969Z" fill="#4070b5"/>
                    </svg>',
            self::APPROVED => '<svg width="12" height="16" viewBox="0 0 12 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path fill-rule="evenodd" clip-rule="evenodd" d="M5.79318 0.205025C6.06885 0.478392 6.06885 0.921608 5.79318 1.19497L4.88055 2.1H5.99993C9.2839 2.1 11.9999 4.7934 11.9999 8.05C11.9999 8.5 11.6248 8.5 11.4999 8.5C11.375 8.5 10.5882 8.55521 10.5882 8.05C10.5882 5.5666 8.5042 3.5 5.99993 3.5H4.88055L5.79318 4.40503C6.06885 4.67839 6.06885 5.12161 5.79318 5.39497C5.51752 5.66834 5.07058 5.66834 4.79492 5.39497L2.67727 3.29497C2.4016 3.02161 2.4016 2.57839 2.67727 2.30503L4.79492 0.205025C5.07058 -0.0683418 5.51752 -0.0683418 5.79318 0.205025Z" fill="#60922d"/>
                        <path fill-rule="evenodd" clip-rule="evenodd" d="M6.20676 15.2969C5.93109 15.0236 5.93109 14.5803 6.20676 14.307L7.11939 13.402H6.00001C2.71604 13.402 7.62939e-06 10.7086 7.62939e-06 7.45195C0 7.00195 0.375111 7.00195 0.5 7.00195C0.624889 7.00195 1.41177 6.94674 1.41177 7.45195C1.41177 9.93535 3.49574 12.002 6.00001 12.002H7.11939L6.20676 11.0969C5.93109 10.8236 5.93109 10.3803 6.20676 10.107C6.48242 9.83361 6.92936 9.83361 7.20502 10.107L9.32267 12.207C9.59833 12.4803 9.59833 12.9236 9.32267 13.1969L7.20502 15.2969C6.92936 15.5703 6.48242 15.5703 6.20676 15.2969Z" fill="#60922d"/>
                    </svg>',
            self::NOT_APPROVED => '<svg width="12" height="16" viewBox="0 0 12 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path fill-rule="evenodd" clip-rule="evenodd" d="M5.79318 0.205025C6.06885 0.478392 6.06885 0.921608 5.79318 1.19497L4.88055 2.1H5.99993C9.2839 2.1 11.9999 4.7934 11.9999 8.05C11.9999 8.5 11.6248 8.5 11.4999 8.5C11.375 8.5 10.5882 8.55521 10.5882 8.05C10.5882 5.5666 8.5042 3.5 5.99993 3.5H4.88055L5.79318 4.40503C6.06885 4.67839 6.06885 5.12161 5.79318 5.39497C5.51752 5.66834 5.07058 5.66834 4.79492 5.39497L2.67727 3.29497C2.4016 3.02161 2.4016 2.57839 2.67727 2.30503L4.79492 0.205025C5.07058 -0.0683418 5.51752 -0.0683418 5.79318 0.205025Z" fill="#de0717"/>
                        <path fill-rule="evenodd" clip-rule="evenodd" d="M6.20676 15.2969C5.93109 15.0236 5.93109 14.5803 6.20676 14.307L7.11939 13.402H6.00001C2.71604 13.402 7.62939e-06 10.7086 7.62939e-06 7.45195C0 7.00195 0.375111 7.00195 0.5 7.00195C0.624889 7.00195 1.41177 6.94674 1.41177 7.45195C1.41177 9.93535 3.49574 12.002 6.00001 12.002H7.11939L6.20676 11.0969C5.93109 10.8236 5.93109 10.3803 6.20676 10.107C6.48242 9.83361 6.92936 9.83361 7.20502 10.107L9.32267 12.207C9.59833 12.4803 9.59833 12.9236 9.32267 13.1969L7.20502 15.2969C6.92936 15.5703 6.48242 15.5703 6.20676 15.2969Z" fill="#de0717"/>
                    </svg>',
            self::SEND_CARGO => '<svg width="12" height="16" viewBox="0 0 12 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path fill-rule="evenodd" clip-rule="evenodd" d="M5.79318 0.205025C6.06885 0.478392 6.06885 0.921608 5.79318 1.19497L4.88055 2.1H5.99993C9.2839 2.1 11.9999 4.7934 11.9999 8.05C11.9999 8.5 11.6248 8.5 11.4999 8.5C11.375 8.5 10.5882 8.55521 10.5882 8.05C10.5882 5.5666 8.5042 3.5 5.99993 3.5H4.88055L5.79318 4.40503C6.06885 4.67839 6.06885 5.12161 5.79318 5.39497C5.51752 5.66834 5.07058 5.66834 4.79492 5.39497L2.67727 3.29497C2.4016 3.02161 2.4016 2.57839 2.67727 2.30503L4.79492 0.205025C5.07058 -0.0683418 5.51752 -0.0683418 5.79318 0.205025Z" fill="#c19046"/>
                        <path fill-rule="evenodd" clip-rule="evenodd" d="M6.20676 15.2969C5.93109 15.0236 5.93109 14.5803 6.20676 14.307L7.11939 13.402H6.00001C2.71604 13.402 7.62939e-06 10.7086 7.62939e-06 7.45195C0 7.00195 0.375111 7.00195 0.5 7.00195C0.624889 7.00195 1.41177 6.94674 1.41177 7.45195C1.41177 9.93535 3.49574 12.002 6.00001 12.002H7.11939L6.20676 11.0969C5.93109 10.8236 5.93109 10.3803 6.20676 10.107C6.48242 9.83361 6.92936 9.83361 7.20502 10.107L9.32267 12.207C9.59833 12.4803 9.59833 12.9236 9.32267 13.1969L7.20502 15.2969C6.92936 15.5703 6.48242 15.5703 6.20676 15.2969Z" fill="#c19046"/>
                    </svg>',
            self::ARRIVED_IN_THE_CITY => '<svg width="12" height="16" viewBox="0 0 12 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path fill-rule="evenodd" clip-rule="evenodd" d="M5.79318 0.205025C6.06885 0.478392 6.06885 0.921608 5.79318 1.19497L4.88055 2.1H5.99993C9.2839 2.1 11.9999 4.7934 11.9999 8.05C11.9999 8.5 11.6248 8.5 11.4999 8.5C11.375 8.5 10.5882 8.55521 10.5882 8.05C10.5882 5.5666 8.5042 3.5 5.99993 3.5H4.88055L5.79318 4.40503C6.06885 4.67839 6.06885 5.12161 5.79318 5.39497C5.51752 5.66834 5.07058 5.66834 4.79492 5.39497L2.67727 3.29497C2.4016 3.02161 2.4016 2.57839 2.67727 2.30503L4.79492 0.205025C5.07058 -0.0683418 5.51752 -0.0683418 5.79318 0.205025Z" fill="#c19046"/>
                        <path fill-rule="evenodd" clip-rule="evenodd" d="M6.20676 15.2969C5.93109 15.0236 5.93109 14.5803 6.20676 14.307L7.11939 13.402H6.00001C2.71604 13.402 7.62939e-06 10.7086 7.62939e-06 7.45195C0 7.00195 0.375111 7.00195 0.5 7.00195C0.624889 7.00195 1.41177 6.94674 1.41177 7.45195C1.41177 9.93535 3.49574 12.002 6.00001 12.002H7.11939L6.20676 11.0969C5.93109 10.8236 5.93109 10.3803 6.20676 10.107C6.48242 9.83361 6.92936 9.83361 7.20502 10.107L9.32267 12.207C9.59833 12.4803 9.59833 12.9236 9.32267 13.1969L7.20502 15.2969C6.92936 15.5703 6.48242 15.5703 6.20676 15.2969Z" fill="#c19046"/>
                    </svg>',
            self::DELIVERY_NOT_CONFIRMED => '<svg width="14" height="14"  fill="none" viewBox="0 0 14 14" xmlns="http://www.w3.org/2000/svg"><path d="m7 1.4c-3.0928 0-5.6 2.5072-5.6 5.6 0 3.0928 2.5072 5.6 5.6 5.6 3.0928 0 5.6-2.5072 5.6-5.6 0-3.0928-2.5072-5.6-5.6-5.6zm-7 5.6c0-3.866 3.134-7 7-7 3.866 0 7 3.134 7 7 0 3.866-3.134 7-7 7-3.866 0-7-3.134-7-7zm7-4.2c0.3866 0 0.7 0.3134 0.7 0.7v3.21l1.895 1.895c0.27337 0.27336 0.27337 0.71658 0 0.98994-0.27336 0.27337-0.71658 0.27337-0.98995 0l-2.1-2.1c-0.13128-0.13127-0.20503-0.30932-0.20503-0.49497v-3.5c0-0.3866 0.3134-0.7 0.7-0.7z" clip-rule="evenodd" fill="#DE0717" fill-rule="evenodd"/></svg>',
            self::DELIVERY_CONFIRMED => '<svg width="14" height="14"  viewBox="0 0 14 14" xmlns="http://www.w3.org/2000/svg"><path d="m0.75 7c0-3.4518 2.7982-6.25 6.25-6.25 3.4518 0 6.25 2.7982 6.25 6.25 0 3.4518-2.7982 6.25-6.25 6.25-3.4518 0-6.25-2.7982-6.25-6.25z" fill="#fff" stroke="#60922D" stroke-linecap="round" stroke-linejoin="round" stroke-width="1.5"/><path d="m10.732 4.1894c0.3302 0.27519 0.36 0.74924 0.0664 1.0588l-4.2666 4.5c-0.15181 0.16012-0.36941 0.25173-0.59792 0.25173s-0.44612-0.09161-0.59793-0.25173l-2.1333-2.25c-0.29353-0.30959-0.26379-0.78364 0.06644-1.0588 0.33022-0.27519 0.83588-0.2473 1.1294 0.06228l1.5354 1.6194 3.6687-3.8694c0.29353-0.30958 0.79923-0.33747 1.1294-0.06228z" clip-rule="evenodd" fill="#60922D" fill-rule="evenodd"/></svg>',

        ];
    }

    /**
     * @return string[]
     */
    public static function keys(): array
    {
        return [
            self::NEW,
            self::SEND_APPROVE,
            self::APPROVED,
            self::SEND_CARGO,
            self::ARRIVED_IN_THE_CITY,
            self::DELIVERY_NOT_CONFIRMED,
            self::DELIVERY_CONFIRMED,
        ];
    }
}
