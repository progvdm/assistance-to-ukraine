<?php

namespace App\Enums\Cargo;

use JetBrains\PhpStorm\ArrayShape;

class ProductTypeEnum
{
    public const KG      = "kg";
    public const PIECES  = "pieces";
    public const LITERS  = "liters";

    /**
     *
     * @param $type
     * @return mixed
     */
    public static function get( $type ): mixed
    {
        if( self::is( $type ) )
            return self::all()[$type];
        else
            return false;
    }
    /**
     *
     * @param $type
     * @return mixed
     */
    public static function getEn( $type ): mixed
    {
        if( self::is( $type ) )
            return self::allEn()[$type];
        else
            return false;
    }

    /**
     *
     * @param $type
     * @return bool
     */
    public static function is( $type ): bool
    {
        return( ! empty( self::all()[$type] ) );
    }


    #[ArrayShape([
        self::KG => "mixed",
        self::PIECES => "mixed",
        self::LITERS => "mixed"])]
    public static function all(): array
    {
        return [
            self::KG => __('cargo.products.KG'),
            self::PIECES => __('cargo.products.PIECES'),
            self::LITERS => __('cargo.products.LITERS')
        ];
    }
    #[ArrayShape([
        self::KG => "mixed",
        self::PIECES => "mixed",
        self::LITERS => "mixed"])]
    public static function allEn(): array
    {
        return [
            self::KG => 'kg',
            self::PIECES => 'pieces',
            self::LITERS => 'liters'
        ];
    }

    /**
     * @return string[]
     */
    public static function keys(): array
    {
        return [
            self::KG,
            self::PIECES,
            self::LITERS
        ];
    }
}
