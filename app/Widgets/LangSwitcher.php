<?php

namespace App\Widgets;

use Arrilot\Widgets\AbstractWidget;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;

class LangSwitcher extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        $locales = [];
        $current = [];
        foreach (LaravelLocalization::getSupportedLocales() as $locale => $opt)
        {
            if($locale == app()->getLocale()){
                $current = [
                    'name' => strtoupper($locale == 'uk' ? 'ua' : $locale),
                    'link' => LaravelLocalization::getLocalizedURL($locale, null, [], false),
                    'active' => $locale == app()->getLocale()
                ];
            }else{
                $locales[] = [
                    'name' => strtoupper($locale == 'uk' ? 'ua' : $locale),
                    'link' => LaravelLocalization::getLocalizedURL($locale, null, [], false),
                    'active' => $locale == app()->getLocale()
                ];
            }
        }
        array_unshift($locales, $current);
        return view('widgets.lang_switcher', [
            'locales' => $locales,
        ]);
    }
}
