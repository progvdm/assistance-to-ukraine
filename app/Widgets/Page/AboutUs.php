<?php

namespace App\Widgets\Page;

use Arrilot\Widgets\AbstractWidget;

class AboutUs extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        //

        return view('widgets.page.about_us', [
            'config' => $this->config,
        ]);
    }
}
