<?php

namespace App\Widgets\Page;

use App\Enums\Users\OrganisationTypeEnum;
use App\Models\User;
use Arrilot\Widgets\AbstractWidget;
use Illuminate\Support\Arr;

class UsersList extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        $title = '';
        $isDonor = Arr::get($this->config, 'donor');

        if($isDonor){
            $title = __('default.Donors');
            $users = User::whereTypeUser(OrganisationTypeEnum::DONOR)->where('email_verified_at', '!=', null)->get();
        }else{
            $title = __('default.Recipients');
            $users = User::whereTypeUser(OrganisationTypeEnum::RECIPIENT)->where('email_verified_at', '!=', null)->get();
        }

        return view('widgets.page.users_list', [
            'title' => $title,
            'users' => $users,
            'config' => $this->config,
        ]);
    }
}
