<?php

namespace App\Widgets\Page;

use App\Models\Cargos\Cargo;
use App\Models\Transfers\Transfer;
use Arrilot\Widgets\AbstractWidget;

class WhatWeDid extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        $country = 'Great Britain';
        $photos = Transfer::getPhotosByCountry($country);

        return view('widgets.page.what_we_did', [
            'config' => $this->config,
            'photos' => $photos
        ]);
    }
}
