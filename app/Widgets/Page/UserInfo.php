<?php

namespace App\Widgets\Page;

use App\Enums\Users\OrganisationTypeEnum;
use App\Models\User;
use Arrilot\Widgets\AbstractWidget;
use Illuminate\Support\Arr;

class UserInfo extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        $id = Arr::get($this->config, 'id');

        $user = User::findOrFail($id);

        return view('widgets.page.user_info', [
            'user' => $user
        ]);
    }
}
