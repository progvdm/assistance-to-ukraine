<?php

namespace App\Widgets;

use Arrilot\Widgets\AbstractWidget;
use Illuminate\Support\Arr;

class ProductSlider extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        $cargo = Arr::get($this->config, 'cargoId');

        return view('widgets.product-slider', [
            'config' => $this->config,
            'photos' => $cargo->transfers->first()->photos
        ]);
    }
}
