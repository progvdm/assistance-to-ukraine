<?php

namespace App\Widgets;

use App\Enums\AidRequest\AidRequestTypesEnum;
use App\Models\AidRequest\AidRequest;
use Arrilot\Widgets\AbstractWidget;

class MarketplaceHome extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        $timezone = env('TIMEZONE', null);

        $list = AidRequest::whereDelete(false)
            ->wherePublished(1)
            ->where('publication_date', '<=', now($timezone)->format('Y-m-d H:i:s'))
            ->where('active_date', '>=', now($timezone)->format('Y-m-d H:i:s'))
            ->limit(40);
        $listOffer = clone $list;
        $listOffer = $listOffer->where('type', AidRequestTypesEnum::OFFER)->get();
        $listNeed = $list->where('type', AidRequestTypesEnum::NEED)->get();

        return view('widgets.marketplace-home', [
            'config' => $this->config,
            'listOffer' => $listOffer,
            'listNeed' => $listNeed,
        ]);
    }
}
