<?php

namespace App\Widgets;

use App\Enums\Users\OrganisationTypeEnum;
use App\Models\Cargos\Cargo;
use App\Models\Transfers\Transfer;
use App\Models\Transfers\TransferFiles;
use Arrilot\Widgets\AbstractWidget;

class Slider extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        $transfers = Transfer::with(['sender'])
            ->whereHas('sender', function ($query) {
                $query->where('type', '=', OrganisationTypeEnum::DONOR);
            })
            ->whereHas('transferFiles', function ($query) {
                $query->whereType(TransferFiles::IMAGE);
            })

            ->limit(15)
            ->orderByDesc('created_at')
            ->get();

        return view('widgets.slider', [
            'config' => $this->config,
            'transfers' => $transfers,
        ]);
    }
}
