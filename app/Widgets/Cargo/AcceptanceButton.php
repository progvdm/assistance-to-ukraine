<?php

namespace App\Widgets\Cargo;

use App\Enums\Transfer\TransferTypeEnum;
use App\Models\Transfers\Transfer;
use App\Models\User;
use Arrilot\Widgets\AbstractWidget;

class AcceptanceButton extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];


    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        /** @var User $user */
        $user = auth()->user();
        $cargo = $this->config['cargo'];

        $transfer = Transfer::whereHas('cargos', function($query) use($cargo) {
            $query->where('cargo_id', $cargo->id);
        })
            ->where('recipient_id', $user->id)
            ->where('type', TransferTypeEnum::SENDING)
            ->first();
        if($transfer){
            $check = Transfer::where('parent', $transfer->id)->first();
            if($check){
                $transfer = null;
            }
        }

        return view('widgets.cargo.acceptance_button', [
            'config' => $this->config,
            'transfer' => $transfer
        ]);
    }
}
