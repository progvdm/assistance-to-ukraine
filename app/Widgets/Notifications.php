<?php

namespace App\Widgets;

use App\Models\User;
use Arrilot\Widgets\AbstractWidget;

class Notifications extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        /** @var User $user */
        $user = auth()->user();

        return view('widgets.notifications', [
            'config' => $this->config,
            'list' => $user->unreadNotifications->limit(10),
        ]);
    }
}
