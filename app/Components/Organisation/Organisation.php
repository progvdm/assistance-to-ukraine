<?php

namespace App\Components\Organisation;

use App\Enums\Users\OrganisationTypeEnum;
use App\Models\User;
use App\Models\Organisation\Organisation as OrganisationModel;
use App\Models\Warehouses\Warehouse;
use Illuminate\Contracts\Auth\Authenticatable;

/**
 * This is the container for Seo module
 *
 * @package App\Components\Seo
 */
class Organisation
{
    /**
     * Auth user model
     *
     * @var User|null|Authenticatable
     */
    protected User|null|Authenticatable $user;

 /**
     * Organisation model
     *
     * @var null|OrganisationModel
     */
    protected null|OrganisationModel $organisation;


    /**
     * Organisation constructor.
     */
    public function __construct()
    {
        $this->user = auth()->user();
        if($this->user){
            $this->organisation = OrganisationModel::find($this->user->organisation_id);
        }

    }

    /**
     * Reload date
     */
    public function reloadData()
    {
        $this->user = auth()->user();
        $this->organisation = OrganisationModel::find($this->user->organisation_id);
    }

  /**
     * has login
     * @return bool
     */
    public function hasLogin() : bool
  {
      return $this->user ? true : false;
  }
  /**
     * isFounder
     * @return bool
     */
    public function isFounder() : bool
  {
      return $this->user->id == $this->organisation->founder_id;
  }
  /**
     * isFounderForbidden
     */
    public function isFounderForbidden()
  {
      if(!$this->isFounder()){
          abort(403);
      }
  }
  /**
     * Get Auth user
     * @return User
     */
    public function getAuthUser() : User
  {
      return $this->user;
  }

  /**
     * Get founder user
     * @return User
     */
    public function getFounder() : User
  {
      return $this->organisation->founder;
  }
  /**
     * isDonor
     * @return bool
     */
    public function isDonor() : bool
  {
      return $this->organisation->type == OrganisationTypeEnum::DONOR;
  }
  /**
     * isRecipient
     * @return bool
     */
    public function isRecipient() : bool
  {
      return $this->organisation->type == OrganisationTypeEnum::RECIPIENT;
  }

    /**
     * Get Auth user
     * @return User
     */
    public function getOrganisation() : \App\Models\Organisation\Organisation
    {
        return $this->organisation;
    }
    /**
     * Get Auth user
     * @return string
     */
    public function getOrganisationName() : string
    {
        return $this->organisation->name;
    }
    /**
     * Get Auth user
     * @return string
     */
    public function getOrganisationId() : string
    {
        return $this->organisation->id;
    }

    /**
     * Get Auth user
     *
     * @return Warehouse
     */
    public function getWarehouse() : Warehouse
    {
        return $this->organisation->warehouse;
    }

    /**
     * Notification users organisation
     */
    public function notificationRecipient($class, $text){

        /*if($transfer->recipient->phone){
            $user = $transfer->recipient;
            $user->notify(new \App\Notifications\MessageNotification($text));
        }
        */

    }
    /**
     * Notification users organisation
     */
    public function notificationDonor($class, $text){
        /*$transfer = $this->transfer;
        if($transfer->sender->phone){
            $user = $transfer->sender;
            $user->notify(new \App\Notifications\MessageNotification($text));
        }*/

    }

    /**
     * Notification users organisation
     * @param $class
     * @param OrganisationModel $organisation
     * @param $text
     * @param $data
     */
    public function notification($class, OrganisationModel $organisation, $text, $data){

        // отрвлять суперадмину уведомляху
    }

    public function getStorageCity(): string
    {
        if($this->organisation->storage_city){
            return $this->organisation->storage_city;
        }
        return $this->organisation->city;
    }

}
