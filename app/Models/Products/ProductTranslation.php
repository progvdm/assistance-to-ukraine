<?php

namespace App\Models\Products;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Products\ProductTranslation
 *
 * @property mixed|string $name
 * @property mixed|string $type
 * @property mixed|string $language
 * @property mixed $product_id
 * @property int $id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|ProductTranslation newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ProductTranslation newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ProductTranslation query()
 * @method static \Illuminate\Database\Eloquent\Builder|ProductTranslation whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ProductTranslation whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ProductTranslation whereLanguage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ProductTranslation whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ProductTranslation whereProductId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ProductTranslation whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ProductTranslation whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class ProductTranslation extends Model
{
    use HasFactory;

    protected $fillable = [
      'product_id',
      'name',
      'type',
      'language',
    ];

    public static function new(Product $product, array $formData)
    {
        foreach (['default', 'uk'] as $productLang) {
            $modelTranslate = new ProductTranslation();
            $modelTranslate->name = $productLang == 'default' ? $formData['name'] : '';
            $modelTranslate->type = $productLang == 'default' ? $formData['type'] : '';
            $modelTranslate->product_id = $product->id;
            $modelTranslate->language = $productLang;
            $modelTranslate->save();
        }
    }
}
