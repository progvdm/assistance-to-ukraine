<?php

namespace App\Models\Products;

use App\Models\Categories\Category;
use App\Models\Transfers\Transfer;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * App\Models\Products\Product
 *
 * @property int|mixed $transfer_start_id
 * @property mixed $quantity
 * @property mixed $original_quantity
 * @property mixed $id
 * @property mixed $volume
 * @property mixed $category_id
 * @property mixed $code
 * @property Category $category
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property int $quantity_diff
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Products\ProductTranslation[] $current
 * @property-read int|null $current_count
 * @property-read Transfer $transferStart
 * @method static \Illuminate\Database\Eloquent\Builder|Product newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Product newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Product query()
 * @method static \Illuminate\Database\Eloquent\Builder|Product whereCategoryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Product whereCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Product whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Product whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Product whereQuantity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Product whereQuantityDiff($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Product whereTransferStartId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Product whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Product whereVolume($value)
 * @mixin \Eloquent
 */
class Product extends Model
{
    use HasFactory;

    protected $fillable = [
      'transfer_start_id',
      'quantity',
      'original_quantity',
      'volume',
      'category_id',
      'code',
    ];

    public function current(): HasMany
    {
        return $this->hasMany(ProductTranslation::class)->where('language', '=', 'default');
    }

    public function translate() : ProductTranslation
    {
        return $this->current()->where('language', '=', 'default')->first();
    }

    public function getName(): string
    {
        return $this->translate()->name;
    }
    public function getCategoryName(): string
    {
        return $this->category->current->name;
    }

    public function getType(): string
    {
        return $this->translate()->type;
    }


    public function new(int $transfer_start_id, array $formData)
    {
        $this->transfer_start_id = $transfer_start_id;
        $this->quantity = $formData['quantity'];
        $this->volume = $formData['volume'];
        $this->category_id = $formData['category_id'];
        $this->code = $formData['code'] ?? '';
        $this->save();

        ProductTranslation::new($this, $formData);

        return $this;
    }

    public function category(): BelongsTo
    {
        return $this->belongsTo(Category::class);
    }

    public function transferStart(): BelongsTo
    {
        return $this->belongsTo(Transfer::class, 'transfer_start_id', 'id');
    }
}
