<?php

namespace App\Models\Notifications;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Notifications\SupportNotificationTranslation
 *
 * @property int $id
 * @property string $locale
 * @property string $subject
 * @property string $text
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|SupportNotificationTranslation newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|SupportNotificationTranslation newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|SupportNotificationTranslation query()
 * @method static \Illuminate\Database\Eloquent\Builder|SupportNotificationTranslation whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SupportNotificationTranslation whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SupportNotificationTranslation whereLocale($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SupportNotificationTranslation whereSubject($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SupportNotificationTranslation whereText($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SupportNotificationTranslation whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property int $support_notification_id
 * @method static \Illuminate\Database\Eloquent\Builder|SupportNotificationTranslation whereSupportNotificationId($value)
 */
class SupportNotificationTranslation extends Model
{
    use HasFactory;

    protected $fillable = ['support_notification_id', 'subject', 'text'];

}
