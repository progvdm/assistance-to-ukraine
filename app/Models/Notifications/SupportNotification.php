<?php

namespace App\Models\Notifications;

use App\ExtendPackage\Translatable;
use Illuminate\Database\Eloquent\Model;
use Orchid\Filters\Filterable;
use Orchid\Screen\AsSource;

/**
 * App\Models\Notifications\SupportNotification
 *
 * @property int $id
 * @property int $send_status
 * @property mixed|null $users
 * @property mixed $channels
 * @property mixed|null $log
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Notifications\SupportNotificationTranslation|null $translation
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Notifications\SupportNotificationTranslation[] $translations
 * @property-read int|null $translations_count
 * @method static \Illuminate\Database\Eloquent\Builder|SupportNotification defaultSort(string $column, string $direction = 'asc')
 * @method static \Illuminate\Database\Eloquent\Builder|SupportNotification filters(?\Orchid\Filters\HttpFilter $httpFilter = null)
 * @method static \Illuminate\Database\Eloquent\Builder|SupportNotification filtersApply(iterable $filters = [])
 * @method static \Illuminate\Database\Eloquent\Builder|SupportNotification filtersApplySelection($selection)
 * @method static \Illuminate\Database\Eloquent\Builder|SupportNotification listsTranslations(string $translationField)
 * @method static \Illuminate\Database\Eloquent\Builder|SupportNotification newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|SupportNotification newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|SupportNotification notTranslatedIn(?string $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|SupportNotification orWhereTranslation(string $translationField, $value, ?string $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|SupportNotification orWhereTranslationLike(string $translationField, $value, ?string $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|SupportNotification orderByTranslation(string $translationField, string $sortMethod = 'asc')
 * @method static \Illuminate\Database\Eloquent\Builder|SupportNotification query()
 * @method static \Illuminate\Database\Eloquent\Builder|SupportNotification translated()
 * @method static \Illuminate\Database\Eloquent\Builder|SupportNotification translatedIn(?string $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|SupportNotification whereChannels($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SupportNotification whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SupportNotification whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SupportNotification whereLog($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SupportNotification whereSendStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SupportNotification whereTranslation(string $translationField, $value, ?string $locale = null, string $method = 'whereHas', string $operator = '=')
 * @method static \Illuminate\Database\Eloquent\Builder|SupportNotification whereTranslationLike(string $translationField, $value, ?string $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|SupportNotification whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SupportNotification whereUsers($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SupportNotification withTranslation()
 * @mixin \Eloquent
 */
class SupportNotification extends Model
{
    use Translatable, Filterable, AsSource;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'send_status',
        'users',
        'channels',
        'log',
    ];

    /**
     * Names of the fields being translated in the "Translation" model.
     *
     * @var array
     */
    protected array $translatedAttributes = ['subject', 'text'];

    /**
     * The relations to eager load on every query.
     *
     * @var array
     */
    protected $with = ['translations'];

    protected $casts = [
        'users' => 'array',
        'channels' => 'array'
    ];


}
