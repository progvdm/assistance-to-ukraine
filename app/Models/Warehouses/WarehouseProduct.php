<?php

namespace App\Models\Warehouses;

use App\Models\Transfers\TransferProduct;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * App\Models\Warehouses\WarehouseProduct
 *
 * @property int $id
 * @property int $warehouse_id
 * @property int $quantity
 * @property int $original_quantity
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property int $transfer_product_id
 * @property-read TransferProduct $product
 * @method static \Illuminate\Database\Eloquent\Builder|WarehouseProduct newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|WarehouseProduct newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|WarehouseProduct query()
 * @method static \Illuminate\Database\Eloquent\Builder|WarehouseProduct whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|WarehouseProduct whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|WarehouseProduct whereOriginalQuantity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|WarehouseProduct whereQuantity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|WarehouseProduct whereTransferProductId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|WarehouseProduct whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|WarehouseProduct whereWarehouseId($value)
 * @mixin \Eloquent
 */
class WarehouseProduct extends Model
{
    use HasFactory;

    protected $fillable = [
      'warehouse_id',
      'transfer_product_id',
      'quantity',
      'original_quantity',
    ];

    public function product(): BelongsTo
    {
        return $this->belongsTo(TransferProduct::class, 'transfer_product_id', 'id');
    }
}
