<?php

namespace App\Models\Pages;

use App\Enums\Cargo\CargoStatusesEnum;
use App\Enums\Users\OrganisationTypeEnum;
use App\Models\Products\Product;
use App\Models\Transfers\Transfer;
use App\Models\User;
use App\Services\Keeex\KeeexService;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Support\Facades\DB;

/**
 * App\Models\Pages\SystemPageTranslation
 *
 * @property integer $id
 * @property int $system_page_id
 * @property string $locale
 * @property string $name
 * @property string $h1
 * @property string|null $text
 * @property string|null $title
 * @property string|null $description
 * @property string|null $keywords
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|SystemPageTranslation newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|SystemPageTranslation newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|SystemPageTranslation query()
 * @method static \Illuminate\Database\Eloquent\Builder|SystemPageTranslation whereId(mixed $id)
 * @method static \Illuminate\Database\Eloquent\Builder|SystemPageTranslation whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SystemPageTranslation whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SystemPageTranslation whereH1($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SystemPageTranslation whereKeywords($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SystemPageTranslation whereLocale($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SystemPageTranslation whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SystemPageTranslation whereSystemPageId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SystemPageTranslation whereText($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SystemPageTranslation whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SystemPageTranslation whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class SystemPageTranslation extends Model
{
    protected $fillable = ['system_page_id', 'name', 'h1', 'text', 'title', 'description', 'keywords'];


}
