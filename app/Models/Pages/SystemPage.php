<?php

namespace App\Models\Pages;


use App\ExtendPackage\Translatable;
use App\Orchid\Presenters\UserPresenter;
use App\Traits\Model\GetContentModel;
use Illuminate\Database\Eloquent\Model;
use Orchid\Attachment\Models\Attachment;
use Orchid\Screen\AsSource;


/**
 * App\Models\Pages\SystemPage
 *
 * @property bool $published
 * @property int $id
 * @property string $slug
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|Attachment[] $files
 * @property-read int|null $files_count
 * @property-read \App\Models\Pages\SystemPageTranslation|null $translation
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Pages\SystemPageTranslation[] $translations
 * @property-read int|null $translations_count
 * @method static \Illuminate\Database\Eloquent\Builder|SystemPage listsTranslations(string $translationField)
 * @method static \Illuminate\Database\Eloquent\Builder|SystemPage newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|SystemPage newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|SystemPage notTranslatedIn(?string $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|SystemPage orWhereTranslation(string $translationField, $value, ?string $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|SystemPage orWhereTranslationLike(string $translationField, $value, ?string $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|SystemPage orderByTranslation(string $translationField, string $sortMethod = 'asc')
 * @method static \Illuminate\Database\Eloquent\Builder|SystemPage query()
 * @method static \Illuminate\Database\Eloquent\Builder|SystemPage translated()
 * @method static \Illuminate\Database\Eloquent\Builder|SystemPage translatedIn(?string $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|SystemPage whereId(mixed $id)
 * @method static \Illuminate\Database\Eloquent\Builder|SystemPage whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SystemPage wherePublished($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SystemPage whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SystemPage whereTranslation(string $translationField, $value, ?string $locale = null, string $method = 'whereHas', string $operator = '=')
 * @method static \Illuminate\Database\Eloquent\Builder|SystemPage whereTranslationLike(string $translationField, $value, ?string $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|SystemPage whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SystemPage withTranslation()
 * @mixin \Eloquent
 */
class SystemPage extends Model
{
    use Translatable, AsSource;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['published'];

    /**
     * Names of the fields being translated in the "Translation" model.
     *
     * @var array
     */
    protected array $translatedAttributes = ['name', 'text', 'title', 'h1', 'keywords', 'description'];

    /**
     * The relations to eager load on every query.
     *
     * @var array
     */
    protected $with = ['translations'];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = ['published' => 'bool'];

    /**
     * @return UserPresenter
     */
    public function presenter()
    {
        return new UserPresenter($this);
    }

    public function files(): \Illuminate\Database\Eloquent\Relations\BelongsToMany
    {
        return $this->belongsToMany(Attachment::class, 'attachmentable', 'attachmentable_id', 'attachment_id');
    }

    public function getFiles(){
        return $this->files()->orderBy('sort')->orderBy('id')->get();
    }

}
