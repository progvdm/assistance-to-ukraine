<?php

namespace App\Models\NovaPoshta;


use Illuminate\Database\Eloquent\Model;


/**
 * App\Models\NovaPoshta\NovaPoshtaCity
 *
 * @property int $id
 * @property int $cityID
 * @property string $ref
 * @property string $description
 * @property string $areaRef
 * @property string $areaDescription
 * @property string $settlementTypeDescription
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|NovaPoshtaCity newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|NovaPoshtaCity newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|NovaPoshtaCity query()
 * @method static \Illuminate\Database\Eloquent\Builder|NovaPoshtaCity whereAreaDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|NovaPoshtaCity whereAreaRef($value)
 * @method static \Illuminate\Database\Eloquent\Builder|NovaPoshtaCity whereCityID($value)
 * @method static \Illuminate\Database\Eloquent\Builder|NovaPoshtaCity whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|NovaPoshtaCity whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|NovaPoshtaCity whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|NovaPoshtaCity whereRef($value)
 * @method static \Illuminate\Database\Eloquent\Builder|NovaPoshtaCity whereSettlementTypeDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|NovaPoshtaCity whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class NovaPoshtaCity extends Model
{
    protected $table = 'nova_poshta_cities';

    protected $fillable = [
      'cityID',
      'ref',
      'description',
      'areaRef',
      'areaDescription',
    ];

}
