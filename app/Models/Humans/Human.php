<?php

namespace App\Models\Humans;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;
use JetBrains\PhpStorm\ArrayShape;

/**
 * App\Models\Humans\Human
 *
 * @property mixed $phone
 * @property mixed $name
 * @property mixed $address
 * @property mixed $id_code
 * @property mixed $responsible_person
 * @property mixed $file
 * @property bool $confirmed
 * @property mixed $id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Humans\HumanPassport|null $passport
 * @method static \Illuminate\Database\Eloquent\Builder|Human newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Human newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Human query()
 * @method static \Illuminate\Database\Eloquent\Builder|Human whereAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Human whereConfirmed($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Human whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Human whereFile($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Human whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Human whereIdCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Human whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Human wherePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Human whereResponsiblePerson($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Human whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Human extends Model
{
    use HasFactory;

    protected $fillable = [
      'name',
      'phone',
      'address',
      'id_code',
      'responsible_person',
      'confirmed',
      'file',
    ];
    #[ArrayShape(['file' => "string"])]
    public function fileSettings(): array
    {
        return [
            'photo' => [
                'path' => 'app/public/',
                'directory' => 'images/'
            ]
        ];
    }

    public function imageSizes(): array
    {
        return [
//            'small' => [
//                'height' => 0,
//                'weight' => 250
//            ],
//            'medium' => [
//                'height' => 0,
//                'weight' => 500
//            ],
//            'big' => [
//              'height' => 0,
//              'weight' => 1000
//            ],
        ];
    }
    public function passport(): HasOne
    {
        return $this->hasOne(HumanPassport::class);
    }

    public static function new($data)
    {
        $human = new self();
        $human->name = $data['name'];
        $human->phone = $data['phone'] ?? '';
        $human->address = $data['address'] ?? '';
        $human->id_code = $data['id_code'] ?? '';
        $human->responsible_person = $data['responsible_person'] ?? '';
        $human->save();
        if(isset($data['passport'])){
            $passport = $data['passport'];
            $passport['human_id'] = $human->id;
            HumanPassport::new($passport);
        }
        return $human->id;
    }
}
