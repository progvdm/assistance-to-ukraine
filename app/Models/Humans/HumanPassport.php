<?php

namespace App\Models\Humans;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasOne;

/**
 * App\Models\Humans\HumanPassport
 *
 * @property int $id
 * @property int $human_id
 * @property string|null $series
 * @property string|null $number
 * @property string|null $date
 * @property string|null $issued
 * @property string|null $registration_address
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Humans\Human $human
 * @method static \Illuminate\Database\Eloquent\Builder|HumanPassport newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|HumanPassport newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|HumanPassport query()
 * @method static \Illuminate\Database\Eloquent\Builder|HumanPassport whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|HumanPassport whereDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|HumanPassport whereHumanId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|HumanPassport whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|HumanPassport whereIssued($value)
 * @method static \Illuminate\Database\Eloquent\Builder|HumanPassport whereNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|HumanPassport whereRegistrationAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|HumanPassport whereSeries($value)
 * @method static \Illuminate\Database\Eloquent\Builder|HumanPassport whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class HumanPassport extends Model
{
    use HasFactory;

    protected $fillable = [
      'series',
      'number',
      'date',
      'issued',
      'registration_address',
      'human_id',
    ];

    public function human(): BelongsTo
    {
        return $this->belongsTo(Human::class);
    }

    public static function new($data)
    {
        if(isset($data['series']) and isset($data['number'])){
            HumanPassport::create($data);
        }
    }
}
