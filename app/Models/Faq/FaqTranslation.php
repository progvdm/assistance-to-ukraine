<?php

namespace App\Models\Faq;

use App\Enums\Cargo\CargoStatusesEnum;
use App\Enums\Users\OrganisationTypeEnum;
use App\Models\Products\Product;
use App\Models\Transfers\Transfer;
use App\Models\User;
use App\Services\Keeex\KeeexService;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Support\Facades\DB;

/**
 * App\Models\Faq\FaqTranslation
 *
 * @property int $id
 * @property int $faq_id
 * @property string $locale
 * @property string $question
 * @property string $answer
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|FaqTranslation newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|FaqTranslation newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|FaqTranslation query()
 * @method static \Illuminate\Database\Eloquent\Builder|FaqTranslation whereAnswer($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FaqTranslation whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FaqTranslation whereFaqId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FaqTranslation whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FaqTranslation whereLocale($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FaqTranslation whereQuestion($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FaqTranslation whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class FaqTranslation extends Model
{
    protected $fillable = ['faq_id', 'answer', 'question'];


}
