<?php

namespace App\Models\Rating;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * App\Models\Rating\UserRating
 *
 * @property int $id
 * @property int $user_id
 * @property int $grade
 * @property string|null $comment
 * @property string|null $ip
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read User|null $user
 * @method static \Illuminate\Database\Eloquent\Builder|UserRating newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|UserRating newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|UserRating query()
 * @method static \Illuminate\Database\Eloquent\Builder|UserRating whereComment($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserRating whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserRating whereGrade($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserRating whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserRating whereIp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserRating whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserRating whereUserId($value)
 * @mixin \Eloquent
 */
class UserRating extends Model
{
    use HasFactory;

    public $fillable =[
      'grade',
      'comment',
      'user_id',
      'ip',
    ];

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }
}
