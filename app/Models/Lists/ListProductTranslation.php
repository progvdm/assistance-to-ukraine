<?php

namespace App\Models\Lists;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Laravel\Scout\Searchable;

/**
 * App\Models\Lists\ListProductTranslation
 *
 * @property int $id
 * @property int $list_product_id
 * @property string $locale
 * @property string $name
 * @property string|null $description
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|ListProductTranslation newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ListProductTranslation newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ListProductTranslation query()
 * @method static \Illuminate\Database\Eloquent\Builder|ListProductTranslation whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ListProductTranslation whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ListProductTranslation whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ListProductTranslation whereListProductId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ListProductTranslation whereLocale($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ListProductTranslation whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ListProductTranslation whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class ListProductTranslation extends Model
{
    use HasFactory;
    use Searchable;
}
