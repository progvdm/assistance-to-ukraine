<?php

namespace App\Models\Lists;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Lists\ListCategoryTranslation
 *
 * @property int $id
 * @property int $list_category_id
 * @property string $locale
 * @property string $name
 * @property string|null $description
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|ListCategoryTranslation newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ListCategoryTranslation newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ListCategoryTranslation query()
 * @method static \Illuminate\Database\Eloquent\Builder|ListCategoryTranslation whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ListCategoryTranslation whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ListCategoryTranslation whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ListCategoryTranslation whereListCategoryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ListCategoryTranslation whereLocale($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ListCategoryTranslation whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ListCategoryTranslation whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class ListCategoryTranslation extends Model
{
    use HasFactory;

}
