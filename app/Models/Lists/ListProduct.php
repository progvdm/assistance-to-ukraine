<?php

namespace App\Models\Lists;

use App\ExtendPackage\Translatable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Laravel\Scout\Searchable;

/**
 * App\Models\Lists\ListProduct
 *
 * @property int $id
 * @property string $import_id
 * @property string|null $type
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|ListProduct listsTranslations(string $translationField)
 * @method static \Illuminate\Database\Eloquent\Builder|ListProduct newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ListProduct newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ListProduct notTranslatedIn(?string $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|ListProduct orWhereTranslation(string $translationField, $value, ?string $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|ListProduct orWhereTranslationLike(string $translationField, $value, ?string $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|ListProduct orderByTranslation(string $translationField, string $sortMethod = 'asc')
 * @method static \Illuminate\Database\Eloquent\Builder|ListProduct query()
 * @method static \Illuminate\Database\Eloquent\Builder|ListProduct translated()
 * @method static \Illuminate\Database\Eloquent\Builder|ListProduct translatedIn(?string $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|ListProduct whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ListProduct whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ListProduct whereImportId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ListProduct whereTranslation(string $translationField, $value, ?string $locale = null, string $method = 'whereHas', string $operator = '=')
 * @method static \Illuminate\Database\Eloquent\Builder|ListProduct whereTranslationLike(string $translationField, $value, ?string $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|ListProduct whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ListProduct whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ListProduct withTranslation()
 * @mixin \Eloquent
 * @property-read \App\Models\Lists\ListProductTranslation|null $translation
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Lists\ListProductTranslation[] $translations
 * @property-read int|null $translations_count
 * @property string $code
 * @property int $list_category_id
 * @method static \Illuminate\Database\Eloquent\Builder|ListProduct whereCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ListProduct whereListCategoryId($value)
 * @property bool $select
 * @property int|null $parent_id
 * @property-read mixed $code_formated
 * @property-read mixed $name_all
 * @property-read ListProduct|null $parent
 * @method static \Illuminate\Database\Eloquent\Builder|ListProduct whereParentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ListProduct whereSelect($value)
 */
class ListProduct extends Model
{
    use HasFactory;
    use Translatable;
    use Searchable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'parent_id',
        'code',
        'select',
        'import_id',
        'list_category_id',
    ];

    /**
     * Names of the fields being translated in the "Translation" model.
     *
     * @var array
     */
    protected array $translatedAttributes = ['name', 'description'];

    /**
     * The relations to eager load on every query.
     *
     * @var array
     */
    protected $with = ['translations'];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = ['select' => 'bool'];

    public function parent(): BelongsTo
    {
        return $this->belongsTo(ListProduct::class);
    }
    public function getNameAllAttribute(){
        $name = '';

        if($this->parent){
            $name =  $this->parent->name_all .' -> ';
        }
        $name .= str_replace([':', '-'],['', ''], $this->name);
        return $name;
        dd($this->parent);


        $explodeCode = explode(' ', $this->code);
        $name = '';
        if(count($explodeCode) >= 1){
            $listProducts = ListProduct::orWhere('code', $explodeCode[0]);
            if(count($explodeCode) > 1){
                $listProducts->orWhere('code', $explodeCode[0].'-intermediate');
            }
            $listProducts = $listProducts->get();
            if($listProducts){
                foreach ($listProducts as $item){
                    if($name != ''){
                        $name .= ' -> ';
                    }
                    $name .= str_replace([':', '-'],['', ''],$item->name);
                }
            }
        }
        if(count($explodeCode) >= 2){
            $listProducts = ListProduct::orWhere('code',$explodeCode[0].' '.$explodeCode[1])
                ->orWhere('code',$explodeCode[0].' '.$explodeCode[1].'-intermediate')
                ->get();
            if($listProducts){
                foreach ($listProducts as $item){
                    $name .= ' -> '. str_replace([':', '-'],['', ''],$item->name);
                }
            }
        }
        if(count($explodeCode) >= 3){
            /** @var ListProduct $listProduct */
            $listProducts = ListProduct::orWhere('code',$explodeCode[0].' '.$explodeCode[1].' '.$explodeCode[2])
                ->orWhere('code',$explodeCode[0].' '.$explodeCode[1].' '.$explodeCode[2].'-intermediate')
                ->get();
            if($listProducts){
                foreach ($listProducts as $item){
                    $name .= ' -> '. str_replace([':', '-'],['', ''],$item->name);
                }
            }
        }
        if(count($explodeCode) >= 4){
            $listProducts = ListProduct::orWhere('code', $explodeCode[0].' '.$explodeCode[1].' '.$explodeCode[2].' '.$explodeCode[3])
                ->orWhere('code', $explodeCode[0].' '.$explodeCode[1].' '.$explodeCode[2].' '.$explodeCode[3].'-intermediate')
                ->get();
            if($listProducts){
                foreach ($listProducts as $item){
                    $name .= ' -> '. str_replace([':', '-'],['', ''],$item->name);
                }
            }
        }

        return $name;
    }
    public function getCodeFormatedAttribute(){
        if(!$this->select){
            return '';
        }
        return $this->code;
    }
}
