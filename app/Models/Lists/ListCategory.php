<?php

namespace App\Models\Lists;

use App\ExtendPackage\Translatable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Lists\ListCategory
 *
 * @property int $id
 * @property string $import_id
 * @property int $parent_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|ListCategory listsTranslations(string $translationField)
 * @method static \Illuminate\Database\Eloquent\Builder|ListCategory newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ListCategory newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ListCategory notTranslatedIn(?string $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|ListCategory orWhereTranslation(string $translationField, $value, ?string $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|ListCategory orWhereTranslationLike(string $translationField, $value, ?string $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|ListCategory orderByTranslation(string $translationField, string $sortMethod = 'asc')
 * @method static \Illuminate\Database\Eloquent\Builder|ListCategory query()
 * @method static \Illuminate\Database\Eloquent\Builder|ListCategory translated()
 * @method static \Illuminate\Database\Eloquent\Builder|ListCategory translatedIn(?string $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|ListCategory whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ListCategory whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ListCategory whereImportId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ListCategory whereParentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ListCategory whereTranslation(string $translationField, $value, ?string $locale = null, string $method = 'whereHas', string $operator = '=')
 * @method static \Illuminate\Database\Eloquent\Builder|ListCategory whereTranslationLike(string $translationField, $value, ?string $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|ListCategory whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ListCategory withTranslation()
 * @mixin \Eloquent
 * @property-read \App\Models\Lists\ListCategoryTranslation|null $translation
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Lists\ListCategoryTranslation[] $translations
 * @property-read int|null $translations_count
 */
class ListCategory extends Model
{
    use HasFactory;
    use Translatable;

    /**
     * Names of the fields being translated in the "Translation" model.
     *
     * @var array
     */
    protected array $translatedAttributes = ['name', 'description'];

    /**
     * The relations to eager load on every query.
     *
     * @var array
     */
    protected $with = ['translations'];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */

}
