<?php

namespace App\Models;

use Orchid\Platform\Models\Role as RoleVendor;

/**
 * App\Models\Role
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\Orchid\Platform\Models\User[] $users
 * @property-read int|null $users_count
 * @method static \Illuminate\Database\Eloquent\Builder|Role averageByDays(string $value, $startDate = null, $stopDate = null, ?string $dateColumn = null)
 * @method static \Illuminate\Database\Eloquent\Builder|Role countByDays($startDate = null, $stopDate = null, ?string $dateColumn = null)
 * @method static \Illuminate\Database\Eloquent\Builder|Role countForGroup(string $groupColumn)
 * @method static \Illuminate\Database\Eloquent\Builder|Role defaultSort(string $column, string $direction = 'asc')
 * @method static \Illuminate\Database\Eloquent\Builder|Role filters(?\Orchid\Filters\HttpFilter $httpFilter = null)
 * @method static \Illuminate\Database\Eloquent\Builder|Role filtersApply(iterable $filters = [])
 * @method static \Illuminate\Database\Eloquent\Builder|Role filtersApplySelection($selection)
 * @method static \Illuminate\Database\Eloquent\Builder|Role maxByDays(string $value, $startDate = null, $stopDate = null, ?string $dateColumn = null)
 * @method static \Illuminate\Database\Eloquent\Builder|Role minByDays(string $value, $startDate = null, $stopDate = null, ?string $dateColumn = null)
 * @method static \Illuminate\Database\Eloquent\Builder|Role newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Role newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Role query()
 * @method static \Illuminate\Database\Eloquent\Builder|Role sumByDays(string $value, $startDate = null, $stopDate = null, ?string $dateColumn = null)
 * @method static \Illuminate\Database\Eloquent\Builder|Role valuesByDays(string $value, $startDate = null, $stopDate = null, string $dateColumn = 'created_at')
 * @mixin \Eloquent
 */
class Role extends RoleVendor
{
    protected $table = 'role_administrators';


}
