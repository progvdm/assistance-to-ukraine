<?php

namespace App\Models\Organisation;

use App\ExtendPackage\Translatable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * App\Models\Organisation\OrganisationType
 *
 * @property int $id
 * @property int|null $parent_id
 * @property int $status
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read mixed $name_formatted
 * @property-read \App\Models\Organisation\OrganisationTypeTranslation|null $translation
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Organisation\OrganisationTypeTranslation[] $translations
 * @property-read int|null $translations_count
 * @method static \Illuminate\Database\Eloquent\Builder|OrganisationType listsTranslations(string $translationField)
 * @method static \Illuminate\Database\Eloquent\Builder|OrganisationType newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|OrganisationType newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|OrganisationType notTranslatedIn(?string $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|OrganisationType orWhereTranslation(string $translationField, $value, ?string $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|OrganisationType orWhereTranslationLike(string $translationField, $value, ?string $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|OrganisationType orderByTranslation(string $translationField, string $sortMethod = 'asc')
 * @method static \Illuminate\Database\Eloquent\Builder|OrganisationType query()
 * @method static \Illuminate\Database\Eloquent\Builder|OrganisationType translated()
 * @method static \Illuminate\Database\Eloquent\Builder|OrganisationType translatedIn(?string $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|OrganisationType whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrganisationType whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrganisationType whereParentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrganisationType whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrganisationType whereTranslation(string $translationField, $value, ?string $locale = null, string $method = 'whereHas', string $operator = '=')
 * @method static \Illuminate\Database\Eloquent\Builder|OrganisationType whereTranslationLike(string $translationField, $value, ?string $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|OrganisationType whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrganisationType withTranslation()
 * @mixin \Eloquent
 */
class OrganisationType extends Model
{
    use Translatable, HasFactory;

    /**
     * Names of the fields being translated in the "Translation" model.
     *
     * @var array
     */
    protected array $translatedAttributes = ['name'];

    /**
     * The relations to eager load on every query.
     *
     * @var array
     */
    protected $with = ['translations'];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = ['published' => 'bool'];


    public function getNameFormattedAttribute(){
        $name = $this->name;
        if($this->parent_id){
            $name = ' - ' . $name;
        }
        return $name;
    }
    public function children(): HasMany
    {
        return $this->hasMany(OrganisationType::class, 'parent_id', 'id');

    }
    public static function getListSelect(){
        $types = OrganisationType::whereParentId(null)->get();
        $collection = collect();
        foreach ($types as $item) {
            $collection->push($item);
            //dd($item->children);
            if($item->children){
                foreach ($item->children as $itemChild) {
                    $collection->push($itemChild);
                }
            }
        }
        return $collection;
    }
}
