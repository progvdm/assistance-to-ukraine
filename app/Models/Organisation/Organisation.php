<?php

namespace App\Models\Organisation;


use App\Enums\Users\OrganisationTypeEnum;
use App\ExtendPackage\Translatable;
use App\Models\User;
use App\Models\Warehouses\Warehouse;
use App\Traits\ImageAttachable;
use App\Traits\Model\GetContentModel;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;
use JetBrains\PhpStorm\ArrayShape;
use Orchid\Filters\Filterable;
use Orchid\Screen\AsSource;

/**
 * App\Models\Organisation\Organisation
 *
 * @property int $id
 * @property bool $published
 * @property int $view_site
 * @property int $view_system
 * @property string $type
 * @property int|null $founder_id
 * @property int $number_cells
 * @property string|null $zip_code
 * @property string|null $id_code
 * @property string|null $photo
 * @property string|null $website
 * @property string|null $facebook
 * @property string|null $instagram
 * @property int $open_info
 * @property string|null $description_file
 * @property string|null $additional_phone
 * @property string|null $additional_email
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property mixed|null $verification
 * @property-read User|null $founder
 * @property-read \App\Models\Organisation\OrganisationTranslation|null $translation
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Organisation\OrganisationTranslation[] $translations
 * @property-read int|null $translations_count
 * @property-read Warehouse|null $warehouse
 * @method static \Illuminate\Database\Eloquent\Builder|Organisation listsTranslations(string $translationField)
 * @method static \Illuminate\Database\Eloquent\Builder|Organisation newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Organisation newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Organisation notTranslatedIn(?string $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|Organisation orWhereTranslation(string $translationField, $value, ?string $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|Organisation orWhereTranslationLike(string $translationField, $value, ?string $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|Organisation orderByTranslation(string $translationField, string $sortMethod = 'asc')
 * @method static \Illuminate\Database\Eloquent\Builder|Organisation query()
 * @method static \Illuminate\Database\Eloquent\Builder|Organisation translated()
 * @method static \Illuminate\Database\Eloquent\Builder|Organisation translatedIn(?string $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|Organisation whereAdditionalEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Organisation whereAdditionalPhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Organisation whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Organisation whereDescriptionFile($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Organisation whereFacebook($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Organisation whereFounderId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Organisation whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Organisation whereIdCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Organisation whereInstagram($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Organisation whereNumberCells($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Organisation whereOpenInfo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Organisation wherePhoto($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Organisation wherePublished($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Organisation whereTranslation(string $translationField, $value, ?string $locale = null, string $method = 'whereHas', string $operator = '=')
 * @method static \Illuminate\Database\Eloquent\Builder|Organisation whereTranslationLike(string $translationField, $value, ?string $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|Organisation whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Organisation whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Organisation whereVerification($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Organisation whereViewSite($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Organisation whereViewSystem($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Organisation whereWebsite($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Organisation whereZipCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Organisation withTranslation()
 * @mixin \Eloquent
 */
class Organisation extends Model
{
    use Translatable, ImageAttachable, Filterable, AsSource;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'published',
        'view_site',
        'view_system',
        'type',
        'founder_id',
        'number_cells',
        'zip_code',
        'id_code',
        'photo',
        'website',
        'facebook',
        'instagram',
        'open_info',
        'description_file',
        'additional_phone',
        'additional_email',
        'verification',
    ];

    /**
     * Names of the fields being translated in the "Translation" model.
     *
     * @var array
     */
    protected array $translatedAttributes = ['name', 'text', 'text_short', 'country', 'city', 'street', 'build', 'storage_city', 'storage_street', 'storage_build'];

    /**
     * The relations to eager load on every query.
     *
     * @var array
     */
    protected $with = ['translations'];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = ['published' => 'bool'];

    // Image
    public string $field = 'photo';

    /**
     * The attributes for which you can use filters in url.
     *
     * @var array
     */
    protected $allowedFilters = [
        'id',
        'name',
    ];

    public function founder(): HasOne
    {
        return $this->hasOne(User::class,'id', 'founder_id');
    }

    public function warehouse(): HasOne
    {
        return $this->hasOne(Warehouse::class);
    }

    public static function getRecipientsForSelect($ignoreAuthUser = true): array
    {
        $organisations =  Organisation::where('type', OrganisationTypeEnum::RECIPIENT)->whereViewSystem(1);
        if($ignoreAuthUser){
            $organisations->where('id', '!=', \Organisation::getOrganisationId());
        }

        return $organisations->get()->toArray();
    }

    public static function getDonorsForSelect($ignoreAuthUser = true): array
    {
        $organisations =  Organisation::where('type', OrganisationTypeEnum::DONOR)->whereViewSystem(1);
        if($ignoreAuthUser){
            $organisations->where('id', '!=', \Organisation::getOrganisationId());
        }
        return $organisations->get()->toArray();
    }
    public static function getForSite($ignoreAuthUser = true)
    {
        $organisations =  Organisation::whereViewSystem(1)->whereViewSite(1);
        if($ignoreAuthUser and \Organisation::hasLogin()){
            $organisations->where('id', '!=', \Organisation::getOrganisationId());
        }
        return $organisations->get();
    }

    public function notification($class, $param){
        /** @var User $superAdmin */
        $superAdmin = $this->founder;

        $superAdmin->notify(new $class($param));
    }

    public function getAddressToDocument(){
        if($this->storage_city and $this->storage_build and $this->storage_street){
            return $this->country .', '. $this->storage_city .', '. $this->storage_street .', '. $this->storage_build;
        }
        return $this->country .', '. $this->city .', '. $this->street .', '. $this->build;

    }

    public function isDonor(){
        return $this->type == OrganisationTypeEnum::DONOR;
    }
    public function isRecipient(){
        return $this->type == OrganisationTypeEnum::RECIPIENT;
    }


    #[ArrayShape(['photo' => "string[]"])]
    public function fileSettings(): array
    {
        return [
            'photo' => [
                'path' => 'app/public/',
                'directory' => 'user_images/'
            ]
        ];
    }

    public function imageSizes(): array
    {
        return [];
    }

}
