<?php

namespace App\Models\Organisation;


use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Organisation\OrganisationTranslation
 *
 * @property int $id
 * @property int $organisation_id
 * @property string $locale
 * @property string $name
 * @property string|null $text
 * @property string|null $text_short
 * @property string|null $country
 * @property string|null $city
 * @property string|null $street
 * @property string|null $build
 * @property string|null $storage_city
 * @property string|null $storage_street
 * @property string|null $storage_build
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|OrganisationTranslation newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|OrganisationTranslation newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|OrganisationTranslation query()
 * @method static \Illuminate\Database\Eloquent\Builder|OrganisationTranslation whereBuild($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrganisationTranslation whereCity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrganisationTranslation whereCountry($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrganisationTranslation whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrganisationTranslation whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrganisationTranslation whereLocale($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrganisationTranslation whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrganisationTranslation whereOrganisationId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrganisationTranslation whereStorageBuild($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrganisationTranslation whereStorageCity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrganisationTranslation whereStorageStreet($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrganisationTranslation whereStreet($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrganisationTranslation whereText($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrganisationTranslation whereTextShort($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrganisationTranslation whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class OrganisationTranslation extends Model
{
    protected $fillable = ['organisation_id', 'name', 'text', 'text_short', 'country', 'city', 'street', 'build', 'storage_city', 'storage_street', 'storage_build'];


}
