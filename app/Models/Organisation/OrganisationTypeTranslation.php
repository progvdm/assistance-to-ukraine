<?php

namespace App\Models\Organisation;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Organisation\OrganisationTypeTranslation
 *
 * @property int $id
 * @property int $organisation_type_id
 * @property string $name
 * @property string $locale
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|OrganisationTypeTranslation newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|OrganisationTypeTranslation newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|OrganisationTypeTranslation query()
 * @method static \Illuminate\Database\Eloquent\Builder|OrganisationTypeTranslation whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrganisationTypeTranslation whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrganisationTypeTranslation whereLocale($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrganisationTypeTranslation whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrganisationTypeTranslation whereOrganisationTypeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrganisationTypeTranslation whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class OrganisationTypeTranslation extends Model
{
    use HasFactory;
}
