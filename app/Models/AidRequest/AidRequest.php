<?php

namespace App\Models\AidRequest;

use App\ExtendPackage\Translatable;
use App\Models\Categories\Category;
use App\Models\Organisation\Organisation;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Notifications\Notifiable;

/**
 * App\Models\AidRequest\AidRequest
 *
 * @property int $id
 * @property int $organisation_id
 * @property int $own_number
 * @property bool $published
 * @property \Illuminate\Support\Carbon|null $publication_date
 * @property \Illuminate\Support\Carbon|null $active_date
 * @property string $type
 * @property string $contact_email
 * @property string $contact_phone
 * @property string $contact_social
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\AidRequest\AidRequestProduct[] $products
 * @property-read int|null $products_count
 * @property-read \App\Models\AidRequest\AidRequestTranslation|null $translation
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\AidRequest\AidRequestTranslation[] $translations
 * @property-read int|null $translations_count
 * @method static \Illuminate\Database\Eloquent\Builder|AidRequest listsTranslations(string $translationField)
 * @method static \Illuminate\Database\Eloquent\Builder|AidRequest newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|AidRequest newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|AidRequest notTranslatedIn(?string $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|AidRequest orWhereTranslation(string $translationField, $value, ?string $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|AidRequest orWhereTranslationLike(string $translationField, $value, ?string $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|AidRequest orderByTranslation(string $translationField, string $sortMethod = 'asc')
 * @method static \Illuminate\Database\Eloquent\Builder|AidRequest query()
 * @method static \Illuminate\Database\Eloquent\Builder|AidRequest translated()
 * @method static \Illuminate\Database\Eloquent\Builder|AidRequest translatedIn(?string $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|AidRequest whereActiveDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AidRequest whereContactEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AidRequest whereContactPhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AidRequest whereContactSocial($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AidRequest whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AidRequest whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AidRequest whereOrganisationId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AidRequest whereOwnNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AidRequest wherePublicationDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AidRequest wherePublished($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AidRequest whereTranslation(string $translationField, $value, ?string $locale = null, string $method = 'whereHas', string $operator = '=')
 * @method static \Illuminate\Database\Eloquent\Builder|AidRequest whereTranslationLike(string $translationField, $value, ?string $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|AidRequest whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AidRequest whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AidRequest withTranslation()
 * @mixin \Eloquent
 * @property bool $delete
 * @property \Illuminate\Support\Carbon $delete_at
 * @property-read Organisation|null $organisation
 * @method static \Illuminate\Database\Eloquent\Builder|AidRequest whereDelete($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AidRequest whereDeleteAt($value)
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read int|null $notifications_count
 * @property int|null $category_id
 * @property-read \Illuminate\Database\Eloquent\Collection|Category[] $category
 * @property-read int|null $category_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\AidRequest\AidRequestLeed[] $leeds
 * @property-read int|null $leeds_count
 * @method static \Illuminate\Database\Eloquent\Builder|AidRequest whereCategoryId($value)
 */
class AidRequest extends Model
{
    use Translatable, HasFactory;
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'organisation_id',
        'category_id',
        'own_number',
        'published',
        'publication_date',
        'active_date',
        'type',
        'organisation_type_id',
        'count',
        'count_type',
        'contact_email',
        'contact_phone',
        'contact_social',
        'delete',
        'delete_at',
    ];

    /**
     * Names of the fields being translated in the "Translation" model.
     *
     * @var array
     */
    protected array $translatedAttributes = ['name', 'description'];

    /**
     * The relations to eager load on every query.
     *
     * @var array
     */
    protected $with = ['translations'];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'published' => 'bool' ,
        'delete' => 'bool' ,
        'publication_date' => 'datetime',
        'active_date' => 'datetime',
        'delete_at' => 'datetime',
    ];

    public function routeNotificationForMail()
    {
        return $this->contact_email;
    }

    public function products(): HasMany
    {
        return $this->hasMany(AidRequestProduct::class);
    }

    public function leeds(): HasMany
    {
        return $this->hasMany(AidRequestLeed::class);
    }

    public function category(): HasMany
    {
        return $this->hasMany(Category::class);
    }

    public function organisation(): HasOne
    {
        return $this->hasOne(Organisation::class, 'id', 'organisation_id');
    }

    public function getProduct(){
        return $this->products->first();
    }
    public function getProductName(){
        /** @var AidRequestProduct $product */
        $product = $this->products->first();
        return $product->listProduct->name_all;
    }
    public function getProductNameForSite($code = true){
        /** @var AidRequestProduct $product */
        $product = $this->products->first();
        if($code){
            return $product->listProduct->code .' - '. $product->listProduct->name_all;
        }
        return $product->listProduct->name_all;
    }

    public function numberSystem():string
    {
        $number = '';
        if(($this->organisation and $this->organisation->isDonor()) or (!$this->organisation and \Organisation::hasLogin() and \Organisation::isDonor())){
            $number = '1';
        }
        if(($this->organisation and $this->organisation->isRecipient()) or (!$this->organisation and \Organisation::hasLogin() and \Organisation::isRecipient())){
            $number = '2';
        }
        $id = $this->id ?? AidRequest::max('id')+1;

        if($id >= 10000 and $id < 100000){
            $number .= '000'.$id;
        }
        if($id >= 1000 and $id < 10000){
            $number .= '0000'.$id;
        }
        if($id >= 100 and $id < 1000){
            $number .= '00000'.$id;
        }
        if($id >= 10 and $id < 100){
            $number .= '000000'.$id;
        }
        if($id >= 1 and $id < 10){
            $number .= '0000000'.$id;
        }
        return $number;

    }
}
