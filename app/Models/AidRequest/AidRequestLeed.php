<?php

namespace App\Models\AidRequest;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Notifications\Notifiable;

/**
 * App\Models\AidRequest\AidRequestLeed
 *
 * @property int $id
 * @property int $aid_request_id
 * @property string $name
 * @property string $email
 * @property string $message
 * @property string $status
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\AidRequest\AidRequest|null $aidRequest
 * @method static \Illuminate\Database\Eloquent\Builder|AidRequestLeed newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|AidRequestLeed newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|AidRequestLeed query()
 * @method static \Illuminate\Database\Eloquent\Builder|AidRequestLeed whereAidRequestId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AidRequestLeed whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AidRequestLeed whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AidRequestLeed whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AidRequestLeed whereMessage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AidRequestLeed whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AidRequestLeed whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AidRequestLeed whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class AidRequestLeed extends Model
{
    use HasFactory;
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'aid_request_id',
        'name',
        'name_organisation',
        'email',
        'message',
        'status',
    ];

    CONST NEW = 'new';
    CONST APPROVE = 'approve';
    CONST REJECTED = 'rejected';

    public function aidRequest(): HasOne
    {
        return $this->hasOne(AidRequest::class, 'id', 'aid_request_id');
    }


    public function isNew():bool
    {
        return $this->status == self::NEW;
    }

    public function isApprove():bool
    {
        return $this->status == self::APPROVE;
    }

    public function isRejected():bool
    {
        return $this->status == self::REJECTED;
    }
}
