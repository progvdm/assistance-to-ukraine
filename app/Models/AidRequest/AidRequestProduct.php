<?php

namespace App\Models\AidRequest;

use App\Models\Lists\ListProduct;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;

/**
 * App\Models\AidRequest\AidRequestProduct
 *
 * @property int $id
 * @property int $list_product_id
 * @property string|null $type
 * @property int|null $quantity
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|AidRequestProduct newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|AidRequestProduct newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|AidRequestProduct query()
 * @method static \Illuminate\Database\Eloquent\Builder|AidRequestProduct whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AidRequestProduct whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AidRequestProduct whereListProductId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AidRequestProduct whereQuantity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AidRequestProduct whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AidRequestProduct whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property int $aid_request_id
 * @method static \Illuminate\Database\Eloquent\Builder|AidRequestProduct whereAidRequestId($value)
 * @property-read ListProduct|null $listProduct
 */
class AidRequestProduct extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'type',
        'quantity',
    ];

    public function listProduct(): HasOne
    {
        return $this->hasOne(ListProduct::class, 'id', 'list_product_id');
    }
}
