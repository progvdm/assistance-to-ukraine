<?php

namespace App\Models\AidRequest;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\AidRequest\AidRequestTranslation
 *
 * @property int $id
 * @property int $aid_request_id
 * @property string $locale
 * @property string|null $description
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|AidRequestTranslation newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|AidRequestTranslation newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|AidRequestTranslation query()
 * @method static \Illuminate\Database\Eloquent\Builder|AidRequestTranslation whereAidRequestId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AidRequestTranslation whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AidRequestTranslation whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AidRequestTranslation whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AidRequestTranslation whereLocale($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AidRequestTranslation whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class AidRequestTranslation extends Model
{
    use HasFactory;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'aid_request_id',
        'name',
        'description',
        'locale',
    ];
}
