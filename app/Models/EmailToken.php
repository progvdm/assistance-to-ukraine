<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\EmailToken
 *
 * @property string $email
 * @property string $token
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|EmailToken newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|EmailToken newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|EmailToken query()
 * @method static \Illuminate\Database\Eloquent\Builder|EmailToken whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|EmailToken whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|EmailToken whereToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|EmailToken whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class EmailToken extends Model
{
    use HasFactory;

    public $fillable = [
      'email',
      'token',
    ];
}
