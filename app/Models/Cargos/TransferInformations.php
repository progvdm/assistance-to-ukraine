<?php

namespace App\Models\Cargos;

use App\Models\Transfers\Transfer;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * App\Models\Cargos\TransferInformations
 *
 * @property mixed $transfer_id
 * @property string $type
 * @property string $status
 * @property string $comment
 * @property string $photo
 * @property string $geo_mark
 * @property string $data
 * @method static whereId(mixed $id)
 * @property int $id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read Transfer $transfer
 * @method static \Illuminate\Database\Eloquent\Builder|TransferInformations newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|TransferInformations newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|TransferInformations query()
 * @method static \Illuminate\Database\Eloquent\Builder|TransferInformations whereComment($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TransferInformations whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TransferInformations whereData($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TransferInformations whereGeoMark($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TransferInformations wherePhoto($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TransferInformations whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TransferInformations whereTransferId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TransferInformations whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TransferInformations whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class TransferInformations extends Model
{
    const TYPE_SITE = 'site';
    const TYPE_APP = 'app';
    const TYPE_KEEEX = 'keeex';
    protected $fillable = [
        'status',
        'transfer_id',
        'type',
        'comment',
        'photo',
        'geo_mark',
        'data',
    ];

    public function transfer(): BelongsTo
    {
        return $this->belongsTo(Transfer::class);
    }
}
