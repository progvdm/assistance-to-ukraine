<?php

namespace App\Models\WhatIsDone;


use App\ExtendPackage\Translatable;
use App\Orchid\Presenters\UserPresenter;
use Illuminate\Database\Eloquent\Model;
use Orchid\Attachment\Models\Attachment;


/**
 * App\Models\WhatIsDone\WhatIsDone
 *
 * @property bool $published
 * @property int $id
 * @property string $slug
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|Attachment[] $images
 * @property-read int|null $images_count
 * @property-read \App\Models\WhatIsDone\WhatIsDoneTranslation|null $translation
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\WhatIsDone\WhatIsDoneTranslation[] $translations
 * @property-read int|null $translations_count
 * @method static \Illuminate\Database\Eloquent\Builder|WhatIsDone orWhereTranslation(string $translationField, $value, ?string $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|WhatIsDone listsTranslations(string $translationField)
 * @method static \Illuminate\Database\Eloquent\Builder|WhatIsDone newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|WhatIsDone newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|WhatIsDone notTranslatedIn(?string $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|WhatIsDone orWhereTranslationLike(string $translationField, $value, ?string $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|WhatIsDone orderByTranslation(string $translationField, string $sortMethod = 'asc')
 * @method static \Illuminate\Database\Eloquent\Builder|WhatIsDone query()
 * @method static \Illuminate\Database\Eloquent\Builder|WhatIsDone translated()
 * @method static \Illuminate\Database\Eloquent\Builder|WhatIsDone translatedIn(?string $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|WhatIsDone whereId(mixed $id)
 * @method static \Illuminate\Database\Eloquent\Builder|WhatIsDone whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|WhatIsDone wherePublished($value)
 * @method static \Illuminate\Database\Eloquent\Builder|WhatIsDone whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|WhatIsDone whereTranslation(string $translationField, $value, ?string $locale = null, string $method = 'whereHas', string $operator = '=')
 * @method static \Illuminate\Database\Eloquent\Builder|WhatIsDone whereTranslationLike(string $translationField, $value, ?string $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|WhatIsDone whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|WhatIsDone withTranslation()
 * @mixin \Eloquent
 */
class WhatIsDone extends Model
{
    use Translatable;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['published', 'slug'];

    /**
     * Names of the fields being translated in the "Translation" model.
     *
     * @var array
     */
    protected array $translatedAttributes = ['name', 'country', 'h1', 'text', 'text_short', 'title', 'description', 'keywords'];

    /**
     * The relations to eager load on every query.
     *
     * @var array
     */
    protected $with = ['translations'];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = ['published' => 'bool'];

    /**
     * @return UserPresenter
     */
    public function presenter()
    {
        return new UserPresenter($this);
    }
    public function getContent($field){
        return $this->{$field};
    }

    public function images()
    {
        return $this->belongsToMany(Attachment::class, 'attachmentable', 'attachmentable_id', 'attachment_id');
    }
}
