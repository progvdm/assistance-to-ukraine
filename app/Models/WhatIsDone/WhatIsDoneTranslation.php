<?php

namespace App\Models\WhatIsDone;


use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\WhatIsDone\WhatIsDoneTranslation
 *
 * @property integer $id
 * @property int $what_is_done_id
 * @property string $locale
 * @property string $name
 * @property string|null $country
 * @property string|null $text_short
 * @property string|null $text
 * @property string $h1
 * @property string|null $title
 * @property string|null $description
 * @property string|null $keywords
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|WhatIsDoneTranslation newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|WhatIsDoneTranslation newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|WhatIsDoneTranslation query()
 * @method static \Illuminate\Database\Eloquent\Builder|WhatIsDoneTranslation whereId(mixed $id)
 * @method static \Illuminate\Database\Eloquent\Builder|WhatIsDoneTranslation whereCountry($value)
 * @method static \Illuminate\Database\Eloquent\Builder|WhatIsDoneTranslation whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|WhatIsDoneTranslation whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|WhatIsDoneTranslation whereH1($value)
 * @method static \Illuminate\Database\Eloquent\Builder|WhatIsDoneTranslation whereKeywords($value)
 * @method static \Illuminate\Database\Eloquent\Builder|WhatIsDoneTranslation whereLocale($value)
 * @method static \Illuminate\Database\Eloquent\Builder|WhatIsDoneTranslation whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|WhatIsDoneTranslation whereText($value)
 * @method static \Illuminate\Database\Eloquent\Builder|WhatIsDoneTranslation whereTextShort($value)
 * @method static \Illuminate\Database\Eloquent\Builder|WhatIsDoneTranslation whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|WhatIsDoneTranslation whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|WhatIsDoneTranslation whereWhatIsDoneId($value)
 * @mixin \Eloquent
 */
class WhatIsDoneTranslation extends Model
{
    protected $fillable = ['what_is_done_id', 'name', 'country', 'h1', 'text', 'text_short', 'title', 'description', 'keywords'];


}
