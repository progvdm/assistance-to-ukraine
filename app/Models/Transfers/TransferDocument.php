<?php

namespace App\Models\Transfers;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Facades\File;

/**
 * App\Models\Transfers\TransferDocument
 *
 * @property int|Transfer $transfer_id
 * @property int|User $user_id
 * @property mixed $type
 * @property mixed $document
 * @property int $id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|TransferDocument newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|TransferDocument newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|TransferDocument query()
 * @method static \Illuminate\Database\Eloquent\Builder|TransferDocument whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TransferDocument whereDocument($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TransferDocument whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TransferDocument whereTransferId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TransferDocument whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TransferDocument whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TransferDocument whereUserId($value)
 * @mixin \Eloquent
 * @property-read \App\Models\Transfers\Transfer $transfer
 */
class TransferDocument extends Model
{
    use HasFactory;

    protected $fillable = [
        'transfer_id',
        'user_id',
        'type',
        'document'
    ];

    public static function fileSettings()
    {
        return [
            'document' => [
                'path' => 'app/public/',
                'directory' => ''
            ]
        ];
    }
    public function getName() : string
    {
        return str_replace('documents/', '', $this->document);
    }
    public function getUrl() :string
    {
        return asset('storage/acts/'.$this->document);
    }

    public function transfer(): BelongsTo
    {
        return $this->belongsTo(Transfer::class);
    }

    public function deleteFile(){
        if(File::exists($this->getUrl())){
            File::delete($this->getUrl());
        }
    }
}
