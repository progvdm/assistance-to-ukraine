<?php

namespace App\Models\Transfers;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Transfers\TransferVideo
 *
 * @property int|mixed $transfer_id
 * @property mixed $video
 * @method static \Illuminate\Database\Eloquent\Builder|TransferVideo newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|TransferVideo newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|TransferVideo query()
 * @mixin \Eloquent
 */
class TransferVideo extends Model
{
    use HasFactory;

    protected $fillable = [
        'transfer_id',
        'video'
    ];
    public function getName() : string
    {
        return str_replace('videos/', '', $this->video);
    }
    public function getUrl() :string
    {
        return asset('storage/videos/'.$this->video);
    }
}
