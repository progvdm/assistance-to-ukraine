<?php

namespace App\Models\Transfers;

use App\Enums\Transfer\TransferTypeEnum;
use App\Models\Cargos\Cargo;
use App\Models\Cargos\TransferInformations;
use App\Models\Humans\Human;
use App\Models\Products\Product;
use App\Models\Organisation\Organisation as OrganisationModel;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Organisation;
use phpDocumentor\Reflection\Types\Collection;
use function Symfony\Component\Translation\t;

/**
 * App\Models\Transfers\Transfer
 *
 * @property mixed $cargo_id
 * @property mixed|string $type
 * @property mixed $sender_id
 * @property mixed $recipient_id
 * @property mixed $id
 * @property mixed $parent
 * @property mixed|string $additional_info
 * @property int|mixed $human_id
 * @property bool $signed_sender
 * @property bool $signed_recipient
 * @property string $status
 * @property bool $status_list
 * @property string $keeex_url
 * @property OrganisationModel $sender
 * @property OrganisationModel $recipient
 * @property Collection $documents
 * @property \Illuminate\Database\Eloquent\Collection| TransferInformations[] $transferGeo
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property mixed|null $info
 * @property-read \Illuminate\Database\Eloquent\Collection|Transfer[] $child
 * @property-read int|null $child_count
 * @property-read int|null $documents_count
 * @property-read int|null $transferGeo_count
 * @property-read Human|null $human
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Transfers\TransferProduct[] $products
 * @property-read int|null $products_count
 * @property-read \Illuminate\Database\Eloquent\Collection|Product[] $productsStart
 * @property-read int|null $products_start_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Transfers\TransferFiles[] $transferFiles
 * @property-read int|null $transfer_files_count
 * @method static \Illuminate\Database\Eloquent\Builder|Transfer newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Transfer newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Transfer query()
 * @method static \Illuminate\Database\Eloquent\Builder|Transfer whereAdditionalInfo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Transfer whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Transfer whereHumanId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Transfer whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Transfer whereInfo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Transfer whereKeeexUrl($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Transfer whereParent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Transfer whereRecipientId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Transfer whereSenderId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Transfer whereSignedRecipient($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Transfer whereSignedSender($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Transfer whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Transfer whereStatusList($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Transfer whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Transfer whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property-read int|null $transfer_geo_count
 */
class Transfer extends Model
{
    use HasFactory;

    protected $fillable = [
      'sender_id',
      'recipient_id',
      'type',
      'patent',
      'additional_info',
      'human_id',
      'info',
      'signed_sender',
      'signed_recipient',
      'status',
      'status_list',
      'keeex_url',
    ];

    public function recipient(): BelongsTo
    {
        return $this->belongsTo(\App\Models\Organisation\Organisation::class, 'recipient_id', 'id');
    }

    public function sender(): BelongsTo
    {
        return $this->belongsTo(\App\Models\Organisation\Organisation::class, 'sender_id', 'id');
    }

    public function products(): HasMany
    {
        return $this->hasMany(TransferProduct::class);
    }
    public function productsStart(): HasMany
    {
        return $this->hasMany(Product::class, 'transfer_start_id', 'id');
    }
    public function transferFiles(): HasMany
    {
        return $this->hasMany(TransferFiles::class);
    }
    public function transferGeo(): HasMany
    {
        return $this->hasMany(TransferInformations::class);
    }

    public function documents(): HasMany
    {
        return $this->hasMany(TransferDocument::class);
    }

    public function human(): BelongsTo
    {
        return $this->belongsTo(Human::class);
    }

    public function child(): HasMany
    {
        return $this->hasMany(Transfer::class, 'id', 'parent');
    }

    public static function new(
        int $sender, int $cargoId, int $recipient, ?int $parent = null, string $type, ?int $humanId = null, ?string $nameDonor = '', ?array $additionalInfo = []
    ): Transfer
    {
        $transfer = new Transfer();
        $transfer->sender_id = $sender;
        $transfer->recipient_id = $recipient;
        $transfer->cargo_id = $cargoId;
        $transfer->human_id = $humanId;
        $transfer->parent = $parent;
        $transfer->type = $type;
        $transfer->name_donor = $nameDonor;
        $transfer->additional_info = json_encode($additionalInfo);
        $transfer->save();

        return $transfer;
    }

    public function getAdditionalInfo(){
        if(is_array($this->additional_info)){
            return $this->additional_info;
        }
        return (array)json_decode($this->additional_info) ?? [];
    }

    public function getFirstImage()
    {
        return $this->photos->first() ? 'storage/images/' . $this->photos->first()->photo : '/images/noimage.jpg';
    }
    public function getImages()
    {
        return $this->transferFiles()->whereType(TransferFiles::IMAGE)->get();
    }
    public function getImagesApi()
    {
        return $this->transferFiles()->whereType(TransferFiles::IMAGE)->where('file', 'LIKE', 'api_%')->get();
    }
    public function getVideos()
    {
        return $this->transferFiles()->whereType(TransferFiles::VIDEO)->get();
    }
    public function getFiles()
    {
        return $this->transferFiles()->whereType(TransferFiles::FILE)->get();
    }

    public static function getPhotosByCountry($country): array
    {
        $transfers = Transfer::with('photos')
            ->whereHas('sender', function ($query) use ($country){
                $query->where('country', $country);
            })
            ->where('type', '=', TransferTypeEnum::SENDING)
            ->limit(50)
            ->get();

        foreach ($transfers as $transfer){
            foreach ($transfer->photos as $photo) {
                $photos[] =  $photo;
            }
        }

        return $photos ?? [];
    }

    public function dateReceiving()
    {
        $receiving = Transfer::where('parent', $this->id)->first();
        return $receiving ? $receiving->created_at->toDateString() : __('cargo.history.No end date');
    }

    public static function getListTransfer($parent_id = null, $type = ''){
        $organisationId = Organisation::getOrganisationId();
        if(Organisation::isDonor()){
            $query = Transfer::whereSenderId($organisationId);
        }else{
            if($type){
                switch ($type){
                    case 'accepted':
                        $query = Transfer::whereRecipientId($organisationId)->where('sender_id', '!=', $organisationId);
                        break;
                    case 'handed_over':
                        $query = Transfer::whereSenderId($organisationId)->where('recipient_id', '!=', $organisationId);
                        break;
                    case 'provided':
                        $query = Transfer::whereSenderId($organisationId)->whereRecipientId($organisationId)->where('human_id', '!=', null);
                        break;
                }

            }else{
                $query = Transfer::whereRecipientId($organisationId)->where('sender_id', '!=', $organisationId);
            }
        }
          if($parent_id){
              $query->where('parent', $parent_id);
          }
        $query->with(['recipient','recipient.warehouse','sender', 'human', 'products', 'products.product',
                'products.product.category',
                'products.product.category.current']);
        $query->orderByDesc('created_at');
           return  $query->paginate(12);
    }
    public static function getNextTransfer($user_id, $productsAllow){
        return Transfer::where('sender_id', $user_id)
            ->whereHas('products', function ($query) use ($productsAllow){
            $query->whereIn('transfer_product_id', $productsAllow);
        })->with(['recipient','recipient.warehouse','recipient.warehouse.warehouseProducts','sender', 'human', 'products', 'products.product'])->get();
    }

    public static function getFilesByIds($ids, $type)
    {
        $collection = TransferFiles::whereIn('transfer_id', $ids)->whereType($type)->get();
        return $collection;
    }

    public static function fileSettings(): array
    {
        return [
            'document' => [
                'path' => 'app/public/',
                'directory' => 'acts/'
            ]
        ];
    }
}
