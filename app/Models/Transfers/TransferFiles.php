<?php

namespace App\Models\Transfers;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use JetBrains\PhpStorm\ArrayShape;
use App\Traits\ImageAttachable;
use Illuminate\Support\Facades\File;


/**
 * App\Models\Transfers\TransferFiles
 *
 * @property int|mixed $transfer_id
 * @property int|mixed $type
 * @property int|mixed $file
 * @property int $id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|TransferFiles newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|TransferFiles newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|TransferFiles query()
 * @method static \Illuminate\Database\Eloquent\Builder|TransferFiles whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TransferFiles whereFile($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TransferFiles whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TransferFiles whereTransferId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TransferFiles whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TransferFiles whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class TransferFiles extends Model
{
    use HasFactory;

    const IMAGE = 'image';
    const FILE = 'file';
    const VIDEO = 'video';

    protected $fillable = [
        'transfer_id',
        'type',
        'file'
    ];

    #[ArrayShape(['photo' => "string[]"])]
    public function fileSettings(): array
    {
        return [
            'photo' => [
                'path' => 'app/public/',
                'directory' => '/'
            ]
        ];
    }

    public function getName() : string
    {
        return $this->file;
    }
    public function getUrl() :string
    {
        $url = '';
        switch ($this->type){
            case TransferFiles::IMAGE :
                $url = asset('storage/images/'.$this->file);
                break;
            case TransferFiles::VIDEO :
                $url = asset('storage/videos/'.$this->file);
                break;
            case TransferFiles::FILE :
                $url = asset('storage/documents/'.$this->file);
                break;

        }
        return $url;
    }
    public function deleteFile(){
        if(File::exists($this->getUrl())){
            File::delete($this->getUrl());
        }
    }
    public function imageSizes(){
        return [];
    }
}
