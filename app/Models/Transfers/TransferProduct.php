<?php

namespace App\Models\Transfers;

use App\Models\Products\Product;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * App\Models\Transfers\TransferProduct
 *
 * @property int|mixed $transfer_id
 * @property mixed $product_id
 * @property mixed $quantity
 * @property int $id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property int|null $transfer_product_id
 * @property-read Product $product
 * @property-read \App\Models\Transfers\Transfer $transfer
 * @method static \Illuminate\Database\Eloquent\Builder|TransferProduct newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|TransferProduct newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|TransferProduct query()
 * @method static \Illuminate\Database\Eloquent\Builder|TransferProduct whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TransferProduct whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TransferProduct whereProductId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TransferProduct whereQuantity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TransferProduct whereTransferId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TransferProduct whereTransferProductId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TransferProduct whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class TransferProduct extends Model
{
    use HasFactory;

    protected $fillable = [
        'transfer_id',
        'transfer_product_id',
        'product_id',
        'quantity',
    ];

    public function product(): BelongsTo
    {
        return $this->belongsTo(Product::class);
    }
    public function transfer(): BelongsTo
    {
        return $this->belongsTo(Transfer::class);
    }
}
