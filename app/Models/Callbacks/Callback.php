<?php

namespace App\Models\Callbacks;


use App\Events\Callback\CallbackEvent;
use Illuminate\Database\Eloquent\Model;


/**
 * App\Models\Callbacks\Callback
 *
 * @property int $id
 * @property string|null $name
 * @property string|null $email
 * @property int $status
 * @property string $text
 * @property string $category
 * @property string|null $file
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|Callback newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Callback newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Callback query()
 * @method static \Illuminate\Database\Eloquent\Builder|Callback whereCategory($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Callback whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Callback whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Callback whereFile($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Callback whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Callback whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Callback whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Callback whereText($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Callback whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Callback extends Model
{
    protected $fillable = [
      'name',
      'email',
      'text',
      'category',
      'file',
      'status',
    ];

    public function notification(){
        $email = env('MAIL_SUPERADMIN');
        if($this->category == __('feedback.Technical support') and env('MAIL_KUSHNARYEV', false)){
            $email = env('MAIL_KUSHNARYEV');
        }
        CallbackEvent::dispatch($this, $email);
    }
}
