<?php

namespace App\Models;

use App\Enums\Transfer\TransferTypeEnum;
use App\Enums\Users\OrganisationTypeEnum;
use App\Models\Cargos\Cargo;
use App\Models\Organisation\Organisation;
use App\Models\Rating\UserRating;
use App\Models\Transfers\Transfer;
use App\Models\Warehouses\Warehouse;
use App\Rules\Auth\EmailUniqueRule;
use App\Traits\ImageAttachable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use JetBrains\PhpStorm\ArrayShape;
use Laravel\Sanctum\HasApiTokens;

/**
 * App\Models\User
 *
 * @method static create($registerData)
 * @method static where(string $string, mixed $value)
 * @method static whereEmail(mixed $value)
 * @property mixed $type_user
 * @property mixed $id
 * @property mixed $email
 * @property \Illuminate\Support\Carbon|mixed $email_verified_at
 * @property mixed|string $password
 * @property mixed $photo
 * @property mixed $name
 * @property mixed $zip_code
 * @property mixed $id_code
 * @property mixed $country
 * @property mixed $phone
 * @property mixed $notification_phone
 * @property mixed $notification_phone_check
 * @property mixed $additional_phone
 * @property mixed $city
 * @property mixed $street
 * @property mixed $build
 * @property mixed $storage_city
 * @property mixed $storage_street
 * @property mixed $storage_build
 * @property bool $status_view
 * @property Warehouse $warehouse
 * @property string|null $second_name
 * @property string|null $remember_token
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $text
 * @property array|null $verification
 * @property string|null $website
 * @property string|null $additional_email
 * @property string $facebook
 * @property string $instagram
 * @property int|null $number_cells
 * @property int $open_info
 * @property string|null $description_file
 * @property string|null $text_short
 * @property-read \Illuminate\Database\Eloquent\Collection|Cargo[] $cargos
 * @property-read int|null $cargos_count
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read int|null $notifications_count
 * @property-read \Illuminate\Database\Eloquent\Collection|UserRating[] $rating
 * @property-read int|null $rating_count
 * @property-read \Illuminate\Database\Eloquent\Collection|Transfer[] $recipientTransfers
 * @property-read int|null $recipient_transfers_count
 * @property-read \Illuminate\Database\Eloquent\Collection|Transfer[] $senderTransfers
 * @property-read int|null $sender_transfers_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\Laravel\Sanctum\PersonalAccessToken[] $tokens
 * @property-read int|null $tokens_count
 * @method static \Database\Factories\UserFactory factory(...$parameters)
 * @method static \Illuminate\Database\Eloquent\Builder|User newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|User newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|User query()
 * @method static \Illuminate\Database\Eloquent\Builder|User whereAdditionalEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereAdditionalPhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereBuild($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereCity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereCountry($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereDescriptionFile($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereEmailVerifiedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereFacebook($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereIdCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereInstagram($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereNameOrganization($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereNumberCells($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereOpenInfo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User wherePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User wherePhoto($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereRememberToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereSecondName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereStreet($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereText($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereTextShort($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereTypeUser($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereVerification($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereWebsite($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereZipCode($value)
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Eloquent\Builder|User whereNotificationPhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereNotificationPhoneCheck($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereStatusView($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereStorageBuild($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereStorageCity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereStorageStreet($value)
 * @property int|null $organisation_id
 * @property string $name_organization
 * @property-read Organisation|null $organisation
 * @method static \Illuminate\Database\Eloquent\Builder|User whereOrganisationId($value)
 * @property int|null $telegram_id
 * @property string|null $telegram_user_name
 * @method static \Illuminate\Database\Eloquent\Builder|User whereTelegramId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereTelegramUserName($value)
 */
class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable, ImageAttachable, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'zip_code',
        'id_code',
        'country',
        'city',
        'street',
        'build',
        'storage_city',
        'storage_street',
        'storage_build',
        'type_user',
        'photo',
        'text',
        'text_short',
        'verification',
        'phone',
        'notification_phone',
        'notification_phone_check',
        'additional_phone',
        'website',
        'additional_email',
        'facebook',
        'instagram',
        'number_cells',
        'status_view',
        'organisation_id',
        'telegram_id',
        'telegram_user_name',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'verification' => 'array',
    ];

    public string $field = 'photo';

    public function organisation(): HasOne
    {
        return $this->hasOne(Organisation::class, 'id', 'organisation_id');
    }


    public function recipientTransfers(): HasMany
    {
        return $this->hasMany(Transfer::class, 'recipient_id', 'id');
    }

    public function sms(): HasMany
    {
        return $this->hasMany(Sms::class, 'user_id', 'id');
    }

    public function senderTransfers(): HasMany
    {
        return $this->hasMany(Transfer::class, 'sender_id', 'id');
    }

    public function rating(): HasMany
    {
        return $this->hasMany(UserRating::class);
    }

    public function routeNotificationForMail()
    {
        //Phone Number without symbols or spaces
        return $this->email;
    }
    public function routeNotificationForMobizon()
    {
        $phone = $this->getPhoneNotification();
        return phone($phone, 'UA')->formatE164();
    }
    public function routeNotificationForTelegram()
    {
        return $this->telegram_id;
    }

    public function getPhoneNotification(){
        return $this->{$this->notification_phone_check} ?? '';
    }

    public function getAddressToDocument(){
        if($this->storage_city and $this->storage_build and $this->storage_street){
            return $this->country .', '. $this->storage_city .', '. $this->storage_street .', '. $this->storage_build;
        }
        return $this->country .', '. $this->city .', '. $this->street .', '. $this->build;

    }

    public function getLastSms(){
        return $this->sms ? $this->sms->last() : null;
    }

    public function generateTelegramToken(): string
    {
        return $this->id .'-'.$this->created_at->timestamp;
    }

    public function getVerification(){
        if($this->verification == null){
            return [
                'sanction' => 1,
                'founderRu' => 1,
                'criminalCourts' => 0,
            ];
        }
        return (array)$this->verification;
    }

    public function getGrade()
    {
        $rating = $this->rating;
        if(!$count = $rating->count()){
            return 0;
        }

        $sum = 0;
        foreach ($rating as $grade)
        {
            $sum += $grade->grade;
        }

        return $sum / $count;
    }

    #[ArrayShape(['photo' => "string[]"])]
    public function fileSettings(): array
    {
        return [
            'photo' => [
                'path' => 'app/public/',
                'directory' => 'user_images/'
            ]
        ];
    }

    public function imageSizes(): array
    {
        return [];
    }
}
