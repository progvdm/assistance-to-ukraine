<?php

namespace App\NotificationChannels;

use App\Services\Telegram\TelegramBotService;
use Illuminate\Notifications\Notification;

class TelegramChannel
{
    private $telegramApi;

    public function __construct()
    {
        $this->telegramApi = new TelegramBotService();

    }

    public function send($notifiable, Notification $notification)
    {
        $message = $notification->toTelegram($notifiable);
        $to = $notifiable->routeNotificationFor('telegram');
        if($to){
            $this->telegramApi->sendMessage($to, $message);
        }
    }
}
