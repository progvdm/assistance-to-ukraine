<?php

namespace App\NotificationChannels;

use App\Services\Telegram\TelegramBotService;
use Illuminate\Notifications\Notification;

class TelegramDebugChannel
{
    private $telegramApi;

    public function __construct()
    {
        $this->telegramApi = new TelegramBotService();

    }

    public function send($notifiable, Notification $notification)
    {
        $message = $notification->toTelegramDebug($notifiable);

        $this->telegramApi->sendMessageDebugSMS($notifiable, $message);
    }
}
