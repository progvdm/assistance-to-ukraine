<?php

namespace App\NotificationChannels;

use Illuminate\Notifications\Notification;
use Laraketai\Mobizon\Exceptions\CouldNotSendNotification;
use Laraketai\Mobizon\MobizonApi;
use Laraketai\Mobizon\MobizonMessage;

class MobizonChannel
{
    /**
     * @var MobizonApi $mobizonApi
     */
    protected $mobizonApi;
    protected $config;
    protected $apiUrl = 'api.mobizon.ua';
    /**
     * MobizonChanel constructor.
     *
     * @param MobizonApi $mobizonApi
     */
    public function __construct(MobizonApi $mobizonApi)
    {
        $this->mobizonApi = $mobizonApi;
        $this->config = config('mobizon');
        $this->mobizonApi->__set('apiServer', $this->apiUrl);
    }
    /**
     * Send the given notification.
     *
     * @param $notifiable
     * @param Notification $notification
     * @throws CouldNotSendNotification
     * @throws \Laraketai\Mobizon\Mobizon_Http_Error
     * @throws \Laraketai\Mobizon\Mobizon_Param_Required
     */
    public function send($notifiable, Notification $notification)
    {
        $to = $notifiable->routeNotificationFor('mobizon');
        if (empty($to)) {
            throw CouldNotSendNotification::missingRecipient();
        }
        $message = $notification->toMobizon($notifiable);
        if (is_string($message)) {
            $message = new MobizonMessage($message);
        }
        $message->alphaname($this->config['alphaname']);
        $this->sendMessage($to, $message);
    }
    /**
     * @param $recipient
     * @param MobizonMessage $message
     * @throws CouldNotSendNotification
     * @throws \Laraketai\Mobizon\Mobizon_Http_Error
     * @throws \Laraketai\Mobizon\Mobizon_Param_Required
     */
    protected function sendMessage($recipient, MobizonMessage $message)
    {
        if (mb_strlen($message->content) > 800) {
            throw CouldNotSendNotification::contentLengthLimitExceeded();
        }
        $params = [
            'recipient' => $recipient,
            'text' => $message->content,
            //'from' => $message->alphaname, //Optional
        ];
        if(!$this->mobizonApi->call('message', 'sendSMSMessage', $params)){
            logger()->error($this->mobizonApi->getMessage());
        }
    }
}
