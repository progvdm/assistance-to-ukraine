<?php

namespace App\Http\Controllers\Page;

use App\Enums\Users\UserTypeEnum;
use App\Http\Controllers\SiteController;
use App\Models\Pages\SystemPage;
use App\Models\User;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Support\Arr;
use Seo;

class PageController extends SiteController
{
    public function __invoke($page): Factory|View|Application
    {
        $page = SystemPage::wherePublished(1)->findOrFail($page);
        return view('pages.page.page');
    }
}
