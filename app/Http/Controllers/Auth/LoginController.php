<?php

namespace App\Http\Controllers\Auth;

use App\Foundation\FlashMessage;
use App\Http\Controllers\SiteController;
use App\Models\Organisation\Organisation;
use Illuminate\Support\Facades\Auth;


class LoginController extends SiteController
{
    public function __invoke($id)
    {
        $organisation = Organisation::findOrFail($id);
        if($organisation->founder->hasVerifiedEmail()){
            Auth::guard('web')->login($organisation->founder);
            request()->session()->regenerate();
            FlashMessage::setMessage(FlashMessage::SUCCESS, 'Успіх');
            return redirect(routeLocale('cabinet.home'));
        }
        FlashMessage::setMessage(FlashMessage::ERROR, 'Користувач не підтвердив пошту');
        return redirect(routeLocale('home'));
    }
}
