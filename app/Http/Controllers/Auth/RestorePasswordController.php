<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\SiteController;
use App\Services\Auth\RestorePasswordService;
use App\Services\Mail\EmailTokenService;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Routing\Redirector;

class RestorePasswordController extends SiteController
{
    public function __invoke($token): View|Factory|Redirector|Application|RedirectResponse
    {
        $restoreService = new EmailTokenService();
        if($userId = $restoreService->restore($token)){
            return view('pages.auth.restore', ['userId' => $userId]);
        }

        return redirect(route('home'));
    }
}
