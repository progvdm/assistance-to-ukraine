<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\SiteController;
use Illuminate\Support\Facades\Auth;

class LogoutController extends SiteController
{
    public function __invoke()
    {
        if(Auth::guard()->check()){
            Auth::guard()->logout();
        }
        return redirect()->back();
    }
}
