<?php

namespace App\Http\Controllers;

use Seo;

class SiteController extends Controller
{
    public function __construct()
    {
        Seo::breadcrumbs()->add(__('dashboard.Dashboard'), 'cabinet.home');
    }

}
