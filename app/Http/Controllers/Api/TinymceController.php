<?php

namespace App\Http\Controllers\Api;


use App\Enums\Cargo\CargoStatusesEnum;
use App\Models\Cargos\TransferInformations;
use App\Models\Transfers\Transfer;
use App\Services\Gps\GpsService;
use App\Services\Transfer\PhotoService;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class TinymceController
{

    public function photoUpload(){
        if(\request()->hasFile('file')){
            $file = \request()->file('file');
            $fileName = \request()->file('file')->getClientOriginalName();
            $file->move(public_path() . '/storage/tinymce/user/images/',$fileName);
            $location = asset('/storage/tinymce/user/images/'. $fileName);
        }
        return response()->json(['location' => $location])->header('Content-Type', 'application/json');
    }
    public function photoUploadAdmin(){
        if(\request()->hasFile('file')){
            $file = \request()->file('file');
            $fileName = \request()->file('file')->getClientOriginalName();
            $file->move(public_path() . '/storage/tinymce/site/images/',$fileName);
            $location = asset('/storage/tinymce/site/images/'. $fileName);
        }
        return response()->json(['location' => $location])->header('Content-Type', 'application/json');
    }

}
