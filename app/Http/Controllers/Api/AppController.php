<?php

namespace App\Http\Controllers\Api;


use App\Enums\Cargo\CargoStatusesEnum;
use App\Models\Cargos\TransferInformations;
use App\Models\Transfers\Transfer;
use App\Services\Gps\GpsService;
use App\Services\Transfer\PhotoService;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class AppController
{

    public function scanQrCode(Request $request)
    {
        if($request->has('lon') and $request->has('lat') and $request->has('code') ){
            $code = $request->get('code');
            $code = explode('/', $code);
            $transferId = end( $code);
            $transfer = Transfer::find($transferId);
            if ($transfer and $request->get('lon') and $request->get('lat')) {
                $findGeo = TransferInformations::where('created_at', '>', Carbon::now()->subMinutes(5))->whereTransferId($transfer->id)->first();
                if(!$findGeo){
                    $location = new TransferInformations();
                    $location->transfer_id = $transfer->id;
                    $location->type = TransferInformations::TYPE_SITE;
                    $location->geo_mark = $request->get('lat') . ',' . $request->get('lon');
                    $location->save();
                }
                /*$nameCityRecipient = $transfer->recipient->city;
                if($this->getCityByGps($request->get('lat'), $request->get('lon'), $nameCityRecipient)){
                    $transfer->status = CargoStatusesEnum::ARRIVED_IN_THE_CITY;
                    $transfer->save();
                }*/

                $documentModel = $transfer->documents->first();

                return response()->json(['success' => true, 'idModel' => $transferId,  'document' => asset('storage/acts/' . $documentModel->document ?? '')])->header('Content-Type', 'application/json');
            }
            return response()->json(['status' => 'error', 'message'=>'Не вдалось знайти дані!', 'document'=> null])->header('Content-Type', 'application/json');
        }
        return response()->json(['status' => 'error', 'message'=>'Не всі дані передано!'])->header('Content-Type', 'application/json');
    }

    public function uploadPhoto(Request $request)
    {
        if($request->get('idModel') and $request->get('base64')){
            $transfer = Transfer::find($request->get('idModel'));
            if($transfer){
                $photoService = new PhotoService();
                $photoService->uploadBase64($request->get('base64'), $transfer->id);
            }
        }
        return response()->json(['success' => true])->header('Content-Type', 'application/json');
    }

    private function getCityByGps($lat, $lon, $city){
        $service = new GpsService();
        $gpsCity = $service->getCityByGeo($lat, $lon);
        if(Str::slug($gpsCity) == Str::slug($city)){
            return true;
        }

        return false;
    }

}
