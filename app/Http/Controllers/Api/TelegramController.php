<?php

namespace App\Http\Controllers\Api;


use App\Models\Sms;
use App\Models\User;
use Carbon\Carbon;
use Log;

class TelegramController
{


    public function telegram(){
        $bot = new \TelegramBot\Api\Client(env('TELEGRAM_BOT_TOKEN'));

        $this->Logging('Post Telegram DATA', request()->all());

        $bot->command('start', function ($message) use ($bot) {
            $chatId = $message->getChat()->getId();
            $user = User::where('telegram_id', $chatId)->first();

            $this->Logging('/start', ['userId' => $user ? $user->id : null, 'chatId' => $chatId ]);

            if($user){
                $bot->sendMessage($chatId, __('Доброго дня :name. Наразі немає повідомлень.', ['name' => $user->name]));
            }else{

                $msg = explode(' ', $message->getText());
                $this->Logging('/start msg', ['msg' => $msg ]);
                if(!isset($msg[1])){
                    $bot->sendMessage($chatId, "Доброго дня! \nВкажіть пошту на яку зареэстрована ваша організація в нашій системі.");
                }else{
                    $msg = $msg[1];
                    $userId = explode('-', $msg)[0];
                    $userTimestamp = explode('-', $msg)[1];
                    if(!$userId or !$userTimestamp){
                        $bot->sendMessage($chatId, "Доброго дня! \nВкажіть пошту на яку зареэстрована ваша організація в нашій системі.");
                    }else{
                        /** @var User $user */
                        $user = User::whereId($userId)->where('created_at', date('Y-m-d H:i:s', $userTimestamp))->first();
                        if($user){

                            $user->telegram_id = $chatId;
                            if($username = $message->getChat()->getUsername()){
                                $user->telegram_user_name = $username;
                            }
                            $user->save();
                            $this->Logging('/start| User true', ['user' => $user, 'chatId' => $chatId ]);
                            $bot->sendMessage($chatId, __('Вітаю :name. Ви успішно підписалися на повідомлення.', ['name'=> $user->name]));
                            $this->sendLatestNotifySMSCCode($user);
                        }else{
                            $bot->sendMessage($chatId, "Доброго дня! \nВкажіть пошту на яку зареэстрована ваша організація в нашій системі.");
                        }
                    }
                }
            }
        });

        $bot->on(function (\TelegramBot\Api\Types\Update $update) use ($bot) {
            $message = $update->getMessage();
            if($message->getChat()){
                $chatId = $message->getChat()->getId();
                $user = User::where('telegram_id', $chatId)->first();
                if ($user) {
                    $bot->sendMessage($chatId, __('Доброго дня :name. Наразі немає повідомлень.', ['name' => $user->name]));
                } else {
                    /** @var User $user */
                    $user = User::whereEmail($message->getText())->first();
                    if ($user) {
                        $user->telegram_id = $chatId;
                        $user->telegram_user_name = $message->getChat()->getUsername();
                        $user->save();
                        $bot->sendMessage($chatId, __('Вітаю :name. Ви успішно підписалися на повідомлення.', ['name'=> $user->name]));
                        $this->sendLatestNotifySMSCCode($user);
                    } else {
                        $bot->sendMessage($chatId, __('Користувача не знайдено! Спробуйте ще раз:'));
                    }

                }
            }

        }, function () {
            //return true;
        });

        $bot->run();
        return response()->json(json_encode(['status' => 'success']));
    }

    public function sendLatestNotifySMSCCode(User $user){
        /** @var Sms $sms */
        $sms = $user->getLastSms();
        if($sms and $sms->status == 0){
            $text = "Останнє повідомлення: \nВаш код для підтвердження: ". $sms->code;
            $user->notify(new \App\Notifications\MessageNotification($text, false));
        }

    }
    private function Logging($message, $data = []){
        Log::build([
            'driver' => 'single',
            'path' => storage_path('logs/Telegram.log'),
        ])->info($message, $data);
    }
}
