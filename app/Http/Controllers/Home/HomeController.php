<?php

namespace App\Http\Controllers\Home;

use Seo;
use App\Http\Controllers\SiteController;
use App\Models\Pages\SystemPage;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;

class HomeController extends SiteController
{
    public function __invoke(): Factory|View|Application
    {
        $page = SystemPage::whereSlug('index')->firstOrFail();
        Seo::meta()->setH1($page->h1);
        Seo::meta()->setTitle($page->title);
        Seo::meta()->setDescription($page->description);
        Seo::meta()->setKeywords($page->keywords);
        return view('pages.home.home');
    }
}
