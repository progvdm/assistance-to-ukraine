<?php

namespace App\Http\Controllers\Pages;

use App\Enums\Users\OrganisationTypeEnum;
use App\Http\Controllers\SiteController;
use App\Models\Pages\SystemPage;
use App\Models\User;
use App\Models\WhatIsDone\WhatIsDone;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Orchid\Attachment\Models\Attachment;
use Seo;

class DocumentsController extends SiteController
{
    private SystemPage $page;


    public function __construct(){
        $this->page = SystemPage::wherePublished(1)->whereSlug('documents')->firstOrFail();
        Seo::breadcrumbs()->add(__('Home'), 'home');
        Seo::breadcrumbs()->add($this->page->name, 'page.documents');

    }
    public function index(): Factory|View|Application
    {
        Seo::meta()->setH1($this->page->h1);
        Seo::meta()->setTitle($this->page->title);
        Seo::meta()->setDescription($this->page->description);
        Seo::meta()->setKeywords($this->page->keywords);

        return view('pages.page.documents', ['page' => $this->page, 'files' => $this->page->getFiles()]);
    }

    public function document(Attachment $pdf){
        Seo::meta()->setTitle($pdf->original_name);
        Seo::meta()->setDescription($pdf->original_name);
        Seo::meta()->setKeywords($this->page->keywords);
        Seo::breadcrumbs()->add($pdf->original_name);

        return view('pages.page.documents-pdf', ['pdf' => $pdf]);

    }
}
