<?php

namespace App\Http\Controllers\Pages;

use App\Http\Controllers\SiteController;
use App\Models\Pages\SystemPage;
use App\Models\WhatIsDone\WhatIsDone;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Seo;

class WhatIsDoneController extends SiteController
{
    private SystemPage $page;


    public function __construct(){
        $this->page = SystemPage::wherePublished(1)->whereSlug('what-is-done')->firstOrFail();
        Seo::breadcrumbs()->add(__('Home'), 'home');
        Seo::breadcrumbs()->add($this->page->name, 'page.whatIsDone');

    }
    public function index(): Factory|View|Application
    {
        Seo::meta()->setH1($this->page->h1);
        Seo::meta()->setTitle($this->page->title);
        Seo::meta()->setDescription($this->page->description);
        Seo::meta()->setKeywords($this->page->keywords);

        $items = WhatIsDone::wherePublished(1)->get();

        return view('pages.page.whatIsDone', ['items' => $items]);
    }

    public function itemView($slug): Factory|View|Application
    {
        /** @var WhatIsDone $page */
        $page = WhatIsDone::wherePublished(1)->whereSlug($slug)->firstOrFail();
        Seo::meta()->setH1($page->h1);
        Seo::meta()->setTitle($page->title);
        Seo::meta()->setDescription($page->description);
        Seo::meta()->setKeywords($page->keywords);
        Seo::breadcrumbs()->add($page->name);
        return view('pages.page.whatIsDonePageItem', ['page' => $page]);
    }
}
