<?php

namespace App\Http\Controllers\Pages;

use App\Enums\Users\OrganisationTypeEnum;
use App\Http\Controllers\SiteController;
use App\Models\Organisation\Organisation;
use App\Models\Pages\SystemPage;
use App\Models\User;
use App\Models\WhatIsDone\WhatIsDone;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Redirect;
use Seo;

class RecipientsController extends SiteController
{
    private SystemPage $page;


    public function __construct(){
        $this->page = SystemPage::wherePublished(1)->whereSlug('recipients')->firstOrFail();
        Seo::breadcrumbs()->add(__('Home'), 'home');
        Seo::breadcrumbs()->add($this->page->name, 'page.recipients');

    }
    public function index(): Factory|View|Application
    {
        Seo::meta()->setH1($this->page->h1);
        Seo::meta()->setTitle($this->page->title);
        Seo::meta()->setDescription($this->page->description);
        Seo::meta()->setKeywords($this->page->keywords);

        return view('pages.page.donors', [
            'type' => OrganisationTypeEnum::RECIPIENT,
            'route' => 'page.recipients.show',
            'h1' => $this->page->h1
        ]);
    }

    public function show(Organisation $organisation)
    {
        if(!$organisation->view_site){
            abort(404);
        }
        if($organisation->type != OrganisationTypeEnum::RECIPIENT){
            return redirect(route('page.donors.show', $organisation));
        }
        Seo::meta()->setH1($organisation->name);
        Seo::meta()->setTitle($this->page->title . ': '. $organisation->name);
        Seo::meta()->setDescription($this->page->description . ': '. $organisation->name);
        Seo::meta()->setKeywords($this->page->keywords);
        Seo::breadcrumbs()->add($organisation->name, '');

        return view('pages.page.userInfo', ['organisation' => $organisation]);
    }

}
