<?php

namespace App\Http\Controllers\Pages;

use App\Enums\Users\OrganisationTypeEnum;
use App\Http\Controllers\SiteController;
use App\Models\Pages\SystemPage;
use App\Models\User;
use App\Models\WhatIsDone\WhatIsDone;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Seo;

class ContactsController extends SiteController
{
    private SystemPage $page;


    public function __construct(){
        $this->page = SystemPage::wherePublished(1)->whereSlug('contacts')->firstOrFail();
        Seo::breadcrumbs()->add(__('Home'), 'home');
        Seo::breadcrumbs()->add($this->page->name);

    }
    public function index(): Factory|View|Application
    {
        Seo::meta()->setH1($this->page->h1);
        Seo::meta()->setTitle($this->page->title);
        Seo::meta()->setDescription($this->page->description);
        Seo::meta()->setKeywords($this->page->keywords);

        return view('pages.page.contacts', ['page' => $this->page]);

    }
}
