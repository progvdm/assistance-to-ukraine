<?php

namespace App\Http\Controllers\Cabinet;

use App\Http\Controllers\SiteController;
use App\Models\Cargos\TransferInformations;
use App\Models\Transfers\Transfer;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Seo;

class GeoLocationsController extends SiteController
{

    public function index(Transfer $transfer): Application|Factory|View|BinaryFileResponse
    {
        Seo::breadcrumbs()->add($transfer->sender->name . ' to ' . $transfer->recipient->name . ' - ' . $transfer->updated_at->format('Y-m-d'));
        Seo::meta()->setTitle($transfer->sender->name . ' to ' . $transfer->recipient->name . ' - ' . $transfer->updated_at->format('Y-m-d'));
        Seo::meta()->setDescription($transfer->sender->name . ' to ' . $transfer->recipient->name . ' - ' . $transfer->updated_at->format('Y-m-d'));
        return view('pages.cargo.locations', ['transfer' => $transfer]);
    }

    public function indexJson(Transfer $transfer)
    {
        $productsAllow = [];
        $transfersAllow = [$transfer->id];
        foreach ($transfer->products as $transferProduct) {
            $productsAllow[$transferProduct->id] = $transferProduct->id;
        }
        $this->getNextTransfer($transfer->recipient_id, $productsAllow, $transfersAllow);

        $mark = TransferInformations::whereIn('transfer_id', array_values($transfersAllow))->get();

        $json = [
            'data' => []
        ];
        foreach ($mark as $key => $item) {
            $index = $key + 1;
            if ($item->type == TransferInformations::TYPE_KEEEX and $item->comment != '') {
                $comment = $item->comment;
            } elseif ($item->type == TransferInformations::TYPE_KEEEX and $item->comment == '') {
                $comment = 'Отримано через сервіс Keeex!';
            } elseif ($item->type == TransferInformations::TYPE_APP) {
                $comment = 'Відскановано qr код через додаток!';
            } else {
                $comment = 'Відскановано qr код!';
            }
            $json['data'][$item->geo_mark] = [
                'name' => $item->created_at->format('Y-m-d'),
                'number' => "$index",
                'lat' => explode(',', $item->geo_mark)[0],
                'lng' => explode(',', $item->geo_mark)[1],
                'comment' => [$comment],
                'images' => [
                    [
                        'link' => '',
                        'text' => '',
                    ]
                ]
            ];
        }
        return response()->json($json)->header('Content-Type', 'application/json');
    }

    private function getNextTransfer($id, $productsAllow, &$transfersAllow)
    {
        $nextTransfers = Transfer::getNextTransfer($id, $productsAllow);
        foreach ($nextTransfers as $nextCargo) {
            $transfersAllow[$nextCargo->id] = $nextCargo->id;
            if ($nextCargo->recipient_id != $nextCargo->sender_id) {
                $productsAllowNext = [];
                foreach ($nextCargo->products as $transferProduct) {
                    $productsAllowNext[$transferProduct->id] = $transferProduct->id;
                }
                $this->getNextTransfer($nextCargo->recipient_id, $productsAllowNext, $transfersAllow);
            }
        }
    }


}
