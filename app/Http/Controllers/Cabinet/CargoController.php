<?php

namespace App\Http\Controllers\Cabinet;

use App\Enums\Cargo\CargoStatusesEnum;
use App\Helper\QrCodeGenerator;
use App\Http\Controllers\SiteController;
use App\Models\Cargos\Cargo;
use App\Models\Cargos\TransferInformations;
use App\Models\Products\Product;
use App\Models\Transfers\Transfer;
use App\Models\Transfers\TransferDocument;
use App\Services\Files\PdfService;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Support\Arr;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Seo;

class CargoController extends SiteController
{

    public function __construct(){
        Seo::breadcrumbs()->add(__('dashboard.Dashboard'), 'cabinet.home');
    }
    public function newShipment(): View|Factory|Application
    {
        \Organisation::isFounderForbidden();
        Seo::breadcrumbs()->add(__('cargo.Provide assistance to individuals'));
        Seo::meta()->setTitle(__('cargo.Provide assistance to individuals'));
        Seo::meta()->setDescription(__('cargo.Provide assistance to individuals'));
        return view('pages.cargo.new-shipment');
    }

    public function newShipmentReceived(): View|Factory|Application
    {
        \Organisation::isFounderForbidden();
        Seo::breadcrumbs()->add(__('cargo.Got new help'));
        Seo::meta()->setTitle(__('cargo.Got new help'));
        Seo::meta()->setDescription(__('cargo.Got new help'));
        return view('pages.cargo.new-shipment-received');
    }

    public function cargo(Transfer $transfer)
    {
        $documentModel = $transfer->documents->first();
        if(auth()->user()){
            return response()->download(filePath(Transfer::fileSettings(), 'document').
                $documentModel->document
            );
        }else{
            return redirect(env('QR_APP_URL', 'https://app.aidmonitor.org'));
        }
    }

    public function accept(Transfer $transfer): Factory|View|Application
    {
        \Organisation::isFounderForbidden();
        Seo::breadcrumbs()->add(__('cargo.Accept cargo'));
        Seo::meta()->setTitle(__('cargo.Accept cargo'));
        Seo::meta()->setDescription(__('cargo.Accept cargo'));
        return view('pages.cargo.accept-cargo', ['transfer' => $transfer]);
    }

    public function approve(Transfer $transfer): Factory|View|Application
    {
        \Organisation::isFounderForbidden();
        Seo::breadcrumbs()->add(__('cargo.Approve'));
        Seo::meta()->setTitle(__('cargo.Approve'));
        Seo::meta()->setDescription(__('cargo.Approve'));
        return view('pages.cargo.approve-cargo', ['transfer' => $transfer]);
    }

    public function donate(?Transfer $transfer): Factory|View|Application
    {
        \Organisation::isFounderForbidden();
        Seo::breadcrumbs()->add(__('cargo.Provide assistance to the organization'));
        Seo::meta()->setTitle(__('cargo.Provide assistance to the organization'));
        Seo::meta()->setDescription(__('cargo.Provide assistance to the organization'));
        return view('pages.cargo.donate-product', ['transfer' => $transfer]);
    }

    public function donateRecipient(): Factory|View|Application
    {
        \Organisation::isFounderForbidden();
        Seo::breadcrumbs()->add(__('cargo.Give help'));
        Seo::meta()->setTitle(__('cargo.Give help'));
        Seo::meta()->setDescription(__('cargo.Give help'));
        return view('pages.cargo.donate-product-recipient');
    }

    public function donatePeople(): Factory|View|Application
    {
        \Organisation::isFounderForbidden();
        Seo::breadcrumbs()->add(__('cargo.Provide assistance to individuals'));
        Seo::meta()->setTitle(__('cargo.Provide assistance to individuals'));
        Seo::meta()->setDescription(__('cargo.Provide assistance to individuals'));
        return view('pages.cargo.donate-product-people');
    }

    public function document(): BinaryFileResponse
    {
        if(request()->has('act')){
            return response()->download(filePath(Transfer::fileSettings(), 'document').
                request()->query('act')
            );
        }else{
            return response()->download(filePath(TransferDocument::fileSettings(), 'document').
                request()->query('document')
            );
        }
        return response()->download(storage_path('app/public/documents/'.request()->query('document') ));
    }

    public function printQrCode(Transfer $transfer){
        $qr = QrCodeGenerator::generate(route('cabinet.cargo', $transfer->id), 200);
        return view('documents.printQrCode', ['qr' => $qr]);
    }

    public function downloadAct(Transfer $transfer): BinaryFileResponse
    {
        $documentModel = $transfer->documents->first();
        return response()->download(filePath(Transfer::fileSettings(), 'document').$documentModel->document);
    }
    public function downloadActAccept(Transfer $transfer): BinaryFileResponse
    {
        $documentModel = $transfer->documents()->whereType(CargoStatusesEnum::DELIVERY_CONFIRMED)->first();
        return response()->download(filePath(Transfer::fileSettings(), 'document').$documentModel->document);
    }

    public function countryPhotos(string $country): Factory|View|Application
    {
        return view('components.pages.what-we-did.photos', [
            'photos' => Transfer::getPhotosByCountry($country)
        ]);
    }

    public function redirectKeeexUrl(Cargo $cargo){
        return redirect($cargo->keeex_url);
    }

    public function signDocument(TransferDocument $transferDocument){
        \Organisation::isFounderForbidden();
        Seo::breadcrumbs()->add(__('cargo.Document signature').' - '. $transferDocument->document);
        Seo::meta()->setTitle(__('cargo.Document signature').' - '. $transferDocument->document);
        Seo::meta()->setDescription(__('cargo.Document signature').' - '. $transferDocument->document);
        if($transferDocument->transfer->sender_id != \Organisation::getOrganisationId()){
            abort(403);
        }
        return view('pages.cargo.sign-document', [
            'transferDocument' => $transferDocument
        ]);
    }

}
