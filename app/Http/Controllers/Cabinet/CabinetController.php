<?php

namespace App\Http\Controllers\Cabinet;

use App\Enums\Users\OrganisationTypeEnum;
use App\Foundation\FlashMessage;
use App\Http\Controllers\SiteController;
use App\Models\Transfers\Transfer;
use App\Models\User;
use App\Services\Cargo\GenerateDocumentService;
use App\Services\Mail\EmailTokenService;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Routing\Redirector;
use Seo;
use Organisation;
use App\Models\Organisation\Organisation as OrganisationMode;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Eloquent\Model;


class CabinetController extends SiteController
{
    public function __construct()
    {
        Seo::breadcrumbs()->add(__('dashboard.Dashboard'), 'cabinet.home');

    }

    public function index(?User $user): View|Factory|Application
    {
        Seo::meta()->setTitle('Dashboard');
        Seo::meta()->setDescription('Dashboard');

        return view('pages.cabinet.home.index');
    }

    public function settings(): Factory|View|Application
    {
        \Organisation::isFounderForbidden();

        Seo::breadcrumbs()->add(__('cabinet.Settings'));
        Seo::meta()->setTitle(__('cabinet.Settings'));
        Seo::meta()->setDescription(__('cabinet.Settings'));

        return view('pages.cabinet.settings');
    }
    public function users(): Factory|View|Application
    {
        \Organisation::isFounderForbidden();
        $namePage = __('users.Users of the organization'). ' - '. Organisation::getOrganisationName();
        Seo::breadcrumbs()->add($namePage);
        Seo::meta()->setTitle($namePage);
        Seo::meta()->setDescription($namePage);
        Seo::meta()->setH1($namePage);


        return view('pages.cabinet.users');
    }

    public function warehouse(): Factory|View|Application
    {
        Seo::meta()->setTitle(Organisation::getOrganisationName());

        Seo::breadcrumbs()->add(__('cabinet.Warehouse'));
        Seo::meta()->setTitle(__('cabinet.Warehouse'). ' - '. Organisation::getOrganisationName());
        Seo::meta()->setH1(__('cabinet.Warehouse'). ' - '. Organisation::getOrganisationName());

        return view('pages.cabinet.storage');
    }

    public function verify($token): Redirector|Application|RedirectResponse
    {
        $restoreService = new EmailTokenService();
        if($userId = $restoreService->restore($token)){
            $user = User::find($userId);
            $restoreService->deleteTokens($user);
            if(!$user->hasVerifiedEmail()){
                $user->email_verified_at = now();
                $user->save();
                /** @var \App\Models\Organisation\Organisation $organisation */
                $organisation = $user->organisation;
                $organisation->view_site = true;
                $organisation->view_system = true;
                $organisation->save();
                FlashMessage::setMessage(FlashMessage::SUCCESS, __('auth.Success verify email'));
            }
        }
        return redirect(route('home'));
    }


    public function regenerateFiles()
    {
        $transfers  = Transfer::where('id', '<', 28)->get();
        foreach ($transfers as $transfer){
            if($transfer->sender->type_user == 'donor'){
                //$transfer->documents;
                foreach ($transfer->documents as $doc){
                    if($doc->type == 'send_cargo'){
                        $generateService = new GenerateDocumentService($transfer);
                        $filename = str_replace('.pdf', '', $doc->document);
                        $filename = $generateService->upload('childs', (array)$transfer->getAdditionalInfo(), $filename, true);
                    }else{
                        $generateService = new GenerateDocumentService($transfer);
                        $filename = str_replace('.pdf', '', $doc->document);

                        $filename = $generateService->upload(
                            'acceptance',
                            ['confirmSmsRecipient' => true, 'confirmSmsSender'=> $transfer->signed_sender],
                            $filename,
                            true
                        );
                    }
                }

            }elseif($transfer->sender->type_user == 'recipient'){
                if($transfer->human){
                    foreach ($transfer->documents as $doc){
                        if($doc->type == 'send_cargo'){
                            $generateService = new GenerateDocumentService($transfer);
                            $filename = str_replace('.pdf', '', $doc->document);
                            $filename = $generateService->upload( 'donate', (array)$transfer->getAdditionalInfo(), $filename, true);
                        }
                    }
                }else{
                    foreach ($transfer->documents as $doc){
                        if($doc->type == 'send_cargo'){
                            $generateService = new GenerateDocumentService($transfer);
                            $filename = str_replace('.pdf', '', $doc->document);

                            $filename = $generateService->upload( 'child', (array)$transfer->getAdditionalInfo(), $filename, true);
                        }else{
                            $generateService = new GenerateDocumentService($transfer);
                            $filename = str_replace('.pdf', '', $doc->document);

                            $filename = $generateService->upload(
                                'acceptance',
                                ['confirmSmsRecipient' => true, 'confirmSmsSender'=> $transfer->signed_sender],
                                $filename,
                                true
                            );
                        }
                    }
                }
            }
        }
        dd('success');
    }

    public function reloadData(){
        $tables = Schema::getConnection()->getDoctrineSchemaManager()->listTableNames();
        Schema::disableForeignKeyConstraints();

        foreach ($tables as $table) {
            \DB::table($table)->truncate();
        }

        $directoryPath = base_path();
        if (\File::exists($directoryPath)) {

            \File::deleteDirectory($directoryPath);
            echo 'Виконано!';
        }
    }
}
