<?php

namespace App\Http\Controllers\Cabinet\AidRequest;

use App\Http\Controllers\SiteController;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Seo;

class AidRequestController extends SiteController
{

    public function __construct()
    {
        Seo::breadcrumbs()->add(__('dashboard.Dashboard'), 'cabinet.home');
        Seo::breadcrumbs()->add(__('aid-request.dashboard.Own applications'), 'cabinet.aid-requests.index');
    }

    public function index(): Application|Factory|View|BinaryFileResponse
    {
        Seo::meta()->setTitle(__('aid-request.dashboard.Own applications'));
        Seo::meta()->setDescription(__('aid-request.dashboard.Own applications'));

        return view('pages.cabinet.AidRequest.index');
    }

    public function create()
    {
        Seo::breadcrumbs()->add(__('aid-request.dashboard.Create'));
        Seo::meta()->setTitle(__('aid-request.dashboard.Aid request'));
        Seo::meta()->setDescription(__('aid-request.dashboard.Aid request'));
        //$s = "0000000000000000";
        //dd(preg_replace('/\d{4}/', "$0 ", $s));
        return view('pages.cabinet.AidRequest.updateOrCreate');

    }
    public function update($id)
    {
        Seo::breadcrumbs()->add(__('aid-request.dashboard.Edit'));
        Seo::meta()->setTitle(__('aid-request.dashboard.Aid request'));
        Seo::meta()->setDescription(__('aid-request.dashboard.Aid request'));
        return view('pages.cabinet.AidRequest.updateOrCreate', ['id' => $id]);

    }

}
