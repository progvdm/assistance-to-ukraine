<?php

namespace App\Http\Livewire\Rating;

use App\Foundation\FlashMessage;
use App\Models\Cargos\Cargo;
use App\Models\Rating\UserRating;
use App\Models\User;
use Livewire\Component;

class Rating extends Component
{
    const MAX = 5;
    const MIN = 1;

    public User $recipient;

    public string $comment = '';
    public int    $grade   = 0;
    public bool   $thanks = false;

    public function mount(Cargo $cargo)
    {
        $this->recipient = $cargo->transfers()->first()->recipient;
        $this->grade = $this->recipient->getGrade();
    }

    public function render()
    {
        return view('livewire.rating.rating');
    }

    public function submit()
    {
        $this->validate(
            $this->rules(),
            $this->messages(),
            []
        );

        $userRating = new UserRating();
        $userRating->user_id = $this->recipient->id;
        $userRating->grade = $this->grade;
        $userRating->comment = $this->comment;
        $userRating->ip = request()->ip();
        $userRating->save();

        $this->thanks = true;
    }

    public function grade($grade)
    {
        $this->grade = $grade;
    }

    public function plus()
    {
        if($this->grade + 1 >= self::MAX)
        {
            $this->grade = self::MAX;
        }else{
            ++$this->grade;
        }
    }

    public function minus()
    {
        if($this->grade - 1 <= self::MIN)
        {
            $this->grade = self::MIN;
        }else{
            --$this->grade;
        }
    }

    public function rules()
    {
        return [
            'comment' => 'nullable|string',
            'grade' => 'required|int|min:0|max:5',
        ];
    }

    public function messages()
    {
        return [
            'grade.required' => __('default.rating.grade required'),
            'grade.int' => __('default.rating.grade int'),
            'grade.min' => __('default.rating.grade minmax'),
            'grade.max' => __('default.rating.grade minmax'),
        ];
    }
}
