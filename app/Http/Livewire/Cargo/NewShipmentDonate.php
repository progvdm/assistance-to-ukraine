<?php

namespace App\Http\Livewire\Cargo;

use App\Enums\Transfer\TransferTypeEnum;
use App\Events\Cargo\CheckEmptyCargo;
use App\Events\Cargo\SendSms;
use App\Models\Cargos\Cargo;
use App\Models\Transfers\Transfer;
use App\Services\Cargo\NewShipmentService;
use App\Services\Cargo\ShipmentService;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Database\Eloquent\Collection as EloquentCollection;
use Livewire\Component;
use Livewire\WithFileUploads;

class NewShipmentDonate extends Component
{
    use WithFileUploads;

    public int|null $transferId = null;
    public Cargo|null $cargo = null;

    public array    $products = [];
    public array    $photos = [];
    public array    $photosSaved = [];
    public array    $videos = [];
    public array    $videosSaved = [];
    public array    $documents = [];
    public array    $documentsSaved = [];
    public int      $recipient = 0;
    public string   $donor_id = '';
    public array    $donors = [];
    public array    $newDonor = [];

    public array    $productTypes = [];
    public array    $recipients = [];
    public array    $formProducts = [];
    public array    $additionalInfo = [];

    public bool     $isDonate   = false;
    public bool     $isDonor    = false;
    public bool     $finish     = false;
    public bool     $isNewDonor = false;
    public ?EloquentCollection $categories = null;
    private ?NewShipmentService $service = null;

    // flash message
    public $message = [];


    public function mount($transferId = null)
    {
        $this->transferId = $transferId;
        $this->initService($transferId);
        $this->newForm();
        $this->service->uploadDataForm($this);
    }

    public function render(): Factory|View|Application
    {
        return view('livewire.cargo.new-shipment-donate');
    }

    protected function initService($transferId){
        if(!$this->service){
            $this->service = new NewShipmentService($transferId);
        }
    }

    public function submit()
    {
        $this->initService($this->transferId);

        $data = $this->validate(
            $this->rules(),
            $this->messages(),
            [],
        );
        $this->service->setProducts($data);

        $transfer = $this->service->createOrUpdate($this, $data, null);

        $this->service->uploadDataForm($this);
        CheckEmptyCargo::dispatch($transfer);

        $this->photos = [];
        $this->videos = [];
        $this->documents = [];
        $this->addPhoto();
        $this->addVideo();
        $this->addDocument();

        unset($this->message['error']);
        $this->message['success'] = __('form.messages.Success');
        $this->emit('urlChange', route('cabinet.donate-product', $transfer));
    }

    public function sendDonate(){
        $this->initService($this->transferId);
        $this->service->sendCargo($this);
        $this->finish = true;
        //$this->newForm();
        $this->message['success'] = __('form.messages.Success');
    }
    public function sendApprove(){
        $this->initService($this->transferId);
        $this->service->sendApprove($this);
        $this->message['success'] = __('form.messages.Success');
    }

    public function newForm()
    {
        $this->formProducts = [];
        $this->photos = [];
        $this->videos = [];
        $this->documents = [];
        $this->additionalInfo = [];
        $this->recipient = 0;

        $this->addProduct();
        $this->addPhoto();
        $this->addVideo();
        $this->addDocument();
    }
    public function updateForm()
    {
        $this->formProducts = [];
        $this->photos = [];
        $this->videos = [];
        $this->documents = [];
        $this->recipient = 0;

        $this->addProduct();
        $this->addPhoto();
        $this->addVideo();
        $this->addDocument();
    }

    public function delete($index, $name)
    {
        unset($this->{$name}[$index]);
        if(!count($this->{$name})){
            switch ($name){
                case 'photos':
                    $this->addPhoto();
                    break;
                case 'videos':
                    $this->addVideo();
                    break;
                case 'documents':
                    $this->addDocument();
                    break;
                case 'products':
                    $this->addProduct();
                    break;
            }
        }
    }

    public function deleteFile($id, $name)
    {
        $this->initService($this->transferId);
        $this->service->deleteFile($this, $id, $name);
        $this->message['success'] = __('form.messages.Success');
    }

    public function addProduct()
    {
        $this->formProducts[] = [
            'name' => '',
            'quantity' => 0,
            'volume' => '',
            'type' => '',
        ];
    }

    public function addPhoto()
    {
        $this->photos[] = ['photo' => ''];
    }

    public function addVideo()
    {
        $this->videos[] = ['video' => ''];
    }

    public function addDocument()
    {
        $this->documents[] = ['document' => ''];
    }
    public function rules(): array
    {
        return [
            'formProducts.*.id' => 'nullable',
            'formProducts.*.name' => 'required',
            'formProducts.*.code' => 'nullable',
            'formProducts.*.volume' => 'nullable',
            'formProducts.*.quantity' => 'required|numeric',
            'formProducts.*.category_id' => 'required',
            'formProducts.*.type' => 'required',
            'formProducts.*.checked' => 'nullable',
            'photos.*.photo' => 'image|mimes:webp,jpg,png,jpeg',
            'videos.*.video' => 'file',
            'documents.*.document' => 'file',

            'additionalInfo.driver_name' => 'string',
            'additionalInfo.brand_car' => 'string',
            'additionalInfo.licence_plate' => 'string',
            'additionalInfo.checkpoint' => 'string',
            'recipient' => 'required|numeric|gt:0',
        ];
    }

    public function messages()
    {
        return [
            'products.*.name' => __('cargo.form.messages.product-name'),
            'products.*.quantity.required' => __('cargo.form.messages.product'),
            'photos.*.photo' => __('cargo.form.messages.photos'),
            'videos.*.video' => __('cargo.form.messages.video'),
            'documents.*.document' => __('cargo.form.messages.document'),
            'recipient.gt' => __('cargo.form.messages.recipient required'),
            'recipient.number' => __('cargo.form.messages.recipient required'),
            'required' => __('form.messages.Required'),
        ];
    }
}
