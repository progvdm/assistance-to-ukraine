<?php

namespace App\Http\Livewire\Cargo;

use App\Enums\Cargo\CargoStatusesEnum;
use App\Enums\Users\OrganisationTypeEnum;
use App\Foundation\FlashMessage;
use App\Models\Transfers\Transfer;
use App\Models\Transfers\TransferDocument;
use App\Models\User;
use App\Notifications\SignDocumentNotification;
use App\Services\Cargo\GenerateDocumentService;
use App\Services\Cargo\ShipmentService;
use App\Services\Gps\GpsService;
use Illuminate\Support\Str;
use Livewire\Component;
use Livewire\WithFileUploads;

class Acceptance extends Component
{
    use WithFileUploads;

    public ?Transfer $transfer = null;

    public array $photos = [];
    public array $videos = [];
    public array $documents = [];
    public array $confirmSmsData = [];

    public string $gpsCity = '';
    public int  $transferId = 0;
    public int  $confirmSms = 0;
    public bool $confirmGeoAndPhoto = true;
    public bool $popupView = true;
    public bool $finish = false;
    public bool $popupConfirmSms = false;

    private ?ShipmentService $service = null;

    // flash message
    public $message = [];

    protected $listeners = ['smsVerification' => 'afterSmsVerification'];

    public function mount(Transfer $transfer)
    {

        $this->transfer = $transfer;
        if($this->transfer->status == CargoStatusesEnum::DELIVERY_CONFIRMED){
            $this->finish = true;
        }
        $this->checkGeoAndPhotoExists();
        $this->addPhoto();
        $this->addVideo();
        $this->addDocuments();

    }

    public function render()
    {
        /*if(!$this->checkAcceptance()){
            FlashMessage::setMessage(FlashMessage::ERROR, __('cargo.Reacceptance'));
            $this->shouldSkipRender = true;
            $this->redirect(routeLocale('cabinet.home'));
        }*/
        $this->emit('openModal');
        return view('livewire.cargo.acceptance');
    }

    public function checkAcceptance()
    {
        if($this->transfer->status == CargoStatusesEnum::DELIVERY_CONFIRMED){
            return false;
        }
        return true;
    }

    public function checkGeoAndPhotoExists()
    {
        $images = $this->transfer->getImagesApi();
        $geoByStorage = false ;
        if($this->transfer->transferGeo->count()){
            $geo = $this->transfer->transferGeo->last();
            $service = new GpsService();
            $this->gpsCity = $service->getCityByGeo(explode(',', $geo->geo_mark)[0], explode(',', $geo->geo_mark)[1]);
            $storage_city = \Organisation::getStorageCity();

            if(Str::slug($this->gpsCity) == Str::slug($storage_city)){
                $geoByStorage = true;
            }
        }

        if(($images->count() < 1 or !$geo or !$geoByStorage) and env('ACCEPANCE_ENEBLED', true)){
            $this->finish = true;
            $this->confirmGeoAndPhoto = false;
            if(!session()->has('sendSmsApp')){
                session()->put('sendSmsApp', true);
                /** @var User $user */
                $user = auth()->user();
                $text = 'Для завантаження фото та підтвердження геолокації перейдіть до https://app.aidmonitor.org';
                $user->notify(new \App\Notifications\MessageNotification($text));
            }

        }
    }
    protected function initService(){
        if(!$this->service){
            $this->service = new ShipmentService($this);
        }
    }

    public function afterSmsVerification()
    {
        $this->initService();
        $data = $this->validate(
            $this->rules(),
            $this->messages(),
            [],
        );

        if(!$this->checkAcceptance()){
            FlashMessage::setMessage(FlashMessage::ERROR, __('cargo.Reacceptance'));
            $this->redirect(routeLocale('cabinet.home'));
        }else{
            $data = $this->validate(
                $this->rules(),
                $this->messages(),
                [],
            );
            $this->initService();

            $transfer = $this->transfer;

            $transfer->status = CargoStatusesEnum::DELIVERY_CONFIRMED;
            $transfer->save();


            $this->service->productsToWarehouse($transfer);
            $this->service->createPhotos($data['photos'], $transfer->id);
            $this->service->createVideos($data['videos'], $transfer->id);
            $this->service->createDocuments($data['documents'], $transfer->id);
            $this->generateDocument(true);
        }
    }

    public function setConfirmSms($value){
        if($value == 'on'){
            $this->confirmSms = true;
        }else{
            $this->confirmSms = false;
        }
    }


    public function submitConfirmSms($popupClose = false){
        if(!$popupClose) {
            if ($this->confirmSmsData['acceptedSmsCode'] == $this->confirmSmsData['sendingSmsCode']) {
                $data = $this->validate(
                    $this->rules(),
                    $this->messages(),
                    [],
                );
                $this->initService();

                $transfer = Transfer::find($this->confirmSmsData['transferID']);

                $transfer->status = CargoStatusesEnum::DELIVERY_CONFIRMED;
                $transfer->save();


                $this->service->productsToWarehouse($transfer);
                $this->service->createPhotos($data['photos'], $transfer->id);
                $this->service->createVideos($data['videos'], $transfer->id);
                $this->service->createDocuments($data['documents'], $transfer->id);
                $this->generateDocument(true);
            } else {
                $this->message['error'] = __('cargo.form.Error sms code');
            }
        }else{
            $this->popupConfirmSms = false;
            unset($this->message['error']);
        }
    }

    protected function generateDocument($confirm = false){

        /** @var Transfer $transfer */
        $transfer = $this->transfer;
        $generateService = new GenerateDocumentService($this->transfer);
        $this->transfer->status = CargoStatusesEnum::DELIVERY_CONFIRMED;
        $this->transfer->save();
        $filename = $transfer->sender->name. ' to '.$transfer->recipient->name;
        $filename = mb_substr($filename, 0, 120). ' '. date('Y-m-d His');

        $filename = $generateService->upload(
            'acceptance',
            ['confirmSmsRecipient' => $confirm, 'confirmSmsSender'=> $this->transfer->signed_sender] + $transfer->getAdditionalInfo() ,
            $filename
        );
        if($document = $transfer->documents()->whereType(CargoStatusesEnum::DELIVERY_CONFIRMED)->first()){
            $document->document = $filename;
            $document->save();
        }else{
            $document = new TransferDocument();
            $document->type = CargoStatusesEnum::DELIVERY_CONFIRMED;
            $document->document = $filename;
            $document->user_id = auth()->user()->id;
            $document->transfer_id = $transfer->id;
            $document->save();
        }
        if($transfer->sender->isRecipient()){
            $delDocument = $transfer->documents()->whereType(CargoStatusesEnum::SEND_CARGO)->first();
            if($delDocument){
                $delDocument->deleteFile();
                $delDocument->delete();
            }
        }
        if($transfer->sender->isDonor()){
            $transfer->sender->notification(SignDocumentNotification::class, $document);
        }


        $transfer->signed_recipient = $confirm;
        $transfer->save();

        if($filename){
            $this->transferId = $transfer->id;
            $this->updateForm();
        }else{
            $this->redirect(routeLocale('cabinet.home'));
        }
        $this->message['success'] = __('form.messages.Success');
        $this->finish = true;
        $this->popupConfirmSms = false;
    }

    protected function updateForm(){
        $this->photos = [];
        $this->videos = [];
        $this->documents = [];
        $this->addPhoto();
        $this->addVideo();
        $this->addDocuments();
    }

    public function delete($index, $name)
    {
        unset($this->{$name}[$index]);
        if(!count($this->{$name})){
            switch ($name){
                case 'photos':
                    $this->addPhoto();
                    break;
                case 'videos':
                    $this->addVideo();
                    break;
                case 'documents':
                    $this->addDocuments();
                    break;
            }
        }
    }
    public function addPhoto()
    {
        $this->photos[] = ['photo' => ''];
    }

    public function addVideo()
    {
        $this->videos[] = ['video' => ''];
    }

    public function addDocuments()
    {
        $this->documents[] = ['document' => ''];
    }

    public function rules(): array
    {
        return [
            'photos.*.photo' => 'image|mimes:webp,jpg,png,jpeg',
            'videos.*.video' => 'file',
            'documents.*.document' => 'file',
            'confirmSms' => 'nullable'
        ];
    }

    public function messages()
    {
        return [
            'photos.*.photo' => __('cargo.form.messages.photos'),
            'videos.*.video' => __('cargo.form.messages.video'),
            'documents.*.document' => __('cargo.form.messages.document'),
        ];
    }
}
