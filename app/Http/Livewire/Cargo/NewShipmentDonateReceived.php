<?php

namespace App\Http\Livewire\Cargo;

use App\Enums\Cargo\CargoStatusesEnum;
use App\Enums\Transfer\TransferTypeEnum;
use App\Enums\Users\OrganisationTypeEnum;
use App\Events\Cargo\CheckEmptyCargo;
use App\Foundation\FlashMessage;
use App\Models\Cargos\Cargo;
use App\Models\Organisation\Organisation;
use App\Models\Organisation\OrganisationTranslation;
use App\Models\Transfers\Transfer;
use App\Models\Transfers\TransferDocument;
use App\Models\User;
use App\Services\Cargo\GenerateDocumentService;
use App\Services\Cargo\ShipmentService;
use App\Services\OpenDataBot\OpenDataBotService;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Database\Eloquent\Collection as EloquentCollection;
use Illuminate\Support\Facades\Hash;
use Livewire\Component;
use Livewire\WithFileUploads;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;

class NewShipmentDonateReceived extends Component
{
    use WithFileUploads;

    public array $products = [];
    public array $photos = [];
    public array $videos = [];
    public array $documents = [];
    public int   $recipient = 0;
    public string$donor_id = '';
    public array $donors = [];
    public array $newDonor = [];

    public array $productTypes = [];
    public array $recipients = [];
    public array $formProducts = [];
    public array $additionalInfo = [];

    public bool $isDonate   = false;
    public bool $isNewDonor = false;
    public bool $finish = false;
    public int  $transferId = 0;
    public ?EloquentCollection $categories = null;
    private ?ShipmentService $service = null;


    public ?Transfer $transfer = null;


    public function mount()
    {
        $this->initService();
        $this->service->uploadDataForm();
        $this->updateForm();
        $this->recipient = \Organisation::getOrganisationId();

    }

    public function render(): Factory|View|Application
    {
        return view('livewire.cargo.new-shipment-donate-received');
    }
    protected function initService(){
        if(!$this->service){
            $this->service = new ShipmentService($this);
        }
    }

    public function submit()
    {
        $this->resetErrorBag();
        $this->resetValidation();

        $this->initService();

        $data = $this->validate(
            $this->rules(),
            $this->messages(),
            [],
        );

        $this->service->setProducts($data);

        $transfer = $this->create($data, null);

        $this->updateForm();
        FlashMessage::setMessage(FlashMessage::SUCCESS, __('form.messages.Success'));

        $this->redirect(routeLocale('cabinet.home'));
    }

    public function substituteCompanyData()
    {
        $this->resetErrorBag();
        $this->resetValidation();

        if(!isset($this->newDonor['organisation']['id_code']) or empty($this->newDonor['organisation']['id_code']))
        {
            return;
        }

        $searchOrganisation = Organisation::whereIdCode($this->newDonor['organisation']['id_code'])->first();
        if($searchOrganisation){
            $this->donor_id = $searchOrganisation->id;
            $this->isNewDonor = false;
        }else{
            $service = new OpenDataBotService();
            $data = $service->getCompanyData($this->newDonor['organisation']['id_code'], 'criminal');
            if($data)
            {
                $this->newDonor['user']['name'] = $data->user_name;
                $this->newDonor['organisation']['name'] = $data->name;
                $this->newDonor['organisation']['country'] = $data->location['country'];
                $this->newDonor['organisation']['city'] = $data->location['city'];
                $this->newDonor['organisation']['street'] = $data->location['street'];
                $this->newDonor['organisation']['build'] = $data->location['build'];
                $this->newDonor['organisation']['zip_code'] = $data->location['zip'];
                $this->newDonor['organisation']['verification']['sanction'] = $data->sanction;
                $this->newDonor['organisation']['verification']['founderRu'] = $data->founderRu;
                $this->newDonor['organisation']['verification']['criminalCourts'] = $data->count_courts;

            }
        }
    }

    protected function create($data, $humanId): ?Transfer
    {
        if($this->donor_id == 'new' ){
            $registerData = $this->validate([
                'newDonor.user.name' => 'required|min:3',
                'newDonor.user.email' => 'required|email',
                'newDonor.organisation.id_code' => 'nullable|string',
                'newDonor.organisation.zip_code' => 'required|string',
                'newDonor.organisation.name' => 'required|string',
                'newDonor.organisation.country' => 'required|string',
                'newDonor.organisation.city' => 'required|string',
                'newDonor.organisation.street' => 'required|string',
                'newDonor.organisation.build' => 'required|string',
            ]);
            $registerData = $registerData['newDonor'];


            if(isset($registerData['user']['email']) and  $registerData['user']['email'] and $userSearsh = User::whereEmail($registerData['user']['email'])->first()){
                $organisationSender = $userSearsh->organisation;
            }elseif(isset($organisationData['id_code']) and  $organisationData['id_code'] and $organisationSearsh = Organisation::whereIdCode($organisationData['id_code'])->first()){
                $organisationSender = $organisationSearsh;
            }else{
                $user = User::create($registerData['user']);
                $organisationData = $registerData['organisation'];
                $organisationSender = new Organisation();
                $organisationSender->published = true;
                $organisationSender->view_site = false;
                $organisationSender->view_system = true;
                $organisationSender->founder_id = $user->id;
                $organisationSender->zip_code = $organisationData['zip_code'];
                $organisationSender->id_code = $organisationData['id_code'] ?? '';
                $organisationSender->type = OrganisationTypeEnum::DONOR;
                $organisationSender->verification = json_encode($organisationData['verification'] ?? []);
                $organisationSender->save();
                foreach (LaravelLocalization::getSupportedLocales() as $locale => $supportedLocale) {
                    /** @var OrganisationTranslation $organisationTranslate */
                    $organisationTranslate = $organisationSender->translateOrNew($locale);
                    $organisationTranslate->organisation_id = $organisationSender->id;
                    $organisationTranslate->name = $organisationData['name'];
                    $organisationTranslate->country = $organisationData['country'];
                    $organisationTranslate->city = $organisationData['city'];
                    $organisationTranslate->street = $organisationData['street'];
                    $organisationTranslate->build = $organisationData['build'];
                    $organisationTranslate->save();
                }
                $user->organisation_id = $organisationSender->id;
                $user->save();
            }

        }else{
            $organisationSender = Organisation::find($this->donor_id);
        }

        if(!$organisationSender) {
            return null;
        }

        $transfer = new Transfer();
        $transfer->sender_id = $organisationSender->id;
        $transfer->recipient_id = $data['recipient'];
        $transfer->human_id = $humanId;
        $transfer->parent = null;
        $transfer->status = CargoStatusesEnum::DELIVERY_CONFIRMED;
        $transfer->type = 'send';
        $transfer->additional_info = json_encode(['confirmSmsRecipient' => true, 'confirmSmsSender'=> true]);
        $transfer->save();
        $this->transfer = $transfer;

        $this->service->createProducts($transfer->id, $data['products']);
        $this->service->createPhotos($data['photos'], $transfer->id);
        $this->service->createVideos($data['videos'], $transfer->id);
        $this->service->createDocuments($data['documents'], $transfer->id);

        $this->transfer = $transfer ?? null;
        $this->createAct($transfer);

        //Аавтоматическое подтверждение
        $this->service->productsToWarehouse($transfer);

        return $transfer;
    }
    public function createAct(Transfer $transfer)
    {
        $filename = $transfer->sender->name. ' to '.$transfer->recipient->name;
        $filename = mb_substr($filename, 0, 120). ' '. date('Y-m-d His');
        $generateService = new GenerateDocumentService($transfer);

        $filename = $generateService->upload(
            'acceptance',
            ['confirmSmsRecipient' => true, 'confirmSmsSender'=> true ] + $transfer->getAdditionalInfo() ,
            $filename
        );

        $document = new TransferDocument();
        $document->type = CargoStatusesEnum::DELIVERY_CONFIRMED;
        $document->document = $filename;
        $document->user_id = auth()->user()->id;
        $document->transfer_id = $transfer->id;
        $document->save();
    }

    public function addNewDonor($value){
        if($value == 'new'){
            $this->isNewDonor = true;
        }else{
            $this->isNewDonor = false;
        }
    }


    public function validateProductQuantity($productId, $quantity){
        if($this->formProducts[$productId]['quantity_storage'] < $quantity){
            $this->formProducts[$productId]['quantity'] = $this->formProducts[$productId]['quantity_storage'];
        }
    }

    public function updateForm()
    {
        $this->formProducts = [];
        $this->photos = [];
        $this->videos = [];
        $this->documents = [];
        $this->recipient = 0;
        $this->addProduct();
        $this->addPhoto();
        $this->addVideo();
        $this->addDocument();
    }

    public function delete($index, $name)
    {
        unset($this->{$name}[$index]);
        if(!count($this->{$name})){
            switch ($name){
                case 'photos':
                    $this->addPhoto();
                    break;
                case 'videos':
                    $this->addVideo();
                    break;
                case 'documents':
                    $this->addDocument();
                    break;
                case 'products':
                    $this->addProduct();
                    break;
            }
        }
    }

    public function addProduct()
    {
        $this->formProducts[] = [
            'name' => '',
            'quantity' => 0,
            'volume' => '',
            'type' => '',
        ];
    }

    public function addPhoto()
    {
        $this->photos[] = ['photo' => ''];
    }

    public function addVideo()
    {
        $this->videos[] = ['video' => ''];
    }

    public function addDocument()
    {
        $this->documents[] = ['document' => ''];
    }

    public function rules(): array
    {

        $rules = [
            'recipient' => 'required|numeric|gt:0',

            'formProducts.*.id' => 'nullable',
            'formProducts.*.name' => 'required',
            'formProducts.*.volume' => 'nullable',
            'formProducts.*.quantity' => 'required|numeric',
            'formProducts.*.code' => 'nullable',
            'formProducts.*.type' => 'required',
            'formProducts.*.checked' => 'nullable',
            'formProducts.*.category_id' => 'required',
            'photos.*.photo' => 'image|mimes:webp,jpg,png,jpeg',
            'videos.*.video' => 'file',
            'documents.*.document' => 'file',
        ];
        $rules['newDonor.*'] = 'nullable';
        return $rules;
    }

    public function messages()
    {
        return [
            'products.*.name' => __('cargo.form.messages.product-name'),
            'products.*.quantity.required' => __('cargo.form.messages.product'),
            'photos.*.photo' => __('cargo.form.messages.photos'),
            'videos.*.video' => __('cargo.form.messages.video'),
            'documents.*.document' => __('cargo.form.messages.document'),
            'recipient.gt' => __('cargo.form.messages.recipient required'),
            'recipient.number' => __('cargo.form.messages.recipient required'),
            'required' => __('form.messages.Required'),
        ];
    }
}
