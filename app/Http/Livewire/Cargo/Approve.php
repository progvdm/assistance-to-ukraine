<?php

namespace App\Http\Livewire\Cargo;

use App\Enums\Cargo\CargoStatusesEnum;
use App\Foundation\FlashMessage;
use App\Models\Transfers\Transfer;
use App\Models\User;
use Livewire\Component;

class Approve extends Component
{

    public ?Transfer $transfer = null;



    // flash message
    public $message = [];


    public function mount(Transfer $transfer)
    {
        $this->transfer = $transfer;
    }

    public function render()
    {
        return view('livewire.cargo.approve');
    }

    public function submit($approve)
    {
        $transfer = $this->transfer;
        if($approve){
            $this->transfer->status_list = true;
            $this->transfer->status = CargoStatusesEnum::APPROVED;
            $text = 'Організація '.$transfer->recipient->name. ' погодила список товарів. Для отримання інформації увійдіть до особистого кабінету Aidmonitor.';
        }else{
            $this->transfer->status_list = false;
            $this->transfer->status = CargoStatusesEnum::NOT_APPROVED;
            $text = 'Організація '.$transfer->recipient->name. ' відмовилась від допомоги. Для отримання інформації увійдіть до особистого кабінету Aidmonitor';
        }
        $this->transfer->save();
        if($transfer->sender){
            $transfer->sender->notification(\App\Notifications\MessageNotification::class, $text);
        }

        FlashMessage::setMessage(FlashMessage::SUCCESS, __('Success'));
        $this->redirect(routeLocale('cabinet.home'));
    }

}
