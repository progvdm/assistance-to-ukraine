<?php

namespace App\Http\Livewire\Cargo;

use App\Foundation\FlashMessage;
use App\Models\Transfers\Transfer;
use App\Models\Transfers\TransferDocument;
use App\Models\User;
use App\Notifications\MessageNotification;
use App\Services\Cargo\GenerateDocumentService;
use Livewire\Component;

class SignDocument extends Component
{

    public ?TransferDocument $transferDocument;
    public bool $finish = false;
    public bool $popupConfirmSms = false;
    public int $code = 0;
    public int $codeUser = 0;
    public string $notificationId = '';

    // flash message
    public $message = [];

    protected $listeners = ['smsVerification' => 'afterSmsVerification'];


    public function mount(TransferDocument $transferDocument)
    {
        $this->transferDocument = $transferDocument;
        if($transferDocument->transfer and $transferDocument->transfer->signed_sender){
            $this->finish = true;
        }
        $this->notificationId = request()->get('notification');
    }
    public function render()
    {
        return view('livewire.cargo.sign-document');
    }

    public function afterSmsVerification(){
        $this->reGenerateDocument();
        $this->finish = true;
        if($this->notificationId){
            /** @var User $user */
            $user = auth()->user();
            $notification = $user->unreadNotifications->where('id', $this->notificationId)->first();
            if($notification) $notification->markAsRead();
        }
        FlashMessage::setMessage(FlashMessage::SUCCESS, __('cargo.form.The document has been successfully signed!'));
        $this->redirect(redirect()->back()->getTargetUrl());
    }


    private function reGenerateDocument(){
        /** @var Transfer $transfer */
        $transfer = $this->transferDocument->transfer;
        // save signed
        $transfer->signed_sender = true;
        $transfer->save();

        // RegenerateDocument
        $generateService = new GenerateDocumentService($transfer);
        $filename = $transfer->sender->name . ' to ' . $transfer->recipient->name;
        $filename = mb_substr($filename, 0, 120). ' '. date('Y-m-d His');

        $this->transferDocument->deleteFile();
        $filename = $generateService->upload(
            'acceptance',
            ['confirmSmsRecipient' => true, 'confirmSmsSender' => $transfer->signed_sender],
            $filename
        );

        $this->transferDocument->document = $filename;
        $this->transferDocument->save();
    }

}
