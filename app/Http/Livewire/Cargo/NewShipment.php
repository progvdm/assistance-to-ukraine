<?php

namespace App\Http\Livewire\Cargo;

use App\Enums\Cargo\CargoStatusesEnum;
use App\Events\Cargo\SendSms;
use App\Models\Organisation\Organisation;
use App\Models\Transfers\Transfer;
use App\Models\User;
use App\Services\Cargo\ShipmentService;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Database\Eloquent\Collection as EloquentCollection;
use Illuminate\Support\Arr;
use Livewire\Component;
use Livewire\WithFileUploads;

class NewShipment extends Component
{
    use WithFileUploads;

    public array    $products = [];
    public array    $photos = [];
    public array    $videos = [];
    public array    $documents = [];
    public int      $recipient = 0;
    public int      $confirmSms = 0;
    public array    $donors = [];

    public array    $productTypes = [];
    public array    $recipients = [];
    public array    $formProducts = [];
    public array    $additionalInfo = [];
    public array    $confirmSmsData = [];

    public bool     $isPeople   = false;
    public bool     $finish     = false;
    public bool     $popupConfirmSms = false;
    public bool     $confirmButton = false;
    public int      $transferId = 0;
    public ?EloquentCollection $categories = null;
    private ?ShipmentService $service = null;


    public ?Transfer $transfer = null;

    // flash message
    public $message = [];

    protected $listeners = ['smsVerification' => 'submitConfirmSms'];

    public function mount()
    {
        $this->initService();
        $this->updateForm();
        $this->service->uploadDataForm(true);
    }
    protected function initService(){
        if(!$this->service){
            $this->service = new ShipmentService($this);
        }
    }

    public function render(): Factory|View|Application
    {
        return view('livewire.cargo.new-shipment');
    }

    public function redirectHome(){
        $this->redirect(routeLocale('cabinet.home'));
    }

    public function updating(){
        $this->popupConfirmSms = false;
    }

    public function submit()
    {
        $this->initService();

        $data = $this->validate(
            $this->rules(),
            $this->messages(),
            [],
        );
       // $this->updateForm();
        $this->confirmSmsData['data'] = $data;
        $this->confirmSmsData['data']['confirmSms'] = false;
        if($data['confirmSms'] and auth()->user()->getPhoneNotification()){

            $this->popupConfirmSms = true;
        }else{
            $this->createTransfer();
        }
    }

    public function setConfirmSms($value){
        if($value == 'on'){
            $this->confirmSms = true;
            $this->resetErrorBag();
        }else{
            $this->confirmSms = false;
        }
    }

    public function confirmSms($sms){
        $this->confirmSmsData['acceptedSmsCode'] = $sms;
        unset($this->message['error']);
    }

    public function submitConfirmSms($popupClose = false){
        $this->confirmSmsData['data']['additionalInfo']['confirmSmsSender'] = true;
        $this->createTransfer();

    }

    protected function createAct($id){
        $transfer = Transfer::find($id);
        $transfer->signed_sender = true;
        $transfer->save();

        if(!$transfer->human){
            $text = 'Вам '.$transfer->recipient->name.' сформовано та відправлено вантаж. Увійдіть до особистого кабінету. aidmonitor.';
            /** @var Organisation $organisation */
            $organisation = $transfer->recipient;
            $organisation->notification(\App\Notifications\MessageNotification::class, $text);
            logger()->info('Send Notifi '.$transfer->recipient->id . $text);
        }
        if($transfer->human){
            $filename = $transfer->sender->name. ' to '.$transfer->human->name;

        }else{
            $filename = $transfer->sender->name. ' to '.$transfer->recipient->name;
        }
        $filename = mb_substr($filename, 0, 120). ' '. date('Y-m-d His');
        if(
            $this->transfer &&
            $this->service->createAct(
                $transfer,
                $this->confirmSmsData['data']['additionalInfo'] ?? [],
                $filename
            )
        ){
            $this->transferId = $transfer->id;
        }else{
            $this->redirect(routeLocale('cabinet.home'));
        }
        unset($this->message['error']);
        $this->message['success'] = __('form.messages.Success');
        $this->finish = true;
        $this->popupConfirmSms = false;
    }

    protected function createTransfer()
    {
        $this->initService();
        $data = $this->validate(
            $this->rules(),
            $this->messages(),
            [],
        );
        $this->setProducts($data);

        if($this->isPeople){
            $humanId = $this->service->createHuman();
        }

        $transfer = $this->create($data, $humanId ?? null);
        $this->updateForm();
        $this->createAct($transfer->id);
    }
    protected function create($data, $humanId): ?Transfer
    {
        /** @var User $user */
        if(!$user = auth()->user()) return null;

        $transfer = new Transfer();
        $transfer->sender_id = \Organisation::getOrganisationId();
        $transfer->recipient_id = $this->isPeople ? \Organisation::getOrganisationId() :$data['recipient'];
        $transfer->human_id = $humanId;
        $transfer->parent = null;
        $transfer->status = CargoStatusesEnum::SEND_CARGO;
        $transfer->type = 'send';
        $transfer->additional_info = json_encode($data['additionalInfo'] ?? []);
        $transfer->save();

        //$this->service->attachCargoParent($cargo, $data);
        $this->service->updateProducts($transfer->id, $data['products']);

        $this->service->createPhotos($data['photos'], $transfer->id);
        $this->service->createVideos($data['videos'], $transfer->id);
        $this->service->createDocuments($data['documents'], $transfer->id);

        $this->transfer = $transfer;
        return $transfer;
    }

    protected function setProducts(&$data)
    {
        $data['products'] = [];
        foreach ($data['formProducts'] as $product) {
            if (Arr::get($product, 'checked') and Arr::get($product, 'quantity') > 0) {
                $data['products'][] = $product;
            }
        }
        if(count($data['products']) == 0){
            $this->addError('products', 'Product list is invalid.');
        }else{
            unset($data['formProducts']);
        }
    }

    public function validateProductQuantity(){
        $this->updating();
        if(!$this->isPeople and !$this->confirmSms){
            $this->addError('confirmSms', __('validation.required', ['attribute' => '']));
        }
        $count = 0;
        foreach ($this->formProducts as &$products){
            if($products['quantity_storage'] < $products['quantity']){
                $products['quantity'] = $products['quantity'] = $products['quantity_storage'];
            }
            $products['quantity_residue'] = (int)$products['quantity_storage'] - (int)($products['quantity'] ?? 0);
            if (Arr::get($products, 'checked') and Arr::get($products, 'quantity') > 0) {
                $count += 1;
            }
        }
        $productsCollection = collect($this->formProducts);
        $this->formProducts = (array)$productsCollection->sortByDesc('checked')->toArray();
        $this->formProducts = array_splice($this->formProducts, 0, 10000);
        if($count > 0){
            $this->confirmButton = true;
        }else{
            $this->confirmButton = false;
        }
    }

    public function updateForm()
    {
        $this->formProducts = [];
        $this->photos = [];
        $this->videos = [];
        $this->documents = [];
        $this->recipient = 0;

        if(!$this->service->warehouseProducts()){
            $this->redirect(routeLocale('cabinet.home'));
        }

        $this->addPhoto();
        $this->addVideo();
        $this->addDocument();
    }

    public function delete($index, $name)
    {
        unset($this->{$name}[$index]);
        if(!count($this->{$name})){
            switch ($name){
                case 'photos':
                    $this->addPhoto();
                    break;
                case 'videos':
                    $this->addVideo();
                    break;
                case 'documents':
                    $this->addDocument();
                    break;
            }
        }
    }

    public function addProduct()
    {
        $this->formProducts[] = [
            'name' => '',
            'quantity' => 0,
            'volume' => '',
            'type' => '',
        ];
    }

    public function addPhoto()
    {
        $this->photos[] = ['photo' => ''];
    }

    public function addVideo()
    {
        $this->videos[] = ['video' => ''];
    }

    public function addDocument()
    {
        $this->documents[] = ['document' => ''];
    }

    public function rules(): array
    {
        $rules =  [
            'recipient' => 'required|numeric|gt:0',

            'formProducts.*.id' => 'nullable',
            'formProducts.*.name' => 'required',
            'formProducts.*.volume' => 'nullable',
            'formProducts.*.type' => 'required',
            'formProducts.*.checked' => 'nullable',
            'formProducts.*.quantity' => 'nullable',
            'photos.*.photo' => 'image|mimes:webp,jpg,png,jpeg',
            'videos.*.video' => 'file',
            'documents.*.document' => 'file',
            'confirmSms' => 'required|integer',
            'additionalInfo.description_product' => 'nullable|string',
        ];
        if($this->isPeople){
            if($this->additionalInfo and isset($this->additionalInfo['isOrganisation']) and $this->additionalInfo['isOrganisation']){
                // Organisation
                $rulesPeople = [
                    'additionalInfo.isOrganisation' => 'nullable',
                    'additionalInfo.name' => 'required|string',
                    'additionalInfo.phone' => 'nullable|string',
                    'additionalInfo.address' => 'nullable|string',
                    'additionalInfo.id_code' => 'required|string',
                    'additionalInfo.responsible_person' => 'nullable|string',
                    'additionalInfo.description_product' => 'nullable|string',
                ];
            }else{
                $rulesPeople = [
                    'additionalInfo.isOrganisation' => 'nullable',
                    'additionalInfo.name' => 'required|string',
                    'additionalInfo.phone' => 'nullable|string',
                    'additionalInfo.passport.series' => 'nullable|string',
                    'additionalInfo.passport.number' => 'nullable|string',
                    'additionalInfo.passport.date' => 'nullable|string',
                    'additionalInfo.passport.issued' => 'nullable|string',
                    'additionalInfo.passport.registration_address' => 'nullable|string',
                    'additionalInfo.description_product' => 'nullable|string',
                ];
            }
            unset($rules['recipient']);

            $rules = array_merge($rules, $rulesPeople);
        }
        return $rules;
    }

    public function messages()
    {
        return [
            'products.*.name' => __('cargo.form.messages.product-name'),
            'products.*.quantity.required' => __('cargo.form.messages.product'),
            'photos.*.photo' => __('cargo.form.messages.photos'),
            'videos.*.video' => __('cargo.form.messages.video'),
            'documents.*.document' => __('cargo.form.messages.document'),
            'recipient.gt' => __('cargo.form.messages.recipient required'),
            'recipient.number' => __('cargo.form.messages.recipient required'),
            'required' => __('form.messages.Required'),
        ];
    }
}
