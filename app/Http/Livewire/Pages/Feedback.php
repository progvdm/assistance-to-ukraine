<?php

namespace App\Http\Livewire\Pages;

use App\Helper\FileUploader;
use App\Models\Callbacks\Callback;
use Carbon\Carbon;
use Livewire\Component;
use Livewire\WithFileUploads;

class Feedback extends Component
{
    use WithFileUploads;

    public string $category = '';
    public string $text = '';
    public string $name = '';
    public string $email = '';
    public int $categoryId = 1;
    public bool $finish = false;
    public $file;

    // flash message
    public $message = [];

    protected $listeners = ['setCategory' => 'setCategory'];

    public function mount()
    {
    }


    public function render()
    {
        return view('livewire.pages.feedback');
    }

    public function updated(){
        unset($this->message['error'], $this->message['success'], $this->message['info']);
    }

    public function submit()
    {
        unset($this->message['error'], $this->message['success'], $this->message['info']);
        if(true){
            $this->validate(
                $this->rules(),
                $this->messages(),
                [],
            );
            $callback = new Callback();
            $callback->text = $this->text;
            $callback->name = $this->name;
            $callback->email = $this->email;
            $callback->category = $this->getCategoryName($this->categoryId);
            if($this->file){
                $uploader = new FileUploader();
                $name = $uploader->uploadOne($this->file, 'callback');
                $callback->file = $name;
            }
            $callback->save();
            $callback->notification();

            $this->message['success'] = __('feedback.Question sent! Our specialist will contact you soon.');
            session()->put('callback', Carbon::now()->timestamp);
            $this->clearForm();
        }else{
            $this->message['error'] = __('feedback.Your question has already been sent. Try again later.');
        }
    }

    private function clearForm(){
        $this->categoryId = 0;
        $this->category = '';
        $this->name = '';
        $this->email = '';
        $this->text = '';
        $this->file = '';
    }

    private function getCategoryName($id){
        $categories = [
            1 => __('feedback.Technical support'),
            2 => __('feedback.Asisstance in registration and operational activities on the site'),
            3 => __('feedback.Cooperation with the aidmonitor project'),
            4 => __('feedback.Other questions'),
        ];
        return $categories[$id] ?? '';
    }

    public function rules(): array
    {
        return [
            'text' => 'required|min:10',
            'name' => 'required|min:3',
            'email' => 'required|email'
        ];
    }

    public function messages()
    {
        return [
            'text.required' => __('feedback.Fill in the text of the appeal!'),
            'text.min' => __('feedback.The text must be at least 10 characters!'),
            'email' => __('feedback.Email not valid!')
        ];
    }
}
