<?php

namespace App\Http\Livewire\Pages;

use App\Models\Organisation\Organisation;
use App\Models\User;
use Livewire\Component;
use Seo;

class UserList extends Component
{
    public string $search = '';
    public string $type = '';
    public string $route = '';
    public string $h1 = '';


    public function render()
    {
        $organisations = Organisation::whereType($this->type)->where('view_site', 1);
        if($search = $this->search){
            $search = trim($search);
            $search = ltrim($search);
            $organisations->whereHas('translations', function ($query) use ($search){
                $query->where('name', 'LIKE', '%'.$search.'%');
            });
        }
        $organisations = $organisations->get();
        return view('livewire.pages.user-list', ['organisations' => $organisations]);
    }
}
