<?php

namespace App\Http\Livewire\Pages;

use Livewire\Component;

class HomeButtonVideo extends Component
{
    public bool $viewVideo = false;

    public bool $hideBlock = false;

    public function render()
    {
        return view('livewire.pages.home-button-video');
    }
}
