<?php

namespace App\Http\Livewire\Marketplace;

use App\Enums\AidRequest\AidRequestTypesEnum;
use App\Models\AidRequest\AidRequest;
use App\Models\Categories\Category;
use App\Models\Organisation\Organisation as OrganisationModel;
use Livewire\Component;
use Livewire\WithPagination;

class MarketplaceList extends Component
{
    use WithPagination;

    public bool $openBlock = false;

    // filter
    public string $searchQuery = '';
    public int $searchOrganisation = 0;
    public int $searchCategory = 0;

    // popup data
    public bool $thank_you = false;
    public int $cardId = 0;
    public string $name = '';
    public string $email = '';
    public string $message = '';

    // Tabs
    public string $tab = 'all';

    public function render()
    {
        $timezone = env('TIMEZONE', null);

        $listOffer = AidRequest::whereDelete(false)
            ->wherePublished(1)
            ->where('publication_date', '<=', now($timezone)->format('Y-m-d H:i:s'))
            ->where('active_date', '>=', now($timezone)->format('Y-m-d H:i:s'));
        $listNeed = AidRequest::whereDelete(false)
            ->wherePublished(1)
            ->where('publication_date', '<=', now($timezone)->format('Y-m-d H:i:s'))
            ->where('active_date', '>=', now($timezone)->format('Y-m-d H:i:s'));
        if($search = $this->searchQuery){
            $search = trim($search);
            $search = ltrim($search);
            $listOffer->whereHas('translations', function ($query) use ($search){
                $query->where('description', 'LIKE', '%'.$search.'%');
            });
            $listNeed->whereHas('translations', function ($query) use ($search){
                $query->where('description', 'LIKE', '%'.$search.'%');
            });
        }
        if($this->searchCategory){
            $category = Category::find($this->searchCategory);
            if($category){
                $listOffer->where('category_id', '=',$category->id);
                $listNeed->where('category_id', '=',$category->id);
            }
        }
        if($this->searchOrganisation){
            $listOffer->where('organisation_id', '=', (int)$this->searchOrganisation);
            $listNeed->where('organisation_id', '=', (int)$this->searchOrganisation);
        }
        if($this->tab and $this->tab != 'all'){
            $listOffer->where('type', $this->tab);
            $listNeed->where('type', $this->tab);
        }
        $list = $listNeed->paginate(8);
        $listOffer = $listOffer->orderByDesc('created_at')->where('type', AidRequestTypesEnum::OFFER)->paginate(4);
        $listNeed = $listNeed->orderByDesc('created_at')->where('type', AidRequestTypesEnum::NEED)->paginate($listOffer->count() == 0 ? 8 : 4);


        $categories =  Category::select(['id'])->orderBy('sort')->with('currents')->get();

        $organisations = [];
        if(auth()->user()){
            $organisations = OrganisationModel::getForSite();
        }

        return view('livewire.marketplace.marketplace-list', [
            'list' => $list,
            'listOffer' => $listOffer,
            'listNeed' => $listNeed,
            'categories' => $categories,
            'organisations' => $organisations,
        ]);
    }

    public function updatingTab()
    {
        $this->resetPage();
    }

    public function paginationView()
    {
        return 'livewire.components.pagination';
    }
}
