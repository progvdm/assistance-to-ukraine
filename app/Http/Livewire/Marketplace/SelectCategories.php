<?php

namespace App\Http\Livewire\Marketplace;

use App\Models\Lists\ListCategory;
use Asantibanez\LivewireSelect\LivewireSelect;
use Illuminate\Support\Collection;

class SelectCategories extends LivewireSelect
{
    public Collection $optionsList;
    /**
     * @param null $searchTerm
     * @return Collection
     */
    // In CarModelSelect component
    public function options($searchTerm = null): Collection
    {

        $searchQuery = ListCategory::where('parent_id', null);
        if ($searchTerm && strlen($searchTerm) > 2) {
            $searchQuery->whereHas('translations', function ($query) use ($searchTerm) {
                $query->where('name', 'LIKE', $searchTerm . '%');
            });
        }
        $cities = [];
        $search = $searchQuery->get();
        if($search->isNotEmpty()){
            foreach ($search as $item) {
                $cities[] = ['value' => $item->id, 'description' => $item->name];
            }
            $this->optionsList = collect($cities);
            return $this->optionsList;
        }
        return collect([
            ['value' => 0, 'description' => 'No select'],
        ]);
    }

    public function selectedOption($value)
    {
        $category = $this->optionsList->where('value', $value)->first();
        return ['value' => $value, 'description' => $category['description']];
    }

    public function styles()
    {
        return [
            'default' => 'p-2 rounded border w-full appearance-none',

            'searchSelectedOption' => 'newPost-select__option--selected',
            'searchSelectedOptionTitle' => '_text-left',
            'searchSelectedOptionReset' => 'newPost-select__option--reset',

            'search' => 'relative',
            'searchInput' => 'newPost-select__option--input',
            'searchOptionsContainer' => 'newPost-select__options',

            'searchOptionItem' => 'newPost-select__option',
            'searchOptionItemActive' => 'bg-indigo-600 text-white font-medium',
            'searchOptionItemInactive' => 'bg-white text-gray-600',

            'searchNoResults' => 'p-8 w-full bg-white border text-center text-xs text-gray-600',
        ];
    }
}
