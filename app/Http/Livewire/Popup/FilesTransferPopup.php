<?php

namespace App\Http\Livewire\Popup;

use App\Models\Transfers\TransferFiles;
use App\Traits\Livewire\FilesPopup;
use Livewire\Component;

class FilesTransferPopup extends Component
{
    use FilesPopup;


    public function mount($transferId)
    {
        $this->typeMedia = TransferFiles::FILE;
        $this->transferId = $transferId;
    }

    public function render()
    {
        [$files, $folders] = $this->getMedia();

        return view('livewire.popup.files-transfer-popup', ['files' => $files, 'folders' => $folders]);
    }
}
