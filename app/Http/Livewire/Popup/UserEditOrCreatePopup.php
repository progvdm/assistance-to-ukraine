<?php

namespace App\Http\Livewire\Popup;

use App\Models\User;
use App\Notifications\RegistrationUserNotification;
use App\Rules\Auth\EmailUniqueRule;
use App\Services\Mail\EmailTokenService;
use Illuminate\Support\Facades\Hash;
use Livewire\Component;
use Str;

class UserEditOrCreatePopup extends Component
{
    public $userId;
    public $userData;

    public function mount($userId = null){
        if($userId){

            $user = User::find($userId);
            if($user){
                $this->userId = $userId;
                $this->userData = $user->toArray();
            }
        }

    }
    public function render()
    {
        return view('livewire.popup.user-edit-or-create-popup');
    }

    public function editUser($userId){

        $user = User::find($userId);
        if($user->organisation_id != \Organisation::getOrganisationId()/* or $user->isSuperadmin()*/){
            $this->emit('userPopupAction', ['error' => '403 forbidden']);

        }else{
            $user->name = $this->userData['name'];
            //$user->type_user = '';
            $user->email = $this->userData['email'];
            $user->phone = $this->userData['phone'];
            $user->save();

            $this->emit('closePopup');
            $this->emit('userListUpdate');
            $this->emit('userPopupAction', ['success' => __('users.User updated!')]);
        }

    }
    public function createUser(){

        $this->validate($this->rules());
        $user = new User();

        $password = Str::random(10);
        $user->name = $this->userData['name'];
        //$user->type_user = '';
        $user->email = $this->userData['email'];
        $user->phone = $this->userData['phone'] ?? null;
        $user->organisation_id = \Organisation::getOrganisationId();
        $user->password = Hash::make($password);
        $user->save();
        $this->emit('closePopup');
        $this->emit('userListUpdate');
        $this->userData = [];
        // Notification user
        $emailTokenService = new EmailTokenService();
        $user->notify(new RegistrationUserNotification($user, ['token' => $emailTokenService->token($user), 'password' => $password]));
        $this->emit('userPopupAction', ['success' => __('users.User created!')]);
    }
    public function rules(){
        return [
            'userData.name' => 'required|string',
            'userData.email' => ['required','string','email', new EmailUniqueRule],
            'userData.phone' => ['regex:/^([0-9]{0,12})?(\+[0-9]{12})?$/'],
        ];
    }
}
