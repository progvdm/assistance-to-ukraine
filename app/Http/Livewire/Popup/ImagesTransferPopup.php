<?php

namespace App\Http\Livewire\Popup;

use App\Dto\Cargo\TransferFilesDto;
use App\Models\Transfers\Transfer;
use App\Models\Transfers\TransferFiles;
use App\Traits\Livewire\FilesPopup;
use Livewire\Component;

class ImagesTransferPopup extends Component
{
    use FilesPopup;


    public function mount($transferId)
    {
        $this->typeMedia = TransferFiles::IMAGE;
        $this->transferId = $transferId;
    }

    public function render()
    {
        [$images, $folders] = $this->getMedia();

        return view('livewire.popup.images-transfer-popup', ['photos' => $images, 'folders' => $folders]);
    }
}
