<?php

namespace App\Http\Livewire\Popup;

use App\Models\AidRequest\AidRequestLeed;
use App\Notifications\AidRequestApplicantNotification;
use App\Notifications\AidRequestNotification;
use Livewire\Component;

class AidRequestPopup extends Component
{
    // popup data
    public bool $thank_you = false;
    public int $cardId = 0;
    public string $name = '';
    public string $name_organisation = '';
    public string $email = '';
    public string $message = '';



    public function render()
    {
        return view('livewire.popup.aid-request-popup');
    }

    public function sendLeed(){
        $data = $this->validate([
            'name' => ['required', 'string'],
            'name_organisation' => ['nullable', 'string'],
            'email' => ['required', 'string', 'email'],
            'message' => 'required|string|max:400|min:10',
        ]);
        $leed = new AidRequestLeed();
        $leed->aid_request_id = $this->cardId;
        $leed->fill($data);
        $leed->status = AidRequestLeed::NEW;
        $leed->save();
        $leed->aidRequest->notify(new AidRequestNotification($leed, ['mail']));
        $leed->aidRequest->organisation->founder->notify(new AidRequestNotification($leed, ['database']));
        $leed->notify(new AidRequestApplicantNotification());

        $this->cardId = 0;
        $this->thank_you = true;
    }


}
