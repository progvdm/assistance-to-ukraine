<?php

namespace App\Http\Livewire\Popup;

use App\Enums\Cargo\CargoStatusesEnum;
use App\Models\Transfers\Transfer;
use App\Models\Transfers\TransferDocument;
use App\Services\Transfer\DocumentService;
use Livewire\Component;
use Livewire\WithFileUploads;

class FileHumanPopup extends Component
{
    use WithFileUploads;

    public int $transferId;
    public ?Transfer $transfer;
    public array $confirmPopupData = [];
    public $file = null;
    // flash message
    protected $listeners = ['addErrorEvent' => 'addErrorCustom'];


    public function render()
    {
        /** @var Transfer $transfer */
        $transfer = Transfer::find($this->transferId);
            /** @var TransferDocument $document */
            $document = $transfer->documents()->first();
            $this->confirmPopupData['transfer']['document'] = $document->document ?? '';
            $this->confirmPopupData['transfer']['created_at'] = $document->created_at ?? '';
            $this->confirmPopupData['human'] = $transfer->human;
            $this->confirmPopupData['uploadAccess'] = ($transfer->sender_id == \Organisation::getOrganisationId() and \Organisation::isFounder());

        return view('livewire.popup.file-human-popup', ['human' => $transfer->human]);
    }

    public function addErrorCustom(){
        $this->addError('file', __('validation.max.file', ['attribute' => 'file', 'max' => 5000]));
    }
    public function updating(){
        $this->resetErrorBag('file');
    }

    public function updatedFile()
    {
        $this->validate([
            'file' => 'mimes:jpg,png,pdf|max:5000'
        ]);
    }

    public function submit()
    {
        $this->validate([
            'file' => 'mimes:jpg,png,pdf|max:5000'
        ]);
        /** @var Transfer $transfer */
         $transfer = Transfer::find($this->transferId);
         if($transfer and $this->file){
             $fileService = new DocumentService();
             $fileService->uploadHuman($this->file, $transfer->human->id);
             $transfer->status = CargoStatusesEnum::DELIVERY_CONFIRMED;
             $transfer->save();
             $this->emit('messageEvent', 'success', __('form.messages.Success'));
             $this->emit('reloadDashboardItem');
         }else{
             $this->emit('messageEvent', 'error', 'Error');
         }
    }
}
