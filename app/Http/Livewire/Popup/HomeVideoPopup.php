<?php

namespace App\Http\Livewire\Popup;

use Livewire\Component;

class HomeVideoPopup extends Component
{
    public function render()
    {
        return view('livewire.popup.home-video-popup');
    }
}
