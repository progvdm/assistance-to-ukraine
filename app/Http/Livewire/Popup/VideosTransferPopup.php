<?php

namespace App\Http\Livewire\Popup;

use App\Models\Transfers\TransferFiles;
use App\Traits\Livewire\FilesPopup;
use Livewire\Component;

class VideosTransferPopup extends Component
{
    use FilesPopup;

    public function mount($transferId)
    {
        $this->typeMedia = TransferFiles::VIDEO;
        $this->transferId = $transferId;
    }

    public function render()
    {
        [$videos, $folders] = $this->getMedia();

        return view('livewire.popup.videos-transfer-popup', ['videos' => $videos, 'folders' => $folders]);
    }
}
