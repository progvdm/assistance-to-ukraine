<?php

namespace App\Http\Livewire\Popup;

use App\Models\Transfers\Transfer;
use Livewire\Component;

class ProductsPopup extends Component
{
    public ?Transfer $transfer;
    public string $name;
    public bool $showPopup = false;

    public function render()
    {
        return view('livewire.popup.products-popup', ['products' => $this->transfer->products]);
    }
}
