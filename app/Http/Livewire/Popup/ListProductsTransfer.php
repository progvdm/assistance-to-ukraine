<?php

namespace App\Http\Livewire\Popup;

use App\Models\Transfers\Transfer;
use Livewire\Component;

class ListProductsTransfer extends Component
{
    public $transferId;


    public function render()
    {
        $selectTransfer = Transfer::find($this->transferId);
        if($selectTransfer){
            $products = $selectTransfer->products;
        }
        return view('livewire.popup.list-products-transfer', ['transferProducts' => $products ?? []]);
    }
}
