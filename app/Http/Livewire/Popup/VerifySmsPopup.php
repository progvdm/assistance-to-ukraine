<?php

namespace App\Http\Livewire\Popup;

use App\Models\Sms;
use App\Models\User;
use Livewire\Component;

class VerifySmsPopup extends Component
{
    public string|null $sms = null;
    public bool $reSendSms = false;
    public bool $telegram = false;
    public string $telegramToken = '';

    public function mount()
    {
        /** @var User $user */
        $user = \Organisation::getAuthUser();
        $sms = new Sms();
        $sms->user_id = $user->id;
        $sms->code = rand('11111', 99999);
        $sms->save();
        $text = 'Ваш код для підтвердження: '. $sms->code;
        $user->notify(new \App\Notifications\MessageNotification($text));

        $this->telegram = $user->telegram_user_name ? false : true;
        $this->telegramToken = $user->generateTelegramToken();
    }
    public function render()
    {
        return view('livewire.popup.verify-sms-popup');
    }

    public function submit(){
        $this->confirm();

    }
    private function confirm(){
        $this->validate([
            'sms' => 'required|numeric|min:00000|max:99999'
        ]);
        /** @var User $user */
        $user = \Organisation::getAuthUser();
        $sms = $user->getLastSms();
        if(!$sms->status and $sms->code == $this->sms){
            $sms->status = 1;
            $sms->save();
            $this->emit('closePopup');
            $this->emit('smsVerification');
        }else{
            $this->addError('sms', __('default.The code is incorrect!'));
        }
    }

    public function reSendSms(){
        if(!$this->reSendSms){
            $this->reSendSms = true;
            $user = \Organisation::getAuthUser();
            $sms = $user->getLastSms();
            $text = 'Ваш код для підтвердження: '. $sms->code;
            $user->notify(new \App\Notifications\MessageNotification($text));
        }

    }
}
