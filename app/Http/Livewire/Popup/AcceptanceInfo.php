<?php

namespace App\Http\Livewire\Popup;

use Livewire\Component;

class AcceptanceInfo extends Component
{
    public function render()
    {
        return view('livewire.popup.acceptance-info');
    }
}
