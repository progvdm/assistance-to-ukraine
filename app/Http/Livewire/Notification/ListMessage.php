<?php

namespace App\Http\Livewire\Notification;

use Illuminate\Notifications\Notification;
use Illuminate\Support\Arr;
use Livewire\Component;

class ListMessage extends Component
{
    public function render()
    {
        $user = \Organisation::getAuthUser();
        $user->refresh();
        return view('livewire.notification.list-message', ['list' => $user->unreadNotifications()->limit(10)->get()]);
    }

    public function clickMessage($notificationId){
        $user = \Organisation::getAuthUser();

        $notification = $user->unreadNotifications->where('id', $notificationId)->first();
        if($notification) $notification->markAsRead();
        $url = Arr::get($notification->data, 'route') ? route(Arr::get($notification->data, 'route'), Arr::get($notification->data, 'route_params')) : '';
        if($url){
            $this->redirect($url);
        }
    }
}
