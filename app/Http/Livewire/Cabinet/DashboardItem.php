<?php

namespace App\Http\Livewire\Cabinet;

use App\Dto\Cargo\TransferFilesDto;
use App\Enums\Users\OrganisationTypeEnum;
use App\Models\Cargos\TransferInformations;
use App\Models\Organisation\Organisation;
use App\Models\Transfers\Transfer;
use App\Models\Transfers\TransferProduct;
use App\Models\Warehouses\Warehouse;
use App\Models\Warehouses\WarehouseProduct;
use Livewire\Component;

class DashboardItem extends Component
{
    public int $transferId;
    public int $page;
    public bool $open = false;
    public string $typeTransfer = '';
    public ?Transfer $transfer;

    public int $popupListProductsId = 0;
    public bool $popupListProducts = false;

    public int $openCategoryId = 0;

    protected $listeners = ['openRow' => 'openRowListen', 'reloadDashboardItem' => '$refresh'];

    public function openRowListen($id){
        if($id == $this->transferId){
            $this->open = true;
        }else{
            $this->open = false;
        }
    }
    public function mount($transfer, $typeTransfer, $numList){
        $this->transfer = $transfer;
        $this->typeTransfer = $typeTransfer;
        $this->numList = $numList;
    }

    public function render()
    {
        return view('livewire.cabinet.dashboard-item', $this->getData());
    }

    private function getData(){
        $isDonor = \Organisation::isDonor();

        $categories = [];
        $productsAllow = [];
        foreach($this->transfer->products as $transferProduct){
            $categories[$transferProduct->product->category->current->name] = $transferProduct->product->category->current->name;
            $productsAllow[] = $transferProduct->id;
        }
        $recipients = [];
        $nextTransfers = Transfer::getNextTransfer($this->transfer->recipient_id, $productsAllow);

        foreach ($nextTransfers as $cargoChild){
            if($cargoChild->human){
                $recipients[$cargoChild->human->name] = $cargoChild->human->name;
            }else{
                $recipients[$cargoChild->recipient->name] = $cargoChild->recipient->name;
            }
        }
        if($isDonor or $this->typeTransfer == 'handed_over'){
            $user = $this->transfer->recipient->name;
        }elseif(!$isDonor and $this->typeTransfer == 'accepted'){
            $user = $this->transfer->sender->name;
        }elseif($this->typeTransfer == 'provided'){
            $user = $this->transfer->human->name;
        }

        if($this->popupListProducts){
            $this->popupListProducts = false;
            $selectTransfer = Transfer::find($this->popupListProductsId);
            if($selectTransfer){
                $products = $selectTransfer->products;
                $this->popupListProducts = true;
            }
        }
        return [
            'transfer' => $this->transfer,
            'transferProducts' => $products ?? [],
            'transferInfo' => ['acts' => ['1'],'files' => [],'photos' => [],'videos' => [], 'locations'=> []],
            'categoriesImplode' => implode(', ', $categories),
            'user' => $user ?? '',
            'waiting' => !$this->transfer->child,
            'childs' => implode(', ', $recipients),
            'openRowData' => $this->open ? $this->openRowData() : [],
        ];
    }
    public function openRow()
    {
        $this->open = true;
    }

    protected function openRowData()
    {
        /** @var Transfer $transfer */
        $transfer = $this->transfer;
        $organisation = \Organisation::getOrganisation();

        if(\Organisation::isDonor() or $this->typeTransfer == 'handed_over'){
            $organisation = $transfer->recipient;
            $organisation_name = $transfer->recipient->name;
        }elseif(!\Organisation::isDonor() and $this->typeTransfer == 'accepted'){
            $organisation = $transfer->recipient;
            $organisation_name = $transfer->sender->name;
        }elseif($this->typeTransfer == 'provided'){
            $organisation = null;
            $organisation_name = $transfer->human->name;
        }


        $products = [];
        $categories = [];
        $productsAllow = [];
        foreach ($transfer->products as $transferProduct) {
            $productsAllow[$transferProduct->id] = $transferProduct->id;
            $this->productsData($transferProduct, $products);
            $this->categoriesData($transferProduct, $categories);
        }
        $nextOrganizationData = [];
        $nextTransfers = Transfer::getNextTransfer($transfer->recipient_id, $productsAllow);
        $countSystem = [
            'categories' => [],
            'products' => [],
            'all' => 0,
            'transferProductsIds' => [],
        ];
        $transfersAllow[] = $transfer->id;
        foreach ($nextTransfers as $nextCargo) {

            $transfersAllow[$transfer->id] = $transfer->id;
            $lines = [];
            $this->nextOrganizationData($nextCargo, $nextOrganizationData, 0, $countSystem, $transfersAllow, $lines, $productsAllow);
        }

        $locations = TransferInformations::where('transfer_id', $transfer->id )->count() ?? count($nextTransfers);
        $locations = $locations + count($nextTransfers);
        ksort($nextOrganizationData, SORT_NUMERIC);
        unset($countSystem['categories']);
        foreach (collect($products) as $product){
            $countProduct = $countSystem['products'][$product->id] ?? 0;
            $countSystem['categories'][$product->category_id] = ($countSystem['categories'][$product->category_id] ?? 0) + $countProduct;
        }
        return  [
            'products' => collect($products),
            'categories' => collect($categories),
            'user' => $organisation_name ?? '',
            'nextOrganizationData' => $nextOrganizationData,
            'transfer' => $transfer,
            'human' => $transfer->human ? true : false,
            'onlineBalance' => $countSystem,
            'locations' => $locations,
            'warehousesData' => $this->getWarehousesData($organisation->id ?? null),
        ];
    }
    private function getWarehousesData($organisation_id = null):array
    {
        /** @var Transfer $transfer */
        $transfer = $this->transfer;
        if ($transfer) {
            $productsIds = $transfer->products->map(function (TransferProduct $product) {
                return $product->id;
            })->toArray();
            //$user_id = auth()->user()->id;
            if (\Organisation::isDonor()) {
                $organisation_id = $transfer->recipient_id;
            }
            $warehouse = Warehouse::where('organisation_id', '=', $organisation_id)->first();
            $residue = [];
            $residueCategory = [];
            if($warehouse){
                foreach ($warehouse->warehouseProducts as $product) {
                    if (in_array($product->transfer_product_id, $productsIds)) {
                        if (isset($remainder[$product->product->id])) {
                            $residue[$product->product->product_id] += $product->quantity;
                        } else {
                            $residue[$product->product->product_id] = $product->quantity;
                        }
                        if (isset($residueCategory[$product->product->product->category_id])) {
                            $residueCategory[$product->product->product->category_id] += $product->quantity;
                        } else {
                            $residueCategory[$product->product->product->category_id] = $product->quantity;
                        }
                    }

                }
            }
        }
        return ['residue' => $residue ?? [], 'residueCategory' => $residueCategory ?? []];
    }
    private function productsData(TransferProduct $transferProduct, &$products)
    {
        $products[$transferProduct->product->id] = (object)[
            'id' => $transferProduct->product_id,
            'amount' => $transferProduct->quantity,
            'category_id' => $transferProduct->product->category_id,
            'units' => $transferProduct->product->type,
            'name' => $transferProduct->product->current->first()->name,
            'residue' => $transferProduct->quantity,
        ];
    }

    private function categoriesData(TransferProduct $transferProduct, &$categories)
    {
        $category = $transferProduct->product->category;
        $categoryAmount = isset($categories[$category->id]) ?
            $categories[$category->id]->amount += $transferProduct->quantity : $transferProduct->quantity;

        $categories[$transferProduct->product->category->id] = (object)[
            'id' => $category->id,
            'amount' => $categoryAmount,
            'name' => $category->current->name,
            'residue' => $categoryAmount
        ];
    }
    private function nextOrganizationData(Transfer $nextTransfer, &$nextOrganizationData, $index, &$countSystem, &$transfersAllow = [], &$lines = [], $productsAllow = [])
    {
        if ($nextTransfer) {
            $amount = 0;
            $lineProducts = [];
            $productsAllowNext = [];
            foreach ($nextTransfer->products as $transferProduct) {
                if(\Organisation::isDonor()){
                    if(in_array($transferProduct->transfer_product_id, $productsAllow)){
                        $amount += $transferProduct->quantity;
                    }
                }else{
                    $amount += $transferProduct->quantity;
                }

                if ($index == 0) {
                    $lineProducts['product-' . $transferProduct->product_id] = 'product-' . $transferProduct->product_id;
                    $lineProducts['category-' . $transferProduct->product->category_id] = 'category-' . $transferProduct->product->category_id;
                }
                $productsAllowNext[$transferProduct->id] = $transferProduct->id;

            }
            $transfersAllow[] = $nextTransfer->id;

            $linesCurrent = [];
            if ($nextTransfer->human_id == null) {
                /** @var Transfer $nextTransfers */
                $nextTransfers = Transfer::getNextTransfer($nextTransfer->recipient_id, $productsAllowNext);
                foreach ($nextTransfers as $item) {
                    //if(!in_array($item->id, $transfersAllow)) {
                    $this->nextOrganizationData($item, $nextOrganizationData, $index + 1, $countSystem, $transfersAllow, $lines, $productsAllowNext);
                    // }
                    $linesCurrent['nextTransfer-' . $item->id] = 'nextTransfer-' . $item->id;
                }
            }

            $isPeople = false;
            if ($nextTransfer->recipient->id == $nextTransfer->sender->id and $nextTransfer->human) {
                $nameHuman = $nextTransfer->human->name;
                $isPeople = true;
                $linesCurrent['people-' . $nextTransfer->id] = 'people-' . $nextTransfer->id;
            } else {
                $linesCurrent['nextTransfer-' . $nextTransfer->id] = 'nextTransfer-' . $nextTransfer->id;

            }
            if ($index < 1) {
                $linesCurrent = array_merge($linesCurrent, $lineProducts);
            }

            $linesСurrentResult = array_merge($linesCurrent, $lines);
            $lines = $linesCurrent;
            $countSystemTaken = [
                'transferProductsIds' =>[],
            ];
            [$amountNew, $residue, $countSystem] = $this->getCountsByTransfer($nextTransfer, $productsAllow, $nextTransfer->recipient, $countSystem, $index);
            $this->getOnlineBalanceByTransfer($nextTransfer, $nextTransfers ?? [], $nextTransfer->recipient, $productsAllow, $countSystemTaken, -1);
            $online = 0;
            foreach ($countSystemTaken['categories'] ?? [] as $obj){
                $online += $obj;
            }
            $countSystemTaken = [
                'transferProductsIds' =>[],
            ];
            //dd($online);
            $nextOrganizationData[$index][$nextTransfer->id] = [
                'id' => $nextTransfer->recipient_id,
                'transfer' => $nextTransfer,
                'senderId' => $nextTransfer->sender_id,
                'name' => $isPeople ? $nameHuman : $nextTransfer->recipient->name,
                'human' => $isPeople ? $nextTransfer->human->toArray() : [],
                'isPeople' => $isPeople ?? false,
                'taken' => $index > 0 ? $amount : $amountNew,
                'status' => $nextTransfer->status,
                'residue' => $residue,
                'onlineBalance' => $online,
                'lines' => '/' . implode('/', $linesСurrentResult) . '/',
            ];
        }

    }
    private function getOnlineBalanceByTransfer(Transfer $transfer, $nextTransfers, Organisation $organisation, $productsAllow, &$countSystemTaken, $index){

        if ($transfer->human_id == null) {
            foreach ($nextTransfers as $item) {
                $productsAllowNext = [];
                foreach ($transfer->products as $transferProduct) {
                    $productsAllowNext[$transferProduct->id] = $transferProduct->id;
                }
                $nextTransfersSend = Transfer::getNextTransfer($item->recipient_id, $productsAllowNext);
                $this->getOnlineBalanceByTransfer($item, $nextTransfersSend, $organisation, $productsAllowNext, $countSystemTaken, $index +1);
            }
        }
        [$val, $val2, $countSystemTaken] = $this->getCountsByTransfer($transfer, $productsAllow, $transfer->recipient, $countSystemTaken, $index);

        return $countSystemTaken;
    }
    private function getCountsByTransfer(Transfer $transfer, $productsAllow, Organisation $organisation, $countSystem, $index = 0): array
    {
        $residue = 0;
        $amount = 0;
        $warehouseProducts = [];
        if($organisation->warehouse){
            $organisation->warehouse->warehouseProducts->map(function (WarehouseProduct $product) use (&$warehouseProducts) {
                $warehouseProducts[$product->transfer_product_id] = $product;
            });
        }

        $products = $transfer->products;
        foreach ($products as $product) {
            if(\Organisation::isDonor()){
                if(in_array($product->transfer_product_id, $productsAllow)){
                    $amount += $product->quantity;
                    if (isset($warehouseProducts[$product->id])) {
                        $residue += $warehouseProducts[$product->id]->quantity;
                    }
                }
            }else{
                $amount += $product->quantity;
                if (isset($warehouseProducts[$product->id])) {
                    $residue += $warehouseProducts[$product->id]->quantity;
                }
            }
            if(in_array($product->transfer_product_id,$productsAllow)){

                $countSystem['products'][$product->product_id] = $countSystem['products'][$product->product_id] ?? 0;
                $countSystem['categories'][$product->product->category_id] = $countSystem['categories'][$product->product->category_id] ?? 0;


                if (!in_array($product->id, $countSystem['transferProductsIds'])) {
                    if ($transfer->human_id and $transfer->human->confirmed) {
                        if ($index != 0) {
                            $countSystem['products'][$product->product_id] -= $product->quantity;
                            $countSystem['categories'][$product->product->category_id] -= $product->quantity;
                        }
                    } else {
                        if($index == 0 ){
                            $countSystem['products'][$product->product_id] += $product->quantity;
                            $countSystem['categories'][$product->product->category_id] += $product->quantity;
                        }

                    }
                    $countSystem['transferProductsIds'][$product->id] = $product->id;
                }
            }
        }

        return [$amount, $residue, $countSystem];

    }
    public function getListProducts($id){
        $this->popupListProducts = true;
        $this->popupListProductsId = $id;
    }
    public function closePopup(){
        $this->popupListProducts = false;
        $this->popupListProductsId = 0;
    }

    public function openCategory(int $categoryId)
    {
        $this->openCategoryId = $categoryId;
    }

    public function closeCategory()
    {
        $this->openCategoryId = 0;
    }
}
