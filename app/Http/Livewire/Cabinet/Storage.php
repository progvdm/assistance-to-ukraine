<?php

namespace App\Http\Livewire\Cabinet;

use App\Foundation\FlashMessage;
use App\Helper\FileUploader;
use App\Helper\ImageUploader;
use App\Models\Categories\Category;
use App\Models\User;
use App\Models\Warehouses\Warehouse;
use Livewire\Component;
use Livewire\WithFileUploads;

class Storage extends Component
{

    public $filter = [
        'name' => '',
        'category' => '',
    ];

    public function mount()
    {
    }

    public function render()
    {
        return view('livewire.cabinet.storage', ['products' => $this->getListProducts(), 'categories' => Category::all()]);
    }

    public function getListProducts()
    {
        $collection = collect([]);
        /** @var Warehouse $warehouse */
        $warehouse = \Organisation::getWarehouse();
        if($warehouse and $warehouse->warehouseProducts){
            $collection = $warehouse->warehouseProducts();
            if($text = $this->filter['name']){
                $collection->whereHas('product', function($query) use ($text){
                    $query->whereHas('product', function ($queryTransferProducts) use ($text){
                        $queryTransferProducts->where(function($query) use ($text){
                            $query->orWhere('code', 'LIKE', '%'.$text.'%');
                            $query->orWhereHas('current', function ($queryCurrent)  use ($text){
                                $queryCurrent->where('name', 'LIKE', '%'.$text.'%');
                            });
                        });
                    });
                });
            }
            if($category = $this->filter['category']){
                $collection->whereHas('product', function($query) use ($category){
                    $query->whereHas('product', function ($queryTransferProducts) use ($category){
                        $queryTransferProducts->where('category_id', '=', $category);
                    });
                });
            }

            $collection = $collection->get();
        }
        return $collection;
    }
}
