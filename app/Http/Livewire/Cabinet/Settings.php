<?php

namespace App\Http\Livewire\Cabinet;

use App\Foundation\FlashMessage;
use App\Helper\FileUploader;
use App\Helper\ImageUploader;
use App\Models\Organisation\Organisation;
use App\Models\User;
use App\Rules\Auth\EmailExistsRule;
use App\Rules\Auth\EmailUniqueRule;
use App\Services\NovaPoshta\NovaPoshtaService;
use App\Services\NovaPoshta\NPService;
use App\Traits\TabsLivewire;
use Livewire\Component;
use Livewire\WithFileUploads;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;

class Settings extends Component
{
    use WithFileUploads;
    use TabsLivewire;

    public array $organisation = [];
    public array $user = [];


    public array  $verification          = [];

    public  string $uploadedDescriptionFile  = '';
    public  string $uploadedPhoto            = '';

    public bool $showConfirm                = false;

    protected $listeners = ['setStorageCity' => 'setStorageCity', 'changeTiny' => 'changeTiny'];

    public function setStorageCity($params){
        foreach ($params as $key => $param){
            $array = explode('.', $key);
            if(count($array) == 3){
                $this->{$array[0]}[$array[1]][$array[2]] = $param;
            }elseif (count($array) == 2){
                $this->{$array[0]}[$array[1]] = $param;
            }
        }
        $this->showConfirm = true;
    }
    public function changeTiny($field, $value){
        $array = explode('.', $field);
        if(count($array) == 3){
            $this->{$array[0]}[$array[1]][$array[2]] = $value;
        }elseif (count($array) == 2){
            $this->{$array[0]}[$array[1]] = $value;
        }
        $this->showConfirm = true;
    }
    public function mount()
    {
        $this->fillForm();
    }

    public function render()
    {
        $locales = collect(LaravelLocalization::getSupportedLocales());
        $locales = $locales->sortBy(function ($locale, $key) {
            return $key == app()->getLocale() ? 0 :1;
        })->map(function ($locale){
            return ['name' => $locale['native']];
        });
        return view('livewire.cabinet.settings', ['locales' => $locales]);
    }

    public function submit()
    {
        $data = $this->validate(
            $this->rules(),
            $this->messages(),
            $this->attributes()
        );
        \Organisation::reloadData();
        $organisation = \Organisation::getOrganisation();

        if(isset($this->organisation['description_file_upload']) and $this->organisation['description_file_upload'] != ""){
            $this->uploadFile($organisation);
            unset($this->organisation['description_file']);

        }

        if(isset($this->organisation['photoUpload']) and $this->organisation['photoUpload'] != ""){
            $this->uploadPhoto($organisation);
            unset($this->organisation['photo']);
        }
        $this->organisation['website'] = trim($this->organisation['website']);
        $this->organisation['facebook'] = trim($this->organisation['facebook']);
        $this->organisation['instagram'] = trim($this->organisation['instagram']);
        $this->organisation['additional_email'] = trim($this->organisation['additional_email']);
        $this->organisation['additional_phone'] = trim($this->organisation['additional_phone']);

        $organisation->fill($this->organisation);
        $organisation->save();
        foreach (LaravelLocalization::getSupportedLocales() as $locale => $supportedLocale) {
            $this->organisation[$locale]['text'] = str_replace('../storage', '/storage', $this->organisation[$locale]['text']);
            $organisation->translateOrNew($locale)->fill($this->organisation[$locale])->save();
        }

        /** @var User $user */
        $user = auth()->user();
        $user->update($this->user);

        FlashMessage::setMessage(FlashMessage::SUCCESS, __('cabinet.Success settings'));
        $this->redirect(redirect()->back()->getTargetUrl());
    }
    public function updated(){
        $this->showConfirm = true;
    }

    public function uploadFile($organisation)
    {
        $uploader = new FileUploader();
        $organisation->description_file = $uploader->uploadOne($this->organisation['description_file_upload'], 'user_files',);
        $organisation->save();
    }

    public function uploadPhoto($organisation)
    {
        if($organisation->photo){
            $organisation->deleteImage();
        }

        $uploader = new ImageUploader();

        $organisation->photo = $uploader->uploadOne($this->organisation['photoUpload'], $organisation);
        $organisation->save();
    }

    public function fillForm()
    {
        \Organisation::reloadData();
        /** @var Organisation $organisation */
        $organisation = \Organisation::getOrganisation();

        $organisation = $organisation->toArray();
        foreach ($organisation['translations'] as $translate){
            $organisation[$translate['locale']] = $translate;
        }
        unset($organisation['translations']);

        $this->organisation = $organisation;
        /** @var User $user */
        $user = auth()->user();
        $this->user = $user->toArray();
        $this->user['telegramToken'] = $user->generateTelegramToken();
        $this->user['telegram_user_name'] = $user->telegram_user_name;
        //dd($this->organisation, $this->user);
        $this->verification = empty($this->user->verification) ?
            ['sanction' => false, 'founderRu' => false, 'criminalCourts' => false] :  $this->user->verification;
    }

    public function rules(): array
    {
        $organisation = [
            'organisation.zip_code'              => 'nullable|string',
            'organisation.id_code'               => 'nullable|string',
            'organisation.additional_email'      => ['nullable','string','email'],
            'organisation.additional_phone'      => ['nullable','regex:/^([0-9]{0,12})?(\+[0-9]{12})?$/'],
            'organisation.facebook'              => 'nullable|string',
            'organisation.instagram'             => 'nullable|string',
            //'organisation.photo'                 => 'nullable|image|mimes:webp,jpg,png,jpeg',
            'organisation.website'               => 'nullable|string',
            'organisation.description_file'      => 'nullable',
            'organisation.open_info'             => 'nullable|integer'
        ];
        foreach (LaravelLocalization::getSupportedLocales() as $key => $language){
            $organisationLang = [
                'organisation.'.$key.'.country' => 'required|string',
                'organisation.'.$key.'.city' => 'required|string',
                'organisation.'.$key.'.street' => 'nullable|string',
                'organisation.'.$key.'.build' => 'nullable|string',
                'organisation.'.$key.'.storage_city' => 'nullable|string',
                'organisation.'.$key.'.storage_street' => 'nullable|string',
                'organisation.'.$key.'.storage_build' => 'nullable|string',

                'organisation.'.$key.'.text' => 'nullable|string',
                'organisation.'.$key.'.text_short' => 'nullable|string',
            ];
            $organisation = array_merge($organisation, $organisationLang);
        }


        $user = [
            'user.name'                  => 'required|string',
            'user.email'                 => ['required','string','email', new EmailUniqueRule],
            'user.notification_phone'    => ['nullable', 'phone'],
            'user.phone'                 => ['required', 'phone'],
        ];


        return $organisation + $user;

        /* return [
            // Organisation



             // User

           /*
             'zip_code'              => 'nullable|string',
             'id_code'               => 'nullable|string',
             'name'     => 'nullable|string',
             'country'               => 'nullable|string',
             'city'                  => 'nullable|string',
             'street'                => 'nullable|string',
             'build'                 => 'nullable|string',
             'storage_city'          => 'nullable|string',
             'storage_street'        => 'nullable|string',
             'storage_build'         => 'nullable|string',
             'name'                  => 'nullable|string',
             'photo'                 => 'nullable|image|mimes:webp,jpg,png,jpeg',
             'text_short'            => 'nullable|string',
             'text'                  => 'nullable|string',
             'phone'                 => ['required', 'regex:/^([0-9]{0,12})?(\+[0-9]{12})?$/'],
             'notification_phone'    => ['nullable', 'regex:/^([0-9]{0,12})?(\+[0-9]{12})?$/'],
             'notification_phone_check'=> 'nullable|string',
             'additional_phone'      => ['regex:/^([0-9]{0,12})?(\+[0-9]{12})?$/'],
             'website'               => 'nullable|string',
             'email'                 => ['required','string','email', new EmailUniqueRule],
             'additional_email'      => 'nullable|string',
             'facebook'              => 'nullable|string',
             'instagram'             => 'nullable|string',
             'number_cells'          => 'nullable|integer',
             'open_info'             => 'nullable|integer',
             'description_file'      => 'nullable',

        ];*/
    }

    public function attributes(): array
    {
        return [
            'zip_code' => __('auth.form.zip_code'),
            'phone' => __('auth.form.phone'),
            'id_code' => __('auth.form.id_code'),
            'name' => __('auth.form.name'),
            'country' => __('auth.form.country'),
            'city' => __('auth.form.city'),
            'street' => __('auth.form.street'),
            'name' => __('auth.form.name'),
            'photo' => __('auth.form.photo'),
            'text' => __('auth.form.text'),
        ];
    }

    public function messages():array
    {
        return [
            'regex' => __('auth.form.messages.Enter valid :attribute!'),
        ];
    }

}
