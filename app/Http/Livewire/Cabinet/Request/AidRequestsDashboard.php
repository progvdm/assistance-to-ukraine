<?php

namespace App\Http\Livewire\Cabinet\Request;

use App\Foundation\FlashMessage;
use App\Models\AidRequest\AidRequest;
use App\Models\AidRequest\AidRequestLeed;
use App\Notifications\AidRequestApplicantNotification;
use Carbon\Carbon;
use Livewire\Component;
use Livewire\WithPagination;

class AidRequestsDashboard extends Component
{
    use WithPagination;

    public int $openId = 0;
    public int $selectLeed = 0;

    // flash message
    public $message = [];

    public function mount(){
        if(request()->has('aid_request_id')){
            $this->openId = request()->get('aid_request_id');
            $this->selectLeed = request()->has('leed_id') ? request()->get('leed_id') : 0;
        }
    }
    public function render()
    {
        $collection = AidRequest::whereDelete(false)->whereOrganisationId(\Organisation::getOrganisationId())->orderByDesc('id')->paginate(10);
        return view('livewire.cabinet.request.aid-requests-dashboard', ['collection' => $collection]);
    }

    public function edit($id){
        return $this->redirect(route('cabinet.aid-requests.update', $id));
    }

    public function delete($id){
        $aidRequest = AidRequest::whereId($id)->whereOrganisationId(\Organisation::getOrganisationId())->firstOrFail();
        $aidRequest->delete = true;
        $aidRequest->delete_at = Carbon::now();
        $aidRequest->save();

        FlashMessage::setMessage(FlashMessage::SUCCESS, __('aid-request.dashboard.Application deleted!'));
        return $this->redirect(route('cabinet.aid-requests.index'));
    }

    public function paginationView()
    {
        return 'livewire.components.pagination';
    }

    public function approveLeed($selectLeed){
        $leed = AidRequestLeed::findOrFail($selectLeed);
        $leed->status = AidRequestLeed::APPROVE;
        $leed->save();
        $leed->notify(new AidRequestApplicantNotification());
        $this->message['success'] = __('Success');
    }

    public function rejectedLeed($selectLeed){
        $leed = AidRequestLeed::findOrFail($selectLeed);
        $leed->status = AidRequestLeed::REJECTED;
        $leed->save();
        $leed->notify(new AidRequestApplicantNotification());

        $this->message['success'] = __('Success');
    }
}
