<?php

namespace App\Http\Livewire\Cabinet\Request;

use App\Enums\AidRequest\AidRequestTypesEnum;
use App\Enums\Cargo\ProductTypeEnum;
use App\Foundation\FlashMessage;
use App\Models\AidRequest\AidRequest;
use App\Models\AidRequest\AidRequestProduct;
use App\Models\Categories\Category;
use App\Models\Lists\ListCategory;
use App\Models\Lists\ListProduct;
use App\Models\Lists\ListProductTranslation;
use App\Models\Organisation\OrganisationType;
use App\Traits\TabsLivewire;
use Carbon\Carbon;
use Illuminate\Contracts\Database\Eloquent\Builder;
use Livewire\Component;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;

class AidRequestsItem extends Component
{
    use TabsLivewire;

    public int $aidId = 0;
    public bool $edit = false;
    public bool $showConfirm = false;
    public bool $showSelectProduct = false;
    public bool $published = true;
    public string $publication_date = '';
    public string $active_date = '';
    public string $type = '';
    public int $own_number = 0;
    public int $idSystem = 0;
    public string $category_id = '';

    public string $type_organisation_id = '';
    public array $product_name = [];
    public string $count_type = '';
    public string $count = '1';
    public array $buildDescription = [
        'uk' => '',
        'en' => ''
    ];

    // Contacts
    public string $contact_email = '';
    public string $contact_phone = '';
    public string $contact_social = '';

    // flash message
    public $message = [];

    public function mount($id = null){
        $this->contact_email = \Organisation::getAuthUser()->email;
        $this->type = $this->type ?? AidRequestTypesEnum::OFFER;
        $this->own_number = AidRequest::whereOrganisationId(\Organisation::getOrganisationId())->count() + 1;
        $this->idSystem = (new AidRequest())->numberSystem();

        if($id){
            $this->aidId = $id;
            $this->edit = true;
            $this->loadData();
        }
    }

    public function render()
    {
        $organisationTypes = OrganisationType::getListSelect();
        $organisation = $organisationTypes->where('id', $this->type_organisation_id)->first();
        $categories = Category::getForSelect();
        $buildDescriptionUk = [
            'organisation' => ($this->type_organisation_id and $organisation) ? $organisation->translate('uk')->name : ':organisation',
            'category' => $categories->find($this->category_id) ? mb_strtolower($categories->find($this->category_id)->getNameForLang('uk')) : ':category',
            'product' => $this->product_name['uk'] ?? ':product',
            'count' => $this->count,
            'type' => ProductTypeEnum::get($this->count_type) ?? ':type',
            'offer_or_need' => $this->type ? ($this->type == 'offer' ? 'пропонує' : 'потребує') : ':type',
        ];

        $buildDescriptionEn = [
            'organisation' => ($this->type_organisation_id and $organisation) ? $organisation->translate('en')->name : ':organisation',
            'category' => $categories->find($this->category_id) ? mb_strtolower($categories->find($this->category_id)->getNameForLang('en')) : ':category',
            'product' => $this->product_name['en'] ?? ':product',
            'count' => $this->count,
            'type' => ProductTypeEnum::getEn($this->count_type) ?? ':type',
            'offer_or_need' => $this->type ? ($this->type == 'offer' ? 'offers' : 'needs') : ':type',
        ];

        $this->buildDescription['uk'] = (string)__(':organisation :offer_or_need :category, а саме :product у кількості :count :type.', $buildDescriptionUk);
        $this->buildDescription['en'] = (string)__(':organisation :offer_or_need :category, namely :product in quantity :count :type.', $buildDescriptionEn);
        $this->buildDescription['uk'] = str_replace(' інше, а саме', '', $this->buildDescription['uk']);
        $this->buildDescription['en'] = str_replace(' other, namely', '', $this->buildDescription['en']);
        return view('livewire.cabinet.request.aid-requests-item', [
            'productsList' => $productsList ?? [],
            'groupsList' => $groupsList ?? [],
            'categories' => $categories,
            'organisationTypes' => $organisationTypes,
            'productTypes' =>  ProductTypeEnum::all(),
        ]);
    }

    public function loadData(){
        $aidRequest = AidRequest::findOrFail($this->aidId);
        $this->published = $aidRequest->published;
        $this->type = $aidRequest->type;
        $this->own_number = $aidRequest->own_number;
        $this->publication_date = $aidRequest->publication_date->format('Y-m-d');
        $this->active_date = $aidRequest->active_date->format('Y-m-d');
        $this->category_id = (string)$aidRequest->category_id;
        $this->type_organisation_id = $aidRequest->organisation_type_id;
        $this->count = $aidRequest->count;
        $this->count_type = $aidRequest->count_type;
        $this->contact_email = $aidRequest->contact_email;
        $this->contact_phone = $aidRequest->contact_phone;
        $this->contact_social = $aidRequest->contact_social;
        foreach ($aidRequest->translations as $item){
            $this->product_name[$item->locale] = $item->name;
        }

        $this->idSystem = $aidRequest->numberSystem();
    }

    public function updated($propertyName){
        $this->showConfirm = true;
        $this->validateOnly($propertyName);
    }

    public function submit(){
        $this->validate(
            $this->rules(),
            $this->messages(),
            $this->attributes(),
        );
        $countAidRequests = AidRequest::whereOrganisationId(\Organisation::getOrganisationId())->count() + 1;
        if($this->edit){
            $aidRequest = AidRequest::findOrFail($this->aidId);
        }else{
            $aidRequest = new AidRequest();
            $aidRequest->own_number = $countAidRequests;
            $aidRequest->organisation_id = \Organisation::getOrganisationId();

        }
        $aidRequest->published = $this->published;
        $aidRequest->type = $this->type;
        $aidRequest->publication_date = Carbon::parse($this->publication_date)->format('Y-m-d H:i:s');
        $aidRequest->active_date = Carbon::parse($this->active_date)->format('Y-m-d H:i:s');
        $aidRequest->category_id = $this->category_id;
        $aidRequest->organisation_type_id = $this->type_organisation_id;
        $aidRequest->count = $this->count;
        $aidRequest->count_type = $this->count_type;
        $aidRequest->contact_email = $this->contact_email;
        $aidRequest->contact_phone = $this->contact_phone;
        $aidRequest->contact_social = $this->contact_social;
        $aidRequest->save();
        foreach (LaravelLocalization::getSupportedLocales() as $locale => $item){
            $aidRequest->translateOrNew($locale)->fill([
                    'name' => $this->product_name[$locale],
                    'description' => $this->buildDescription[$locale],
                    'aid_request_id' => $aidRequest->id
                ])->save();
        }

        FlashMessage::setMessage(FlashMessage::SUCCESS, __('Success'));
        $this->redirectRoute('cabinet.aid-requests.update', $aidRequest);
    }

    public function rules(){
        // прописать валидацию типа заявки
        $rules =  [
            'type' => ['required', 'string'],
            'publication_date' => ['required', 'string'],
            'active_date' => ['required', 'string'],
            'category_id' => ['required', 'integer'],
            'type_organisation_id' => ['required', 'integer'],
            'count' => ['integer','required'],
            'count_type' => ['required', 'string'],
            'contact_email' => ['required', 'string', 'email'],
            'contact_phone' => ['regex:/^([0-9]{0,12})?(\+[0-9]{12})?$/'],
            'contact_social' => 'nullable|string',
        ];
        foreach (LaravelLocalization::getSupportedLocales() as $lang => $item){
            $rules['product_name.'.$lang] = 'required|string|max:50';
        }
        return $rules;
    }
    public function messages(){
        return [
            'count.required' => __('validation.required or integer')
        ];
    }
    public function attributes(){
        $attributes = [
            'contact_email' => __('aid-request.validate.Email'),
            'contact_phone' => __('aid-request.validate.Phone'),
            'contact_social' => __('aid-request.validate.Social'),
        ];
        foreach (LaravelLocalization::getSupportedLocales() as $lang => $item){
            $attributes['description.'.$lang] = __('aid-request.validate.Description');
        }
        return $attributes;
    }
}
