<?php

namespace App\Http\Livewire\Cabinet\Request;

use App\Models\AidRequest\AidRequest;
use Livewire\Component;

class AidRequestPreview extends Component
{
    public $aidId;
    public function render()
    {
        $aidRequest = AidRequest::findOrFail($this->aidId);
        return view('livewire.cabinet.request.aid-request-preview', ['aidRequest' => $aidRequest]);
    }
}
