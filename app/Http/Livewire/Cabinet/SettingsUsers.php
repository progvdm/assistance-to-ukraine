<?php

namespace App\Http\Livewire\Cabinet;

use App\Models\User;
use App\Notifications\RegistrationUserNotification;
use App\Rules\Auth\EmailUniqueRule;
use App\Services\Mail\EmailTokenService;
use Illuminate\Support\Facades\Hash;
use Livewire\Component;
use Str;

class SettingsUsers extends Component
{
    // flash message
    public $message = [];

    protected $listeners = ['userPopupAction' => 'userPopupAction', 'userListUpdate' => '$refresh'];

    public function render()
    {
        $users = User::where('id', '!=', \Organisation::getAuthUser()->id)->where('organisation_id', \Organisation::getOrganisationId())->get();

        return view('livewire.cabinet.settings-users', ['users' => $users]);
    }

    public function updated(){
        unset($this->message['error'], $this->message['success'], $this->message['info']);
    }

    public function userPopupAction($messages){
        $this->message = $messages;
    }

    public function deleteUser($userId){
        $user = User::find($userId);
        if($user and $user->organisation_id != \Organisation::getOrganisationId()){
            $this->message['error'] = '403 forbidden';
        }else{
            $user->delete();
            $this->message['success'] = 'User deleted!';
        }
    }

}
