<?php

namespace App\Http\Livewire\Cabinet;

use App\Models\NovaPoshta\NovaPoshtaCity;
use App\Services\NovaPoshta\NovaPoshtaService;
use Asantibanez\LivewireSelect\LivewireSelect;
use Illuminate\Support\Collection;

class SelectCities extends LivewireSelect
{
    public Collection $optionsList;
    /**
     * @param null $searchTerm
     * @return Collection
     */
    // In CarModelSelect component
    public function options($searchTerm = null) : Collection
    {


        if ($searchTerm && strlen($searchTerm) > 2) {
            $cities = [];
             $search = NovaPoshtaCity::where('description', 'LIKE', $searchTerm.'%')->get();
            foreach ($search as $city){
                $cities[] = ['value' => addcslashes($city->description, "'"), 'description' => $city->description];
            }
            $this->optionsList = collect($cities);
            return $this->optionsList;
        }

        return collect([
            ['value' => 'No select', 'description' => 'No select'],
        ]);
    }
    public function selectedOption($value){
        $this->emit('setStorageCity', [$this->name => stripcslashes($value)]);
        return ['value' => stripcslashes($value), 'description' => stripcslashes($value)];
    }

    public function styles()
    {
        return [
            'default' => 'p-2 rounded border w-full appearance-none',

            'searchSelectedOption' => 'newPost-select__option--selected',
            'searchSelectedOptionTitle' => '_text-left',
            'searchSelectedOptionReset' => 'newPost-select__option--reset',

            'search' => 'relative',
            'searchInput' => 'newPost-select__option--input',
            'searchOptionsContainer' => 'newPost-select__options',

            'searchOptionItem' => 'newPost-select__option',
            'searchOptionItemActive' => 'bg-indigo-600 text-white font-medium',
            'searchOptionItemInactive' => 'bg-white text-gray-600',

            'searchNoResults' => 'p-8 w-full bg-white border text-center text-xs text-gray-600',
        ];
    }
}
