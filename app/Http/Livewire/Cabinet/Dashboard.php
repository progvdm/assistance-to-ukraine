<?php

namespace App\Http\Livewire\Cabinet;

use App\Dto\Cargo\TransferFilesDto;
use App\Enums\Cargo\CargoStatusesEnum;
use App\Enums\Users\OrganisationTypeEnum;
use App\Models\Cargos\TransferInformations;
use App\Models\Organisation\Organisation;
use App\Models\Transfers\Transfer;
use App\Models\Transfers\TransferDocument;
use App\Models\Transfers\TransferFiles;
use App\Models\Transfers\TransferProduct;
use App\Models\User;
use App\Models\Warehouses\Warehouse;
use App\Models\Warehouses\WarehouseProduct;
use App\Services\Transfer\DocumentService;
use Illuminate\Support\Facades\Session;
use Livewire\Component;
use Livewire\WithFileUploads;
use Livewire\WithPagination;

class Dashboard extends Component
{
    use WithFileUploads;
    use WithPagination;

    public string $typeTransfer = '';
    public array $data = [];
    public array $allProducts = [];
    public int $openCategoryId = 0;
    public ?User $user = null;


    public array $transferInfo = [];
    public bool $popupPhoto = false;
    public bool $popupVideo = false;
    public bool $popupFiles = false;
    public bool $filesPopupHuman = false;
    public bool $popupListProducts = false;
    public bool $popuplocations = false;
    public $photos = null;

    protected $listeners = ['messageEvent' => 'messageEvent'];

    // flash message
    public array $message = [];


    public function mount()
    {
        $this->typeTransfer = Session::get('typeTransfer', 'accepted');
    }

    public function render()
    {
        $transfers = Transfer::getListTransfer(null, $this->typeTransfer);
        return view('livewire.cabinet.dashboard', ['transfers' => $transfers]);
    }
    public function messageEvent($type, $message){
        $this->message[$type] = $message;
    }

    public function changeTab($tab){
        $this->message = [];
        $this->resetPage();
        $this->typeTransfer = $tab;
        session()->put('typeTransfer', $tab);
    }

    public function paginationView()
    {
        return 'livewire.components.pagination';
    }
}
