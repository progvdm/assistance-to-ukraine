<?php

namespace App\Http\Livewire\Auth;

use App\Foundation\FlashMessage;
use App\Models\User;
use App\Services\Auth\RestorePasswordService;
use App\Services\Mail\EmailTokenService;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\Hash;
use Livewire\Component;

class Restore extends Component
{
    public string $password                    = "";
    public string $password_confirmation       = "";
    public ?User  $user                        = null;

    public function mount($userId)
    {
        $this->user = User::find($userId);
    }

    public function render(): Factory|View|Application
    {
        return view('livewire.auth.restore');
    }

    public function submit()
    {
        $credentials  = $this->validate(
            $this->rules(),
            $this->messages(),
            $this->attributes()
        );

        $this->user->password = Hash::make($credentials['password']);
        $this->user->save();

        $service = new EmailTokenService();
        $service->deleteTokens($this->user);

        FlashMessage::setMessage(FlashMessage::SUCCESS, __('auth.Success restore password'));
        $this->redirect(routeLocale('home'));
    }

    public function rules(): array
    {
        return [
            'password'              => 'required|confirmed|string|min:4|max:12',
        ];
    }

    public function attributes(): array
    {
        return [
            'password' => __('auth.form.password'),
        ];
    }

    public function messages(): array
    {
        return [
            'password.required' => __('auth.form.messages.required.password'),
            'password.confirmed' => __('auth.form.messages.confirmed_password'),
        ];
    }
}
