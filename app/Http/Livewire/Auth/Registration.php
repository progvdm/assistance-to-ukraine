<?php

namespace App\Http\Livewire\Auth;

use App\Enums\Users\OrganisationTypeEnum;
use App\Foundation\FlashMessage;
use App\Models\Organisation\Organisation;
use App\Models\Organisation\OrganisationTranslation;
use App\Models\User;
use App\Models\Warehouses\Warehouse;
use App\Notifications\VerifyEmailNotification;
use App\Rules\Auth\EmailUniqueRule;
use App\Services\Mail\EmailTokenService;
use App\Services\OpenDataBot\OpenDataBotService;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Auth\StatefulGuard;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Livewire\Component;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;

class Registration extends Component
{
    public array $user             = [];
    public array $organisation     = [];
    public string $type_user     = '';
    public string $oferta     = '';


    //Check
    public array $verification           = [];

    public function render(): Factory|View|Application
    {
        return view('livewire.auth.registration');
    }

    public function mount($type = null)
    {
        if($type){
            $this->type_user = $type;
            $this->organisation['type'] = $type;
        }
        $this->userTypes = OrganisationTypeEnum::all();
    }

    public function submit()
    {
        $this->resetErrorBag();
        $data = $this->validate(
            $this->rules(),
            $this->messages(),
            $this->attributes()
        );

        if($this->guard()->check())
        {
            $this->redirect(routeLocale('cabinet.home'));
        }
        $service = new OpenDataBotService();

        if($data['organisation']['type'] == OrganisationTypeEnum::RECIPIENT and !$service->getCompanyData($data['organisation']['id_code'])){
            $this->addError('organisation.id_code', __('auth.No organization with such EDRPOU was found!'));
        }else{
            /** @var Organisation $organisation */
            /** @var User $user */
            [$organisation, $user] = $this->create($data);
            $this->createWarehouse($organisation);

            $emailTokenService = new EmailTokenService();
            $link = routeLocale('verify-email', [$emailTokenService->token($user)]);
            $user->notify(new VerifyEmailNotification($link));

            FlashMessage::setMessage(FlashMessage::SUCCESS, __('auth.send_verify_email'));
            $this->redirect(redirect()->back()->getTargetUrl());
        }

    }

    public function create($registerData)
    {
        $registerData['user']['password'] = Hash::make($registerData['user']['password']);
        $user = User::create($registerData['user']);
        $organisationData = $registerData['organisation'];
        $organisation = new Organisation();
        $organisation->published = true;
        $organisation->view_site = false;
        $organisation->view_system = false;
        $organisation->founder_id = $user->id;
        $organisation->zip_code = $organisationData['zip_code'];
        $organisation->id_code = $organisationData['id_code'];
        $organisation->type = $organisationData['type'];
        $organisation->verification = json_encode($organisationData['verification'] ?? []);
        $organisation->save();
        foreach (LaravelLocalization::getSupportedLocales() as $locale => $supportedLocale) {
            /** @var OrganisationTranslation $organisationTranslate */
            $organisationTranslate = $organisation->translateOrNew($locale);
            $organisationTranslate->organisation_id = $organisation->id;
            $organisationTranslate->name = $organisationData['name'];
            $organisationTranslate->country = $organisationData['country'];
            $organisationTranslate->city = $organisationData['city'];
            $organisationTranslate->street = $organisationData['street'];
            $organisationTranslate->build = $organisationData['build'];
            $organisationTranslate->save();
        }
        $user->organisation_id = $organisation->id;
        $user->save();

        return [$organisation, $user];
    }

    public function createWarehouse($organisation)
    {
        $warehouse = new Warehouse();
        $warehouse->organisation_id = $organisation->id;
        $warehouse->save();
    }

    public function substituteCompanyData()
    {
        $this->resetErrorBag('organisation.id_code');
        $this->validateOnly('organisation.id_code');
        if(!isset($this->organisation['id_code']) or empty($this->organisation['id_code']))
        {
            return;
        }

        $service = new OpenDataBotService();
        $data = $service->getCompanyData($this->organisation['id_code'], 'criminal');
        if($data)
        {
            $this->user['name'] = $data->user_name;
            $this->organisation['name'] = $data->name;
            $this->organisation['country'] = $data->location['country'];
            $this->organisation['city'] = $data->location['city'];
            $this->organisation['street'] = $data->location['street'];
            $this->organisation['build'] = $data->location['build'];
            $this->organisation['zip_code'] = $data->location['zip'];
            $this->organisation['verification']['sanction'] = $data->sanction;
            $this->organisation['verification']['founderRu'] = $data->founderRu;
            $this->organisation['verification']['criminalCourts'] = $data->count_courts;
        }elseif(isset($this->organisation['type']) and $this->organisation['type'] == OrganisationTypeEnum::RECIPIENT){
            $this->addError('organisation.id_code', __('auth.No organization with such EDRPOU was found!'));
        }
    }

    protected function guard(): Guard|StatefulGuard
    {
        return Auth::guard();
    }

    public function rules(){
        return [

            'oferta' => 'required',
            'user.name' => 'required|string',
            'user.email' => ['required', 'string', 'email', new EmailUniqueRule],
            'user.phone' => ['required', 'phone'],
            'user.password' => 'required|confirmed|string|min:4|max:30',

            'organisation.zip_code' => 'required|string',
            'organisation.id_code' => 'required|unique:organisations,id_code|string',
            'organisation.type' => 'required|string',

            'organisation.name' => 'required|string',
            // Address
            'organisation.country' => 'required|string',
            'organisation.city' => 'required|string',
            'organisation.street' => 'required|string',
            'organisation.build' => 'required|string',
            'organisation.verification.sanction' => 'nullable|integer',
            'organisation.verification.founderRu' => 'nullable|integer',
            'organisation.verification.criminalCourts' => 'nullable|integer',
        ];
    }
    public static function attributes(): array
    {
        return [
            'organisation.zip_code' => __('auth.form.zip_code'),
            'organisation.id_code' => __('auth.form.id_code'),
            'organisation.country' => __('auth.form.country'),
            'organisation.city' => __('auth.form.city'),
            'organisation.street' => __('auth.form.street'),
            'organisation.build' => __('auth.form.build'),
            'organisation.name' => __('auth.form.name'),
            'organisation.type' => __('auth.form.type_user'),

            'user.email' => __('auth.form.email'),
            'user.phone' => __('auth.form.phone'),
            'user.password' => __('auth.form.password'),
            'user.name' => __('auth.form.name'),
            'oferta' => '',
        ];
    }

    public static function messages(): array
    {
        return [
            'organisation.zip_code.required' => __('auth.form.messages.required.zip_code'),
            'organisation.country.required' => __('auth.form.messages.required.country'),
            'organisation.city.required' => __('auth.form.messages.required.city'),
            'organisation.street.required' => __('auth.form.messages.required.street'),
            'organisation.name.required' => __('auth.form.messages.required.name'),
            'organisation.type.required' => __('auth.form.messages.required.type_user'),
            'organisation.id_code.required' => __('auth.form.messages.required.id_code'),
            'organisation.id_code.unique' => __('auth.form.messages.unique.id_code'),

            'user.email.required' => __('auth.form.messages.required.email'),
            'user.phone.required' => __('auth.form.messages.required.phone'),
            'user.email.email' => __('auth.form.messages.email'),
            'user.password.required' => __('auth.form.messages.required.password'),
            'user.password.confirmed' => __('auth.form.messages.confirmed_password'),
            'user.name.required' => __('auth.form.messages.required.name'),
            'regex' => __('auth.form.messages.Enter valid :attribute!'),
        ];
    }


}
