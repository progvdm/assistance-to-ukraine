<?php

namespace App\Http\Livewire\Auth;

use App\Foundation\FlashMessage;
use App\Models\User;
use App\Notifications\RestorePasswordNotification;
use App\Rules\Auth\EmailExistsRule;
use App\Services\Mail\EmailTokenService;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Livewire\Component;

class RestorePassword extends Component
{
    public string $email       = "";
    public bool $notify       = false;

    public function render(): Factory|View|Application
    {
        return view('livewire.auth.restore-password');
    }

    public function submit()
    {
        $credentials  = $this->validate(
            $this->rules(),
            $this->messages(),
            $this->attributes()
        );

        /** @var User $user */
        $user = User::whereEmail($credentials['email'])->first();

        $restoreService = new EmailTokenService();

        $link = routeLocale('restore-password', [$restoreService->token($user)]);
        if(!$this->notify){
            $this->notify = true;
            $user->notify(new RestorePasswordNotification($link));
        }


        FlashMessage::setMessage(FlashMessage::SUCCESS, __('auth.send_restore_email'));
        $this->redirect(route('home'));
    }

    public function rules(): array
    {
        return [
            'email' => ['required','string','email', new EmailExistsRule()],
        ];
    }

    public function attributes(): array
    {
        return [
            'email' => __('auth.form.email'),
        ];
    }

    public function messages(): array
    {
        return [
            'email.required' => __('auth.form.messages.required.email'),
            'email.email' => __('auth.form.messages.email'),
        ];
    }
}
