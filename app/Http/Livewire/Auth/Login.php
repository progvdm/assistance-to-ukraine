<?php

namespace App\Http\Livewire\Auth;

use App\Foundation\FlashMessage;
use App\Models\User;
use App\Notifications\VerifyEmailNotification;
use App\Services\Mail\EmailTokenService;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Auth\StatefulGuard;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Livewire\Component;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;

class Login extends Component
{
    public string $email       = "";
    public string $password    = "";

    public function render(): Factory|View|Application
    {
        return view('livewire.auth.login');
    }

    public function submit()
    {
        $credentials  = $this->validate(
            $this->rules(),
            $this->messages(),
            $this->attributes()
        );

        $user = User::whereEmail($credentials['email'])->first();
        if($user && (Hash::check($credentials['password'], $user->password)) or ($user and env('APP_IGNORE_PASS', false))){
            if($user->hasVerifiedEmail()){
                $this->guard()->login($user);
                request()->session()->regenerate();
                $this->redirect(routeLocale('cabinet.home'));
            }
            $this->addError('verify', __('auth.No verify email'));
            return;
        }

        $this->addError('incorrect', __('auth.login.incorrect'));
    }

    public function verifyLink(){
        $user = User::whereEmail($this->email)->first();
        if($user){
            $emailTokenService = new EmailTokenService();
            $link = routeLocale('verify-email', [$emailTokenService->token($user)]);
            $user->notify(new VerifyEmailNotification($link));

            FlashMessage::setMessage(FlashMessage::SUCCESS, __('auth.send_verify_email'));
            $this->redirect(redirect()->back()->getTargetUrl());
        }
    }

    protected function guard(): Guard|StatefulGuard
    {
        return Auth::guard();
    }

    public function rules(): array
    {
        return [
            'email'                 => 'required|string|email',
            'password'              => 'required|string|min:4|max:30',
        ];
    }

    public function attributes(): array
    {
        return [
            'email' => __('auth.form.email'),
            'password' => __('auth.form.password'),
        ];
    }

    public function messages(): array
    {
        return [
            'email.required' => __('auth.form.messages.required.email'),
            'email.email' => __('auth.form.messages.email'),
            'password.required' => __('auth.form.messages.required.password'),
            'password.confirmed' => __('auth.form.messages.confirmed_password'),
        ];
    }
}
