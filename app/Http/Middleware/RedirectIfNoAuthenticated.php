<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

class RedirectIfNoAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        if(in_array(Route::getCurrentRoute()->getName(), $this->except())){
            return $next($request);
        }

        if(!auth()->guard()->check()){
            return redirect(route('home'));
        }

        return $next($request);
    }

    protected function  except(): array
    {
        return [
            'cabinet.cargo'
        ];
    }
}
