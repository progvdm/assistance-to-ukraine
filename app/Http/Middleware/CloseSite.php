<?php

namespace App\Http\Middleware;

use Arr;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

class CloseSite
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        if (env('CLOSE_SITE_ENABLED', false)) {
            if( Arr::get($_SERVER, 'PHP_AUTH_USER') != env('CLOSE_SITE_USER', 'root') ||
                Arr::get($_SERVER, 'PHP_AUTH_PW') != env('CLOSE_SITE_PASSWORD', 'root')){
                header('HTTP/1.0 401 Unauthorized');
                header('WWW-Authenticate: Basic realm="My Realm"');
                echo "<h1>This is a test version of the site!</h1><p>It is closed from public access. To continue working with the platform, go to <a href='https://aidmonitor.org'>aidmonitor.org</a>.</p>";
                exit;
            }
        }
        return $next($request);
    }
}
