<?php

namespace App\Http\Middleware\Locale;

use Closure;
use Illuminate\Http\Request;

class SetLocale
{
    public function handle($request, Closure $next)
    {
        app()->setLocale($request->segment(1));
        return $next($request);
    }
}
