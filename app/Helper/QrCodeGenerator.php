<?php

namespace App\Helper;

use SimpleSoftwareIO\QrCode\Facades\QrCode;

class QrCodeGenerator
{
    public static function generate($link, $size = 100)
    {
        return QrCode::size($size)->generate($link);
    }
}
