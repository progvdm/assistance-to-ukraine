<?php

namespace App\Helper;

use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;

class ImageUploader
{
    protected array $defaultSizes = [
        'small' => ['height' => 250, 'weight' => 100],
        'medium' => ['height' => 450, 'weight' => 200],
        'big' => ['height' => 750, 'weight' => 500],
    ];

    public function uploadOne($uploadedFile, $model, $folder = null, $disk = 'public', $filename = null)
    {
        $nameOriginal = str_replace('.'.$uploadedFile->getClientOriginalExtension(), ' '.date('Y-m-d His'). '.' .$uploadedFile->getClientOriginalExtension() , $uploadedFile->getClientOriginalName());
        $name = !is_null($filename)
            ? $filename
            : $nameOriginal;

        $settings = Arr::get($model->fileSettings(), $model->field);
        if(is_null($folder)){
            $folder = Arr::get($settings, 'directory');
        }

        $uploadedFile->storeAs($folder, $name, $disk);

        foreach($model->imageSizes() as $folderSize => $size){
            $weight = $this->weight($size, $folderSize);
            $height = $this->height($size, $folderSize);

            $pathToFolder = filePath($model->fileSettings(), $model->field);

            Image::make($pathToFolder.'/'.$name)
                ->resize($weight, $height)
                ->save($this->makeDirectory($pathToFolder.$folderSize).'/'.$name);
        }

        return $name;
    }

    public function weight($size, $folderSize){
        return Arr::has($size, 'weight')
            ? Arr::get($size, 'weight') : $this->defaultSizes[$folderSize]['weight'];
    }

    public function height($size, $folderSize){
        return Arr::has($size, 'height')
            ? Arr::get($size, 'height') : $this->defaultSizes[$folderSize]['height'];
    }

    public function makeDirectory($path)
    {
        if(!is_dir($path)){
            mkdir($path, 0755, true);
        }

        return $path;
    }
}
