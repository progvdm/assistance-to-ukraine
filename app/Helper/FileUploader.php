<?php

namespace App\Helper;

use Illuminate\Support\Str;

class FileUploader
{
    public function uploadOne($uploadedFile, $folder = null, $disk = 'public', $filename = null)
    {
        $nameOriginal = str_replace('.'.$uploadedFile->getClientOriginalExtension(), ' '.date('Y-m-d His'). '.' .$uploadedFile->getClientOriginalExtension() , $uploadedFile->getClientOriginalName());
        $name = !is_null($filename)
            ? preg_replace('/[^ a-zа-яё\d]/ui', '',$filename ). '.'.$uploadedFile->getClientOriginalExtension()
            : $nameOriginal;
        $uploadedFile->storeAs($folder, $name, $disk);
        return $name;
    }
}
