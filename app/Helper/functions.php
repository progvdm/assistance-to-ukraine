<?php

use App\Providers\Ui;
use Illuminate\Support\Arr;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;

if (! function_exists('routeLocale')) {
    /**
     * Generate the URL to a named route with locale.
     *
     * @param array|string $name
     * @param mixed|array $parameters
     * @param bool $absolute
     * @return string
     */
    function routeLocale(array|string $name, array $parameters = [], bool $absolute = true): string
    {
        return LaravelLocalization::getLocalizedURL(app()->getLocale(), route($name, $parameters, $absolute));
    }
}

if (! function_exists('filePath')) {
    /**

     *
     * @param array $settings
     * @param string $fieldName
     * @return string
     */
    function filePath(array $settings, string $fieldName): string
    {
        $filedSettings = Arr::get($settings, $fieldName);
        $storage = Arr::get($filedSettings, 'path');
        $directory = Arr::get($filedSettings, 'directory');
        return storage_path($storage . $directory);
    }
}

if (!function_exists('svg')) {
    /**
     * Render svg element.
     *
     * @param  string  $sprite
     * @param  string  $id
     * @param  int|array|string  $size
     * @param  string|null  $class
     * @param  array  $attributes
     * @return \Illuminate\Support\HtmlString
     */
    function svg(string $sprite, string $id, $size = [], ?string $class = null, array $attributes = [])
    {
        return app(Ui::class)->svg(...func_get_args());
    }
}
