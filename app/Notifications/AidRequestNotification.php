<?php

namespace App\Notifications;

use App\Models\AidRequest\AidRequestLeed;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class AidRequestNotification extends Notification
{
    use Queueable;

    public AidRequestLeed $leed;
    public array $channels = [];
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(AidRequestLeed $leed, array $channels)
    {
        $this->leed = $leed;
        $this->channels = $channels;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return $this->channels;
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->subject(__('mail.You have received feedback on the application №:number', ['number' => $this->leed->aidRequest->numberSystem()]))
                    ->greeting(__('mail.You have received feedback on the application №:number from :name', ['name' => $this->leed->name, 'number' => $this->leed->aidRequest->numberSystem()]))
                    ->line(__('mail.Email: :email', ['email' => $this->leed->email ]))
                    ->line(__('mail.Message:') . $this->leed->message)
                    ->line($this->leed->message)
                    ->line('')
                    //->action('Notification Action', url('/'))
                    ->line(__('mail.Thank you for using our application!'));
    }
    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'route' => 'cabinet.aid-requests.index',
            'route_params' => ['aid_request_id' => $this->leed->aidRequest->id, 'leed_id' =>$this->leed->id ],
            'color' => 'info',
            'title' => 'notifications.You have received feedback on the application №:number',
            'title_replace' => ['name' => $this->leed->name, 'number' => $this->leed->aidRequest->numberSystem()],
            'description' => '',
        ];
    }
}
