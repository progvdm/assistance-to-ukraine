<?php

namespace App\Notifications;

use \App\Models\Notifications\SupportNotification as SupportNotificationModel;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;

class SupportNotification extends Notification
{
    use Queueable;

    public SupportNotificationModel $supportNotification;

    /**
     * Create a new notification instance.
     *
     * @param SupportNotificationModel $supportNotification
     */
    public function __construct(SupportNotificationModel $supportNotification)
    {
        $this->supportNotification = $supportNotification;
    }
    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return $this->supportNotification->channels;
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $message = (new MailMessage)
            ->subject('AIDMonitor:Info');
        foreach (LaravelLocalization::getSupportedLocales() as $locale => $supportedLocale) {
            if($this->supportNotification->translate($locale) and $this->supportNotification->translate($locale)->text){
                $message->line('');
                $message->line($this->supportNotification->translate($locale)->text);
            }
        }
        return $message->line('');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        $text  = '';
        foreach (LaravelLocalization::getSupportedLocales() as $locale => $supportedLocale) {
            if($this->supportNotification->translate($locale) and $this->supportNotification->translate($locale)->text){
                $text .= "\n";
                $text .= "\n";
                $text .= $this->supportNotification->translate($locale)->text;
            }
        }
        return [
            'route' => '',
            'route_params' => [],
            'color' => 'info',
            'title' => '',
            'description' => $text,
        ];
    }

    public function toTelegram($notifiable)
    {
        $text  = '';
        foreach (LaravelLocalization::getSupportedLocales() as $locale => $supportedLocale) {
            if($this->supportNotification->translate($locale) and $this->supportNotification->translate($locale)->text){
                $text .= "\n";
                $text .= "\n";
                $text .= $this->supportNotification->translate($locale)->text;
            }
        }
        return $text;
    }
}
