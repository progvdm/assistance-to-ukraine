<?php

namespace App\Notifications;

use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class RegistrationUserNotification extends Notification
{
    use Queueable;

    public ?User $user;
    public array $data = [];
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($user, $data)
    {
        $this->user = $user;
        $this->data = $data;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        $channels = ['mail'];

        return $channels;
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->subject(__('mail.Registration: '). $this->user->name)
                    ->greeting(__('mail.You have been registered in the AID Monitor system'))
                    ->line(__('mail.Organization: ').$this->user->organisation->name)
                    ->line(__('mail.Login details:'))
                    ->line(__('login: '). $this->user->email)
                    ->line(__('password: '). $this->data['password'])
                    ->line('----------------------------------------------------------------------')
                    ->line(__('mail.You need to confirm your mail:'))
                    ->action(__('mail.Confirm Mail'), routeLocale('verify-email', [$this->data['token']]))
                    ->line(__('mail.Thank you for using our application!'));
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
