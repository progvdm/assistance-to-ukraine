<?php

namespace App\Notifications;

use App\NotificationChannels\MobizonChannel;
use App\NotificationChannels\TelegramChannel;
use App\NotificationChannels\TelegramDebugChannel;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Laraketai\Mobizon\MobizonMessage;

class MessageNotification extends Notification
{
    use Queueable;

    public string $message = '';
    public bool $mobizon = true;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($message, $mobizon = true)
    {
        $this->message = $message;
        $this->mobizon = $mobizon;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        $channels = [TelegramDebugChannel::class, TelegramChannel::class];
        if(env('MOBIZON_ENEBLED', false) and $this->mobizon){
            $channels[] = MobizonChannel::class;
        }

        return $channels;
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->line('The introduction to the notification.')
                    ->action('Notification Action', url('/'))
                    ->line('Thank you for using our application!');
    }

    public function toMobizon($notifiable)
    {
        return MobizonMessage::create($this->message);
    }

    public function toTelegramDebug($notifiable)
    {
        return $this->message;
    }

    public function toTelegram($notifiable)
    {
        return $this->message;
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
