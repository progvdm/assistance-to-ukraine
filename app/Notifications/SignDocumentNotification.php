<?php

namespace App\Notifications;

use App\Models\Transfers\TransferDocument;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class SignDocumentNotification extends Notification
{
    use Queueable;

    public ?TransferDocument $transferDocument;

    /**
     * Create a new notification instance.
     *
     * @param TransferDocument $transferDocument
     */
    public function __construct(TransferDocument $transferDocument)
    {
        $this->transferDocument = $transferDocument;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return [/*'mail', */'database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->line('The introduction to the notification.')
                    ->action('Notification Action', url('/'))
                    ->line('Thank you for using our application!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'route' => 'cabinet.sign-document',
            'route_params' => ['transferDocument' => $this->transferDocument->id],
            'color' => 'info',
            'title' => 'notifications.You need to sign the document!',
            'description' => $this->transferDocument->document,
            ];
    }
}
