<?php

namespace App\Notifications;

use App\Models\AidRequest\AidRequestLeed;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class AidRequestApplicantNotification extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        $channels = ['mail'];

        return $channels;
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $mailMessage = new MailMessage();
        switch ($notifiable->status){
            case AidRequestLeed::NEW :
                $mailMessage->subject(__('mail.Response to application №:number', ['number' => $notifiable->aidRequest->numberSystem()]))
                    ->greeting(__('mail.Response to application №:number', ['number' => $notifiable->aidRequest->numberSystem()]))
                    ->line(__('mail.Your application has been completed! wait for a response from the organization'))
                    ->line(__('mail.Details:'))
                    ->line($notifiable->aidRequest->description)
                    ->line('')
                    ->line(__('mail.Thank you for using our application!'));
                break;
            case AidRequestLeed::APPROVE :
                $mailMessage->subject(__('mail.Your product request №:number has been approved', ['number' => $notifiable->aidRequest->numberSystem()]))
                    ->greeting(__('mail.Your product request №:number has been approved', ['number' => $notifiable->aidRequest->numberSystem()]))
                    ->line(__('mail.Your application has been approved! For further work, register in our system'))
                    ->line(__('mail.Organization:'). ' '. $notifiable->aidRequest->organisation->name )
                    ->line(__('mail.Mail for communication with the organization:'). ' '. $notifiable->aidRequest->contact_email )
                    ->line(__('mail.Details:'))
                    ->line($notifiable->aidRequest->description)
                    ->line('')
                    ->line(__('mail.Thank you for using our application!'));
                break;
            case AidRequestLeed::REJECTED :
                $mailMessage->subject(__('mail.Your request for №:number was rejected!', ['number' => $notifiable->aidRequest->numberSystem()]))
                    ->greeting(__('mail.Your request for №:number was rejected!', ['number' => $notifiable->aidRequest->numberSystem()]))
                    ->line(__('mail.Details:'))
                    ->line($notifiable->aidRequest->description)
                    ->line('')
                    ->line(__('mail.Thank you for using our application!'));
                break;
        }
        return $mailMessage;
    }
}
