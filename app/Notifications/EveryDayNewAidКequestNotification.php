<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class EveryDayNewAidКequestNotification extends Notification
{
    use Queueable;

    public array $data = [
        'offer' => 0,
        'need' => 0,
    ];
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($countOffer, $countNeed)
    {
        $this->data = [
            'offer' => $countOffer,
            'need' => $countNeed,
        ];
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database', 'mail'];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'route' => 'page.marketplace',
            'route_params' => [],
            'color' => 'info',
            'title' => 'notifications.New aid requests!',
            'title_replace' => $this->data,
            'description' => '',
        ];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->subject("AID Monitor | Marketplace")
            ->line('')
            ->greeting('Нові заявки на маркетплейсі! | New applications on the marketplace!')
            ->line('')
            ->line('Пропозиції/Offers: '.$this->data['offer'])
            ->line('Потреби/Need: '.$this->data['need'])
            ->line('')
            ->line(__('mail.Thank you for using our application!'));
    }

}
