<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class CustomAidRequestNotification extends Notification
{
    use Queueable;

    public string $message;
    public string $route;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($message, $route = '')
    {
        $this->message = $message;
        $this->route = $route;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database'];
    }


    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'route' => $this->route,
            'route_params' => [],
            'color' => 'info',
            'title' => $this->message,
            'description' => '',
        ];
    }
}
