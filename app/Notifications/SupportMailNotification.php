<?php

namespace App\Notifications;

use App\Models\AidRequest\AidRequestLeed;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class SupportMailNotification extends Notification
{
    use Queueable;

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        $channels = ['mail'];

        return $channels;
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->subject("Support: Відповідь на заявку. ")
            ->line('')
            ->greeting('Відповідь на заявку')
            ->line('Доброго дня Євгенія Олександрівна!')
            ->line('Задвоэну анкету для отримання гуманітарної допомоги видалили.')
            ->line('')
            ->line('З повагою, техпідтримка.')
            ->line('')
            ->line(__('mail.Thank you for using our application!'));
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'route' => 'cabinet.settings',
            'route_params' => [],
            'color' => 'info',
            'title' => 'Вам потрібно підвісити лого вашої організації!',
            'description' => '',
        ];
    }
}
