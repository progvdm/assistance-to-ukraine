<?php

namespace App\Jobs;

use App\Enums\AidRequest\AidRequestTypesEnum;
use App\Models\AidRequest\AidRequest;
use App\Models\User;
use App\Notifications\EveryDayNewAidКequestNotification;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Notification;

class NotificationNewAidRequest implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $countOffer = AidRequest::where('created_at', '>', now()->subDay())->where('type', AidRequestTypesEnum::OFFER)->count();
        $countNeed = AidRequest::where('created_at', '>', now()->subDay())->where('type', AidRequestTypesEnum::NEED)->count();

        if($countOffer > 0 or $countNeed > 0){
            $users = User::all();
            Notification::send($users, new EveryDayNewAidКequestNotification($countOffer, $countNeed));
        }

    }
}
