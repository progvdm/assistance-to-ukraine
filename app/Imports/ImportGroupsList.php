<?php

namespace App\Imports;

use App\Models\Lists\ListCategory;
use App\Models\Lists\ListCategoryTranslation;
use Maatwebsite\Excel\Concerns\OnEachRow;
use Maatwebsite\Excel\Row;

class ImportGroupsList implements OnEachRow
{
    public function onRow(Row $row)
    {
        $row      = $row->toArray();
        $category =  ListCategory::where('import_id', $row[0])->first();
        if(!$category){
            $category = new ListCategory();
            if($row[1] != null){
                $globalCategory = ListCategory::where('import_id', $row[1])->first();
                $category->parent_id = $globalCategory ? $globalCategory->id : null;
            }
            $category->import_id = $row[0];
            $category->save();
            /** @var ListCategoryTranslation $translate */
            $translate = $category->translateOrNew('uk');
            $translate->list_category_id = $category->id;
            $translate->name = $row[2];
            $translate->save();
            /** @var ListCategoryTranslation $translate */
            $translate = $category->translateOrNew('en');
            $translate->list_category_id = $category->id;
            $translate->name = $row[3];
            $translate->save();
        }
    }
}
