<?php

namespace App\Imports;

use App\Models\Lists\ListCategory;
use App\Models\Lists\ListProduct;
use App\Models\Lists\ListProductTranslation;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Row;

class ImportProductsList implements ToCollection
{

    public function collection(Collection $rows)
    {
        $collection = $this->parseFile($rows);
        foreach ($collection as $item) {
            $code = $item->code;
            $categoryId = 'sub-'.mb_substr($code, 0, 2);
            $category =  ListCategory::where('import_id', $categoryId)->first();

            $product = new ListProduct();
            $product->import_id = $item->code;
            $product->code = $item->code ?? 'info';
            $product->select = $item->code ? true : false;
            $product->parent_id = null;
            $product->list_category_id = $category->id;
            $product->save();

            /** @var ListProductTranslation $translateUa */
            $translateUa = $product->translateOrNew('uk');
            $translateUa->list_product_id = $product->id;
            $translateUa->name = $item->name;
            $translateUa->save();
            /** @var ListProductTranslation $translateEn */
            $translateEn = $product->translateOrNew('en');
            $translateEn->list_product_id = $product->id;
            $translateEn->name = $item->name_en;
            $translateEn->save();

            $this->insertItems($item->items, $product->id, $category->id);
        }
    }

    public function insertItems($collection, $parentId, $categoryId){
        foreach ($collection as $item) {
            $product = new ListProduct();
            $product->import_id = $item->code;
            $product->code = $item->code ?? 'info';
            $product->select = $item->code ? true : false;
            $product->parent_id = $parentId;
            $product->list_category_id = $categoryId;
            $product->save();
            /** @var ListProductTranslation $translateUa */
            $translateUa = $product->translateOrNew('uk');
            $translateUa->list_product_id = $product->id;
            $translateUa->name = $item->name;
            $translateUa->save();
            /** @var ListProductTranslation $translateEn */
            $translateEn = $product->translateOrNew('en');
            $translateEn->list_product_id = $product->id;
            $translateEn->name = $item->name_en;
            $translateEn->save();

            $this->insertItems($item->items, $product->id, $categoryId);
        }
    }

    public function parseFile(Collection $rows)
    {
        $data = collect();
        foreach ($rows as $row)
        {
            if(isset($row[1])){

                $code = $row[0];
                if($row[0]){
                    $code = ((int)$code < 1000 and mb_substr($code, 0, 1)) ? '0'.$code : $code;
                }
                $search = substr($row[1], 0, 18);
                $item = (object)['code' => $code, 'name' => $row[1], 'name_en' => $row[2], 'items' => collect()];
                switch (substr_count($search, '- ', 0)) {
                    case 0:
                        $data->push($item);
                        break;
                    case 1:
                        $data->last()->items->push($item);
                        break;
                    case 2:
                        $data->last()->items->last()->items->push($item);
                        break;
                    case 3:
                        $data->last()->items->last()->items->last()->items->push($item);
                        break;
                    case 4:
                        $data->last()->items->last()->items->last()->items->last()->items->push($item);
                        break;
                    case 5:
                        $data->last()->items->last()->items->last()->items->last()->items->last()->items->push($item);
                        break;
                    case 6:
                        $data->last()->items->last()->items->last()->items->last()->items->last()->items->last()->items->push($item);
                        break;
                    case 7:
                        $data->last()->items->last()->items->last()->items->last()->items->last()->items->last()->items->last()->items->push($item);
                        break;
                    case 8:
                        $data->last()->items->last()->items->last()->items->last()->items->last()->items->last()->items->last()->items->last()->items->push($item);
                        break;
                    case 9:
                        $data->last()->items->last()->items->last()->items->last()->items->last()->items->last()->items->last()->items->last()->items->last()->items->push($item);
                        break;
                }

            }

        }
        return $data;
    }
}
