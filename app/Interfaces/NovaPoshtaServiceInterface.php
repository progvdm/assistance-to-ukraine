<?php

namespace App\Interfaces;

use stdClass;

/**
 * Interface NovaPoshtaServiceInterface
 *
 */
interface NovaPoshtaServiceInterface
{
    /**
     * NovaPoshtaServiceInterface constructor.
     *
     */
    public function __construct();

    /**
     * Get all areas
     *
     * @return array
     */
    public function getAreas(): array;

    /**
     * Get settlements using findByString attribute
     *
     * @param  string  $query
     * @param  bool|null  $hasWarehouses
     * @return array
     */
    public function getSettlements(string $query, bool $hasWarehouses = null): array;

    /**
     * Get city using ref attribute
     *
     * @param  string  $ref
     * @return stdClass|null
     */
    public function getCity(string $ref): ?stdClass;

    /**
     * Get cities using findByString attribute
     *
     * @param  string  $query
     * @param  int  $Limit
     * @return array
     */
    public function getCities(string $query, int $Limit): array;

    /**
     * Get city warehouses using city ref
     *
     * @param  string  $cityRef
     * @return array
     */
    public function getCityWarehouses(string $cityRef): array;
}
