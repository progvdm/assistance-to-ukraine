const postcssFlexbugsFixes = require('postcss-flexbugs-fixes');
const postcssPresetEnv = require('postcss-preset-env');
const postcssSortMediaQueries = require('postcss-sort-media-queries');
const postcssDiscardDuplicates = require('postcss-discard-duplicates');
const sortMediaQueries = require('sort-css-media-queries');

// own
const { getBrowserslistQueries } = require('../utils/browsers-list');

/** @param {string} env */
module.exports = (env) => {
	return [
		postcssPresetEnv({
			stage: 3,
			autoprefixer: {
				grid: true,
				cascade: false,
				flexbox: 'no-2009'
			},
			features: {
				'custom-properties': false
			},
			browsers: getBrowserslistQueries({ env })
		}),
		postcssFlexbugsFixes(),
        postcssDiscardDuplicates(),
		postcssSortMediaQueries({
			sort: sortMediaQueries
		})
	];
};
