const fromCWD = require('from-cwd');

module.exports = {
	src: {
		js: fromCWD('./resources/js/'),
		sass: fromCWD('./resources/sass/')
	},
	svgSprites: {
		folders: fromCWD('./resources/svg/*'),
		entry: fromCWD('./resources/svg/readme.js')
	},
	resolveModules: [fromCWD('./resources/sass/'), fromCWD('./node_modules/')],
	public: {
		root: '/',
		build: '/build/',
		svg: '/svg/'
	},
	dist: {
		root: fromCWD('./public/'),
		build: fromCWD('./public/build/'),
		buildTemp: fromCWD('./public/.build-temp/'),
		svg: fromCWD('./public/svg/')
	}
};
