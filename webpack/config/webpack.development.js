/** @fileOverview Конфигурация webpack под dev режим разработки */

const { join } = require('path');
const { merge } = require('webpack-merge');

// webpack plugins
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const { WebpackManifestPlugin } = require('webpack-manifest-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');

// own
const paths = require('./paths');
const getPostcssPlugins = require('./postcss');
const babelLoader = require('./babel-loader');
const { generateManifest } = require('../utils/generate-manifest');
const commonWebpackConfig = require('../../webpack.config');

const env = 'modern';

/**
 * Составляем webpack конфиг заточенный под инкрементальную сборку в dev режиме
 * @type {Configuration}
 */
module.exports = ['modern', 'legacy'].map((env) => merge(commonWebpackConfig, {
	mode: process.env.NODE_ENV,
	devtool: 'inline-source-map',
	watch: true,
	entry: {
		app: join(paths.src.js, 'app.js'),
		styles: join(paths.src.sass, 'app.scss')
	},
	output: {
		path: paths.dist.build,
		publicPath: paths.public.build,
		filename: '[name].bundle.js',
        chunkFilename: 'chunks/[name].js'
    },
	module: {
		rules: [
			{
				test: /\.(png|jpg|jpeg|svg|gif|woff|woff2|eot|ttf)$/,
				loader: 'url-loader'
			},
			babelLoader(env),
			{
				test: /\.(scss|sass|css)$/,
				use: [
					MiniCssExtractPlugin.loader,
					{
						loader: 'css-loader',
						options: {
							sourceMap: false,
							importLoaders: 1,
							url: false
						}
					},
					{
						loader: 'postcss-loader',
						options: {
							sourceMap: false,
							postcssOptions: {
								plugins: getPostcssPlugins(env)
							}
						}
					},
					{
						loader: 'sass-loader',
						options: {
							sourceMap: false,
							implementation: require('sass'),
							additionalData: [
								`$ENV: '${env}';`,
								`$STATIC_PATH: '/';`,
								`$PUBLIC_PATH: '${paths.public.root}';`
							].join('\n')
						}
					}
				]
			}
		]
	},
	plugins: [
		new CleanWebpackPlugin(),
		new MiniCssExtractPlugin({
			filename: (pathData) => {
				return '[name].bundle.css';
			},
			insert: function(linkTag) {
				const preloadLinkTag = document.createElement('link')
				preloadLinkTag.rel = 'preload'
				preloadLinkTag.as = 'style'
				preloadLinkTag.href = linkTag.href
				var reference = document.querySelector('#style-insert');

				if (reference) {
					reference.parentNode.insertBefore(preloadLinkTag, reference.previousSibling);
					reference.parentNode.insertBefore(linkTag, reference.previousSibling);
				}
			}
		}),
		new WebpackManifestPlugin({
			fileName: `manifest.${env}.json`,
			generate: generateManifest
		})
	],
	stats: {
		all: undefined,
		chunks: false,
		chunkGroups: false,
		modules: false,
		assets: false,
		errors: false,
		warnings: false
	}
}));
