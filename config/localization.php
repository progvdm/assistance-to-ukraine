<?php

return [
    'available_locales' => [
        'en',
        'de',
        'uk'
    ],

    'route_settings' => [
        'prefix' => '{locale}',
        'where' => ['locale' => '[a-zA-Z]{2}'],
        'middleware' => 'locale'
    ],
];
