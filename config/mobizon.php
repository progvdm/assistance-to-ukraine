<?php
return [
    'alphaname' => 'AID Monitor', //Optional, if you don't have registered alphaname, just skip this param and your message will be sent with our free common alphaname.
    'secret' => env('MOBIZON_API_KEY'), // Your secret API key
];
