<?php

return [
    'manifest_path' => [
        'svg' => 'svg/manifest.json',
        'src' => 'build/manifest.{mode}.json',
    ],
];
